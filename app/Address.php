<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = ['user_id', 'location', 'lat', 'lng'];

    public function getNameAttribute($value)
    {
        return ucwords($value);
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
      public function getGoogleLocationAttribute()
    {
        return 'http://maps.google.com/maps?q=loc:'.$this->location.'';
    }
}
