<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';
    protected $fillable = ['country_name_en','country_name_ar','country_name_kur','country_image','status'];

    protected $hidden = ['created_at','updated_at'];

    public function getCountryImageAttribute($value)
    {
        if ($value) {
            return asset('images/countries/' . $value);
        } else {
            return asset('images/countries/country.png');
        }
    }


    public function setCountryImageAttribute($value)
    {
        if ($value) {
            $img_name = time() . rand(1111, 9999) . '.' . $value->getClientOriginalExtension();
            $value->move(public_path('images/countries/'), $img_name);
            $this->attributes['country_image'] = $img_name;
        }
    }

    public function cities()
    {
        return $this->hasMany(City::class);
    }
}
