<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingCompany extends Model
{
    protected $table = 'shipping_companies';
    protected $fillable = [
        'company_number', 'type', 'company_en', 'company_ar', 'company_kur', 'phone_number', 'password', 'company_image', 'license_image', 'company_status',
        'lat', 'lng', 'location', 'country_id','status'
    ];

    public function getCompanyImageAttribute($value)
    {
        if ($value) {
            return asset('images/profile/' . $value);
        } else {
            return asset('images/profile/user.png');
        }
    }

    public function getLicenseImageAttribute($value)
    {
        if ($value) {
            return asset('images/company/' . $value);
        } else {
            return asset('images/profile/no-image.jpg');
        }
    }

    public function setCompanyImageAttribute($value)
    {
        if ($value) {
            $img_name = time() . rand(1111, 9999) . '.' . $value->getClientOriginalExtension();
            $value->move(public_path('images/profile/'), $img_name);
            $this->attributes['company_image'] = $img_name;
        }
    }

    public function setLicenseImageAttribute($value)
    {
        if ($value) {
            $img_name2 = time() . rand(1111, 9999) . '.' . $value->getClientOriginalExtension();
            $value->move(public_path('images/company/'), $img_name2);
            $this->attributes['license_image'] = $img_name2;
        }
    }

    function shipping_orders($user_id)
    {
        return Order::where('shipping_id', $user_id)->pluck('id')->count();
    }

    public function shipping_fees()
    {
        return $this->hasMany(ShippingFee::class, 'company_id');
    }

    // results in a "problem", se examples below
    public function shipping_by_city($city_id)
    {
        return $this->shipping_fees()->where('city_id', '=', $city_id);
    }

    public function messages()
    {
        return $this->hasMany(CompanyMessaging::class, 'company_id');
    }
}
