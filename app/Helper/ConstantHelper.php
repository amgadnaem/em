<?php


namespace App\Helper;


trait ConstantHelper
{
    public static $lang = "lang";
    public static $jwt = "jwt";
    public static $en = "en";
    public static $ar = "ar";

}
