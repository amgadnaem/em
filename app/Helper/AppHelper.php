<?php


namespace App\Helper;


class AppHelper
{
    public static function  getKeyHeader($input , $key , $default){
        return $input->hasHeader($key) ? $input->header($key) : $default;
    }
    public static function send_to_user($tokens, $msg , $type, $shop_id, $order_id=""){
        self::send($tokens, $msg , $type, $shop_id , $order_id);
    }

    public static function send($tokens, $msg , $type,$shop_id,$order_id)
    {
        $api_key =  self::getServerKey();
        $fields = array
        (
            "registration_ids" => $tokens,
            "priority" => 10,
            'data' => [

                'title' => $msg->title,
                'sound' => 'default',
                'body' => $msg->message,
                'type' => $type,
                'shop_id' => $shop_id,
                'order_id' => $order_id,
            ],
            'notification' => [
                'type'    => $type,
                'body' => $msg,
                'order_id' => $order_id,
                'title' => $msg->title,
                'sound' => 'default'
            ],
            'vibrate' => 1,
            'sound' => 1
        );
        $headers = array
        (
            'accept: application/json',
            'Content-Type: application/json',
            'Authorization: key=' .$api_key
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        //  var_dump($result);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }


    public static function getServerKey()
    {
        return 'AAAAJIhBdDE:APA91bHLPrry6EpPfm8cSqvb0Vg5d3pa22K5i_e3uPTKev8JR_7SsVjGktN-paxPXo7k26OYB4oJbWZN6RxkOrQ2KCLvLH_ifqUyfRJow7EG4doyamUVXVlGK8aRnaKPVqIib3md5MoU';
    }

    public static function unlinkFile($image, $path)
    {
        if($image != null){
            if(!strpos($image, 'https')){
                if (file_exists(public_path("uploads/$path/") . $image)) {
                    unlink(public_path("uploads/$path/") . $image);
                }
            }
        }
        return true;
    }

    public static function generateQRCode()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://qrcode-monkey.p.rapidapi.com/qr/custom?size=600&file=png&config=%257B%2522bodyColor%2522%253A%20%2522%25230277BD%2522%252C%20%2522body%2522%253A%2522mosaic%2522%257D&data=https%253A%252F%252Fwww.qrcode-monkey.com",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "x-rapidapi-host: qrcode-monkey.p.rapidapi.com",
                "x-rapidapi-key: a3a1baa53fmsh0320faa63de8bdcp10d9edjsnbaed5b54e493"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }
    }

    public static function generateCustomQRCode($data)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://qrcode-monkey.p.rapidapi.com/qr/transparent",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{ \"data\": \"$data\",   \"image\": \"http://em.my-staff.net/images/scan-qr.jpg\",  \"x\": 1,  \"y\": 1,  \"size\": 400,  \"crop\": true,  \"file\": \"png\",  \"download\": false}",
            CURLOPT_HTTPHEADER => array(
                "accept: application/json",
                "content-type: application/json",
                "x-rapidapi-host: qrcode-monkey.p.rapidapi.com",
                "x-rapidapi-key: a3a1baa53fmsh0320faa63de8bdcp10d9edjsnbaed5b54e493"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }
    }
}
