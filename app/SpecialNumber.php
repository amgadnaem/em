<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecialNumber extends Model
{
    protected $fillable = ['special_number','number_id'];
    public function cost()
    {
        return $this->belongsTo(SpecialNumberCost::class,'number_id');
    }
}
