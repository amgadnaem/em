<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripActiveRequest extends Model
{
    protected $fillable = [
        'trip_id','driver_id','driver_status','arrive','time_out'
    ];
}
