<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopFavorite extends Model
{
    protected $table = 'shop_favorites';
    protected $fillable = ['user_id', 'shop_id'];
    protected $hidden = ['created_at', 'updated_at'];

    public function shop()
    {
        return $this->belongsTo(Shop::class, 'shop_id');
    }
}
