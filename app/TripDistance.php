<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripDistance extends Model
{
    protected $fillable = [
        'trip_id','distance','waiting_time'
    ];
}
