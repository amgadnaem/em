<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductColorsSize extends Model
{
    public $table = 'product_colors_sizes';
    public $fillable = ['size_id', 'product_id', 'color_id', 'stock','general_price','special_price'];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function size_name($size_id)
    {
        return Size::whereId($size_id)->pluck('size_name')->first();
    }

    public function color_name($color_id)
    {
        return ProductColor::whereId($color_id)->pluck('color')->first();
    }
}
