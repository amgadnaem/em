<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    protected $fillable = [
        'car_name','car_name', 'car_color', 'car_number',
        'driver_number', 'first_name', 'last_name', 'phone_number',
        'password', 'driver_image', 'activate_code', 'user_status',
        'lat', 'lng', 'location', 'country_id', 'city_id', 'dob', 'status', 'jwt_token'
    ];

    public function getDriverImageAttribute($value)
    {
        if ($value) {
            return asset('images/drivers/' . $value);
        } else {
            return asset('images/profile/user.png');
        }
    }

    public function setDriverImageAttribute($value)
    {
        if ($value) {
            $img_name = time() . rand(1111, 9999) . '.' . $value->getClientOriginalExtension();
            $value->move(public_path('images/drivers/'), $img_name);
            $this->attributes['driver_image'] = $img_name;
        }
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
