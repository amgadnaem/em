<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceCar extends Model
{
    protected $fillable = [
        'country_id','start','wait','minimum_fare','service_fees','cancel_before_fees','cancel_after_fees'
    ];
}
