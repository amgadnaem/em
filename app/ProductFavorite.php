<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductFavorite extends Model
{
    //
    protected $table = 'product_favorites';
    protected $fillable = ['user_id', 'product_id'];
    protected $hidden = ['created_at', 'updated_at'];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
