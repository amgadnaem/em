<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function OrderStatus()
    {
        return $this->belongsTo(OrderStatus::class, 'order_status')->select('id', 'status_name_ar');
    }


    public function products()
    {
        return $this->belongsToMany(Product::class, 'orders_products', 'order_id', 'product_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function delegate()
    {
        return $this->belongsTo(User::class, 'delegate_id');
    }

    public function address()
    {
        return $this->belongsTo(Address::class, 'delivery_address_id');
    }

    public function shop()
    {
        return $this->belongsTo(Shop::class, 'shop_id');
    }
    function status_order($status_id)
    {
        return OrderStatus::whereId($status_id)->pluck('status_name_ar')->first();
    }


}
