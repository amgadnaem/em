<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TripPath extends Model
{
    protected $fillable = [
        'trip_id','type','address','lat','lng'
    ];

    public static function filterbylatlng($mylat,$mylng,$radius,$model,$car_id,$flag=null,$conditionarray=null)
    {
        $haversine = "(6371 * acos(cos(radians($mylat))
                           * cos(radians($model.lat))
                           * cos(radians($model.lng)
                           - radians($mylng))
                           + sin(radians($mylat))
                           * sin(radians($model.lat))))";
        $datainradiusrange = DB::table($model)->select('*')
            ->selectRaw("{$haversine} AS distance")
            ->whereRaw("{$haversine} < ?", [$radius])->where('car_id',$car_id)
            ->where('active', 1)->where('accept',1)->where('status', 0)->where('available',1)->select('id', 'lat', 'lng')->get();

        return $datainradiusrange;
    }
}
