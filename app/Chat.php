<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    function getCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->diffForHumans();
    }
    
     public function getImageAttribute($value)
    {
        if ($value) {
            return asset('images/chat/' . $value);
        }
    }

    public function setImageAttribute($value)
    {
        if ($value) {
            $image = time() . rand(1111, 9999) . '.' . $value->getClientOriginalExtension();
            $value->move(public_path('images/chat/'), $image);
            $this->attributes['image'] = $image;
        }
    }
}
