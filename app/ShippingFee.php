<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingFee extends Model
{
    protected $fillable = ['city_from','city_to','company_id','shipping_fees'];

    public function city()
    {
        return $this->belongsTo(City::class);
    }
    public function company()
    {
        return $this->belongsTo(ShippingCompany::class,'company_id');
    }
}
