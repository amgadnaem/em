<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    //

    protected $table = 'sliders';
    protected $fillable = ['slider_image', 'status'];
    protected $hidden = ['created_at', 'updated_at'];

    public function getImageAttribute($value)
    {
        if ($value) {
            return asset('images/slider/' . $value);
        } else {
            return asset('images/slider/no-slider.jpg');
        }
    }


    public function setImageAttribute($value)
    {
        if ($value) {
            $img_name = time() . rand(1111, 9999) . '.' . $value->getClientOriginalExtension();
            $value->move(public_path('images/slider/'), $img_name);
            $this->attributes['image'] = $img_name;
        }
    }
}
