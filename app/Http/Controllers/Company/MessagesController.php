<?php

namespace App\Http\Controllers\Company;

use App\CompanyMessaging;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class MessagesController extends Controller
{
    public function sendView()
    {
        $company_id = Session::get('company_id');

        $messages = CompanyMessaging::where('company_id',$company_id)->get();

        return view('Company.pages.messages.send', compact('messages'));
    }

    public function Send(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'message' => 'required',
        ],[
            'message.required' => 'من فضلك ادخل الرسالة',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $company_id = Session::get('company_id');
        $message = CompanyMessaging::create(array_merge($request->all(),[
            'company_id' => $company_id,
            'admin_id' => 1,
            'type' => 1,
        ]));
        $message->load('company');

//        if ($message){
        return response()->json($message);
//        }else{
//            session()->flash('error', __('خطا في البيانات'));
//            return redirect()->back();
//        }
    }
}
