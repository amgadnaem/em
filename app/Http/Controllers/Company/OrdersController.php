<?php

namespace App\Http\Controllers\Company;

use App\Address;
use App\ModelHasRole;

use App\Notification;
use App\Order;
use App\OrdersProducts;
use App\OrderStatus;
use App\User;
use App\UserCart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class OrdersController extends Controller
{
    // show orders

    function index()
    {
        $company_id = Session::get('company_id');
        $all_orders = Order::where('shipping_id', $company_id)->orderby('id','desc')->get();
        if (Session::get('type') == 5) {
            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Auth::user()->id)->exists();
        } else {
            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Session::get('company_id'))->exists();
        }
        $order_status = OrderStatus::orderBy('id','asc')->get();


        return view('Company.pages.Orders.index', compact('all_orders', 'roles', 'order_status'));

    }

    function ordersSearch(Request $request)
    {
        $shop_id = Session::get('frontSession');
        if (Session::get('type') == 5) {
            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Auth::user()->id)->exists();
        } else {
            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Session::get('frontSession'))->exists();
        }

        $orderSearch = Order::whereBetween('order_date', array($request->from, $request->to))->get();
        $order_status = OrderStatus::all();
        return view('Company.pages.Orders.search', compact('orderSearch', 'order_status', 'roles'));

    }

    function order_details($order_id)
    {
        $shop_id = Session::get('frontSession');
        if (Session::get('type') == 5) {
            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Auth::user()->id)->exists();
        } else {
            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Session::get('frontSession'))->exists();
        }

        $order = Order::where('id', $order_id)->with('user', 'products')->first();
        $order_location = Address::where('id', $order->delivery_address_id)->pluck('location')->first();
        $order_products = OrdersProducts::where('order_id', $order_id)->get();
        return view('Company.pages.Orders.details', compact('order', 'order_products', 'roles', 'order_location'));

    }

    function  print($order_id)
    {
        $order = Order::where('id', $order_id)->with('user', 'products')->first();
        $order_products = OrdersProducts::where('order_id', $order_id)->get();

//        $product_extra = UserCart::where('order_id', $order_id)->first();

        return view('Company.pages.Orders.print', compact('order', 'order_products'));

    }


    function EditStatusOrder(Request $request, $id)
    {
        $editOrders = Order::where('id', $id)->first();
        $editOrders->order_status = $request->order_status;
        $token = User::where('id', $editOrders->user_id)->pluck('firebase_token')->first();
        $send_notification = $this->notify($editOrders->user_id, $token, $request->order_status);
        $editOrders->save();
        session()->flash('success', __('تم تغيير حاله الطلب  بنجاح'));
        return redirect()->back();

    }

    public function notify($uth_user, $token, $status)
    {

        define('API_ACCESS_KEY', 'AAAAZ3M4L8k:APA91bHWagNVxMhhSR1mhrIoMU5g6b_10SNgsJ1eoO--3RKVkaTCOIZYG9AFMe4HDHj1-MIl4ex11j4tXg-sE4xbuQ-PW4Qga1QZTTBRuRtobMPcAnZJNlioWf5lx0zBiw5-1pjmHboz');
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        if ($status == 3) {
            $notification = [
                'title' => 'تغيير حاله الطلب',
                'body' => 'جارى تنفيذ طلبك',
                'type' => "2",
                'icon' => 'myIcon',
                'sound' => 'mySound'
            ];


        } else if ($status == 4) {
            $notification = [
                'title' => 'تغيير حاله الطلب',
                'body' => 'تم الانتهاء من طلبك',
                'type' => "2",
                'icon' => 'myIcon',
                'sound' => 'mySound'
            ];
            // $notification_table = Notification::create([
            //     'user_id' => $uth_user,
            //     'title' => 'تغيير حاله الطلب',
            //     'message' => 'تم الانتهاء من طلبك',
            //     'admin_id' => 1
            // ]);

        } else {
            return false;

        }

        $extraNotificationData = ["message" => $notification, "moredata" => 'dd'];


        $fcmNotification = [

            'to' => $token,
            'notification' => $notification,
            'data' => $notification
        ];

        $headers = [
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;

    }
}
