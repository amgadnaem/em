<?php

namespace App\Http\Controllers\Company;

use App\ModelHasRole;
use App\Order;
use App\Setting;
use App\ShippingCompany;
use App\Shop;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class DashboardController extends Controller
{

    //
    function dashboard()
    {
        $company_id = Session::get('company_id');
        $details_shipping = ShippingCompany::where('id', $company_id)->first();
        $orders = Order::where('shipping_id',$company_id)->pluck('id')->count();
        $shipping = Setting::where('key', 'shipping')->pluck('value')->first();
        if (Session::get('type') == 5) {
            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Auth::user()->id)->exists();
        } else {
            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Session::get('frontSession'))->exists();
        }
        return view('Company.index', compact('details_shipping', 'orders', 'roles','shipping'));
    }
}
