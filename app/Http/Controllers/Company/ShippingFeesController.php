<?php

namespace App\Http\Controllers\Company;

use App\City;
use App\Country;
use App\ShippingCompany;
use App\ShippingFee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class ShippingFeesController extends Controller
{
    public function index()
    {
        $company_id = Session::get('company_id');
        $type = Session::get('type');
        $company = ShippingCompany::where('id', $company_id)->first();
        if ($type == 2) {
            $name = 'دوله';
            $cities = Country::where('status', 1)->get();
        } else {
            $name = 'مدينه';
            $cities = City::where('country_id', $company->country_id)->get();
        }
        $shipping_fees = ShippingFee::where('company_id', $company_id)->get();
        foreach ($shipping_fees as $city) {
            if ($type == 1) {
                $city->city_from = City::where('id', $city->city_from)->first();
                $city->city_to = City::where('id', $city->city_to)->first();
            } else {
                $city->city_from = Country::where('id', $city->country_from)->first();
                $city->city_to = Country::where('id', $city->country_to)->first();
            }
        }
        return view('Company.pages.shipping.index', compact('cities', 'type', 'name', 'shipping_fees'));
    }

    //Add Fees
    function Add(Request $request)
    {
        if ($request->type == 1) {
            $validator = Validator::make($request->all(), [
                'shipping_fees' => 'required',
                'city_from' => 'required|exists:cities,id',
                'city_to' => 'required|exists:cities,id',
            ], [
                'shipping_fees.required' => 'من فضلك أدخل تكلفة التوصيل',
                'city_from.required' => 'من فضلك أختر  توصيل من مدينه',
                'city_to.required' => 'من فضلك أختر توصيل الى مدينه',
                'city_from.exists' => 'للأسف المدينة غير موجودة',
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                'city_from' => 'required|exists:cities,id',
                'city_to' => 'required|exists:cities,id',
            ], [
                'city_from.required' => 'من فضلك أختر  توصيل من دوله',
                'city_to.required' => 'من فضلك أختر توصيل الى دوله',
                'city_from.exists' => 'للأسف الدوله غير موجودة',
            ]);
        }

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $company_id = Session::get('company_id');
        if ($request->type == 2) {
            $shipping = new ShippingFee();
            $shipping->company_id = $company_id;
            $shipping->country_from = $request->city_from;
            $shipping->country_to = $request->city_to;
            $shipping->save();
        } else {
            $shipping = ShippingFee::create(array_merge($request->all(), ['company_id' => $company_id]));
        }
        if ($shipping) {
            session()->flash('success', __('تم اضافه التكلفة بنجاح'));
            return redirect()->back();
        }
    }

    // Update Shipping
    public function Edit($id, Request $request)
    {


        if ($request->type == 1) {
            $validator = Validator::make($request->all(), [
                'shipping_fees' => 'required',
                'city_from' => 'required|exists:cities,id',
                'city_to' => 'required|exists:cities,id',
            ], [
                'shipping_fees.required' => 'من فضلك أدخل تكلفة التوصيل',
                'city_from.required' => 'من فضلك أختر  توصيل من مدينه',
                'city_to.required' => 'من فضلك أختر توصيل الى مدينه',
                'city_from.exists' => 'للأسف المدينة غير موجودة',
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                'city_from' => 'required|exists:cities,id',
                'city_to' => 'required|exists:cities,id',
            ], [
                'city_from.required' => 'من فضلك أختر  توصيل من دوله',
                'city_to.required' => 'من فضلك أختر توصيل الى دوله',
                'city_from.exists' => 'للأسف الدوله غير موجودة',
            ]);
        }

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $company_id = Session::get('company_id');
        if($request->type == 2) {
            $shipping = ShippingFee::where('id',$id)->first();
            $shipping->country_from = $request->city_from;
            $shipping->country_to = $request->city_to;
            $shipping->save();
        } else {
            $shipping = ShippingFee::where('id', $id)->update(array_merge($request->except('_method', '_token'), ['company_id' => $company_id]));
        }

        if ($shipping) {
            session()->flash('success', __('تم تعديل البيانات بنجاح'));
            return redirect()->back();
        }
    }

    // delete Shipping
    function destroy($id)
    {
        ShippingFee::where('id', $id)->delete();
        session()->flash('success', __('تم حذف التكلفة بنجاح'));
        return redirect()->back();
    }
}
