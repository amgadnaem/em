<?php

namespace App\Http\Controllers\AdminShop;

use App\ModelHasRole;
use App\Order;
use App\Setting;
use App\Shop;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class DashboardController extends Controller
{

    //
    function dashboard()
    {
        $shop_id = Session::get('frontSession');
        $details_shop = Shop::where('id', $shop_id)->first();
        $orders = Order::whereShopId($shop_id)->pluck('id')->count();
        $shipping = Setting::where('key', 'shipping')->pluck('value')->first();
        if (Session::get('type') == 5) {
            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Auth::user()->id)->exists();
        } else {
            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Session::get('frontSession'))->exists();
        }
        return view('AdminShop.index', compact('details_shop', 'orders', 'roles','shipping'));
    }
}
