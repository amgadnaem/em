<?php

namespace App\Http\Controllers\AdminShop;

use App\ShopMessaging;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class AdminMessagesController extends Controller
{
    public function sendView()
    {
        $shop_id = Session::get('frontSession');

        $messages = ShopMessaging::where('shop_id',$shop_id)->get();

        return view('AdminShop.pages.messages.send', compact('messages'));
    }

    public function Send(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'message' => 'required',
        ],[
            'message.required' => 'من فضلك ادخل الرسالة',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $shop_id = Session::get('frontSession');
        $message = ShopMessaging::create(array_merge($request->all(),[
            'shop_id' => $shop_id,
            'admin_id' => 1,
            'type' => 1,
        ]));
        $message->load('shop');

//        if ($message){
        return response()->json($message);
//        }else{
//            session()->flash('error', __('خطا في البيانات'));
//            return redirect()->back();
//        }
    }
}
