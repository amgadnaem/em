<?php

namespace App\Http\Controllers\AdminShop;

use App\Exports\ProductsSampleExport;
use App\Imports\ProductsImport;
use App\ModelHasRole;
use App\Http\Controllers\Controller;
use App\Product;
use App\ProductColor;
use App\ProductColorsSize;
use App\Service;
use App\Shop;
use App\ShopCategories;
use App\Size;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use SimpleSoftwareIO\QrCode\Facades\QrCode;


class ShopController extends Controller
{
    //

    function index()
    {
        $session_id = Session::get('frontSession');
        $show_shop = Shop::whereId($session_id)->first();
        $services = Service::all();
        if (Session::get('type') == 5) {
            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Auth::user()->id)->exists();
        } else {
            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Session::get('frontSession'))->exists();
        }
        return view('AdminShop.pages.Shop.edit', compact('show_shop', 'services', 'roles'));
    }

    public function update($id, Request $request)
    {

        $shop = Shop::where('id', $id)->first();

        $shop->name_app_en = $request->name_app_en;
        $shop->name_app_ar = $request->name_app_ar;
        $shop->name_app_kur = $request->name_app_kur;

        $shop->name_en = $request->name_en;
        $shop->name_ar = $request->name_ar;
        $shop->name_kur = $request->name_kur;

        $shop->desc_ar = $request->desc_ar;
        $shop->desc_en = $request->desc_en;
        $shop->desc_kur = $request->desc_kur;

        $shop->lat = $request->lat;
        $shop->lng = $request->lng;
        $shop->location = $request->location;

        if ($request->shop_image) {
            $shop->shop_image = $request->shop_image;
            $shop->save();
        }
        session()->flash('success', __('تم تعديل البيانات بنجاح'));
        return redirect()->route('shop.index');
    }

    //Show Categories Shops

    function categories()
    {
        $session_id = Session::get('frontSession');
        $shop_id = Shop::where('id', $session_id)->first();
        if (Session::get('type') == 5) {
            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Auth::user()->id)->exists();
        } else {
            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Session::get('frontSession'))->exists();
        }
        $all_category = ShopCategories::where('shop_id', $session_id)->get();

        return view('AdminShop.pages.Shop.categories', compact('shop_id', 'all_category', 'roles'));

    }

    //Add Category

    function AddCategoryShop(Request $request)
    {
        $shop_id = Session::get('frontSession');

        $validate = $request->validate(
            [
                'category_name_en' => 'required',
                'category_name_ar' => 'required',
                'category_name_kur' => 'required',
            ],
            [
                'category_name_en.required' => trans('validation.category_name_en'),
                'category_name_ar.required' => trans('validation.category_name_ar'),
                'category_name_kur.required' => trans('validation.category_name_kur'),
            ]);

        $add_service = array(
            'category_name_en' => $request->category_name_en,
            'category_name_ar' => $request->category_name_ar,
            'category_name_kur' => $request->category_name_kur,
            'shop_id' => $shop_id,
        );
        $insert_category = ShopCategories::create($add_service);
        $insert_category->save();
        if ($insert_category) {
            if ($request->category_image) {
                $insert_category->category_image = $request->category_image;
                $insert_category->save();
            }
            session()->flash('success', __('تم اضافه قسم  بنجاح'));
            return redirect()->back();
        }
    }

    public function updateCategoryShop($id, $status)
    {
        $shop_id = Session::get('frontSession');
        $shop = ShopCategories::where('shop_id', $shop_id)->where('id', $id)->first();
        $shop->status = $status;
        $shop->save();
        session()->flash('success', __('تم تعديل البيانات بنجاح'));
        return redirect()->back();
    }

    // Update service

    public function updateCategory($id, Request $request)
    {
        $EditCategory = ShopCategories::whereId($id)->first();
        $EditCategory->category_name_en = $request->category_name_en;
        $EditCategory->category_name_ar = $request->category_name_ar;
        $EditCategory->category_name_kur = $request->category_name_kur;
        $EditCategory->save();

        if ($request->category_image) {
            $EditCategory->category_image = $request->category_image;
            $EditCategory->save();
        }
        session()->flash('success', __('تم تعديل بيانات القسم بنجاح'));
        return redirect()->back();
    }

    // delete Category
    function destroyCategory($id)
    {
        ShopCategories::where('id', $id)->delete();
        session()->flash('success', __('تم حذف بيانات القسم بنجاح'));
        return redirect()->back();
    }

// add product
    function products()
    {
        $shop_id = Session::get('frontSession');
        $products = Product::where('shop_id', $shop_id)->get();
        return view('AdminShop.pages.products.index', compact('products'));
    }
    public function exportProductsSample()
    {
        return (new ProductsSampleExport())->download('products-sample.xlsx');
    }
    function importProductsView()
    {
        return view('AdminShop.pages.products.bulk');
    }

    public function importProductsFile()
    {
        try {
            Excel::import(new ProductsImport(),request()->file('file'));
        } catch (\Exception $e) {
            $failures = $e->failures();
            foreach ($failures as $failure) {
                session()->flash('error', $failure->errors()[0]);
                return back();
            }

        }
        session()->flash('success', __("تم إضافة المنتجات بنجاح"));
        return back();
    }

    function addShopProducts()
    {
        $shop_id = Session::get('frontSession');
        $service_id = Session::get('service_id');
        $shop_type = Service::where('id', $service_id)->pluck('type')->first();
        if (Session::get('type') == 5) {
            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Auth::user()->id)->exists();
        } else {
            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Session::get('frontSession'))->exists();
        }
        $sizes = Size::where('status', 1)->get();
        $colors = ProductColor::where('status', 1)->get();
        $categories = ShopCategories::where('shop_id', $shop_id)->where('status', 1)->get();
        $categories = ShopCategories::where('shop_id', $shop_id)->where('status', 1)->get();
        return view('AdminShop.pages.products.create', compact('sizes', 'colors', 'categories', 'roles', 'shop_type'));
    }

    //Add Product shops
    function addProduct(Request $request, $product_image = null)
    {
        $product_code = Product::where('code', $request->product_code)->first();
        if ($product_code) {
            session()->flash('success', __(' كود المنتج موجود من قبل'));
            return redirect()->back();
        }
        $shop_id = Session::get('frontSession');
        $service_id = Session::get('service_id');
        $add_product = new Product();
        $add_product->name_en = $request->name_en;
        $add_product->name_ar = $request->name_ar;
        $add_product->name_kur = $request->name_kur;
        QrCode::format('png')->size(500)->generate($request->product_code, public_path('images/products/qrCode/' . $request->product_code . '.png'));
        $add_product->code = $request->product_code;
        $add_product->qr_code = $request->product_code;
        $add_product->general_price = $request->general_price;
        $add_product->special_price = $request->special_price;
        $add_product->product_stock = !empty($request->product_stock) ? $request->product_stock : null;

        $add_product->shop_id = $shop_id;
        $add_product->category_id = $request->category_id;
        $add_product->desc_en = $request->desc_en;
        $add_product->desc_ar = $request->desc_ar;
        $add_product->desc_kur = $request->desc_kur;
        $add_product->product_image = $request->product_image;
        $add_product->type = Service::where('id', $service_id)->pluck('type')->first();
        if (!empty($request->hosted_price)) {
            $add_product->hosted_price = $request->hosted_price;
            $add_product->hosted = 1;
        } else {
            $add_product->hosted = 0;
        }
        $add_product->save();
        $add_product->product_image = $product_image;
        $add_product->save();

        if ($add_product) {
            if ($add_product->type == 1) {
                $add_color = new ProductColorsSize();
                $add_color->product_id = $add_product->id;
                $add_color->color_id = $request->color_id;
                $add_color->size_id = $request->size_id;
                $add_color->stock = $request->product_stock;
                $add_color->save();
                session()->flash('success', __('تم اضافه المنتج بنجاح'));
                return redirect()->route('products');
            } else if ($add_product->type == 2) {
                $add_color = new ProductColorsSize();
                $add_color->product_id = $add_product->id;
                $add_color->size_id = $request->size_id;
                $add_color->stock = $request->product_stock;
                $add_color->general_price = $request->general_price;
                $add_color->special_price = $request->special_price;
                $add_color->save();
                session()->flash('success', __('تم اضافه المنتج بنجاح'));
                return redirect()->route('products');
            }
            session()->flash('success', __('تم اضافه المنتج بنجاح'));
            return redirect()->route('products');

        }

    }

    function addBulkImagesView()
    {
        return view('AdminShop.pages.products.upload_images');
    }
    function addBulkProductsImages(Request $request)
    {
        $image = $request->file('file');
        if ($image){
            $imageName = $image->getClientOriginalName();
            $image->move(public_path('images/products/'), $imageName);
        }
        return session()->flash('success', __('تم تعديل بيانات البروفايل بنجاح'));
    }

    function editShopProducts($id)
    {
        $shop_id = Session::get('frontSession');
        $service_id = Session::get('service_id');
        $shop_type = Service::where('id', $service_id)->pluck('type')->first();
        if (Session::get('type') == 5) {
            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Auth::user()->id)->exists();
        } else {
            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Session::get('frontSession'))->exists();
        }
        $sizes = Size::where('status', 1)->get();
        $colors = ProductColor::where('status', 1)->get();
        $categories = ShopCategories::where('shop_id', $shop_id)->where('status', 1)->get();
        $categories = ShopCategories::where('shop_id', $shop_id)->where('status', 1)->get();
        return view('AdminShop.pages.products.create', compact('sizes', 'colors', 'categories', 'roles', 'shop_type'));
    }

    function getProductFromCategory($id, Request $request)
    {
        $session_id = Session::get('frontSession');
        if (Session::get('type') == 5) {
            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Auth::user()->id)->exists();
        } else {
            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Session::get('frontSession'))->exists();
        }
        $products = Product::where('category_id', $id)->get();

        return view('AdminShop.pages.Shop.products', compact('products', 'roles'));
    }


    // add size and color

  function addProductColor($product_id)
    {
        $shop_id = Session::get('frontSession');
        $service_id = Session::get('service_id');

        if (Session::get('type') == 5) {
            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Auth::user()->id)->exists();
        } else {
            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Session::get('frontSession'))->exists();
        }
        $all_sizes = Size::where('status', 1)->get();
        $product = Product::where('id', $product_id)->first();
        $all_colors = ProductColor::where('status', 1)->get();
        $all_product_colors = ProductColorsSize::where('product_id', $product_id)->get();
        $type = Service::where('id', $service_id)->pluck('type')->first();

        return view('AdminShop.pages.Shop.sizes', compact('all_sizes', 'all_colors', 'all_product_colors', 'product', 'roles', 'type'));

    }


   function addSizes(Request $request, $id)
    {
        $service_id = Session::get('service_id');
        $type = Service::where('id', $service_id)->pluck('type')->first();
        if ($type == 1) {

            $check_size_color = ProductColorsSize::where('size_id', $request->size_id)->where('color_id', $request->color_id)->where('product_id', $id)->first();

            if ($check_size_color) {
                session()->flash('success', __('مقاس المنتج موجود من قبل'));
                return redirect()->back();
            }

            $validate = $request->validate(
                [
                    'size_id' => 'required',
                    'color_id' => 'required',
                    'product_stock' => 'required',
                ],
                [
                    'size_id.required' => trans('validation.size_id'),
                    'color_id.required' => trans('validation.color_id'),
                    'product_stock.required' => trans('validation.product_stock'),
                ]);

            $add_size = array(
                'size_id' => $request->size_id,
                'color_id' => $request->color_id,
                'product_id' => $id,
                'stock' => $request->product_stock,
            );
            $insert_size = ProductColorsSize::create($add_size);
            $insert_size->save();
            if ($insert_size) {
                session()->flash('success', __('تم اضافه مقاسات للمنتج  بنجاح'));
                return redirect()->back();
            }
        } else {

            $check_size = ProductColorsSize::where('size_id', $request->size_id)->where('product_id', $id)->first();

            if ($check_size) {
                session()->flash('success', __('حجم المنتج موجود من قبل'));
                return redirect()->back();
            }

            $add_size = array(
                'size_id' => $request->size_id,
                'general_price' => $request->general_price,
                'special_price' => $request->special_price,
                'product_id' => $id,
                'stock' => $request->product_stock,
            );
            $insert_size = ProductColorsSize::create($add_size);
            $insert_size->save();
            if ($insert_size) {
                session()->flash('success', __('تم اضافه حجم للمنتج بنجاح'));
                return redirect()->back();
            }
        }

    }

    // update size and color

     function updateSizes(Request $request, $id)
    {
        $service_id = Session::get('service_id');
        $type = Service::where('id', $service_id)->pluck('type')->first();
        $product_sizes = ProductColorsSize::where('id', $id)->first();
        $product_sizes->size_id = $request->size_id;
        if ($type == 1) {
            $product_sizes->color_id = $request->color_id;
        } elseif ($type == 2) {
            $product_sizes->general_price = $request->general_price;
            $product_sizes->special_price = $request->special_price;
        }
        $product_sizes->stock = $request->product_stock;
        $product_sizes->save();
        if ($product_sizes) {
            session()->flash('success', __('تم تعديل البيانات بنجاح'));
            return redirect()->back();
        }
    }

    // destroy size and color

    function destroySize($id)
    {
        ProductColorsSize::where('id', $id)->delete();
        session()->flash('success', __('تم حذف مقاس المنتج بنجاح'));
        return redirect()->back();
    }



    //Active or dis active product

    public function updateProduct($id, $status)
    {
        $product = Product::where('id', $id)->first();
        $product->status = $status;
        $product->save();
        session()->flash('success', __('تم تغيير حاله المنتج بنجاح'));
        return redirect()->back();
    }


    // Update Product shop

    function editProducts($id, Request $request)
    {
        $edit_product = Product::where('id', $id)->first();
        $edit_product->name_en = $request->name_en;
        $edit_product->name_ar = $request->name_ar;
        $edit_product->name_kur = $request->name_kur;
        QrCode::format('png')->size(500)->generate($request->product_code, public_path('images/products/qrCode/' . $request->product_code . '.png'));
        $edit_product->code = $request->product_code;
        $edit_product->qr_code = $request->product_code;
        $edit_product->general_price = $request->general_price;
        $edit_product->special_price = $request->special_price;
        $edit_product->desc_en = $request->desc_en;
        $edit_product->desc_ar = $request->desc_ar;
        $edit_product->desc_kur = $request->desc_kur;
        $edit_product->product_stock = $request->product_stock;


        if (!empty($request->hosted_price)) {
            $edit_product->hosted_price = $request->hosted_price;
            $edit_product->hosted = 1;
        } else {
            $edit_product->hosted = 0;
        }
        $edit_product->save();
        if ($edit_product) {
            if ($request->product_image) {
                $edit_product->product_image = $request->product_image;
                $edit_product->save();
            }
            session()->flash('success', __('تم تعديل البيانات بنجاح'));
            return redirect()->back();
        }


    }

    // delete Services

    function destroy($id)
    {
        $product = Product::where('id', $id)->first();

        if($product->getOriginal('product_image') != null){
            if(!strpos($product->getOriginal('product_image'), 'https')){
                if (file_exists(public_path("images/products/") . $product->getOriginal('product_image'))) {
                    unlink(public_path("images/products/") . $product->getOriginal('product_image'));
                }
            }
        }
        $product->delete();
        session()->flash('success', __('تم حذف المنتج بنجاح'));
        return redirect()->back();
    }

}
