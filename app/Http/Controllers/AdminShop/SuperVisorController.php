<?php

namespace App\Http\Controllers\AdminShop;

use App\ModelHasRole;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;

class SuperVisorController extends Controller
{

    //
    function AddSuper()
    {
        $numbers = DB::SELECT("select id, count(*) as count, date(created_at) as date from users WHERE date(created_at) >= DATE(NOW()) - INTERVAL 7 DAY GROUP BY date(created_at)");
        $numbersOfShops = DB::SELECT("select id, count(*) as count, date(created_at) as date from shops WHERE date(created_at) >= DATE(NOW()) - INTERVAL 7 DAY GROUP BY date(created_at)");
        return view('AdminPanel.backend.pages.admin.create')
            ->with('ofusers', $numbers)
            ->with('numberofshops', $numbersOfShops);
    }

    function CreateSuperVisor(Request $request)
    {
        $request->validate([
            'permissions' => 'required|min:1'
        ]);

        $request->password = bcrypt($request->password);
        $phone = $request->phone;
        $jwt_token = Str::random(25);
        $AdminAccount = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone_number' => $phone,
            'password' => $request->password,
            'type' => 5,
            'shop_id' => Session::get('frontSession'),
            'jwt_token' => $jwt_token,
        ]);

        $AdminAccount->givePermissionTo($request->permissions);
        if ($request->user_image) {
            $AdminAccount->user_image = $request->user_image;
            $AdminAccount->save();
        }
        session()->flash('success', __('تم اضافه مشرف بنجاح'));
        return redirect()->back();

    }


    //Active or dis active product


    public function UpdateStatus($id, $status)
    {
        $product = User::where('id', $id)->first();
        $product->status = $status;
        $product->save();
        session()->flash('success', __('تم ايقاف حساب المشرف بنجاح'));
        return redirect()->back();


    }

    function ShowAccountsAdmin()
    {
        $show_supervisor = User::where('type', 5)->pluck('shop_id');
        $select_accounts = User::whereIn('shop_id', $show_supervisor)->get();
        if (Session::get('type') == 5) {
            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Auth::user()->id)->exists();
        } else {
            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Session::get('frontSession'))->exists();
        }
        return view('AdminShop.pages.SuperVisor.index')->with('select_accounts', $select_accounts)->with('roles', $roles);

    }


    public function EditAccount($id, Request $request)
    {
        $user = $this->updateUser($id, $request->all());

        session()->flash('success', __('تم تعديل بيانات المشرف بنجاح'));
        return redirect()->route('SupervisorShop');
    }

    public function updateUser($id, $input)
    {
        $user = User::whereId($id)->first();

        $user->first_name = $input['first_name'];
        $user->last_name = $input['last_name'];
        $user->phone_number = $input['phone'];
        if (isset($input['password'])) {
            $user->password = Hash::make($input['password']);
        }
        if (isset($input['user_image'])) {
            $user->user_image = $input['user_image'];
        }
        $user->save();
        return $user;
    }

    function destroy($id)
    {
        User::where('id', $id)->delete();
        session()->flash('success', __('تم حذف  حساب المشرف بنجاح'));
        return redirect()->back();


    }
}
