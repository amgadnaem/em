<?php

namespace App\Http\Controllers\AdminShop;

use App\Message;
use App\Repositories\Message\MessageRepository;
use App\Shops;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class MessagesController extends Controller
{
    //Show message

    protected $repository;
    protected $notificationRepository;

    public function __construct(MessageRepository $repository)
    {
        $this->repository = $repository;
    }
    function index()
    {
        $shop_id = Session::get('frontSession');
        $reply = Message::where('from', $shop_id)->first();
        $body = Message::where('shop_id',$shop_id)->orderBy('created_at', 'ASC')->get();
        return view('AdminPanel.backend.pages.AdminShop.pages.message.reply', compact('reply', 'body'));
    }

    public function listMessages(Request $request)
    {
        $messages = Message::where('from', $request->user_id)->orWhere('to', $request->user_id)->orderBy('created_at', 'ASC')->get();
        return response()->json($messages);
    }

    public function createAdminMessage(Request $request)
    {
        $shop_id = Session::get('frontSession');
        $message = new Message();
        $message->from = $shop_id;
        $message->to = $request->shop_id;
        $message->body = $request->send_message;
        $message->shop_id = $shop_id;
        if ($message->save()) {
            session()->flash('success', __('تم ارسال الرساله بنجاح'));
            return redirect()->back();
        }
        return false;
    }
}
