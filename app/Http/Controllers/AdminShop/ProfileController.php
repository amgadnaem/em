<?php

namespace App\Http\Controllers\AdminShop;


use App\ModelHasRole;
use App\Shop;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ProfileController extends Controller
{
    function index()
    {
        $type = Session::get('type');

        $shop_id = Session::get('frontSession');
        if (Session::get('type') == 5) {
            $shop_admin = User::where('id', Session::get('frontSession'))->first();
            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Auth::user()->id)->exists();
        } else {
            $shop_admin = Shop::where('id', Session::get('frontSession'))->first();

            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Session::get('frontSession'))->exists();
        }

        return view('AdminShop.pages.profile.index', compact('shop_admin','roles'));
    }

    function editProfile(Request $request, $id, $type)
    {

        if ($type == 3) {
            $edit_admin = Shop::where('id', $id)->first();
            $edit_admin->name_ar = $request->name_ar;
            $edit_admin->phone_number = $request->phone_number;
            $edit_admin->save();
            if ($request->shop_image) {
                $edit_admin->shop_image = $request->admin_image;
                $edit_admin->save();
            }
        } else {
            $edit_admin = User::where('id', $id)->first();
            $edit_admin->first_name = $request->first_name;
            $edit_admin->phone_number = $request->phone_number;
            $edit_admin->save();
            if ($request->admin_image) {
                $edit_admin->user_image = $request->admin_image;
                $edit_admin->save();
            }
        }

        session()->flash('success', __('تم تعديل بيانات البروفايل بنجاح'));
        return redirect()->back();

    }
}
