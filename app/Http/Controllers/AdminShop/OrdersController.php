<?php

namespace App\Http\Controllers\AdminShop;

use App\ModelHasRole;

use App\Order;
use App\OrdersProducts;
use App\OrderStatus;
use App\UserCart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class OrdersController extends Controller
{
    // show orders

    function index()
    {
        $shop_id = Session::get('frontSession');
        $all_orders = Order::where('shop_id', $shop_id)->get();
        if (Session::get('type') == 5) {
            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Auth::user()->id)->exists();
        } else {
            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Session::get('frontSession'))->exists();
        }

        return view('AdminShop.pages.Orders.index', compact('all_orders', 'roles'));

    }

    function ordersSearch(Request $request)
    {
        $shop_id = Session::get('frontSession');
        if (Session::get('type') == 5) {
            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Auth::user()->id)->exists();
        } else {
            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Session::get('frontSession'))->exists();
        }

        $orderSearch = Order::whereBetween('order_date', array($request->from, $request->to))->get();
        $order_status = OrderStatus::all();
        return view('AdminShop.pages.Orders.search', compact('orderSearch', 'order_status','roles'));

    }

    function order_details($order_id)
    {
        $shop_id = Session::get('frontSession');
        if (Session::get('type') == 5) {
            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Auth::user()->id)->exists();
        } else {
            $roles = ModelHasRole::where('role_id', '2')->where('model_id', Session::get('frontSession'))->exists();
        }

        $order = Order::where('id', $order_id)->with('user', 'products')->first();
        $order_products = OrdersProducts::where('order_id', $order_id)->get();
        return view('AdminShop.pages.Orders.details', compact('order', 'order_products','roles'));

    }

    function  print($order_id)
    {
        $order = Order::where('id', $order_id)->with('user', 'products')->first();
        $order_products = OrdersProducts::where('order_id', $order_id)->get();

//        $product_extra = UserCart::where('order_id', $order_id)->first();

        return view('AdminShop.pages.Orders.print', compact('order', 'order_products'));

    }
}
