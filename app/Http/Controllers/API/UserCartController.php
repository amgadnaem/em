<?php

namespace App\Http\Controllers\API;

use App\City;
use App\Country;
use App\Http\Requests\UserCart\DeleteCartRequest;
use App\Product;
use App\Repositories\Cart\CartRepository;
use App\Repositories\User\UserRepository;
use App\ShippingCompany;
use App\ShippingFee;
use App\Shop;
use App\UserCart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UserCartController extends APIController
{
    protected $repository;
    protected $userRepository;
    protected $lang;


    public function __construct(Request $request, CartRepository $repository, UserRepository $userRepository)
    {
        $this->repository = $repository;
        $this->userRepository = $userRepository;
        $this->setLang($request->header('lang'));
    }

//Show Product user from cart
    public function index(Request $request)
    {

        $check_user = $this->userRepository->checkJwtUser($request);
        if (!$check_user) {
            return $this->respondForbidden(trans('messages.auth.user_check'));
        }
        $item_details = $this->repository->getCartItemDetails($check_user, $this->lang);

        return $this->respond(
            200,
            trans('messages.cart.list'),
            $item_details
        );
    }

    // get all shipping company

    function getShippingCompany(Request $request)
    {
        $check_user = $this->userRepository->checkJwtUser($request);
        if (!$check_user) {
            return $this->respondForbidden(trans('messages.auth.user_check'));
        }

//        $shipping_company = ShippingCompany::where('city_id', $check_user->city_id)->where('company_status', 1)->where('status', 1)->select('id', 'company_' . $this->lang . ' as company_name', 'phone_number','shipping')->get();
        $product_cart = UserCart::where('user_id', $check_user->id)->first();
        $shop_id = Product::where('id', $product_cart->product_id)->pluck('shop_id')->first();
        $get_shop_city = Shop::where('id', $shop_id)->first();
        // $shipping_fees = ShippingFee::where('city_id', $check_user->city_id)->get();
        if ($check_user->country_id == $get_shop_city->country_id) {
            $shipping_fees = DB::select("SELECT * FROM shipping_fees
  WHERE (city_from = $check_user->city_id AND city_to = $get_shop_city->city_id)
  OR (city_to = $get_shop_city->city_id AND city_from = $check_user->city_id)");
        } else {

            $shipping_fees = DB::select("SELECT * FROM shipping_fees
  WHERE (country_from = $check_user->country_id AND country_to = $get_shop_city->country_id)
  OR (country_to = $get_shop_city->country_id AND country_from = $check_user->country_id)");
        }

        foreach ($shipping_fees as $key => $shipping_fee) {
            $company_item['id'] = $shipping_fee->company_id;
            $company = ShippingCompany::where('id', $shipping_fee->company_id)->first();
            if ($this->lang == 'ar') {
                $company_item['company_name'] = $company->company_ar;
            } else if ($this->lang == 'kur') {
                $company_item['company_name'] = $company->company_kur;
            } else {
                $company_item['company_name'] = $company->company_en;
            }
            $company_item['phone_number'] = $company->phone_number;
            $company_item['shipping'] = !empty($shipping_fee->shipping_fees) ? $shipping_fee->shipping_fees : 0;
            if ($check_user->country_id == $get_shop_city->country_id) {
                $company_item['city_from'] = City::where('id', $shipping_fee->city_from)->pluck('city_name_' . $this->lang)->first();
                $company_item['city_to'] = City::where('id', $shipping_fee->city_to)->pluck('city_name_' . $this->lang)->first();
            } else {
                $company_item['city_from'] = Country::where('id', $shipping_fee->country_from)->pluck('country_name_' . $this->lang)->first();
                $company_item['city_to'] = Country::where('id', $shipping_fee->country_to)->pluck('country_name_' . $this->lang)->first();
            }
            $company_list[] = $company_item;
        }
//        if ($shipping_company) {
//            return $this->respond(200, trans('messages.cart.shipping'), $shipping_company);
//        }

        return $this->respond(200, trans('messages.cart.shipping'), !empty($company_list) ? $company_list : []);

        return $this->respondWithError(trans('messages.cart.noShipping'));
    }

    //Store Product in cart
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
        ],
            [
                'product_id.required' => trans('validation.product_id'),
            ]
        );
        if ($validator->fails()) {
            return $this->respondWithError($validator->errors()->first());
        }

        $user = $this->userRepository->checkJwtUser($request);
        if (!$user) {
            return $this->respondForbidden(trans('messages.auth.user_check'));
        }

        $check_product_building = $this->repository->checkIfProductBuilding($request->product_id);
        if ($check_product_building['type'] == 3) {
            return $this->respondWithError(trans('messages.cart.building'));
        }

        if ($user) {
            if ($request->cart_item_id) {
                if ($this->repository->update($request->except('jwt_token'))) {
                    return $this->respondWithMessage(trans('messages.cart.updated'));
                } else {
                    return $this->respondWithError(trans('messages.cart.over_available_stock'));
                }
            } else {

                $checkProduct = $this->repository->checkIfProductClothes($request->product_id);
                if ($checkProduct['type'] == 1) {
                    $validator = Validator::make($request->all(), [
                        'size_id' => 'required',
                        'color_id' => 'required',
                    ],
                        [
                            'size_id.required' => trans('validation.size_id'),
                            'color_id.required' => trans('validation.color_id'),
                        ]
                    );
                    if ($validator->fails()) {
                        return $this->respondWithError($validator->errors()->first());
                    }
                    $ifExists = $this->repository->checkIfProductSizeExists($user->id, $request->product_id, $request->size_id, $request->color_id);
                    if ($ifExists) {
                        return $this->respondWithError(trans('messages.cart.already_in_cart'));
                    }
                    $cart_item = $request->except('jwt_token');
                    $cart_item['user_id'] = $user->id;
                    if ($this->repository->createProduct($cart_item, $checkProduct['type'])) {
                        return $this->respondWithMessage(trans('messages.cart.added'));
                    } else {
                        return $this->respondWithError(trans('messages.cart.over_available_stock'));
                    }
                } else if ($check_product_building['type'] == 2) {
                    $validator = Validator::make($request->all(), [
                        'size_id' => 'required',
                    ],
                        [
                            'size_id.required' => trans('validation.size_id'),
                        ]
                    );
                    if ($validator->fails()) {
                        return $this->respondWithError($validator->errors()->first());
                    }
                    $ifExists = $this->repository->checkIfProductFoodExists($user->id, $request->product_id, $request->size_id);
                    if ($ifExists) {
                        return $this->respondWithError(trans('messages.cart.already_in_cart'));
                    }
                    $cart_item = $request->except('jwt_token');
                    $cart_item['user_id'] = $user->id;
                    if ($this->repository->createProduct($cart_item, $check_product_building['type'])) {

                        return $this->respondWithMessage(trans('messages.cart.added'));
                    } else {
                        return $this->respondWithError(trans('messages.cart.over_available_stock'));
                    }
                } else {
                    $ifExists = $this->repository->checkIfProductExists($user->id, $request->product_id);
                    if ($ifExists) {
                        return $this->respondWithError(trans('messages.cart.already_in_cart'));
                    }
                    $cart_item = $request->except('jwt_token');
                    $cart_item['user_id'] = $user->id;
                    if ($this->repository->create($cart_item)) {
                        return $this->respondWithMessage(trans('messages.cart.added'));
                    } else {
                        return $this->respondWithError(trans('messages.cart.over_available_stock'));
                    }
                }

            }
        } else {
            return $this->respondWithError(trans('messages.auth.user_check'));

        }
    }

    //DeleteCart
    public function delete(DeleteCartRequest $request)
    {
        $user = $this->userRepository->checkJwtUser($request);
        if (!$user) {
            return $this->respondForbidden(trans('messages.auth.user_check'));
        }
        if ($this->repository->delete($request->cart_item_id)) {
            return $this->respondWithMessage(trans('messages.cart.removed'));
        }
        return $this->respondWithError(trans('messages.cart.missing_details'));
    }
}
