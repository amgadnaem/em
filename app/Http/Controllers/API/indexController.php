<?php

namespace App\Http\Controllers\API;

use App\Country;
use App\Repositories\Country\CountryRepository;
use App\Slider;
use Illuminate\Http\Request;

class indexController extends APIController
{
    //
    protected $countryRepository;

    function __construct(Request $request, CountryRepository $countryRepository)
    {
        $this->setLang($request->header('lang'));
        $this->countryRepository = $countryRepository;

    }

    // get all countries
    function getCountries()
    {
        $countries = Country::where('status', 1)->get();
        $data = $this->countryRepository->getAllCountry($countries, $this->lang);
        return $this->respond(200, trans('messages.auth.countryInfo'), $data);
    }

//    function getCountries1()
//    {
//        $countries = Country::where('status', 1)->select('country_name_' . $this->lang . ' as country_name', 'country_image')->get();
//        $slider = Slider::where('status', 1)->select('slider_image')->get();
//        return response()->json([
//            'status' => 200,
//            'message' => trans('messages.auth.countryInfo'),
//            'data' => [
//                'countries' => $countries,
//                'sliders' => $slider,
//            ]
//        ]);
//    }
}
