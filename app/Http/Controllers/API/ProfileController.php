<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\User\ChangePassword;
use App\Repositories\User\UserRepository;
use App\User;
use App\Shop;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class ProfileController extends APIController
{
    protected $userRepository;


    public function __construct(Request $request, UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->setLang($request->header('lang'));

    }

    //EditProfile
    public function edit(Request $request,$user_image = null)
    {
        $check_user = $this->userRepository->checkJwtUser($request);
        if (!$check_user) {
            return $this->respondWithError(trans('messages.auth.user_check'));
        }
        $user = User::where('id', $check_user->id)->first();
        $user->city_id = $request->city_id;
        $user->lat = $request->lat;
        $user->lng = $request->lng;
        $user->location = $request->address;
        $user->user_image = $request->image;
        //If user saved successfully, then return true
        if ($user_image) {
            $user->user_image = $user_image;
            $user->save();
        }
        if ($user->save()) {
            $data = $this->userRepository->getLoggedUserDetails($user,$this->lang);
            return $this->respond(
                200,
                trans('messages.profile.updated'),
                $data
            );
        }

        return $this->respondWithError(trans('messages.something_went_wrong'));
    }

    /// Change orForgetPassword

     public function changePassword(ChangePassword $request)
    {

        if ($request->type == 3) {
            $shop = $this->userRepository->checkJwtShop($request);
            if (!empty($shop)) {
                if (isset($request->old_password)) {
                    $check = Hash::check($request->old_password, $shop->password);
                    if (!$check) {
                        return $this->respondWithError(trans('messages.profile.wrong_old_password'));
                    }
                }
                $data['password'] = Hash::make($request->new_password);
                $shop->update($data);
                return response()->json([
                    'status' => 200,
                    'message' => trans('messages.auth.forgetPassword')
                ]);
            } else {
             $forget_password = Shop::where('phone_number', $request->phone_number)->where('type',$request->type)->first();

                if ($forget_password) {
                    $forget_password->password = Hash::make($request->new_password);
                    $forget_password->save();
                    return response()->json([
                        'status' => 200,
                        'message' => trans('messages.auth.forgetPassword')
                    ]);
                }
                return $this->respondWithError(trans('messages.something_went_wrong'));
            }
        } else {
            $user = $this->userRepository->checkJwtUser($request);
            if ($user) {
                if (isset($request->old_password)) {
                    $check = Hash::check($request->old_password, $user->password);
                    if (!$check) {
                        return $this->respondWithError(trans('messages.profile.wrong_old_password'));
                    }
                }
                $data['password'] = Hash::make($request->new_password);
                $user->update($data);
                return response()->json([
                    'status' => 200,
                    'message' => trans('messages.auth.forgetPassword')
                ]);
            } else {
                $forget_password = User::where('phone_number', $request->phone_number)->where('type',$request->type)->first();
                if ($forget_password) {
                    $forget_password->password = Hash::make($request->new_password);
                    $forget_password->save();
                    return response()->json([
                        'status' => 200,
                        'message' => trans('messages.auth.forgetPassword')
                    ]);
                }
                return $this->respondWithError(trans('messages.something_went_wrong'));
            }
        }
    }
}
