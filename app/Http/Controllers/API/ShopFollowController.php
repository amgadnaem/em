<?php

namespace App\Http\Controllers\API;

use App\Repositories\User\UserRepository;
use App\ShopFollow;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShopFollowController extends APIController
{
    public $repository;
    public $followShop;

    function __construct(Request $request, UserRepository $repository, ShopFollow $followShop)
    {
        $this->repository = $repository;
        $this->followShop = $followShop;
        $this->setLang($request->header('lang'));
    }

    function CreateFollowShop(Request $request)
    {
        $check_user = $this->repository->checkJwtUser($request);
        if (!$check_user) {
            return $this->respondWithError(trans('messages.auth.user_check'));

        }
        $message = '';
        $isFollow = 0;

        $checkExists = ShopFollow::where('user_id', $check_user->id)->where('shop_id', $request->shop_id)->pluck('id')->first();
        if (!$checkExists) {
            if ($this->followShop->create([
                    'user_id' => $check_user->id,
                    'shop_id' => $request->shop_id
                ]
            )) {
                $isFollow = 1;
                $message = trans('messages.follow.added');
            }
        } else {

            if ($this->followShop->destroy($checkExists)) {
                $isFollow = 0;
                $message = trans('messages.follow.removed');
            }
        }
        return response()->json([
            'status' => 200,
            'isFollow' => $isFollow,
            'message' => $message
        ]);
    }
}
