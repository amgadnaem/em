<?php

namespace App\Http\Controllers\API\Shops;

use App\Http\Controllers\API\APIController;
use App\Http\Requests\Shops\addShop;
use App\Http\Requests\Shops\Category;
use App\Product;
use App\Repositories\Cart\CartRepository;
use App\Repositories\Product\ProductRepository;
use App\Repositories\Shop\ShopRepository;
use App\Repositories\User\UserRepository;
use App\Shop;
use App\ShopCategories;
use App\User;
use App\ShopChat;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class indexController extends APIController
{
    protected $userRepository;
    protected $productRepository;
    protected $shopRepository;
    protected $lang;

    public function __construct(Request $request,
                                UserRepository $userRepository,
                                ProductRepository $productRepository,
                                ShopRepository $shopRepository
    )
    {
        $this->userRepository = $userRepository;
        $this->shopRepository = $shopRepository;
        $this->productRepository = $productRepository;
        $this->setLang($request->header('lang'));
    }

    function addShop(addShop $request)
    {
        $phone_number = $request->phone_number;
        if ($this->userRepository->checkIfShopExists($phone_number)) {
            return $this->respondWithError(trans('messages.auth.account_exists'));
        }
        $add_shop = $this->shopRepository->createShop($request);
        if ($add_shop) {
             return $this->respond(
                    200,
                    trans('messages.auth.register'),
                    $this->userRepository->getLoggedShopDetails($add_shop, $this->lang)
                );
        }
    }

    // complete data shop

    public function completeAccountShop(Request $request)
    {
        $user = $this->userRepository->updateAccountShops($request, $request->license_image, $request->ID_image, $this->lang);
        if ($user) {
            return $this->respond('200', trans('messages.auth.register'), $user);
        }
    }


// add category

    function AddCategory(Category $request)
    {

        $shop_owner = $this->userRepository->checkJwtShop($request);
        if (!$shop_owner) {
            return $this->respondForbidden(trans('messages.auth.user_check'));
        }

        $add_category = new ShopCategories();
        $add_category->category_name_en = $request->category_name;
        $add_category->category_name_ar = $request->category_name;
        $add_category->category_name_kur = $request->category_name;
        $add_category->shop_id = $shop_owner->id;
        $add_category->save();
        if ($add_category) {
            if ($request->category_image) {
                $add_category->category_image = $request->category_image;
                $add_category->save();
            }
            return response()->json([
                'status' => 200,
                'message' => trans('messages.shops.addCategory')
            ]);
        }
        return $this->respondWithError(trans('messages.something_went_wrong'));
    }

// show all category

    function all_category(Request $request)
    {
        $shop_owner = $this->userRepository->checkJwtShop($request);
        if (!$shop_owner) {
            return $this->respondForbidden(trans('messages.auth.user_check'));
        }
        $get_category_shop = ShopCategories::where('shop_id', $shop_owner->id)->get();
         $category_item = [];
        $category_list = [];
        foreach ($get_category_shop as $categories) {

            $category_item['id'] = $categories->id;
            if ($this->lang = 'ar') {
                $category_item['name'] = $categories->category_name_ar;
            } else {
                $category_item['name'] = $categories->category_name_en;

            }
            $category_item['service_image'] = $categories->category_image;
            $category_list[] = $category_item;
        }
        if ($get_category_shop) {
            return $this->respond('200', trans('messages.category.list'), $category_list);
        }
        return $this->respondWithError(trans(trans('messages.category.no')));
        
     
    }

// get all products shops
    function products(Request $request)
    {
        $shop_owner = $this->userRepository->checkJwtShop($request);
        if (!$shop_owner) {
            return $this->respondForbidden(trans('messages.auth.user_check'));
        }
        $products = Product::where('category_id',$request->category_id)->where('hosted','!=',2)->get();
        $product_details = $this->productRepository->getAllProducts($products, $this->lang);
        return $this->respond(200, trans('messages.products.list'), $product_details);
    }

    function productsDetails(Request $request)
    {
        $shop_owner = $this->userRepository->checkJwtShop($request);
        if (!$shop_owner) {
            return $this->respondForbidden(trans('messages.auth.user_check'));
        }
        $product = Product::where('id', $request->product_id)->first();
        $product_details = $this->productRepository->getProductShopDetails($product, $this->lang);

        if (!empty($product_details)) {
            return $this->respond(200, trans('messages.products.page'), $product_details);

        }
        return $this->respondWithError(trans('messages.something_went_wrong'));

    }

// search for person
    public
    function searchPersons(Request $request)
    {
        $shop_owner = $this->userRepository->checkJwtShop($request);
        if (!$shop_owner) {
            return $this->respondForbidden(trans('messages.auth.user_check'));
        }
        $type = $request->type;
        if ($type == 1) {
            $users = User::whereStatus(1)
                ->where('status', 1)
                ->where('type', 1)
                ->where('phone_number', $request->user_number)
                ->get();
        } elseif ($type == 2) {

            $users = User::whereStatus(1)
                ->where('status', 1)
                ->where('type', 1)
                ->Where('first_name', 'like', "%{$request->user_name}%")
                ->Where('last_name', 'like', "%{$request->user_name}%")
                ->get();
        } else {
            $users = User::whereStatus(1)
                ->where('status', 1)
                 ->where('type', 1)
                ->where('phone_number', $request->user_number)
                ->Where('first_name', 'like', "%{$request->user_name}%")
                ->Where('last_name', 'like', "%{$request->user_name}%")
                ->get();
        }

        $data = $this->userRepository->getAllUsers($users,$shop_owner);
        return $this->respond(
            200,
            trans('messages.auth.list'),
            $data
        );
    }
    
     function all_chats(Request $request)
    {
        $auth_login = $this->shopRepository->checkJwtShop($request);
        if (!$auth_login) {
            return $this->respondForbidden(trans('messages.auth.user_check'));
        }
        $all_chats = ShopChat::where('shop_id', $auth_login->id)->where('type',1)->groupBy('from')->get();
        $chats = $this->getChatDetails($all_chats);
          if ($chats) {
            return $this->respond(200, trans('messages.chats.list'), $chats);
        } else {
            return $this->respond(200, trans('messages.chats.list'), $chats);

        }
    }

  function getChatDetails($chats)
    {
        $chat_list = [];
        $chat_item = [];
        foreach ($chats as $chat) {
            $chat_item['id'] = $chat->from;
            $chat = User::where('id', $chat->from)->first();
              $chat_item['name'] = $chat['first_name'];

            $chat_item['image'] = $chat['user_image'];
            $chat_list[] = $chat_item;
        }
        return $chat_list;

    }

}
