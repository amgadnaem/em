<?php

namespace App\Http\Controllers\API\Shops;

use App\Http\Controllers\API\APIController;
use App\PermanentUser;
use App\Repositories\User\UserRepository;
use App\ShopFavorite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PermanentsController extends APIController
{
    public $repository;

    function __construct(Request $request, UserRepository $repository)
    {
        $this->repository = $repository;
        $this->setLang($request->header('lang'));
    }

    // get all permanent user in shops
    function getAllPermanentUsers(Request $request)
    {
        $shop_owner = $this->repository->checkJwtShop($request);
        if (!$shop_owner) {
            return $this->respondWithError(trans('messages.auth.user_check'));
        }
        $permanent_users = PermanentUser::where('shop_id', $shop_owner->id)->get();
        $permanents_details = $this->repository->getAllPermanentUsers($permanent_users);
        if (count($permanents_details) > 0) {
            return $this->respond('200', trans('messages.auth.permanentUsers'), $permanents_details);
        }
        return $this->respondWithError(trans('messages.something_went_wrong'));

    }

    // add user from shop to Permanents users

    function addDeletePermanents(Request $request)
    {
        $shop_owner = $this->repository->checkJwtShop($request);
        if (!$shop_owner) {
            return $this->respondWithError(trans('messages.auth.user_check'));
        }
        $message = '';
        $isPermanents = 0;
        $permanents = PermanentUser::where('user_id', $request->user_id)->where('shop_id', $shop_owner->id)->pluck('id')->first();
        if (!$permanents) {
            $permanent = new PermanentUser();
            $permanent->shop_id = $shop_owner->id;
            $permanent->user_id = $request->user_id;
            $permanent->save();
            if ($permanent) {
                $isPermanents = 1;
                $message = trans('messages.auth.addUser');
            }
        } else {
            if (PermanentUser::destroy($permanents)) {
                $isPermanents = 0;
                $message = trans('messages.auth.removedUser');
            }
        }
        return response()->json([
            'status' => 200,
            'message' => $message,
            'isPermanents' => $isPermanents,

        ]);

    }
}
