<?php

namespace App\Http\Controllers\API\Shops;

use App\HostProduct;
use App\Http\Controllers\API\APIController;
use App\Notification;
use App\Product;
use App\ProductColorsSize;
use App\Repositories\User\UserRepository;
use App\Shop;
use App\UserReview;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HostProductController extends APIController
{
    public $repository;

    function __construct(Request $request, UserRepository $repository)
    {
        $this->repository = $repository;
        $this->setLang($request->header('lang'));
    }

    
    
     // add host from another shop

    function add_product_host(Request $request)
    {
        $shop_owner = $this->repository->checkJwtShop($request);
        if (!$shop_owner) {
            return $this->respondForbidden(trans('messages.auth.user_check'));
        }

        $product_host = Product::where('id', $request->product_id)->first();
        if ($product_host->type == 1) {
            $check_exist = HostProduct::where('to_shop', $shop_owner->id)->where('hosted_product_id', $request->product_id)->first();
            $check_exist1 = !empty($check_exist->product_id) ? $check_exist->product_id : null;
            $check_size_color = ProductColorsSize::where('product_id', $check_exist1)->where('size_id', $request->size_id)->where('color_id', $request->color_id)->first();
            if ($check_exist && $check_size_color) {
                return $this->respondWithError(trans('messages.products.already_in_host'));
            }
            $stock = ProductColorsSize::where('product_id', $request->product_id)->where('size_id', $request->size_id)->where('color_id', $request->color_id)->pluck('stock')->first();
            if ($request->qty > $stock) {
                return $this->respondWithError(trans('messages.cart.over_available_stock'));
            }
        } elseif ($product_host->type == 3 || $product_host->type == 4) {
            $check_exist = HostProduct::where('to_shop', $shop_owner->id)->where('from_shop', $request->shop_id)->where('hosted_product_id', $request->product_id)->first();
            if ($check_exist) {
                return $this->respondWithError(trans('messages.products.already_in_host'));
            }
            if ($request->qty > $product_host['product_stock']) {
                return $this->respondWithError(trans('messages.cart.over_available_stock'));
            }
        }
        $add_product = new Product();
        $add_product->name_en = $product_host->name_en;
        $add_product->name_ar = $product_host->name_ar;
        $add_product->name_kur = $product_host->name_kur;
        $add_product->code = $product_host->code;
        $add_product->qr_code = $product_host->code;
        $add_product->general_price = $product_host->general_price;
        $add_product->shop_id = $shop_owner->id;
        $add_product->category_id = $request->category_id;
        if ($product_host->type == 3 || $product_host->type == 4) {
            $add_product->product_stock = $request->qty;
        }
        $add_product->desc_en = $product_host->desc_en;
        $add_product->desc_ar = $product_host->desc_ar;
        $add_product->desc_kur = $product_host->desc_kur;
        $add_product->type = $product_host->type;
        $add_product->hosted = 2;
        $add_product->status = 0;
        $add_product->hosted_price = $product_host->hosted_price;

        $add_product->save();
        if ($add_product) {
            if ($product_host->type == 1) {
                $add_size = new ProductColorsSize();
                $add_size->size_id = $request->size_id;
                $add_size->color_id = $request->color_id;
                $add_size->product_id = $add_product->id;
                $add_size->stock = $request->qty;
                $add_size->save();
            }
            $add_hosted = new HostProduct();
            $add_hosted->from_shop = $request->shop_id;
            $add_hosted->to_shop = $shop_owner->id;
            $add_hosted->product_id = $add_product->id;
            $add_hosted->hosted_product_id = $request->product_id;
            $add_hosted->save();
                      $this->notify($this->lang, $request->product_id, $shop_owner->id, $request->shop_id);

            return response()->json([
                'status' => 200,
                'message' => trans('messages.products.add')
            ]);
        }
    }
    
    

    // send notify

    public function notify($lang, $product_id, $shop_id, $shop_hosted)
    {
          define('API_ACCESS_KEY', 'AAAAJIhBdDE:APA91bHLPrry6EpPfm8cSqvb0Vg5d3pa22K5i_e3uPTKev8JR_7SsVjGktN-paxPXo7k26OYB4oJbWZN6RxkOrQ2KCLvLH_ifqUyfRJow7EG4doyamUVXVlGK8aRnaKPVqIib3md5MoU');
          
        $shop = Shop::where('id', $shop_id)->first();
        $shop_hosted1 = Shop::where('id', $shop_hosted)->first();
        $product_hosted = Product::where('id', $product_id)->first();
  
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        if ($lang == 'en') {
            $notification1 = [
                'title' => 'Product Host',
                'body' => 'Shop' . $shop->name_app_en . ' Want To Product ' . $product_hosted->name_en,
                    'product_id'=>$product_id,
                    'shop_id'=>$shop_id,
                'type' => "3"
            ];

        } elseif ($lang == 'ar' || $lang == 'kur') {
            $notification1 = [
                'title' => 'استضافه منتج',
                'body' => 'متجر ' . $shop->name_app_ar . $product_hosted->name_ar . ' يريد ان يستضيف منك منتج ',
                'type' => "3",
                  'product_id'=>$product_id,
                      'shop_id'=>$shop_id,
              
            ];
        }
        $notification = new Notification();
        $notification->from = $shop_id;
        $notification->to = $shop_hosted;
        $notification->title_en = 'Product Host';
        $notification->message_en = 'Shop ' . $shop->name_app_en . ' Want To Product ' . $product_hosted->name_en;
        $notification->title_ar = 'استضافه منتج';
        $notification->message_ar = ' متجر' . $shop->name_app_ar . ' يريد ان يستضيف منك منتج';
        $notification->title_kur = 'استضافه منتج';
        $notification->message_kur = ' متجر' . $shop->name_app_ar . ' يريد ان يستضيف منك منتج';
        $notification->type = 3;
        $notification->flag = 3;
          $notification->status = 5;
        $notification->save();
        $extraNotificationData = ["message" => $notification1, "moredata" => 'dd'];
        $fcmNotification = [
            'to' => $shop_hosted1->firebase_token,
            'data' => $extraNotificationData
        ];

        $headers = [
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    // confirm or cancel request host

    function confirm_host_product(Request $request)
    {
         $shop_owner = $this->repository->checkJwtShop($request);
        if (!$shop_owner) {
            return $this->respondForbidden(trans('messages.auth.user_check'));
        }

            $check_exist = HostProduct::where('from_shop', $shop_owner->id)->where('to_shop', $request->shop_id)->where('hosted_product_id', $request->product_id)->where('status', 0)->first();
        if ($request->status == 1) {
            $check_exist->status = 1;
            $check_exist->save();
            if ($check_exist) {
                $product = Product::where('id', $check_exist->product_id)->first();
                $product->status = 1;
                $this->confirmOrder($this->lang, $shop_owner->id, $request);
                if ($product->save()) {
                    return response()->json([
                        'status' => 200,
                        'message' => trans('messages.order.confirm')
                    ]);
                }
            }
        } else {
            $product = Product::where('id', $check_exist->product_id)->delete();
            if ($product) {
                return response()->json([
                    'status' => 200,
                    'message' => trans('messages.order.cancel')
                ]);
            }
        }
    }

    public function confirmOrder($lang, $shop_owner, $request)
    {
        $shop_owner = Shop::where('id', $shop_owner)->first();
        $send_shop = Shop::where('id', $request->shop_id)->first();
   define('API_ACCESS_KEY', 'AAAAJIhBdDE:APA91bHLPrry6EpPfm8cSqvb0Vg5d3pa22K5i_e3uPTKev8JR_7SsVjGktN-paxPXo7k26OYB4oJbWZN6RxkOrQ2KCLvLH_ifqUyfRJow7EG4doyamUVXVlGK8aRnaKPVqIib3md5MoU');

        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        if ($lang == 'en') {
            $notification1 = [
                'title' => 'Accept Order',
                'type'=>10,

                'body' => 'Accept Shop on Order',
            
            ];

        } elseif ($lang == 'ar' || $lang == 'kur') {
            $notification1 = [
                'title' => ' قبول طلبك',
               'type'=>10,
                'body' => 'وافق المتجر على طلبك ',
         
            ];
        }


        $notification = new Notification();
        $notification->from = $shop_owner;
        $notification->to = $request->shop_id;
        $notification->title_en = 'Shop Accept Order To Host Product ';
        $notification->message_en = 'Shop ' . $shop_owner->name_app_en . ' Shop Accept Order To Host Product ';
        $notification->title_ar = 'قبول استضافه المنتج';
        $notification->message_ar = ' متجر' . $shop_owner->name_app_ar . 'تم قبول طلب استضافه المنتج';
        $notification->title_kur = 'قبول استضافه المنتج';
        $notification->message_kur = ' متجر' . $shop_owner->name_app_ar . 'تم قبول طلب استضافه المنتج';
        $notification->type = 3;
        $notification->flag = 3;
        $notification->save();
        $extraNotificationData = ["message" => $notification1, "moredata" => 'dd'];

        $fcmNotification = [

            'to' => $send_shop->firebase_token, //single token
            'data' => $extraNotificationData
        ];

        $headers = [
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;

    }

    // get product hosted and shared
    function shared_hosted_product(Request $request)
    {

        $shop_owner = $this->repository->checkJwtShop($request);
        if (!$shop_owner) {
            return $this->respondForbidden(trans('messages.auth.user_check'));
        }

        $type = $request->type;
        if ($type == 1) {
            $hosted_product = HostProduct::where('to_shop', $shop_owner->id)->where('status',1)->get();
            $hosted_item = [];
            $hosted_list = [];
            foreach ($hosted_product as $hosted) {
                $hosted_item['product_id'] = $hosted->product_id;
                $product = Product::where('id', $hosted->product_id)->where('status',1)->first();

              $hosted_item['price'] = $product['hosted_price'];
                if ($this->lang == 'ar') {
                    $hosted_item['name'] = $product['name_ar'];
                    $hosted_item['description'] = $product['desc_ar'];
                } elseif ($this->lang == 'kur') {
                    $hosted_item['name'] = $product['name_kur'];
                    $hosted_item['description'] = $product['desc_kur'];
                } else {
                    $hosted_item['name'] = $product['name_en'];
                    $hosted_item['description'] = $product['desc_en'];
                }
                $hosted_item['rate'] = $this->getProductRate($hosted->hosted_product_id);
                $hosted_item['image'] = Product::where('id', $hosted->hosted_product_id)->pluck('product_image')->first();
                $hosted_list[] = $hosted_item;
            
            }
                return $this->respond(200, trans('messages.products.host_list'), $hosted_list);
        } else {

            $result = [];
            $shared_product = HostProduct::where('from_shop', $shop_owner->id)->where('status',1)->get();
            foreach ($shared_product as $row) {
                $key = $row->hosted_product_id;
                $product = Product::where('id', $row->hosted_product_id)->first();
                $result[$key]['id'] = $row->hosted_product_id;

                $result[$key]['price'] = $product->hosted_price;
                $result[$key]['image'] = $product->product_image;
                if ($this->lang == 'ar') {
                    $result[$key]['name'] = $product->name_ar;
                    $result[$key]['description'] = $product->desc_ar;
                } elseif ($this->lang == 'kur') {
                    $result[$key]['name'] = $product->name_kur;
                    $result[$key]['description'] = $product->desc_kur;
                } else {
                    $result[$key]['name'] = $product->name_en;
                    $result[$key]['description'] = $product->desc_en;
                }
                $shops = Shop::where('id', $row->to_shop)->first();
                $result[$key]['rate'] = $this->getProductRate($row->hosted_product_id);
                $result[$key]['shops'][] = array('id' => $shops->id,
                    'shop_name' => $shops->name_app_ar,
                    'shop_image' => $shops->shop_image,
                    'description' => $shops->desc_ar,
                );
            }
            $data = [];
            foreach ($result as $key => $value) {
                $data[] = $value;
            }
            return $this->respond(200, trans('messages.products.shared_list'), $data);

        }
    }

    // rate product
    public function getProductRate($product_id)
    {
        $avg_rate = UserReview::where('product_id', $product_id)->avg('rate_value');
        $result = round($avg_rate, 1);
        return (string)$result;
    }
    
    // delete product shared

    function delete_shared_product(Request $request)
    {
        $shop_owner = $this->repository->checkJwtShop($request);
        if (!$shop_owner) {
            return $this->respondForbidden(trans('messages.auth.user_check'));
        }
        $deleteProduct = HostProduct::where('from_shop', $shop_owner->id)->where('hosted_product_id', $request->product_id)->delete();
        if ($deleteProduct) {
            return response()->json([
                'status' => 200,
                'message' => trans('messages.products.delete')
            ]);
        }
        return $this->respondWithError(trans('messages.something_went_wrong'));

    }
}
