<?php

namespace App\Http\Controllers\API\Shops;

use App\Account;
use App\Http\Controllers\API\APIController;
use App\Repositories\Shop\ShopRepository;
use App\Repositories\User\UserRepository;
use App\Shop;
use App\ShopAccount;
use App\SpecialNumber;
use App\SpecialNumberCost;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SpecialNumberController extends APIController
{
    protected $shopRepository;
    protected $userRepository;
    protected $lang;


    public function __construct(Request $request,UserRepository $userRepository,

                                ShopRepository $shopRepository
    )
    {
        $this->shopRepository = $shopRepository;
        $this->userRepository = $userRepository;
        $this->setLang($request->header('lang'));
    }

    // add special numbers

    function addSpecialNumber(Request $request)
    {

        for ($i = 1; $i <= 9; $i++) {
            $special_number = new SpecialNumber();
            $special_number->special_number = $i;
            $special_number->number_id = 3;
            $special_number->save();
        }
        if ($special_number) {
            return response()->json([
                'status' => 200,
                'message' => 'تم اضافه رقم مميز  بنجاح'
            ]);
        }
    }

    function getAllSpecialNumber()
    {
         $all_numbers = SpecialNumberCost::pluck('id');
        $special_numbers = SpecialNumber::where('status', 1)->whereIn('number_id', $all_numbers)->select('id', 'special_number', 'number_id')->get();
        foreach ($special_numbers as $row) {

            $key = $row->number_id;
             $result[$key]['id'] = $row->id;
            $result[$key]['number'] = $row->number_id . ' number';
            $result[$key]['price'] = SpecialNumberCost::where('id', $row->number_id)->pluck('price')->first();
            $result[$key]['numbers_list'][] = array('id' => $row->id,
                'special_number' => $row->special_number);
        }

        $brands = [];

        foreach ($result as $key => $value) {

            $brands[] = $value;
        }

        return $this->respond(200, trans('messages.shops.numbers_list'), $brands);

    }

  function buySpecialNumber(Request $request)
    {
        $shop_owner = $this->shopRepository->checkJwtShop($request);
        if (!$shop_owner) {
            return $this->respondWithError(trans('messages.auth.user_check'));
        }
        if ($request->type == 1) {
            $send_number = SpecialNumber::where('id', $request->number_id)->first();
            $send_number->status = 0;
            $send_number->save();
            if ($send_number) {
                $shop_number = Shop::where('id', $shop_owner->id)->first();
                $shop_number->shop_number = $send_number['special_number'];
                $shop_number->save();
                if ($shop_number) {
                    $accounts = new ShopAccount();
                    $accounts->shop_id = $shop_owner->id;
                    $accounts->account_id = $request->account_id;
                    $accounts->number_id = $send_number->id;
                    $accounts->payment_method = $request->payment_method;
                    $accounts->save();
                    if ($shop_number) {
                        return response()->json([
                            'status' => 200,
                            'message' => trans('messages.shops.sendNumber'),
                            'data'=>$this->userRepository->getLoggedShopDetails($shop_number, $this->lang)
                        ]);
                    }
                }
            }
        } else {
            $rand_number = rand(1111, 9999);
            $shop_number = Shop::where('id', $shop_owner->id)->first();
            $shop_number->shop_number = $rand_number;
            $shop_number->save();
            if ($shop_number) {
                  return response()->json([
                            'status' => 200,
                            'message' => trans('messages.shops.sendNumber'),
                            'data'=>$this->userRepository->getLoggedShopDetails($shop_number, $this->lang)
                        ]);
            }
        }


    }

    // get account admin from shop

     function accounts(Request $request)
    {
        $shop_owner = $this->shopRepository->checkJwtShop($request);
        if (!$shop_owner) {
            return $this->respondWithError(trans('messages.auth.user_check'));
        }
        $type = $request->type;
        if ($type == 1) {
            $accounts = Account::select('id', 'title_' . $this->lang . ' as title', 'cost')->get();
            if ($accounts) {
                return $this->respond(200, trans('messages.notification.accounts'), $accounts);
            }
        } else {
            $Accounts = ShopAccount::where('shop_id', $shop_owner->id)->get();
            $account_item = [];
            $account_list = [];
            foreach ($Accounts as $account) {
                $account_item['id'] = $account->id;
                $account_details = Account::where('id', $account->account_id)->first();
                $number_cost = SpecialNumber::where('id', $account->number_id)->pluck('number_id')->first();
                if ($this->lang == 'ar') {
                    $account_item['title'] = $account_details->title_ar;
                    $account_item['message'] = $account_details->message_en;
                } elseif ($this->lang == 'kur') {
                    $account_item['account_id'] = $account_details->title_kur;
                    $account_item['message'] = $account_details->message_en;
                } else {
                    $account_item['account_id'] = $account_details->title_en;
                    $account_item['message'] = $account_details->message_en;
                }
                $account_item['cost'] = $account_details->cost;
                if (!empty($account->number_id)) {
                    $account_item['special_number_cost'] = SpecialNumberCost::where('id', $number_cost)->pluck('price')->first();

                }
                $account_list[] = $account_item;
            }
            if ($account_list) {
                return $this->respond(200, trans('messages.notification.accounts'), $account_list);
            } else {
                 return $this->respond(200, trans('messages.notification.accounts'), $account_list);
            }

        }

    }

}
