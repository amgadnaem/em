<?php

namespace App\Http\Controllers\API\Shops;

use App\Address;
use App\Http\Controllers\API\APIController;
use App\Order;
use App\Repositories\Shop\ShopRepository;
use App\Repositories\User\UserRepository;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class OrderController extends APIController
{
    protected $shopRepository;
    protected $userRepository;
    protected $lang;


    public function __construct(Request $request,
                                UserRepository $userRepository,
                                ShopRepository $shopRepository
    )
    {
        $this->shopRepository = $shopRepository;
        $this->userRepository = $userRepository;
        $this->setLang($request->header('lang'));
    }

    // get order shops
    function shop_order(Request $request)
    {
        $type = $request->type;
        if ($type == 3) {
            $check_login = $this->shopRepository->checkJwtShop($request);
            if (!$check_login) {
                return $this->respondForbidden(trans('messages.auth.user_check'));
            }

        } else {
            $check_login = $this->userRepository->checkJwtUser($request);
            if (!$check_login) {
                return $this->respondForbidden(trans('messages.auth.user_check'));
            }
        }
        $status = $request->status;
        if ($status == 1) {
                   $orders = User::selectRaw("( FLOOR(6371 * ACOS( COS( RADIANS( $check_login->lat) ) * COS( RADIANS( lat ) ) * COS( RADIANS( lng ) - RADIANS($check_login->lng) ) + SIN( RADIANS($check_login->lat) ) * SIN( RADIANS( lat ) ) )) ) distance,
        orders.id AS id,orders.type AS type,orders.order_number AS order_number,
        orders.order_date AS order_date,orders.created_at AS created_at,orders.delegate_id,orders.delivery_address_id AS delivery_address_id,orders.subtotal_fees AS subtotal_fees,orders.shipping_fees AS shipping_fees,orders.tax_fees as tax_fees,
        orders.order_status AS order_status")
                ->join('orders', 'users.id', '=', 'orders.user_id')
                ->havingRaw("distance <= 50")
                ->where('orders.order_status', 1)
                ->where('orders.cancel_order', '!=', 1)
                ->whereNotIn('orders.delegate_id', Order::where('order_status', 2)->orwhere('order_status', 3)->pluck('delegate_id'))
                ->orderBy('created_at', 'desc')
                ->get();
            $order_item = [];
            $order_list = [];
            foreach ($orders as $order) {
                $order_item['id'] = $order->id;
                $order_item['type'] = $order->type;
                $order_item['order_number'] = $order->order_number;
                $order_item['order_date'] = $order->order_date;
                $order_item['order_status'] = $order->order_status;
                $order_item['address'] = Address::where('id', $order->delivery_address_id)->pluck('location')->first();
                $order_item['cost'] = (float)( $order->tax_fees + $order->subtotal_fees + $order->shipping_fees);
                $order_list[] = $order_item;
            }
            return $this->respond(200, trans('messages.order.list'), $order_list);
        } else {
            $type = $request->type;
            if ($type == 3 || $status == 1) {
                $check_login = $this->shopRepository->checkJwtShop($request);
                if (!$check_login) {
                    return $this->respondForbidden(trans('messages.auth.user_check'));
                }
                $orders = Order::where('shop_id', $check_login->id)->orderBy('id','desc')->get();

            } else {
                $check_login = $this->userRepository->checkJwtUser($request);
                if (!$check_login) {
                    return $this->respondForbidden(trans('messages.auth.user_check'));
                }
                $orders = Order::where('delegate_id', $check_login->id)->where('cancel_order',0)->orderBy('id','desc')->get();
            }

            $order_item = [];
            $order_list = [];
            foreach ($orders as $order) {
                $order_item['id'] = $order->id;
                $order_item['type'] = $order->type;
                $order_item['order_number'] = $order->order_number;
                $order_item['order_date'] = $order->order_date;
                $order_item['order_status'] = $order->order_status;
                $order_item['address'] = Address::where('id', $order->delivery_address_id)->pluck('location')->first();
                $order_item['cost'] = (float)$order->subtotal_fees + $order->shipping_fees + $order->tax_fees;
                $order_list[] = $order_item;
            }
            return $this->respond(200, trans('messages.order.list'), $order_list);
        }
    }


}
