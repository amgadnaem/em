<?php

namespace App\Http\Controllers\API\Shops;

use App\Http\Controllers\API\APIController;
use App\Product;
use App\Service;
use App\Repositories\Product\ProductRepository;
use App\Repositories\User\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class ProductsController extends APIController
{
    protected $userRepository;
    protected $productRepository;
    protected $lang;


    public function __construct(Request $request, UserRepository $userRepository, ProductRepository $productRepository)
    {
        $this->userRepository = $userRepository;
        $this->productRepository = $productRepository;
        $this->setLang($request->header('lang'));
    }

    // add product

    function addProduct(Request $request)
    {
        $shop_owner = $this->userRepository->checkJwtShop($request);
        if (!$shop_owner) {
            return $this->respondForbidden(trans('messages.auth.user_check'));
        }
        $add_product = new Product();
        $add_product->name_en = $request->product_name;
        $add_product->name_ar = $request->product_name;
        $add_product->name_kur = $request->product_name;
        QrCode::format('png')->size(500)->generate($request->product_code, public_path('images/products/qrCode/' . $request->product_code . '.png'));
        $add_product->code = $request->product_code;
        $add_product->qr_code = $request->product_code;
        $add_product->general_price = $request->general_price;
        $add_product->special_price = $request->special_price;
        $add_product->shop_id = $shop_owner->id;
        $add_product->category_id = $request->category_id;
        $add_product->product_stock = !empty($request->product_stock) ? $request->product_stock : 1;
        $add_product->desc_en = $request->product_desc;
        $add_product->desc_ar = $request->product_desc;
        $add_product->desc_kur = $request->product_desc;

        $add_product->type = Service::where('id',$shop_owner->service_id)->pluck('type')->first();

        if ($request->type_price == 1) {
            $validator = Validator::make($request->all(), [
                'hosted_price' => 'required',
            ], [
                    'hosted_price.required' => trans('validation.hosted_price'),
                ]
            );
            if ($validator->fails()) {
                return $this->respondWithError($validator->errors()->first());
            }
            $add_product->hosted_price = $request->hosted_price;
        }
        $add_product->save();
        if ($request->product_image) {
            $add_product->product_image = $request->product_image;
            $add_product->save();
        }
        if ($add_product) {
            return response()->json([
                'status' => 200,
                'message' => trans('messages.products.add')
            ]);
        }
    }

    // edit product

    function editProduct(Request $request)
    {
        $edit_product = Product::where('id', $request->product_id)->first();
        $edit_product->name_en = $request->product_name;
        $edit_product->name_ar = $request->product_name;
        $edit_product->name_kur = $request->product_name;
        QrCode::format('png')->size(500)->generate($request->product_code, public_path('images/products/qrCode/' . $request->product_code . '.png'));
        $edit_product->code = $request->product_code;
        $edit_product->general_price = $request->general_price;
        $edit_product->special_price = $request->special_price;
        $edit_product->category_id = !empty($request->category_id) ? $request->category_id : $edit_product->category_id;
        $edit_product->product_stock = !empty($request->product_stock) ? $request->product_stock : 1;
        $edit_product->desc_en = $request->product_desc;
        $edit_product->desc_ar = $request->product_desc;
        $edit_product->desc_kur = $request->product_desc;

        $edit_product->hosted_price = $request->hosted_price;
        $edit_product->save();
        if ($request->product_image) {
            $edit_product->product_image = $request->product_image;
            $edit_product->save();
        }
        if ($edit_product) {
            return response()->json([
                'status' => 200,
                'message' => trans('messages.products.edit')
            ]);
        }

    }

    // search about product

    public function searchProducts(Request $request)
    {
        $user = $this->userRepository->checkJwtUser($request);
        $type = $request->type;
        if ($type == 1) {
            $products = Product::whereStatus(1)
                ->where('name_en', 'like', "%{$request->searach}%")
                ->orWhere('name_ar', 'like', "%{$request->searach}%")
                ->orWhere('name_kur', 'like', "%{$request->searach}%")
                ->get();
            $data = $this->productRepository->getAllProductsDetailPaginate($products, $this->lang, $user);
            return $this->respond(
                200,
                trans('messages.products.list'),
                $data
            );
        } else {
            $products = Product::whereStatus(1)
                ->where('qr_code', $request->searach)
                ->select('id')
                ->get();
            return $this->respond(
                200,
                trans('messages.products.list'),
                $products
            );
        }

    }

        // delete product
    function deleteProduct(Request $request)
    {
        $deleteProduct = Product::where('id', $request->product_id)->delete();
        if ($deleteProduct) {
            return response()->json([
                'status' => 200,
                'message' => trans('messages.products.delete')
            ]);
        }
        return $this->respondWithError(trans('messages.something_went_wrong'));

    }


}
