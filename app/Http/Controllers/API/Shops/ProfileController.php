<?php

namespace App\Http\Controllers\API\Shops;

use App\Http\Controllers\API\APIController;
use App\Repositories\Shop\ShopRepository;
use App\Repositories\User\UserRepository;
use App\Shop;
use App\ShopPhone;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class ProfileController extends APIController
{
    protected $shopRepository;
    protected $userRepository;
    protected $lang;


    public function __construct(Request $request, UserRepository $userRepository,

                                ShopRepository $shopRepository
    )
    {
        $this->shopRepository = $shopRepository;
        $this->userRepository = $userRepository;
        $this->setLang($request->header('lang'));
    }

    // check password shops
    function secret_password(Request $request)
    {
        $shop_owner = $this->shopRepository->checkJwtShop($request);
        if (!$shop_owner) {
            return $this->respondForbidden(trans('messages.auth.user_check'));
        }
        $shop_account = Shop::where('id', $shop_owner->id)->first();
        if ($request->switch == 1) {
            $shop_account->secret_password = Hash::make($request->password);
            $shop_account->vip = $request->switch;
            $shop_account->save();
            if ($shop_account) {
                return response()->json([
                    'status' => 200,
                    'message' => trans('messages.auth.secret_password')
                ]);
            }
        } else {
            $shop_account->secret_password = '';
            $shop_account->vip = '';
            $shop_account->save();
            if ($shop_account) {
                return response()->json([
                    'status' => 200,
                    'message' => trans('messages.auth.no_secret_password')
                ]);
            }
        }

    }

    //EditProfile
    public function edit(Request $request, $image = null)
    {
        $shop_owner = $this->shopRepository->checkJwtShop($request);
        if (!$shop_owner) {
            return $this->respondForbidden(trans('messages.auth.user_check'));
        }
        $user = Shop::where('id', $shop_owner->id)->first();
        $user->name_app_en = $request->name;
        $user->name_app_ar = $request->name;
        $user->name_app_kur = $request->name;
        $user->phone_number = $request->phone_number;
        $user->city_id = $request->city_id;
        $user->lat = $request->lat;
        $user->lng = $request->lng;
        $user->location = $request->address;
        $user->desc_en = $request->description;
        $user->desc_ar = $request->description;
        $user->desc_kur = $request->description;
        $user->shop_image = $request->image;
        //If shop saved successfully, then return true
        if ($image) {
            $user->shop_image = $image;
            $user->save();
        }
        if ($user->save()) {
            $data = $this->userRepository->getLoggedShopDetails($user, $this->lang);
            return $this->respond(
                200,
                trans('messages.profile.updated'),
                $data
            );
        }
        return $this->respondWithError(trans('messages.something_went_wrong'));
    }

    // add multi phone
    function add_phone(Request $request)
    {

        $shop_owner = $this->shopRepository->checkJwtShop($request);
        if (!$shop_owner) {
            return $this->respondForbidden(trans('messages.auth.user_check'));
        }
        if (empty($request->id)) {
            $check_phone = ShopPhone::where('phone', $request->phone)->first();
            if ($check_phone) {
                return $this->respondWithError(trans('messages.auth.account_exists'));
            }
            $add_shop = new ShopPhone();
            $add_shop->shop_id = $shop_owner->id;
            $add_shop->phone = $request->phone;
            $add_shop->save();
              $phone_list = ShopPhone::where('shop_id',$shop_owner->id)->select('id','phone')->orderby('id','desc')->first();
            if ($add_shop) {
                return response()->json([
                    'status' => 200,
                    'message' => trans('messages.new_phone.add_phone'),
                    'data'=>$phone_list
                ]);
            }
        } else if (!empty($request->id) && !empty($request->phone)) {
            $edit_phone = ShopPhone::where('id', $request->id)->first();
            $edit_phone->phone = $request->phone;
            $edit_phone->save();
            if ($edit_phone) {
                return response()->json([
                    'status' => 200,
                    'message' => trans('messages.new_phone.edit_phone')
                ]);
            }
        } else if (!empty($request->id)) {
            $deleteProduct = ShopPhone::where('id', $request->id)->delete();
            if ($deleteProduct) {
                return response()->json([
                    'status' => 200,
                    'message' => trans('messages.new_phone.delete_phone')
                ]);
            }
        }

    }

    // get all phone shop
    function all_phones (Request $request)
    {
        $shop_owner = $this->userRepository->checkJwtShop($request);
        if (!$shop_owner) {
            return $this->respondForbidden(trans('messages.auth.user_check'));
        }
        $shop_phones = ShopPhone::where('shop_id', $shop_owner->id)->select('id','phone')->get();
        if (!empty($shop_phones)) {
            return $this->respond('200', trans('messages.new_phone.phones_list'), $shop_phones);
        }

    }
}
