<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\PromoCode\ApplyPromoCodeRequest;
use App\Repositories\PromoCode\PromoCodeRepository;
use App\Repositories\User\UserRepository;
use App\Repositories\UserApplyPromo\UserApplyPromoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PromoCodeController extends APIController
{
    //


    protected $repository;
    protected $userRepository;
    protected $userApplyPromoRepository;

    public function __construct(Request $request, PromoCodeRepository $repository,
                                UserApplyPromoRepository $userApplyPromoRepository,
                                UserRepository $userRepository
    )
    {
        $this->setLang($request->header('lang'));
        $this->repository = $repository;
        $this->userRepository = $userRepository;
        $this->userApplyPromoRepository = $userApplyPromoRepository;

    }


    public function store1(ApplyPromoCodeRequest $request)
    {
        $check_user = $this->userRepository->checkJwtUser($request);
        if (!$check_user) {
            return $this->respondWithError(trans('messages.auth.user_check'));
        }

        $promo_code = $this->repository->getPromoCode($request->promo_code);
        if (!$promo_code) {
            return $this->respondWithError(trans('messages.promo_code.not_exists_or_expired'));
        }

        $number_of_uses = $this->userApplyPromoRepository->getNumberOfUses($check_user, $request->promo_id, $request->order_id);
        if ($number_of_uses >= $promo_code->valid_times) {
            return $this->respondWithError(trans('messages.promo_code.used_before'));
        } else {
            $data['discounted_price'] = $this->userApplyPromoRepository->create($request->only('user_id','order_id'), $promo_code);
            return $this->respond(
                200,
                trans('messages.promo_code.added'),
                $data
            );
        }
    }


     public function store(Request $request)
    {
        $check_user = $this->userRepository->checkJwtUser($request);
        if (!$check_user) {
            return $this->respondWithError(trans('messages.auth.user_check'));
        }
        $user = $check_user->id;
        $promo_code = $this->repository->getPromoCode($request->promo_code);
        $promo = $promo_code['id'];
        if (!$promo_code) {
            return $this->respondWithError(trans('messages.promo_code.not_exists_or_expired'));
        }
        $number_of_uses = $this->userApplyPromoRepository->getNumberOfUses($user, $promo);
        if ($number_of_uses >= $promo_code->valid_times) {
            return $this->respondWithError(trans('messages.promo_code.used_before'));
        } else {
            $data = $this->userApplyPromoRepository->create($user, $promo_code, $request->price);
            return $this->respond(
                200,
                trans('messages.promo_code.added'),
                $data
            );
        }
    }

}
