<?php

namespace App\Http\Controllers\API;

use App\Address;
use App\Repositories\User\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AddressController extends APIController
{
    public $repository;

    function __construct(Request $request, UserRepository $repository)
    {
        $this->repository = $repository;
        $this->setLang($request->header('lang'));
    }


    function updateAddress(Request $request)
    {
        $user_auth = $this->repository->checkJwtUser($request);
        if (!$user_auth) {
            return $this->respondForbidden(trans('messages.auth.user_check'));
        }
        $address_id = $request->address_id;
        if ($address_id) {
            $address_id = $request->address_id;
            $address = Address::whereId($address_id)->where('user_id', $user_auth->id)->first();
            $address->location = $request->location;
            $address->lat = $request->lat;
            $address->lng = $request->lng;
            if ($address->save()) {
                return response()->json([
                    'status' => 200,
                    'message' => trans('messages.order.addressOrder'),
                ]);
            }
        }
        $create_address = Address::create([

            'user_id' => $user_auth->id,
            'location' => $request->location,
            'lat' => $request->lat,
            'lng' => $request->lng,
        ]);

        if ($create_address) {

            return response()->json([
                'status' => 200,
                'message' => trans('messages.order.addressOrder'),
                'address_id' => $create_address->id
            ]);
        }

    }
}
