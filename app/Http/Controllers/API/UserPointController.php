<?php

namespace App\Http\Controllers\API;
use App\Notification;

use App\PackingCart;
use App\PackingNumbers;
use App\Repositories\User\UserRepository;
use App\Repositories\Shop\ShopRepository;
use App\SellerPoints;
use App\User;
use App\Shop;
use App\UserPoints;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class UserPointController extends APIController
{
    public $repository;
    protected $shopRepository;
    protected $lang;

    public function __construct(Request $request, UserRepository $repository, ShopRepository $shopRepository)
    {
        $this->shopRepository = $shopRepository;
        $this->repository = $repository;
        $this->setLang($request->header('lang'));
    }
// get all cart
    function index()
    {
        $all_cards = PackingCart::where('status', 1)->select('id','card_name_' . $this->lang . ' as card_name', 'desc_' . $this->lang . ' as desc', 'points')->get();
        if ($all_cards) {
            return $this->respond(
                200,
                trans('messages.Packing.list'),
                $all_cards
            );
        }
    }

    // get seller points card

    function SellerPoints(\App\Http\Requests\Seller\SellerPoints $request)
    {
        $lat = $request->lat;
        $lng = $request->lng;
        $points = SellerPoints::selectRaw("( FLOOR(6371 * ACOS( COS( RADIANS( $lat) ) * COS( RADIANS( lat ) ) * COS( RADIANS( lng ) - RADIANS($lng) ) + SIN( RADIANS($lat) ) * SIN( RADIANS( lat ) ) )) ) distance,
                        id,seller_name_$this->lang as seller_name,lat,lng,location")
            ->havingRaw("distance <= 50")
            ->where("status", 1)
            ->orderBy('distance', 'asc')
            ->get();
        if (count($points)) {
            return response()->json([
                'status' => 200,
                'message' => trans('messages.Packing.sellerPoints'),
                'data' => [
                    'AllPoints' => $points,
                ],

            ]);
        }
        return $this->respondWithError(trans('messages.Packing.noSeller'));

    }



    function storeCard(Request $request)
    {
        for ($i = 1; $i <= 5; $i++) {
            $card_number[$i] = Str::random(16);
            $add_card = new PackingNumbers();
            $add_card->card_id = 1;
            $add_card->card_number = $card_number[$i];
            QrCode::size(500)->format('png')->generate($card_number[$i], public_path('images/qrCode/' . $card_number[$i] . '.png'));
            $add_card->card_image = $card_number[$i] . '.png';
            $add_card->save();
        }
        if ($add_card) {
            return $this->respond(
                200,
                'تم اضافه كروت التعبئه بنجاح',
                $add_card
            );
        }
    }

// add packing card
    function addUserCard(Request $request)
    {
        $type = $request->type;
        if ($type == 1 || $type == 2) {
            $check_login = $this->repository->checkJwtUser($request);
            if (!$check_login) {
                return $this->respondForbidden(trans('messages.auth.user_check'));
            }
        } elseif ($type == 3) {
            $check_login = $this->shopRepository->checkJwtShop($request);
            if (!$check_login) {
                return $this->respondForbidden(trans('messages.auth.user_check'));
            }
        }
        $check_card = PackingNumbers::where('card_number', $request->card_number)->exists();
        if ($check_card) {
            $add_card = PackingNumbers::where('card_number', $request->card_number)->first();
            if ($add_card['status'] == 1) {
                if ($add_card) {
                    $add_points = UserPoints::where('user_id', $check_login->id)->where('type', $type)->first();
                    if($add_points) {
                        $points = PackingCart::where('id', $add_card['card_id'])->pluck('points')->first();
                        $add_points->user_id = $check_login->id;
                        $add_points->type = $type;
                        $add_points->value = $add_points['value'] + $points;
                        $add_points->save();
                    } else {
                        $add_points = new UserPoints();
                        $add_points->user_id = $check_login->id;
                        $points = PackingCart::where('id', $add_card['card_id'])->pluck('points')->first();
                        $add_points->value = $points;
                        $add_points->type = $request->type;
                        $add_points->save();
                    }
                    $change_status_card = $this->changeStatusCode($request->card_number);
                   return response()->json([
                        'status'=>200,
                        'message'=>trans('messages.Packing.add'),
                        'points'=> (string)$add_points->value
                    ]);
                } else {
                    return $this->respondWithError(trans('messages.Packing.check'));
                }
            }
            return $this->respondWithError(trans('messages.Packing.check_code'));
        }
        return $this->respondWithError(trans('messages.Packing.validate'));
    }

    function changeStatusCode($card_number)
    {
        $update_card = PackingNumbers::where('card_number', $card_number)->first();
        $update_card->status = 0;
        $update_card->save();
        return $update_card;
    }
    
     function sendMoney(Request $request)
    {
        $to_type = $request->to_type;
        $from_type = $request->from_type;
        if ($to_type == 1 || $to_type == 2) {
            $check_phone = User::where('phone_number', $request->phone_number)->pluck('id')->first();
        }
        if ($to_type == 3) {
            $check_phone = Shop::where('phone_number', $request->phone_number)->pluck('id')->first();
        }
        if ($from_type == 1 || $from_type == 2) {
            $check_login = $this->repository->checkJwtUser($request);
            if (!$check_login) {
                return $this->respondForbidden(trans('messages.auth.user_check'));
            }
        }
        if ($from_type == 3) {
            $check_login = $this->shopRepository->checkJwtShop($request);
            if (!$check_login) {
                return $this->respondForbidden(trans('messages.auth.user_check'));
            }
        }
        if ($check_phone) {
            $userPoints = UserPoints::where('user_id', $check_login->id)->where('type', $request->from_type)->first();
            if ($request->points <= $userPoints['value']) {
                $userPoints->user_id = $check_login->id;
                $userPoints->type = $request->from_type;
                $userPoints->value = $userPoints['value'] - $request->points;
                $userPoints->save();
                if ($userPoints) {
                    $receive = UserPoints::where('user_id', $check_phone)->where('type', $request->to_type)->first();
                    if ($receive) {
                        $receive->user_id = $check_phone;
                        $receive->type = $request->to_type;
                        $receive->value = $receive['value'] + $request->points;
                        $receive->save();
                    $this->send_points($from_type, $to_type, $receive->value, $request->phone_number, $check_login->id, $request->points, $this->lang);

                            return response()->json([
                            'status' => 200,
                            'message' => trans('messages.Packing.send'),
                            'points' => (string)$userPoints->value
                        ]);
                    } else {
                        $add_userPoints = new UserPoints();
                        $add_userPoints->user_id = $check_phone;
                        $add_userPoints->value = $request->points;
                        $add_userPoints->type = $request->to_type;
                        $add_userPoints->save();
                    $this->send_points($from_type, $to_type, $add_userPoints->value, $request->phone_number, $check_login->id, $request->points, $this->lang);

                        return response()->json([
                            'status' => 200,
                            'message' => trans('messages.Packing.send')
                         
                        ]);
                    }
                } else {
                    return $this->respondWithError(trans('messages.Packing.check'));
                }
            } else {
                   return $this->respondWithError(trans('messages.Packing.check_points'));  
            }
       
        }
        return $this->respondWithError(trans('messages.auth.phone_not_exist'));
    }
    
    
     function send_points($sender, $receive, $total_points, $phone_number,  $sender_id,$points, $lang = "en")
    {
        if ($receive == 1 || $receive == 2) {
            $token = User::where('phone_number', $phone_number)->pluck('firebase_token')->first();
            $id = User::where('phone_number', $phone_number)->pluck('id')->first();
        }
        if ($receive == 3) {
            $token = Shop::where('phone_number', $phone_number)->pluck('firebase_token')->first();
            $id = Shop::where('phone_number', $phone_number)->pluck('id')->first();
        }

        define('API_ACCESS_KEY', 'AAAAJIhBdDE:APA91bHLPrry6EpPfm8cSqvb0Vg5d3pa22K5i_e3uPTKev8JR_7SsVjGktN-paxPXo7k26OYB4oJbWZN6RxkOrQ2KCLvLH_ifqUyfRJow7EG4doyamUVXVlGK8aRnaKPVqIib3md5MoU');
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        if ($lang == 'ar') {
            $notification = [
                'title' => ' تحويل نقاط ',
               
                'body' => $points . 'تم تحويل نقاط الى رصيدك بنجاح وعددها',

                'points' => $total_points,
                'type'=>7
            ];
        } elseif ($lang == 'kur') {
            $notification = [
                'title' => ' تحويل نقاط ',
                'body' => $points . 'تم تحويل نقاط الى رصيدك بنجاح وعددها',

                'points' => $total_points,
                'type'=>7
            ];
        } else {
            $notification = [
                'title' => 'Points Transfer',
                'body' => 'convert ' . $points . ' points successfully to Your Points',
                'points' => $total_points,
                'type'=>7
            ];
        }

        $extraNotificationData = ["message" => $notification, "moredata" => 'dd'];

        $fcmNotification = [
            'to' => $token, //single token
            'data' => $extraNotificationData
        ];

        $notification = new Notification();
        $notification->from = $sender_id;
        $notification->to = $id;
        $notification->title_en = 'Points Transfer';
        $notification->message_en = 'convert ' . $points . ' points successfully to Your Points';
        $notification->title_ar = ' تحويل نقاط';
        $notification->message_ar = $points . 'تم نحويل نقاط الى رصيدك بنجاح وعددها';
        $notification->title_kur = 'Points Transfer';
        $notification->message_kur = $points . 'تم نحويل نقاط الى رصيدك بنجاح وعددها';
        $notification->type = $receive;
        $notification->flag = $id;
        $notification->save();
        $headers = [
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);

        if ($result) {
            return true;
        } else {
            return false;
        }

    }



  function receiveMoney(Request $request)
    {
        $to_type = $request->to_type;
        $from_type = $request->from_type;
        if ($to_type == 1 || $to_type == 2) {
            $check_phone = User::where('phone_number', $request->phone_number)->pluck('id')->first();
            $check_points = UserPoints::where('user_id', $check_phone)->where('type', 1)->orwhere('type', 2)->pluck('value')->first();
            if ($check_points < $request->points) {
                return $this->respondWithError(trans('messages.Packing.borrow_points'));
            }
        }
        if ($to_type == 3) {
            $check_phone = Shop::where('phone_number', $request->phone_number)->pluck('id')->first();
            $check_points = UserPoints::where('user_id', $check_phone)->where('type', 3)->pluck('value')->first();
            if ($check_points < $request->points) {
                return $this->respondWithError(trans('messages.Packing.borrow_points'));
            }
        }
        if ($from_type == 1 || $from_type == 2) {
            $check_login = $this->repository->checkJwtUser($request);
            if (!$check_login) {
                return $this->respondForbidden(trans('messages.auth.user_check'));
            }
        }
        if ($from_type == 3) {
            $check_login = $this->shopRepository->checkJwtShop($request);
            if (!$check_login) {
                return $this->respondForbidden(trans('messages.auth.user_check'));
            }
        }
        if ($check_phone) {
            $userPoints = UserPoints::where('user_id', $check_phone)->first();
            if ($request->points <= $userPoints['value']) {
                if ($userPoints) {
                    $add_userPoints = new UserPoints();
                    $add_userPoints->user_id = $check_login->id;
                    $add_userPoints->sender = $request->from_type;
                    $add_userPoints->receiver = $check_phone;
                    $add_userPoints->type = $request->to_type;
                    $add_userPoints->value = $request->points;
                    $add_userPoints->status = 0;
                    $add_userPoints->save();
                         $this->notify_receive($from_type, $to_type, $request->phone_number, $check_login->id, $request->points,$this->lang);
                    return response()->json([
                        'status' => 200,
                        'message' => trans('messages.Packing.waiting')
                    ]);
                } else {
                    return $this->respondWithError(trans('messages.Packing.check'));
                }
            }
            return $this->respondWithError(trans('messages.Packing.borrow_points'));
        } else {
            return $this->respondWithError(trans('messages.auth.phone_not_exist'));
 
        }
    }

    function notify_receive($from_type, $to_type, $phone_number, $check_login,$points,$lang = "en")
    {
           define('API_ACCESS_KEY', 'AAAAJIhBdDE:APA91bHLPrry6EpPfm8cSqvb0Vg5d3pa22K5i_e3uPTKev8JR_7SsVjGktN-paxPXo7k26OYB4oJbWZN6RxkOrQ2KCLvLH_ifqUyfRJow7EG4doyamUVXVlGK8aRnaKPVqIib3md5MoU');
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        
        if ($from_type == 1 || $from_type == 2) {
            $name = User::where('id', $check_login)->pluck('first_name')->first();
        } if ($from_type == 3) {
            $name = Shop::where('id', $check_login)->pluck('name_app_ar')->first();
        } if ($to_type == 1 || $to_type == 2) {
            $token = User::where('phone_number', $phone_number)->pluck('firebase_token')->first();
            $id = User::where('phone_number', $phone_number)->pluck('id')->first();
        }  if ($to_type == 3) {
            $token = Shop::where('phone_number', $phone_number)->pluck('firebase_token')->first();
            $id = Shop::where('phone_number', $phone_number)->pluck('id')->first();
        }

     
        if($lang == 'ar') {
            $notification = [
                'title' => 'استلاف رصيد',
                    'name'=>$name,
                'points'=>$points,
                 'body' => ' '.$name. ' يريد '.$points,
                'icon' => 'myIcon',
                 'type'=>5
            ];
        } if ($lang == 'kur') {
            $notification = [
                'title' => 'استلاف رصيد',
                    'name'=>$name,
                'points'=>$points,
                'body' => ' '.$name. ' يريد '.$points,
                'icon' => 'myIcon',
                  'type'=>5
            ];
        } if ($lang == 'en') {
            $notification = [
                'title' => 'Points Borrow',
                    'name'=>$name,
                'points'=>$points,
                'body' => $name.' wants'. $points.' points .',
                 'type'=>5
            ];
        }
        
         $extraNotificationData = ["message" => $notification,"moredata" =>'dd'];

       

        $fcmNotification = [
            'to' => $token, //single token
            'data' => $extraNotificationData
        ];

        $notification = new Notification();
        $notification->from = $check_login;
        $notification->to = $id;
        $notification->title_en = 'Points Borrow';
        $notification->message_en = $name.' need a points from you, He wants '. $points.' points .';
        $notification->title_ar = 'استلاف رصيد';
        $notification->message_ar = ' '.$name. ' يحتاج نقاط منك هو يريد '.$points;
        $notification->title_kur = 'Points Borrow';
        $notification->message_kur = $name.' need a points from you, He wants '. $points.' points .';
        $notification->type = $to_type;
        $notification->flag = $id;
        $notification->status = 5;
        $notification->save();
        $headers = [
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);
          if ($result) {
                return true;
            } else {
                return false;
            }

    }

   function acceptBorrowPoints(Request $request)
    {
        $type = $request->type;
        if ($type == 1 || $type == 2) {
            $check_login = $this->repository->checkJwtUser($request);
            if (!$check_login) {
                return $this->respondForbidden(trans('messages.auth.user_check'));
            }
        } elseif ($type == 3) {
            $check_login = $this->shopRepository->checkJwtShop($request);
            if (!$check_login) {
                return $this->respondForbidden(trans('messages.auth.user_check'));
            }
        }
        $status = $request->status;
        if ($status == 1) {
            $borrowPoints = UserPoints::where('type', $type)->where('receiver', $check_login->id)->where('status', 0)->first();
//            $borrowPoints->status = 1;
            // $borrowPoints->save();
            if ($borrowPoints) {
                $userPoints = UserPoints::where('user_id', $check_login->id)->where('type', $type)->first();
                $userPoints->user_id = $check_login->id;
                $userPoints->value = $userPoints['value'] - $borrowPoints['value'];
                $userPoints->save();
                if ($userPoints) {
                    $add_userPoints = UserPoints::where('user_id', $borrowPoints['user_id'])->where('type', $borrowPoints['sender'])->where('status', 1)->first();
                    if ($add_userPoints) {
                        $add_userPoints->value += $borrowPoints['value'];
                        $add_userPoints->save();
                    $this->accept_points($borrowPoints->sender, $borrowPoints->receiver, $borrowPoints->user_id, $borrowPoints->type, $add_userPoints->value, $this->lang);

                    } else {
                        $add_userPoints1 = new UserPoints();
                        $add_userPoints1->user_id = $borrowPoints['user_id'];
                        $add_userPoints1->value = $borrowPoints['value'];
                        $add_userPoints1->type = $borrowPoints['sender'];
                        $add_userPoints1->save();
                        $this->accept_points($borrowPoints->sender, $borrowPoints->receiver, $borrowPoints->user_id, $borrowPoints->type, $add_userPoints1->value, $this->lang);

                    }
                    if ( ($add_userPoints) || ($add_userPoints1) ) {
                        $delete_process_borrow = UserPoints::where('id', $borrowPoints['id'])->first();
                        $delete_process_borrow->delete();
                    }
                
                         $notify = Notification::where('id', $request->id)->first();
                    $notify->status = 6;
                    $notify->save();

                }
                return response()->json([
                        'status' => 200,
                        'message' => trans('messages.Packing.accept'),
                           'points'=>$userPoints->value
                    ]);
            }
        } else {
            $rejectBorrowPoints = UserPoints::where('type', $type)->where('receiver', $check_login->id)->where('status', 0)->first();
            $rejectBorrowPoints->delete();
            $notify = Notification::where('id', $request->id)->first();
            $notify->status = 6;
            $notify->save();
            if ($rejectBorrowPoints) {
                return response()->json([
                    'status' => 200,
                    'message' => trans('messages.Packing.reject')
                ]);
            }
        }
    }

    function accept_points($sender, $receive, $user, $type, $total_points, $lang = "en")
    {
          define('API_ACCESS_KEY', 'AAAAJIhBdDE:APA91bHLPrry6EpPfm8cSqvb0Vg5d3pa22K5i_e3uPTKev8JR_7SsVjGktN-paxPXo7k26OYB4oJbWZN6RxkOrQ2KCLvLH_ifqUyfRJow7EG4doyamUVXVlGK8aRnaKPVqIib3md5MoU');
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        
        if ($sender == 1 || $sender == 2) {
            $token = User::where('id', $user)->pluck('firebase_token')->first();
            $id = User::where('id', $user)->pluck('id')->first();
        }
        if ($sender == 3) {
            $token = Shop::where('id', $user)->pluck('firebase_token')->first();
            $id = Shop::where('id', $user)->pluck('id')->first();
        }


        if ($lang == 'ar') {
            $notification = [
                'title' => 'قبول طلبك لتحويل نقاط ',
                'body' => 'تم قبول ونحويل النقاط التى ارسلتها بنجاح',
                 'points'=>$total_points,
                'type'=>7
            ];
        } elseif ($lang == 'kur') {
            $notification = [
                'title' => 'قبول طلبك لتحويل نقاط ',
                'body' => 'تم قبول ونحويل النقاط التى ارسلتها بنجاح',
                 'points'=>$total_points,
                'type'=>7
            ];
        } else {
            $notification = [
                'title' => 'Accept request',
                'body' => 'Accept request and convert points successfully',
                 'points'=>$total_points,
                'type'=>7
            ];
        }

        $extraNotificationData = ["message" => $notification, "moredata" => 'dd'];

        $fcmNotification = [
            'to' => $token, //single token
            'data' => $extraNotificationData
        ];

        $notification = new Notification();
        $notification->from = $receive;
        $notification->to = $id;
        $notification->title_en = 'Accept request';
        $notification->message_en = 'Accept request and convert points successfully';
        $notification->title_ar = ' قبول طلبك لتحويل نقاط';
        $notification->message_ar = 'تم قبول ونحويل النقاط التى ارسلتها بنجاح';
        $notification->title_kur = 'Accept request';
        $notification->message_kur = 'Accept request and convert points successfully';
        $notification->type = $sender;
        $notification->flag = $id;
        $notification->save();
        $headers = [
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);

           if ($result) {
                return true;
            } else {
                return false;
            }

    }
}
