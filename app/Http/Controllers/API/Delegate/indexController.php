<?php

namespace App\Http\Controllers\API\Delegate;

use App\Address;
use App\Http\Controllers\API\APIController;
use App\Notification;
use App\Order;
use App\OrdersProducts;
use App\OrderStatus;
use App\PackingCart;
use App\Product;
use App\ProductColorsSize;
use App\Repositories\Shop\ShopRepository;
use App\Repositories\User\UserRepository;
use App\Shop;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class indexController extends APIController
{
    protected $shopRepository;
    protected $userRepository;
    protected $lang;


    public function __construct(Request $request,
                                UserRepository $userRepository,
                                ShopRepository $shopRepository
    )
    {
        $this->shopRepository = $shopRepository;
        $this->userRepository = $userRepository;
        $this->setLang($request->header('lang'));
    }

    public function ConfirmOrderFromDelegate(Request $request)
    {
        $check_delegate = $this->userRepository->checkJwtUser($request);
        if (!$check_delegate) {
            return $this->respondForbidden(trans('messages.auth.user_check'));
        }
        $order_id = $request->order_id;
        $type = $request->type;
        if ($type == 4) {
            if ($request->status == 2) {
                $order = Order::where('id', $order_id)->first();
                $order->order_status = 2;
                $type1 = "4";
                $order->delegate_id = $check_delegate->id;
                $this->confirmOrder($order->user_id, $check_delegate->id, $type1, $request->status);
                if ($order->save()) {
                    return response()->json([
                        'status' => 200,
                        'message' => trans('messages.order.confirm')
                    ]);
                }
            } else if ($request->status == 3) {
                $order = Order::where('id', $order_id)->first();
                $order->order_status = $request->status;
                $type1 = "4";
                $order->delegate_id = $check_delegate->id;
                $this->confirmOrder($order->user_id, $check_delegate->id, $type1, $request->status);
                if ($order->save()) {
                    return response()->json([
                        'status' => 200,
                        'message' => trans('messages.order.confirm')
                    ]);
                }
            } else if ($request->status == 4) {
                $order = Order::where('id', $order_id)->first();
                $order->order_status = $request->status;
                $type1 = "4";
                $order->delegate_id = $check_delegate->id;
                $this->confirmOrder($order->user_id, $check_delegate->id, $type1, $request->status);
                
                   $order_items = [];
                $order_list = [];
                $order_products = OrdersProducts::where('order_id', $order_id)->get();
                foreach ($order_products as $orders) {
                    $order_items['product_id'] = $orders->product_id;
                    $check_product = Product::where('id', $orders->product_id)->pluck('type')->first();
                    if ($check_product == 1) {
                        $edit_stock = ProductColorsSize::where('product_id', $orders->product_id)->where('size_id', $orders->size_id)->where('color_id', $orders->color_id)->first();
                        $edit_stock->stock -= $orders->order_qty;
                        $edit_stock->save();
                    } else {
                        $edit_stock = Product::where('id', $orders->product_id)->first();
                        $edit_stock->product_stock -= $orders->order_qty;
                        $edit_stock->save();
                    }
                }
                
                if ($order->save()) {
                    return response()->json([
                        'status' => 200,
                        'message' => trans('messages.order.confirm')
                    ]);
                }
            } else {
                $order = Order::where('id', $order_id)->first();
                $order->delegate_id = $check_delegate->id;
                $order->cancel_order = 1;
                if ($order->save()) {
                    return response()->json([
                        'status' => 200,
                        'message' => trans('messages.order.cancel')
                    ]);
                }
            }
        } elseif ($type == 1) {
            if ($request->status == 2) {
                $order = Order::where('id', $order_id)->first();
                $order->order_status = 2;
                $type1 = "1";
                $order->delegate_id = $check_delegate->id;
                $this->confirmOrder($order->user_id, $check_delegate->id, $type1, $request->status);
                if ($order->save()) {
                    return response()->json([
                        'status' => 200,
                        'message' => trans('messages.order.confirm')
                    ]);
                }
            } else if ($request->status == 3) {
                $order = Order::where('id', $order_id)->first();
                $order->order_status = $request->status;
                $type1 = "1";
                $order->delegate_id = $check_delegate->id;
                $this->confirmOrder($order->user_id, $check_delegate->id, $type1, $request->status);
                if ($order->save()) {
                    return response()->json([
                        'status' => 200,
                        'message' => trans('messages.order.confirm')
                    ]);
                }
            } else if ($request->status == 4) {
                $order = Order::where('id', $order_id)->first();
                $order->order_status = $request->status;
                $type1 = "1";
                $order->delegate_id = $check_delegate->id;
                $this->confirmOrder($order->user_id, $check_delegate->id, $type1, $request->status);
                if ($order->save()) {
                    return response()->json([
                        'status' => 200,
                        'message' => trans('messages.order.confirm')
                    ]);
                }
            } else {
                $order = Order::where('id', $order_id)->first();
                $order->delegate_id = $check_delegate->id;
                $order->cancel_order = 1;
                if ($order->save()) {
                    return response()->json([
                        'status' => 200,
                        'message' => trans('messages.order.cancel')
                    ]);
                }
            }

        }
    }

    public function confirmOrder($user_id, $delegate, $type, $status)
    {
        $token = User::where('id', $user_id)->pluck('firebase_token')->first();
        define('API_ACCESS_KEY', 'AAAAJIhBdDE:APA91bHLPrry6EpPfm8cSqvb0Vg5d3pa22K5i_e3uPTKev8JR_7SsVjGktN-paxPXo7k26OYB4oJbWZN6RxkOrQ2KCLvLH_ifqUyfRJow7EG4doyamUVXVlGK8aRnaKPVqIib3md5MoU');
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        if ($status == 2) {
            $notification = [
           
             'title' => ' تم قبول طلبك من قبل المندوب',
                'body' => 'المندوب استلم طلبك',
                'type' => $type
            ];
        } elseif ($status == 3) {

            $notification = [
                'title' => ' طلبك بالطريق',
                'body' => 'طلبك بالطريق',
                'type' => $type
            ];
        } elseif ($status == 4) {
            $notification = [
                'title' => 'تم تسليم الطلب الى العميل',
                'body' => 'تم تسليم الطلب الى العميل',
                'type' => $type
            ];
        }

        $extraNotificationData = ["message" => $notification, "moredata" => 'dd'];

        $fcmNotification = [

            'to' => $token, //single token
            'data' => $extraNotificationData
        ];

        $notification = new Notification();
        $notification->from = $delegate;
        $notification->to = $user_id;
        $notification->title_en = 'Order Accepted';
        $notification->message_en = 'Delegate Delivered Order';
        $notification->title_ar = 'قبول طلبك';
        $notification->message_ar = 'المندوب استلم طلبك';
        $notification->title_kur = 'قبول طلبك';
        $notification->message_kur = 'المندوب استلم طلبك';
        $notification->type = 2;
        $notification->flag = 1;
        $notification->save();
        $headers = [
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;

    }

    public function OrderDetails(Request $request)
    {
        $check_user = $this->userRepository->checkJwtUser($request);
        if (!$check_user) {
            return $this->respondForbidden(trans('messages.auth.user_check'));
        }
        $type = $request->type;
        $order_item = [];
        $order_list = [];
        if ($type == 1) {
            $order_id = $request->order_id;
            $order = Order::where('id', $order_id)->first();
            if ($order->type == 3) {
                $shop = Shop::where('id', $order['shop_id'])->first();
                $order_item['id'] = $shop->id;
                $order_item['shop_number'] = $shop->shop_number;
                $order_item['user_image'] = $shop->shop_image;
                if ($this->lang == 'ar') {
                    $order_item['name'] = $shop->name_app_ar;
                } elseif ($this->lang == 'kur') {
                    $order_item['name'] = $shop->name_app_kur;
                } else {
                    $order_item['name'] = $shop->name_app_en;
                }

            } else {
                $user = User::where('id', $order['user_id'])->first();
                $order_item['id'] = $user->id;
                $order_item['phone_number'] = $user->phone_number;
                $order_item['user_image'] = $user->user_image;
                $order_item['name'] = $user->first_name . $user->last_name;

            }
            $order_item['card'] = PackingCart::where('id', $order['packing_id'])->select('id', 'card_name_' . $this->lang . ' as card_name', 'desc_' . $this->lang . ' as desc')->first();
            $order_item['address'] = Address::where('id', $order['delivery_address_id'])->select('id', 'lat', 'lng', 'location')->first();
            $order_item['status'] = $order['order_status'];
            $order_item['status'] = OrderStatus::where('id', $order['order_status'])->select('id', 'status_name_' . $this->lang . ' as status')->first();
            $order_item['order_number'] = $order['order_number'];
            $order_item['subtotal_fees'] = (string)$order['subtotal_fees'];
            $order_item['tax_fees'] = (string)$order['tax_fees'];
            $order_item['shipping_fees'] = (string)$order['shipping_fees'];
        }

        return $this->respond(
            200,
            trans('messages.order.info'),
            $order_item
        );
    }
}
