<?php

namespace App\Http\Controllers\API;
use App\Notification;
use App\NotificationSetting;
use App\Repositories\Notify\NotifyRepository;
use App\Repositories\User\UserRepository;
use App\Repositories\Shop\ShopRepository;
use App\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationController extends APIController
{
  
    
      public $repository;
    protected $shopRepository;
 

    function __construct(Request $request,
                         UserRepository $repository,
                         ShopRepository $shopRepository
                        
    )
    {
        $this->repository = $repository;
        $this->shopRepository = $shopRepository;
     
        $this->setLang($request->header('lang'));
    }

    // get setting notification
    public function settings(Request $request)
    {
        $check_user = $this->repository->checkJwtUser($request);
        if (!$check_user) {
            return $this->respondForbidden(trans('messages.auth.user_check'));
        }
        $notify_item = [];
        $notify_list = [];
        $notifySettings = Service::where('status', 1)->get();
        foreach ($notifySettings as $setting) {
            $notify_item['id'] = $setting->id;
             if($this->lang == 'ar') {
                $notify_item['name'] = $setting->service_name_ar; 
            } elseif ($this->lang == 'kur') {
                $notify_item['name'] = $setting->service_name_kur;
            } else {
                $notify_item['name'] = $setting->service_name_en;
            }
          
            $notify_item['image'] = $setting->service_image;
            $user_setting = NotificationSetting::where('user_id',$check_user->id)->where('service_id',$setting->id)->pluck('service_id')->first();
            if($user_setting == $setting->id ) {
                $notify_item['switch'] = false;
            } else {
                $notify_item['switch'] = true;

            }
            $notify_list[] = $notify_item;
        }

        return $this->respond(
            200,
            trans('messages.notification.show'),
            $notify_list
        );

    }

    function statusNotification(Request $request)
    {
        $check_user = $this->repository->checkJwtUser($request);
        if (!$check_user) {
            return $this->respondForbidden(trans('messages.auth.user_check'));

        }
        $message = '';
        $isNotify = 0;
        $favorite = NotificationSetting::where('user_id', $check_user->id)->where('service_id', $request->service_id)->pluck('id')->first();
        if (!$favorite) {
            if (NotificationSetting::create([
                    'user_id' => $check_user->id,
                    'service_id' => $request->service_id
                ]
            )) {
                $isNotify = 0;
                $message = trans('messages.notification.removed');

                
            }
        } else {
            if (NotificationSetting::destroy($favorite)) {
                $isNotify = 1;
                $message = trans('messages.notification.added');
            }
        }
        return response()->json([
            'status' => 200,
            'isNotify' => $isNotify,
            'message' => $message
        ]);
    }
    
     // get notification

    function index(Request $request)
    {

        $type = $request->type;
        if ($type == 1 || $type == 2) {
            $auth_login = $this->repository->checkJwtUser($request);
            if (!$auth_login) {
                return $this->respondForbidden(trans('messages.auth.user_check'));
            }
            $notification = Notification::where('to', $auth_login->id)->where('flag',$auth_login->id)->select('id','title_'.$this->lang. ' as title','message_'.$this->lang. ' as message', 'created_at','status')->orderBy('id','desc')->get();
            if ($notification) {
                return $this->respond(
                    200,
                    trans('messages.notification.show'),
                    $notification
                );
            } else {
                $message = trans('messages.notification.nodata');
                return $this->respondWithMessage($message);
            }
        } else {
             $auth_login = $this->shopRepository->checkJwtShop($request);
            if (!$auth_login) {
                return $this->respondForbidden(trans('messages.auth.user_check'));
            }
            $notification = Notification::where('to', $auth_login->id)->where('flag', $type)->select('id','type','from as shop_id','title_'.$this->lang. ' as title','message_'.$this->lang. ' as message', 'created_at','status')->orderBy('id','desc')->get();
            if ($notification) {
                return $this->respond(
                    200,
                    trans('messages.notification.show'),
                    $notification
                );
            } else {
                $message = trans('messages.notification.nodata');
                return $this->respondWithMessage($message);
            } 
        }
    }

}
