<?php

namespace App\Http\Controllers\API;

use App\Product;
use App\Repositories\Product\ProductRepository;
use App\Repositories\Shop\ShopRepository;
use App\Repositories\User\UserRepository;
use App\Shop;
use Illuminate\Http\Request;

class SearchController extends APIController
{
    public $repository;
    protected $shopRepository;
    protected $productRepository;

    function __construct(Request $request,
                         UserRepository $repository,
                         ShopRepository $shopRepository,
                         ProductRepository $productRepository
    )
    {
        $this->repository = $repository;
        $this->shopRepository = $shopRepository;
        $this->productRepository = $productRepository;
        $this->setLang($request->header('lang'));
    }

    // search about product from menu
    public function search_product(Request $request)
    {
        $user = $this->repository->checkJwtUser($request);
        if (!$user) {
            return $this->respondForbidden(trans('messages.auth.user_check'));
        }

        $status = $request->status;
        $service_id = $request->service_id;
        if ($status == 1) {
            $country_id = $user->country_id;
        } else {
            $country_id = $request->country_id;
        }
        $type = $request->type;
        if ($type == 1) {
            if ($status == 1 || $status == 2) {
                $shop_country = Shop::where("Subscribe_end", '>', Now())
                    ->where('status', 1)
                    ->where('shop_status', 1)
                    ->where('country_id', $country_id)
                    ->pluck('id');
            } else {
                $shop_country = Shop::where("Subscribe_end", '>', Now())
                    ->where('status', 1)
                    ->where('shop_status', 1)
                    ->pluck('id');
            }
            if (preg_match('/\p{Arabic}/u', $request->search)) {
                $products = Product::whereIn('shop_id', $shop_country)->whereStatus(1)
                    ->Where('name_ar', 'like', "%{$request->search}%")
                    ->get();
            } else if (!preg_match('/[^A-Za-z0-9]+/', $request->search)) {
                $products = Product::whereIn('shop_id', $shop_country)->whereStatus(1)
                    ->Where('name_en', 'like', "%{$request->search}%")
                    ->get();
            } else {
                $products = Product::whereIn('shop_id', $shop_country)->whereStatus(1)
                    ->Where('name_kur', 'like', "%{$request->search}%")
                    ->get();
            }



            $all_products = $this->productRepository->getAllProductsDetailPaginate($products, $this->lang, $user);
            return $this->respond(
                200,
                trans('messages.products.list'),
                $all_products
            );
        } else {
            if ($status == 1 || $status == 2) {
                $shops = Shop::where("Subscribe_end", '>', Now())
                    ->where('status', 1)
                    ->where('shop_status', 1)
                    ->where('service_id', $service_id)
                    ->where('country_id', $country_id)
                    ->get();
            } else {
                $shops = Shop::where("Subscribe_end", '>', Now())
                    ->where('status', 1)
                    ->where('shop_status', 1)
                    ->where('service_id', $service_id)
                    ->get();
            }
            $all_shops = $this->shopRepository->getShopDetails($shops, $type, $this->lang, $user);
            return $this->respond(
                200,
                trans('messages.shops.list'),
                $all_shops
            );
        }

    }

}
