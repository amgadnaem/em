<?php

namespace App\Http\Controllers\API;

use App\Notification;
use App\Order;
use App\PackingCart;
use App\Repositories\Shop\ShopRepository;
use App\Repositories\User\UserRepository;
use App\Setting;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CardOrderController extends APIController
{
    public $repository;
    protected $shopRepository;
    protected $lang;

    public function __construct(Request $request, UserRepository $repository, ShopRepository $shopRepository)
    {
        $this->shopRepository = $shopRepository;
        $this->repository = $repository;
        $this->setLang($request->header('lang'));
    }

    // make packing card order

    public function packing_card_order(Request $request)
    {
        $type = $request->type;
        if ($type == 1 || $type == 2) {
            $check_login = $this->repository->checkJwtUser($request);
            if (!$check_login) {
                return $this->respondForbidden(trans('messages.auth.user_check'));
            }
        } elseif ($type == 3) {
            $check_login = $this->shopRepository->checkJwtShop($request);
            if (!$check_login) {
                return $this->respondForbidden(trans('messages.auth.user_check'));
            }
        }
        $order_number = rand(100000, 999999);
        $order = new Order();
        if ($type == 1 || $type == 2) {
            $order->user_id = $check_login->id;
            $order->type = $request->type;
        } else {
            $order->shop_id = $check_login->id;
            $order->type = $request->type;
        }
        $order->order_number = $order_number;
        $order->packing_id = $request->packing_id;
        $order->delivery_method = $request->delivery_method;
        $order->payment_method = $request->payment_method;
        $order->delivery_address_id = $request->delivery_address_id;
        $order->shipping_fees = Setting::where('key', 'shipping')->pluck('value')->first();
        $order->tax_fees = Setting::where('key', 'tax')->pluck('value')->first();
        $order->subtotal_fees = PackingCart::where('id', $request->packing_id)->pluck('points')->first();
        $order->order_date = NOW();
        $order->save();
        if (!empty($order)) {
            $this->sendOrderToDelegate($check_login, $type, $order->id);
            return $this->respondWithMessage(trans('messages.order.completed'));
        } else {
            return $this->respondWithError(trans('messages.something_went_wrong'));
        }
    }

    // send notification to delegate

    function sendOrderToDelegate($user_exist, $type, $order_id)
    {
        define('API_ACCESS_KEY', 'AAAAJIhBdDE:APA91bHLPrry6EpPfm8cSqvb0Vg5d3pa22K5i_e3uPTKev8JR_7SsVjGktN-paxPXo7k26OYB4oJbWZN6RxkOrQ2KCLvLH_ifqUyfRJow7EG4doyamUVXVlGK8aRnaKPVqIib3md5MoU');
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $location_user = User::where('id', $user_exist->id)->select('lat', 'lng')->first();
        $notification_data = User::selectRaw("( FLOOR(6371 * ACOS( COS( RADIANS($location_user->lat) ) * COS( RADIANS( users.lat ) ) * COS( RADIANS( users.lng ) - RADIANS($location_user->lng) ) + SIN( RADIANS($location_user->lat) ) * SIN( RADIANS( users.lat ) ) )) ) as distance, users.id as id,users.lat,users.first_name,users.firebase_token as firebase_token")
            ->havingRaw("distance <= 50")
            ->where('users.type', 2)
            ->where('users.confirm_account', 1)
            ->where('users.status', 1)
            ->whereNotIn('id', DB::table('orders')->select('orders.delegate_id')->where('orders.order_status', '=', 2)->orwhere('orders.order_status', '=', 3))
            ->pluck('firebase_token')->toArray();
        $notification_data1 = User::selectRaw("( FLOOR(6371 * ACOS( COS( RADIANS($location_user->lat) ) * COS( RADIANS( users.lat ) ) * COS( RADIANS( users.lng ) - RADIANS($location_user->lng) ) + SIN( RADIANS($location_user->lat) ) * SIN( RADIANS( users.lat ) ) )) ) as distance, users.id as id,users.lat,users.first_name,users.firebase_token as firebase_token")
            ->havingRaw("distance <= 50")
            ->where('users.type', 2)
            ->where('users.confirm_account', 1)
            ->where('users.status', 1)
            ->whereNotIn('id', DB::table('orders')->select('orders.delegate_id')->where('orders.order_status', '=', 2)->orwhere('orders.order_status', '=', 3))
            ->pluck('id')->toArray();

        if ($notification_data != NULL) {
            $notification = [
                'title' => 'طلب جديد',
                'body' => 'تم ارسال طلب  خاص بكرت تعبئه',
                'type' => '1',
                'order_id' => $order_id
            ];
              $extraNotificationData = ["message" => $notification, "moredata" => 'dd'];
            $fcmNotification = [
                'registration_ids' => $notification_data, //single token
                'data' => $extraNotificationData
            ];
            foreach ($notification_data1 as $data) {
                $notification = new Notification();
                $notification->from = $user_exist;
                $notification->to = $data;
                $notification->title_en = 'New Order';
                $notification->message_en = 'Send New card order  ';
                $notification->title_ar = 'طلب جديد';
                $notification->message_ar = 'تم ارسال طلب  خاص بكرت تعبئه';
                $notification->title_kur = 'طلب جديد';
                $notification->message_kur = 'تم ارسال طلب  خاص بكرت تعبئه';
                $notification->type = 2;
                $notification->flag = $data;
                $notification->save();
            }
            $headers = [
                'Authorization: key=' . API_ACCESS_KEY,
                'Content-Type: application/json'
            ];
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $fcmUrl);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
            $result = curl_exec($ch);
            curl_close($ch);
            if ($result) {
                return true;
            } else {
                return false;
            }
        }
    }
}
