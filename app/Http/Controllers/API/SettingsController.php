<?php

namespace App\Http\Controllers\API;

use App\Faq;
use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends APIController
{
    protected $lang;

    function __construct(Request $request)
    {
        $this->setLang($request->header('lang'));
    }

    public function index(Request $request)
    {
        $type = $request->type;
        $app = $request->app;
        if ($type == 1 && $app == 1) {
            $settingApp = Setting::where('key', 'terms_' . $this->lang)->where('app', 1)->pluck('value')->first();
        } elseif ($type == 2 && $app == 1) {
            $settingApp = Setting::where('key', 'about_us_' . $this->lang)->where('app', 1)->pluck('value')->first();
        } else if ($type == 1 && $app == 2) {
            $settingApp = Setting::where('key', 'terms_' . $this->lang)->where('app', 2)->pluck('value')->first();
        } elseif ($type == 2 && $app == 2) {
            $settingApp = Setting::where('key', 'about_us_del_' . $this->lang)->where('app', 2)->pluck('value')->first();
        } else if ($type == 1 && $app == 3) {
            $settingApp = Setting::where('key', 'terms_' . $this->lang)->where('app', 3)->pluck('value')->first();
        } elseif ($type == 2 && $app == 3) {
            $settingApp = Setting::where('key', 'about_shop_us_' . $this->lang)->where('app', 3)->pluck('value')->first();
        } elseif ($type == 1) {
            $settingApp = Setting::where('status', 2)->select('key', 'value')->get();
        } elseif ($type == 2) {
            $settingApp = Faq::select('question_' . $this->lang . ' as question', 'answer_' . $this->lang . ' as answer')->get();
        }
        if ($settingApp) {
            return response()->json([
                'status' => 200,
                'message' => trans('validation.message'),
                'data' => $settingApp
            ]);
        }
        return $this->respondWithError(trans('validation.no_data'));
    }
}
