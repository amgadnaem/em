<?php

namespace App\Http\Controllers\API;

use App\Repositories\User\UserRepository;
use App\ShopFavorite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShopFavoriteController extends APIController
{
    public $repository;
    public $favoriteShop;

    function __construct(Request $request, UserRepository $repository, ShopFavorite $favoriteShop)
    {
        $this->repository = $repository;
        $this->favoriteShop = $favoriteShop;
        $this->setLang($request->header('lang'));

    }

    function CreateFavoriteShop(Request $request)
    {
        $check_user = $this->repository->checkJwtUser($request);
        if (!$check_user) {
            return $this->respondWithError(trans('messages.auth.user_check'));

        }
        $message = '';
        $isFavorite = 0;

        $favorite = ShopFavorite::where('user_id', $check_user->id)->where('shop_id', $request->shop_id)->pluck('id')->first();
        if (!$favorite) {
            if ($this->favoriteShop->create([
                    'user_id' => $check_user->id,
                    'shop_id' => $request->shop_id
                ]
            )) {
                $isFavorite = 1;
                $message = trans('messages.favorites.addedShop');
            }
        } else {

            if ($this->favoriteShop->destroy($favorite)) {
                $isFavorite = 0;
                $message = trans('messages.favorites.removedShop');
            }
        }
        return response()->json([
            'status' => 200,
            'isFavorite' => $isFavorite,
            'message' => $message
        ]);
    }
}
