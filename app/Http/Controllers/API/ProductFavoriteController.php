<?php

namespace App\Http\Controllers\API;

use App\ProductFavorite;
use App\Repositories\Product\ProductRepository;
use App\Repositories\Shop\ShopRepository;
use App\Repositories\User\UserRepository;
use App\Repositories\UserFavorite\UserFavoriteRepository;
use App\ShopFavorite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductFavoriteController extends APIController
{
    public $repository;
    public $favoriteProduct;
    protected $shopRepository;
    protected $userFavoriteRepository;

    function __construct(Request $request, UserRepository $repository, ProductFavorite $favoriteProduct, UserFavoriteRepository $userFavoriteRepository,ShopRepository $shopRepository)
    {
        $this->repository = $repository;
        $this->userFavoriteRepository = $userFavoriteRepository;
        $this->favoriteProduct = $favoriteProduct;
        $this->shopRepository = $shopRepository;
        $this->setLang($request->header('lang'));

    }

    // get favorite user
    public function index(Request $request)
    {
        $check_user = $this->repository->checkJwtUser($request);
        if (!$check_user) {
            return $this->respondForbidden(trans('messages.auth.user_check'));
        }
        $type = $request->type;
        if ($type == 1) {
            $productsFavorites = ProductFavorite::where('user_id', $check_user->id)->latest()->get();
            $data = $this->userFavoriteRepository->getUserFavorites($productsFavorites, $this->lang, $check_user);
        } else {
            $shopFavorites = ShopFavorite::where('user_id', $check_user->id)->latest()->get();
            $data = $this->userFavoriteRepository->getShopFavorites($shopFavorites, $this->lang,$check_user);

        }

        return $this->respond(
            200,
            trans('messages.favorites.list'),
            $data
        );

    }

    function createFavoriteProduct(Request $request)
    {
        $check_user = $this->repository->checkJwtUser($request);
        if (!$check_user) {
            return $this->respondForbidden(trans('messages.auth.user_check'));

        }
        $message = '';
        $isFavorite = 0;
        $favorite = ProductFavorite::where('user_id', $check_user->id)->where('product_id', $request->product_id)->pluck('id')->first();
        if (!$favorite) {
            if (ProductFavorite::create([
                    'user_id' => $check_user->id,
                    'product_id' => $request->product_id
                ]
            )) {
                $isFavorite = 1;
                $message = trans('messages.favorites.added');
            }
        } else {
            if (ProductFavorite::destroy($favorite)) {
                $isFavorite = 0;
                $message = trans('messages.favorites.removed');
            }
        }
        return response()->json([
            'status' => 200,
            'isFavorite' => $isFavorite,
            'message' => $message
        ]);
    }
}
