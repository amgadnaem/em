<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response as IlluminateResponse;

use App\User;
use Response;

/**
 * Base API Controller.
 */
class APIController extends Controller
{
    /**
     * default status code.
     *
     * @var int
     */
    protected $statusCode = 200;


    /**
     * default language.
     *
     * @var string
     */
    protected $lang = 'en';


    public function getStatusCode()
    {
        return $this->statusCode;
    }



    public function setLang($lang)
    {           $this->lang  = $lang;
        return \App::setlocale($this->lang);
    }


    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }


    public function respond($status, $message, $data)
    {
        $array = [
            'status' => $this->statusCode,
            'message' => $message,
            'data' => $data
        ];
        return response()->json($array);
    }


    public function success($message, $data)
    {
        $array = [
            'status' => 200,
            'message' => $message,
            'data' => $data
        ];
        return response()->json($array);
    }

        public function respondWithMessage($message)
        {
            $array = [
                'status' => $this->statusCode,
                'message' => $message
            ];
            return response()->json($array);
        }


    public function respondWithPagination($items, $data)
    {
        $data = array_merge($data, [
            'paginator' => [
                'total_count'  => $items->total(),
                'total_pages'  => ceil($items->total() / $items->perPage()),
                'current_page' => $items->currentPage(),
                'limit'        => $items->perPage(),
            ],
        ]);

        return $this->respond($data);
    }


    public function respondCreated($data)
    {
        return $this->setStatusCode(201)->respond([
            'data' => $data,
        ]);
    }


    public function respondCreatedWithData($data)
    {
        return $this->setStatusCode(201)->respond($data);
    }


    public function respondWithError($message)
    {
        $this->setStatusCode(401);
        $array = [
            'status' => $this->statusCode,
            'message' => $message
        ];
        return response()->json($array);
    }


    public function respondNotFound($message = 'Not Found')
    {
        return $this->setStatusCode(IlluminateResponse::HTTP_NOT_FOUND)->respondWithError($message);
    }

    public function respondInternalError($message = 'Internal Error')
    {
        return $this->setStatusCode(500)->respondWithError($message);
    }


    protected function respondUnauthorized($message = 'Unauthorized')
    {
        $this->setStatusCode(401);
        return $this->respondWithError($message);
    }


    protected function respondForbidden($message = 'Forbidden')
    {
        $this->setStatusCode(403);
        $array = [
            'status' => $this->statusCode,
            'message' => $message
        ];
        return response()->json($array);
    }


    public function respondVerify($message)
    {
        $this->setStatusCode(405);
        $array = [
            'status' => $this->statusCode,
            'message' => $message
        ];
        return response()->json($array);
    }



    protected function respondWithNoContent()
    {
        return $this->setStatusCode(204)->respond(null);
    }

    /**Note this function is same as the below function but instead of responding with error below function returns error json
     * Throw Validation.
     *
     * @param string $message
     *
     * @return mix
     */
    public function throwValidation($message)
    {
        return $this->setStatusCode(422)
            ->respondWithError($message);
    }

    public function checkJWT($token)
    {

        $check = User::where('jwt_token',$token)->first();
        if($check){
            return true;
        }else{
            return $this->respondUnauthorized();
        }
    }

}
