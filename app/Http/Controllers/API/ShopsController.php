<?php

namespace App\Http\Controllers\API;

use App\Product;
use App\Repositories\Product\ProductRepository;
use App\Repositories\Shop\ShopRepository;
use App\Repositories\User\UserRepository;
use App\Shop;
use App\ShopCategories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ShopsController extends APIController
{
    public $repository;
    protected $shopRepository;
    protected $productRepository;

    function __construct(Request $request,
                         UserRepository $repository,
                         ShopRepository $shopRepository,
                         ProductRepository $productRepository
    )
    {
        $this->repository = $repository;
        $this->shopRepository = $shopRepository;
        $this->productRepository = $productRepository;
        $this->setLang($request->header('lang'));
    }

    //get Shops with city

    function index(Request $request)
    {
        $type = $request->type;
        if ($type == 1) {
            $check_login = $this->repository->checkJwtUser($request);
        } else {
            $check_login = $this->shopRepository->checkJwtShop($request);
        }
        if($type == 5) {
            if (!empty($request->country_id)) {
                $shops = Shop::where("Subscribe_end", '>', Now())
                    ->where('status', 1)
                    ->where('shop_status', 1)
                    ->where('service_id', $request->service_id)
                    ->where('country_id', $request->country_id)
                    ->get();
            } else {
                $shops = Shop::where("Subscribe_end", '>', Now())
                    ->where('status', 1)
                    ->where('shop_status', 1)
                    ->where('service_id', $request->service_id)
                    ->get();
            }
        } else {
            if (isset($check_login) && !empty($check_login) && $type == 1) {
                $shops = Shop::
                where("Subscribe_end", '>', Now())
                    ->where('service_id', $request->service_id)
                    ->where('country_id', $check_login->country_id)
                    ->where('status', 1)
                    ->where('shop_status', 1)
                    ->get();
            } else if (isset($check_login) && !empty($check_login) && $type == 3) {
                $shops = Shop::where('shop_status', 1)
                    ->where('status', 1)
                    ->where("Subscribe_end", '>', Now())
                    ->where('service_id', $check_login->service_id)
                    ->where('city_id', $check_login->city_id)
                    ->where('id', '!=', $check_login->id)
                    ->get();
            } else {
                $shops = Shop::where('shop_status', 1)
                    ->where('status', 1)
                    ->where("Subscribe_end", '>', Now())
                    ->where('service_id', $request->service_id)
                    ->where('country_id', $request->country_id)
                    ->get();
            }
        }

        $all_shops = $this->shopRepository->getShopDetails($shops, $type, $this->lang, $check_login);
        if (count($all_shops)) {
            return response()->json([
                'status' => 200,
                'message' => trans('messages.shops.list'),
                'data' => $all_shops,
            ]);
        }
        return $this->respondWithError(trans(trans('messages.shops.no')));

    }



    function searchShops(Request $request)
    {
           $type = $request->type;
        if ($type == 1) {
            $user = $this->repository->checkJwtUser($request);
        } else {
            $user = $this->shopRepository->checkJwtShop($request);
        }

        if (isset($user) && !empty($user)) {
            $shops = Shop::where('shop_status', 1)
                ->where('status', 1)
                ->where("Subscribe_end", '>', Now())
                ->where('city_id', $user->city_id)
                ->where('name_app_en', 'like', "%{$request->search}%")
                ->orWhere('name_app_ar', 'like', "%{$request->search}%")
                ->orWhere('name_app_kur', 'like', "%{$request->search}%")
                ->get();
        } else {
            $shops = Shop::where('shop_status', 1)
                ->where('status', 1)
                ->where("Subscribe_end", '>', Now())
                ->where('country_id', $request->country_id)
                ->where('name_app_en', 'like', "%{$request->search}%")
                ->orWhere('name_app_ar', 'like', "%{$request->search}%")
                ->orWhere('name_app_kur', 'like', "%{$request->search}%")
                ->get();
        }
        $all_shops = $this->shopRepository->getShopDetails($shops, $type, $this->lang, $user);
            return response()->json([
                'status' => 200,
                'message' => trans('messages.shops.list'),
                'data' => !empty($all_shops) ? $all_shops : [],
            ]);


    }

    // check password shops
    function checkPasswordShop(Request $request)
    {
        $shop = Shop::where('id', $request->shop_id)->first();
        if ($shop) {
            if (password_verify($request->password, $shop->secret_password)) {
                return response()->json([
                    'status' => 200,
                    'message' => trans('messages.auth.login')
                ]);
            }
            return $this->respondUnauthorized(trans('messages.auth.password'));

        }
    }

    // get category shops
    function getCategory(Request $request)
    {
        $get_category_shop = ShopCategories::where('shop_id', $request->shop_id)
            ->get();

        $category_item = [];
        $category_list = [];
        foreach ($get_category_shop as $categories) {

            $category_item['id'] = $categories->id;
            if ($this->lang = 'ar') {
                $category_item['name'] = $categories->category_name_ar;
            } else {
                $category_item['name'] = $categories->category_name_en;

            }
            $category_item['service_image'] = $categories->category_image;
            $category_list[] = $category_item;
        }
        if ($get_category_shop) {
            return $this->respond('200', trans('messages.category.list'), $category_list);
        }
        return $this->respondWithError(trans(trans('messages.category.no')));
    }

    // get product from category
    function getProductFromCategory(Request $request)
    {
        $user = $this->repository->checkJwtUser($request);
        $products = Product::where('category_id', $request->category_id)->get();
        $all_products = $this->productRepository->getAllProductsDetailPaginate($products, $this->lang, $user);

        return $this->respond(
            200,
            trans('messages.products.list'),
            $all_products
        );


    }
}
