<?php

namespace App\Http\Controllers\API;

use App\Chat;
use App\OrderChat;
use App\Repositories\User\UserRepository;
use App\Shop;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class OrderChatController extends APIController
{
    protected $userRepository;


    public function __construct(Request $request,
                                UserRepository $userRepository
    )
    {
        $this->userRepository = $userRepository;
        $this->setLang($request->header('lang'));
    }

    public function index(Request $request)
    {
        $from_type = $request->from_type;
        
        
        if ($from_type == 1) {
            
            $check_login = $this->userRepository->checkJwtUser($request);
            if (!$check_login) {
                return $this->respondWithError(trans('messages.auth.user_check'));
            }
            $all_chats = DB::select("SELECT
	*
FROM
	chats
WHERE
	order_id = $request->order_id  
	    and (( from_type = $request->from_type and to_type = $request->to_type ) 
   or ( from_type = $request->to_type and to_type = $request->from_type ) )
;");
        } elseif ($from_type == 2) {
            $check_login = $this->userRepository->checkJwtUser($request);
            if (!$check_login) {
                return $this->respondWithError(trans('messages.auth.user_check'));
            }
            $all_chats = DB::select("SELECT
	*
FROM
	chats
WHERE
	order_id = $request->order_id  
	    and (( from_type = $request->from_type and to_type = $request->to_type ) 
   or ( from_type = $request->to_type and to_type = $request->from_type ) )
;");
        } elseif ($from_type == 3) {
            $check_login = $this->userRepository->checkJwtShop($request);
            if (!$check_login) {
                return $this->respondWithError(trans('messages.auth.user_check'));
            }
            $all_chats = DB::select("SELECT
	*
FROM
	chats
WHERE
	order_id = $request->order_id  
	    and (( from_type = $request->from_type and to_type = $request->to_type ) 
   or ( from_type = $request->to_type and to_type = $request->from_type ) )
;");
        }

        $chat_item = [];
        $chat_list = array();
        foreach ($all_chats as $chats) {
            $user = User::where('type', 1)->where('id', $chats->from)->orwhere('id', $chats->to)->where('type', 1)->select('id', 'first_name as name', 'user_image')->first();
            $delegate = User::where('type', 2)->where('id', $chats->from)->orwhere('id', $chats->to)->select('id', 'first_name as name', 'user_image')->first();
            $shop = Shop::where('type', 3)->where('id', $chats->from)->orwhere('id', $chats->to)->select('id', 'name_app_' . $this->lang . ' as  name', 'shop_image')->first();
            $chat_item['id'] = $chats->id;
            $chat_item['message'] = $chats->message;
            $chat_item['type'] = $chats->from_type;
            $chat_item['image'] = !empty($chats->image) ? 'https://em.my-staff.net/images/chat/'.$chats->image : null ;

             $chat_item['date'] =  \Carbon\Carbon::parse($chats->created_at)->diffForHumans();

            $chat_list[] = $chat_item;
        }
        if ($chat_list) {
            if (($from_type == 1 && $request->to_type == 2) || ($from_type == 2 && $request->to_type == 1)) {
                return response()->json([
                    'status' => 200,
                    'message' => trans('validation.message'),
                    'chats' => $chat_list,
                    'user' => $user,
                    'delegate' => $delegate,
                ]);
            } elseif (($from_type == 1 && $request->to_type == 3) || ($from_type == 3 && $request->to_type == 1)) {

                return response()->json([
                    'status' => 200,
                    'message' => trans('validation.message'),
                    'chats' => $chat_list,
                    'user' => $user,
                    'shop' => $shop,
                ]);

            } elseif (($from_type == 2 && $request->to_type == 3) || ($from_type == 3 && $request->to_type == 2)) {

                return response()->json([
                    'status' => 200,
                    'message' => trans('validation.message'),
                    'chats' => $chat_list,
                    'shop' => $shop,
                    'delegate' => $delegate,
                ]);

            } else {
                return response()->json([
                    'status' => 200,
                    'message' => trans('validation.no_data'),
                ]);

            }

        } else {
            return response()->json([
                'status' => 401,
                'message' => trans('validation.no_data'),
            ]);
        }
    }

    // make chat between user and delegate
    function orderChat(Request $request)
    {
        $from_type = $request->from_type;
        if ($from_type == 1) {
            $user_exist = $this->userRepository->checkJwtUser($request);
            if (!$user_exist) {
                return $this->respondWithError(trans('messages.auth.user_check'));
            }
                  $this->sendNotify($request->order_id,$request->to, $from_type, $request->to_type,$user_exist->id);

            $chat = new Chat();
            $chat->from = $user_exist->id;
            $chat->to = $request->to;
            $chat->message = $request->message;
            $chat->order_id = $request->order_id;
            $chat->from_type = 1;
            $chat->to_type = $request->to_type;
            $chat->save();
            if ($request->image) {
                $chat->image = $request->image;
                $chat->save();
            }
        } elseif ($from_type == 2) {
            $user_exist = $this->userRepository->checkJwtUser($request);
            if (!$user_exist) {
                return $this->respondWithError(trans('messages.auth.user_check'));
            }
                  $this->sendNotify($request->order_id,$request->to, $from_type, $request->to_type,$user_exist->id);

            $chat = new Chat();
            $chat->from = $user_exist->id;
            $chat->to = $request->to;
            $chat->message = $request->message;
            $chat->order_id = $request->order_id;
            $chat->from_type = 2;
            $chat->to_type = $request->to_type;
            $chat->save();
            if ($request->image) {
                $chat->image = $request->image;
                $chat->save();
            }
        } elseif ($from_type == 3) {
            $user_exist = $this->userRepository->checkJwtShop($request);
            if (!$user_exist) {
                return $this->respondWithError(trans('messages.auth.user_check'));
            }
                  $this->sendNotify($request->order_id,$request->to, $from_type, $request->to_type,$user_exist->id);

            $chat = new Chat();
            $chat->from = $user_exist->id;
            $chat->to = $request->to;
            $chat->message = $request->message;
            $chat->order_id = $request->order_id;
            $chat->from_type = 3;
            $chat->to_type = $request->to_type;
                      

            $chat->save();
            if ($request->image) {
                $chat->image = $request->image;
                $chat->save();
            }
        }


        if ($chat) {
            return response()->json([
                'status' => 200,
                'message' => trans('messages.auth.send message'),
            ]);
        }
        return $this->respondWithError(trans('messages.something_went_wrong'));
    }
    
    
    function sendNotify($order_id,$id, $from_type,$to_type, $sender_id)
    {
        
        define('API_ACCESS_KEY', 'AAAAJIhBdDE:APA91bHLPrry6EpPfm8cSqvb0Vg5d3pa22K5i_e3uPTKev8JR_7SsVjGktN-paxPXo7k26OYB4oJbWZN6RxkOrQ2KCLvLH_ifqUyfRJow7EG4doyamUVXVlGK8aRnaKPVqIib3md5MoU');
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
              if ( ($to_type == 2 ) || ($to_type == 1 )) {
            $send_shop = User::where('id', $id)->pluck('firebase_token')->first();
        } else {
             $send_shop = Shop::where('id', $id)->pluck('firebase_token')->first();

        }


        if ($send_shop != NULL) {
           
              $notification1 = [
                    'title' => 'رساله جديده',
                    'body' => 'لديك رساله جديده  بخصوص الاوردر',
                    'sender_id' => $id,
                    'type'=>6,
                    'order_id' => $order_id,
                    'sender_id'=>$sender_id,
                    'flag_type'=>$from_type
                ];
            
            $extraNotificationData = ["message" => $notification1, "moredata" => 'dd'];
            $fcmNotification = [
                'to' => $send_shop, //single token
                'data' => $extraNotificationData
            ];
            $headers = [
                'Authorization: key=' . API_ACCESS_KEY,
                'Content-Type: application/json'
            ];
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $fcmUrl);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
            $result = curl_exec($ch);
            curl_close($ch);
            if ($result) {
                return true;
            } else {
                return false;
            }
        }
    }
}
