<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\User\ActiveCode;
use App\Http\Requests\User\ChangePassword;
use App\Repositories\User\UserRepository;
use App\ShippingCompany;
use App\Shop;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class UserController extends APIController
{
    public $repository;

    function __construct(Request $request, UserRepository $repository)
    {
        $this->repository = $repository;
        $this->setLang($request->header('lang'));
    }

       //  RegisterUser

    public function register(Request $request)
    {
        $type = $request->type;
        if ($type == 1) {
            $phone_number = $request->phone_number;
            if ($this->repository->checkIfAccountExists($phone_number)) {
                return $this->respondWithError(trans('messages.auth.account_exists'));
            }

            $validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'password' => 'required|min:6',
                'phone_number' => 'required|min:6',
                'country_id' => 'required',
                'city_id' => 'required',
            ],
                [
                    'first_name.required' => trans('validation.first_name'),
                    'last_name.required' => trans('validation.last_name'),
                    'password.required' => trans('validation.password'),
                    'phone_number.required' => trans('validation.phone_number'),
                    'country_id.required' => trans('validation.country_id'),
                    'city_id.required' => trans('validation.city_id'),
                ]
            );

            if ($validator->fails()) {
                return $this->respondWithError($validator->errors()->first());
            }
            $register = $this->repository->create($request);
            if ($register) {
                return $this->respond(
                    200,
                    trans('messages.auth.register'),
                    $this->repository->getLoggedUserDetails($register, $this->lang)
                );
            }
        } elseif ($type == 2) {
            $phone_number = $request->phone_number;
            if ($this->repository->checkIfAccountExists($phone_number)) {
                return $this->respondWithError(trans('messages.auth.account_exists'));
            }

            $validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'dob' => 'required',
                'password' => 'required|min:6',
                'phone_number' => 'required|min:6',
                'country_id' => 'required',
                'city_id' => 'required',
                'lat' => 'required',
                'lng' => 'required',
                'location' => 'required',
            ],
                [
                    'first_name.required' => trans('validation.first_name'),
                    'last_name.required' => trans('validation.last_name'),
                    'dob.required' => trans('validation.dob'),
                    'phone_number.required' => trans('validation.phone_number'),
                    'password.required' => trans('validation.password'),
                    'country_id.required' => trans('validation.country_id'),
                    'city_id.required' => trans('validation.city_id'),
                    'lat.required' => trans('validation.lat'),
                    'lng.required' => trans('validation.lng'),
                    'location.required' => trans('validation.location'),
                ]
            );

            if ($validator->fails()) {
                return $this->respondWithError($validator->errors()->first());
            }
            $register = $this->repository->createDelegate($request);
            if ($register) {
                return $this->respond(
                    200,
                    trans('messages.auth.register'),
                    $this->repository->getLoggedUserDetails($register, $this->lang)
                );
            }
        } elseif ($type == 3) {
            $phone_number = $request->phone_number;
            $shipping = ShippingCompany::where('phone_number', $phone_number)->first();
            if ($shipping) {
                return $this->respondWithError(trans('messages.auth.account_exists'));
            }
            $validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'password' => 'required|min:6',
                'phone_number' => 'required|min:6',
                'country_id' => 'required',
                'city_id' => 'required',
                'image' => 'required',
                'license_image' => 'required',
            ],
                [
                    'first_name.required' => trans('validation.first_name'),
                    'phone_number.required' => trans('validation.phone_number'),
                    'password.required' => trans('validation.password'),
                    'country_id.required' => trans('validation.country_id'),
                    'city_id.required' => trans('validation.city_id'),
                    'image.required' => trans('validation.image'),
                    'license_image.required' => trans('validation.license_image'),
                ]
            );

            if ($validator->fails()) {
                return $this->respondWithError($validator->errors()->first());
            }
            $register = $this->repository->createShipping($request);
            if ($register) {
                return $this->respond(
                    200,
                    trans('messages.auth.register'),
                    $this->repository->getLoggedCompanyDetails($register, $this->lang)
                );
            }
        } elseif ($type == 4) {
            $phone_number = $request->phone_number;
            if ($this->repository->checkIfAccountExists($phone_number)) {
                return $this->respondWithError(trans('messages.auth.account_exists'));
            }

            $validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'dob' => 'required',
                'car_name' => 'required',
                'car_color' => 'required',
                'car_number' => 'required',
                'phone_number' => 'required|min:6',
                'country_id' => 'required',
                'city_id' => 'required',
                'lat' => 'required',
                'lng' => 'required',
                'location' => 'required',
            ],
                [
                    'first_name.required' => trans('validation.first_name'),
                    'last_name.required' => trans('validation.last_name'),
                    'dob.required' => trans('validation.dob'),
                    'car_name.required' => trans('validation.car_name'),
                    'car_color.required' => trans('validation.car_color'),
                    'car_number.required' => trans('validation.car_number'),
                    'phone_number.required' => trans('validation.phone_number'),
                    'country_id.required' => trans('validation.country_id'),
                    'city_id.required' => trans('validation.city_id'),
                    'lat.required' => trans('validation.lat'),
                    'lng.required' => trans('validation.lng'),
                    'location.required' => trans('validation.location'),
                ]
            );

            if ($validator->fails()) {
                return $this->respondWithError($validator->errors()->first());
            }
            $register = $this->repository->createDriver($request);
            if ($register) {
                return $this->respond(
                    200,
                    trans('messages.auth.register'),
                    $this->repository->getLoggedUserDetails($register, $this->lang)
                );
            }
        }

    }

    // Active code
    public function activateAccount(ActiveCode $request)
    {
        $type = $request->type;
        if ($type == 1) {
            $data = $this->repository->verifyAccount($request);
            if (!$data) {
                return $this->respondWithError(trans('messages.auth.wrong_activate_code'));
            }
        } else {
            $data = $this->repository->verifyAccountShop($request);
            if (!$data) {
                return $this->respondWithError(trans('messages.auth.wrong_activate_code'));
            }
        }
        return response()->json([
            'status' => 200,
            'message' => trans('messages.auth.activated_successfully'),
            'data' => $this->repository->getLoggedUserDetails($data, $this->lang)
        ]);

    }

    // login

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone_number' => 'required',
            'password' => 'required',
        ],
            [
                'phone_number.required' => trans('validation.phone_number'),
                'password.required' => trans('validation.password'),
            ]
        );

        if ($validator->fails()) {
            return $this->respondWithError($validator->errors()->first());
        }
        if ($request->type == 1 || $request->type == 2) {
            $credentials = $request->only(['phone_number', 'password','type']);
            if (Auth::attempt($credentials)) {
                $user = Auth::user();
            
           if ($request->type == 2) {
                    if ($user->confirm_account == 0) {
                        return $this->respondWithError(trans('messages.auth.delegate_confirm'));
                    }
                }
                    
                if ($user->status == 0) {
                    return $this->respondWithError(trans('messages.auth.account_suspended'));
                }
                $user->jwt_token = Str::random(25);
                $user->firebase_token = $request->firebase;
                $user->save();
                return $this->success(
                    trans('messages.auth.login'),
                    $this->repository->getLoggedUserDetails($user, $this->lang)
                );
            } else {
                return $this->respondUnauthorized(trans('messages.auth.phone_password'));
            }
        } elseif ($request->type == 3) {

            $shop = Shop::where('phone_number', $request->phone_number)->first();
            if ($shop) {
                $password = password_verify($request->password, $shop->password);
                if ($password) {
                    if ($shop->shop_status == 0) {
                        return $this->respondWithError(trans('messages.auth.delegate_confirm'));
                    }
                    if ($shop->status == 0) {
                        return $this->respondWithError(trans('messages.auth.account_suspended'));
                    }
                    $shop->jwt_token = Str::random(25);
                    $shop->firebase_token = $request->firebase;
                    $shop->save();
                    return $this->success(
                        trans('messages.auth.login'),
                        $this->repository->getLoggedShopDetails($shop, $this->lang)
                    );
                } else {
                    return $this->respondUnauthorized(trans('messages.auth.password'));

                }
            } else {
                return $this->respondUnauthorized(trans('messages.auth.phone_not_exists'));
            }
        }

    }


    function update_location(Request $request)
    {
        $check_jwt = $this->repository->checkJwtUser($request);
        if (!$check_jwt) {
            return $this->respondWithError(trans('messages.auth.user_check'));
        }
        $checkExists = User::where('id', $check_jwt->id)->first();
        if ($checkExists) {
            $checkExists->lat = $request->lat;
            $checkExists->lng = $request->lng;
            $checkExists->location = $request->location;
            $checkExists->save();
            return response()->json([
                'status' => 200,
                'message' => trans('messages.auth.location')
            ]);
        }
        return response()->json([
            'status' => 401,
            'message' => trans('messages.auth.wrong_activate_code')
        ]);

    }


   // Check Account user

    public function checkPhone(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'phone_number' => 'required',
        ], [
                'phone_number.required' => trans('validation.phone_number'),
            ]
        );
        if ($validator->fails()) {
            return $this->respondWithError($validator->errors()->first());
        }
        $type = $request->type;
        if($type == 1 || $type == 2) {
            $phone = User::where('phone_number', $request->phone_number)->where('type',$request->type)->first();
        } else {
            $phone = Shop::where('phone_number', $request->phone_number)->where('type',$request->type)->first();
        }
        if (!$phone) {
            return $this->respondWithError(trans('messages.auth.phone_not_exists'));
        }
        return response()->json([

            'status' => 200,
            'message' => trans('messages.auth.check_phone'),
        ]);

    }
    
       // check phone register

    public function checkPhoneRegister(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone_number' => 'required',
        ], [
                'phone_number.required' => trans('validation.phone_number'),
            ]
        );
        if ($validator->fails()) {
            return $this->respondWithError($validator->errors()->first());
        }
        $type = $request->type;
        if($type == 1 || $type == 2) {
            $phone = User::where('phone_number', $request->phone_number)->where('type',$request->type)->first();
        } else {
            $phone = Shop::where('phone_number', $request->phone_number)->where('type',$request->type)->first();
        }
        if (!$phone) {
            return response()->json([

                'status' => 200,
                'message' => trans('messages.auth.phone_not_exists'),
            ]);
        }
        return $this->respondWithError(trans('messages.auth.account_exists'));

    

    }




}
