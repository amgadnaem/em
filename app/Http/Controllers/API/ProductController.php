<?php

namespace App\Http\Controllers\API;

use App\Product;
use App\ProductColor;
use App\ProductSize;
use App\Repositories\Product\ProductRepository;
use App\Repositories\User\UserRepository;
use App\Size;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends APIController
{
    public $repository;
    protected $productRepository;

    function __construct(Request $request, UserRepository $repository, ProductRepository $productRepository)
    {
        $this->repository = $repository;
        $this->productRepository = $productRepository;
        $this->setLang($request->header('lang'));
    }

    // get Details Product

    function getDetailsProduct(Request $request)
    {
        $user = $this->repository->checkJwtUser($request);

        $product_id = $request->product_id;
        $product = Product::where('id', $product_id)->first();
        $product_details = $this->productRepository->getProductDetails($product, $this->lang, $user);
        return $this->respond(
            200,
            trans('messages.products.list'),
            $product_details
        );

    }

    function getDetailsProduct12(Request $request)
    {
        $user = $this->repository->checkJwtUser($request);

        $product_id = $request->product_id;
        $product = Product::where('id', $product_id)->first();

        if ($product->is_clothes == 1) {
            $product_details = $this->productRepository->getProductDetails($product, $this->lang, $user);
            return $this->respond(
                200,
                trans('messages.products.list'),
                $product_details
            );
//            $product_size = Product::where('id', $product_id)->get();
//            $product_list = [];
//            $product_item = [];
//            foreach ($product_size as $product) {
//                $product_item['id'] = $product->id;
//                if ($this->lang == 'ar') {
//                    $product_item['product_name'] = $product->name_ar;
//                } else if ($this->lang == 'en') {
//                    $product_item['product_name'] = $product->name_en;
//                } else {
//                    $product_item['product_name'] = $product->name_kur;
//                }
//                $product_item['description'] = $product->desc_ar;
//                $product_item['product_image'] = $product->product_image;
//                $product_item['product_sizes'] = $this->getSizes($product_id);
//                $product_list[] = $product_item;
//            }
//
//            return $this->respond(
//                200,
//                trans('messages.products.list'),
//                $product_list
//            );
        } else {
            $product_size = Product::where('id', $product_id)->get();
            $product_list = [];
            $product_item = [];
            foreach ($product_size as $product) {
                $product_item['id'] = $product->id;
                if ($this->lang == 'ar') {
                    $product_item['product_name'] = $product->name_ar;
                } else if ($this->lang == 'en') {
                    $product_item['product_name'] = $product->name_en;
                } else {
                    $product_item['product_name'] = $product->name_kur;
                }
                $product_item['description'] = $product->desc_ar;
                $product_item['product_image'] = $product->product_image;
                $product_item['general_price'] = $product->general_price;
                $product_list[] = $product_item;
            }
            return $this->respond(
                200,
                trans('messages.products.list'),
                $product_list
            );
        }

    }


    public function getSizes($product_id)
    {
        $sizes = ProductSize::where('product_id', $product_id)->get();
        $add_items = [];
        $add_list = [];
        foreach ($sizes as $SizeDetails) {
            $add_items['size_id'] = $SizeDetails->id;
            $add_items['size_name'] = Size::where('id', $SizeDetails->size_id)->pluck('size_name')->first();
            $add_items['general_price'] = $SizeDetails->general_price;
            $add_items['special_price'] = $SizeDetails->special_price;
            $add_items['product_colors'] = $this->getColors($SizeDetails->id);
            $add_list[] = $add_items;
        }
        return $add_list;
    }

    public function getColors($size_id)
    {
        $sizes = ProductColor::where('product_size_id', $size_id)->select('color')->get();
        return $sizes;
    }

    // get colors from size

    function getColorsFromSize(Request $request)
    {
        $sizes = ProductSize::where('id', $request->size_id)->get();
        $product_list = [];
        $product_item = [];
        foreach ($sizes as $SizeDetails) {
            $product_item['id'] = $SizeDetails->id;
            $product_item['general_price'] = $SizeDetails->general_price;
            $product_item['special_price'] = $SizeDetails->special_price;
            $product_item['product_color'] = ProductColor::where('product_size_id', $SizeDetails->id)->select('id', 'color')->get();
            $product_list[] = $product_item;
        }
        return $this->respond(
            200,
            trans('messages.products.list'),
            $product_list
        );

    }

}
