<?php

namespace App\Http\Controllers\API;

use App\Service;
use App\Slider;
use Illuminate\Http\Request;

class ServicesController extends APIController
{
    function __construct(Request $request)
    {
        $this->setLang($request->header('lang'));
    }

    // Show All Services

    function index()
    {
        $services = Service::select('id', 'service_name_' . $this->lang . ' as name', 'service_image')->get();
        $sliders = Slider::where('status', 1)->select('slider_image')->get();
        if ($services || $sliders) {
            return response()->json([
                'status' => 200,
                'message' => trans('validation.message'),
                'data' => [
                    'services' => $services,
                    'sliders' => $sliders,
                ]
            ]);
        }
        return $this->respondWithError(trans('validation.no_data'));

    }

}
