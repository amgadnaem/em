<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\UserReview\StoreReviewRequest;
use App\Repositories\User\UserRepository;
use App\UserReview;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserReviewController extends APIController
{
    public $repository;

    function __construct(Request $request, UserRepository $repository)
    {
        $this->repository = $repository;
        $this->setLang($request->header('lang'));

    }

    // add rate to products

    public function store(StoreReviewRequest $request)
    {
        $check_user = $this->repository->checkJwtUser($request);
        if (!$check_user) {
            return $this->respondWithError(trans('messages.auth.user_check'));
        }
        $check_user_rate = UserReview::where('user_id', $check_user->id)->where('product_id', $request->product_id)->exists();
        if (!$check_user_rate) {
            $add_rate = new UserReview();
            $add_rate->user_id = $check_user->id;
            $add_rate->product_id = $request->product_id;
            $add_rate->rate_value = $request->rate_value;
            $add_rate->save();
            if ($add_rate) {
                $count_rate = UserReview::where('product_id', $request->product_id)->avg('rate_value');
                return response()->json([
                    'status' => 200,
                    'message' => trans('messages.review.added'),
                    'rate' => $count_rate
                ]);
            } else {
                $this->respondWithError(trans('messages.something_went_wrong'));
            }
        } else {
            $edit_rate = UserReview::where('user_id', $check_user->id)->where('product_id', $request->product_id)->first();
            $edit_rate->rate_value = $request->rate_value;
            $edit_rate->save();
            if ($edit_rate) {
                $count_rate = UserReview::where('product_id', $request->product_id)->avg('rate_value');
                return response()->json([
                    'status' => 200,
                    'message' => trans('messages.review.edit'),
                    'rate' => $count_rate
                ]);
            } else {
                $this->respondWithError(trans('messages.something_went_wrong'));
            }
        }

    }

}
