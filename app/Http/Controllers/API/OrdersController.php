<?php

namespace App\Http\Controllers\API;

use App\Address;
use App\Country;
use App\Http\Requests\Order\StoreOrderRequest;
use App\Order;
use App\OrdersProducts;
use App\OrderStatus;
use App\PermanentUser;
use App\Product;
use App\ProductColorsSize;
use App\ProductSize;
use App\PromoCode;
use App\Repositories\Address\AddressRepository;
use App\Repositories\Cart\CartRepository;
use App\Repositories\Order\OrderRepository;
use App\Repositories\User\UserRepository;
use App\Setting;
use App\ShippingCompany;
use App\ShippingFee;
use App\User;
use App\UserCart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class OrdersController extends APIController
{
    protected $repository;
    protected $cartRepository;
    protected $addressRepository;
    protected $userRepository;


    public function __construct(Request $request,
                                OrderRepository $repository,
                                UserRepository $userRepository,
                                AddressRepository $addressRepository,
                                CartRepository $cartRepository)
    {
        $this->repository = $repository;
        $this->cartRepository = $cartRepository;
        $this->userRepository = $userRepository;
        $this->addressRepository = $addressRepository;
        $this->setLang($request->header('lang'));
    }


    public function confirmOrder(StoreOrderRequest $request)
    {
        $user_exist = $this->userRepository->checkJwtUser($request);
        if (!$user_exist) {
            return $this->respondWithError(trans('messages.auth.user_check'));
        }
        $complete_order = UserCart::where('user_id', $user_exist->id)->get();
        $cart_item_query = [];
        foreach ($complete_order as $cart) {
            $product = Product::where('id', $cart->product_id)->select('shop_id', 'general_price', 'special_price', 'hosted', 'type', 'shop_id')->first();
            $permanent = PermanentUser::where('user_id', $user_exist->id)->where('shop_id', $product['shop_id'])->pluck('id')->first();
            if (!empty($cart->size_id) && !empty($cart->color_id)) {
                if (($cart->hosted == 2) && ($cart->type == 1 || $cart->type == 3 || $cart->type == 4)) {
                    $cart_item['price'] = $product['general_price'];
                    $cart_item['total_price'] = $cart->qty * $product['general_price'];
                } else if ($permanent) {
                    $cart_item['price'] = $product['special_price'];
                    $cart_item['total_price'] = $cart->qty * $product['special_price'];
                } else {
                    $cart_item['price'] = $product['general_price'];
                    $cart_item['total_price'] = $cart->qty * $product['general_price'];
                }
            } else if (!empty($cart->size_id)) {
                $product_price = ProductColorsSize::where('size_id', $cart->size_id)->where('product_id', $cart->product_id)->select('general_price', 'special_price')->first();
                if ($permanent) {
                    $cart_item['price'] = $product_price['special_price'];
                    $cart_item['total_price'] = $cart->qty * $product_price['special_price'];
                } else {
                    $cart_item['price'] = $product_price['general_price'];
                    $cart_item['total_price'] = $cart->qty * $product_price['general_price'];
                }
            } else {
                $product_price = Product::where('id', $cart->product_id)->select('hosted', 'general_price', 'special_price')->first();
                if (($product_price->hosted == 2) && ($product_price->type == 3 || $product_price->type == 4)) {
                    $cart_item['price'] = $product['general_price'];
                    $cart_item['total_price'] = $cart->qty * $product['general_price'];
                } else {
                    if ($permanent) {
                        $cart_item['price'] = $product['special_price'];
                        $cart_item['total_price'] = $cart->qty * $product['special_price'];
                    } else {
                        $cart_item['price'] = $product['general_price'];
                        $cart_item['total_price'] = $cart->qty * $product['general_price'];
                    }
                }
            }
            $add_price = !empty($cart_item['price']) ? $cart_item['price'] : 0;
            $cart_item_query [] = ($cart->qty * $add_price);
        }
        $subtotal = array_sum($cart_item_query);
        if (count($complete_order) > 0) {
            $order_number = rand(100000, 999999);
            $order = new Order();
            $order->user_id = $user_exist->id;
            $order->shop_id = $product['shop_id'];
            $order->order_number = $order_number;
            $order->delivery_method = $request->delivery_method;
            $order->payment_method = $request->payment_method;
            $order->shipping_id = $request->shipping_id;
            $order->delivery_address_id = $request->delivery_address_id;
            $order->used_promo = !empty($request->promo_code) ? $request->promo_code : 0;

            if ($request->promo_code != 0) {
                $promo = PromoCode::where('id', $request->promo_code)->first();
                if ($promo['discount_type'] == 'fixed') {
                    $order->subtotal_fees = $subtotal - $promo->discount_amount;
                } elseif ($promo['discount_type'] == 'percentage') {
                    $order->subtotal_fees = $subtotal - (($subtotal * $promo->discount_amount) / 100);
                }
                if ($order->subtotal_fees < 0) {
                    $order->subtotal_fees = 0;
                }
            } else {
                $order->subtotal_fees = $subtotal;
            }
            if ($request->delivery_method == 3) {
                $order->shipping_fees = ShippingFee::where('company_id', $request->shipping_id)->pluck('shipping_fees')->first();
            } else {
                $order->shipping_fees = Setting::where('key', 'shipping')->pluck('value')->first();
            }
            $country_fees = Country::where('id', $user_exist->country_id)->pluck('tax')->first();
            $order->tax_fees = $subtotal - (($subtotal * $country_fees) / 100);
            $order->order_date = NOW();
            $order->save();
            foreach ($complete_order as $cart_item1) {
                $product = Product::where('id', $cart_item1->product_id)->select('shop_id', 'general_price', 'special_price', 'hosted', 'type', 'shop_id')->first();

                $permanent = PermanentUser::where('user_id', $user_exist->id)->where('shop_id', $product['shop_id'])->pluck('id')->first();
                if (!empty($cart_item1->size_id) && !empty($cart_item1->color_id)) {
                    if (($cart_item1->hosted == 2) && ($cart_item1->type == 1 || $cart_item1->type == 3 || $cart_item1->type == 4)) {
                        $cart_item['price'] = $product['general_price'];
                        $cart_item['total_price'] = $cart->qty * $product['general_price'];
                    } else if ($permanent) {
                        $cart_item['price'] = $product['special_price'];
                        $cart_item['total_price'] = $cart_item1->qty * $product['special_price'];
                    } else {
                        $cart_item['price'] = $product['general_price'];
                        $cart_item['total_price'] = $cart_item1->qty * $product['general_price'];
                    }
                } else if (!empty($cart_item1->size_id)) {
                    $product_price = ProductColorsSize::where('size_id', $cart_item1->size_id)->where('product_id', $cart_item1->product_id)->select('general_price', 'special_price')->first();
                    if ($permanent) {
                        $cart_item['price'] = $product_price['special_price'];
                        $cart_item['total_price'] = $cart_item1->qty * $product_price['special_price'];
                    } else {
                        $cart_item['price'] = $product_price['general_price'];
                        $cart_item['total_price'] = $cart_item1->qty * $product_price['general_price'];
                    }
                } else {

                    $product_price = Product::where('id', $cart_item1->product_id)->where('id', $cart_item1->product_id)->select('hosted', 'general_price', 'special_price')->first();

                    if (($product_price->hosted == 2) && ($product_price->type == 3 || $product_price->type == 4)) {
                        $cart_item['price'] = $product['general_price'];
                        $cart_item['total_price'] = $cart_item1->qty * $product['general_price'];
                    } else {
                        if ($permanent) {
                            $cart_item['price'] = $product['special_price'];
                            $cart_item['total_price'] = $cart_item1->qty * $product['special_price'];
                        } else {
                            $cart_item['price'] = $product['general_price'];
                            $cart_item['total_price'] = $cart_item1->qty * $product['general_price'];
                        }
                    }
                }
                $row = [
                    'product_id' => $cart_item1->product_id,
                    'order_id' => $order->id,
                    'order_qty' => $cart_item1->qty,
                    'price' => ($cart_item['price']),
                    'size_id' => $cart_item1->size_id,
                    'color_id' => $cart_item1->color_id,
                ];
                $create_products = OrdersProducts::create($row);
            }
            if (!empty($create_products)) {
                if ($request->delivery_method == 1) {
                    $this->repository->sendOrderToDelegate($user_exist, $product->shop_id, $order->id);
                } else if ($request->delivery_method == 3) {
                    $this->repository->sendOrderToShipping($user_exist, $product->shop_id, $order->id, $request->shipping_id);
                }
                $this->cartRepository->deleteAllCartItems($user_exist);
                return $this->respondWithMessage(trans('messages.order.completed'));
            } else {
                return $this->respondWithError(trans('messages.something_went_wrong'));
            }
        }
        return $this->respondWithError(trans('messages.something_went_wrong'));

    }


    // get list order
    public function listOrder(Request $request)
    {
        $check_user = $this->userRepository->checkJwtUser($request);
        if (!$check_user) {
            return $this->respondWithError(trans('messages.auth.user_check'));
        }
        $result = [];
        $orders = Order::where('user_id', $check_user->id)->orderBy('id', 'desc')->get();
        foreach ($orders as $row) {
            $key = $row->order_date;
            $id = $row->id;


            $order_date = date("F j, Y", strtotime($key));
            $result[$key]['id'] = $id;
            $result[$key]['order_date'] = $order_date;
            $result[$key]['orders'][] = array('id' => $row->id,
                'order_number' => $row->order_number,
                'date' => $row->order_date,
                'status' => OrderStatus::where('id', $row->order_status)->select('id', 'status_name_' . $this->lang . ' as name')->first(),
                'total_price' => (string)($row->subtotal_fees + $row->tax_fees + $row->shipping_fees));
        }
        $data = [];
        foreach ($result as $key => $value) {
            $data[] = $value;
        }

        return $this->respond(
            200,
            trans('messages.order.info'),
            $data
        );


    }

    public function listOrder11(Request $request)
    {
        $check_user = $this->userRepository->checkJwtUser($request);
        if (!$check_user) {
            return $this->respondWithError(trans('messages.auth.user_check'));
        }
        $result = [];
        $orders = Order::where('user_id', $check_user->id)->get();
        foreach ($orders as $row) {
            $key = $row->order_date;
            $order_date = date("F j, Y", strtotime($key));
            $result[$key]['order_date'] = $order_date;

            $result[$key]['orders'][] = array(
                'order_number' => $row->order_number,
                'date' => $row->order_date,
                'total_price' => (string)($row->subtotal_fees + $row->tax_fees + $row->shipping_fees),
                'status' => OrderStatus::where('id', $row->order_status)->pluck('status_name_' . $this->lang)->first()
            );

            return $order_id = Order::where('user_id', $check_user->id)->where('order_date', $key)->pluck('id')->toArray();
            $orders_products = OrdersProducts::whereIn('order_id', $order_id)->get();
            $product_list = [];
            foreach ($orders_products as $products) {
                $product_item = Product::where('id', $products->product_id)->select('name_' . $this->lang . '  as product_name', 'product_image')->first();
                $product_item['qty'] = $products->order_qty;
                $product_item['price'] = $products->price;
//                $product_item['size'] = $products->sizeName($products->size);
                $order_number = Order::where('id', $products->order_id)->pluck('order_number')->first();
                $product_item['order_number'] = $order_number;
                $product_list[] = $product_item;
            }
            $result[$key]['products'] = $product_list;


        }
        $data = [];
        foreach ($result as $key => $value) {
            $data[] = $value;
        }

        return $this->respond(
            200,
            trans('messages.order.info'),
            $data
        );
    }

    // order details
    public function OrderDetails(Request $request)
    {
        $type = $request->type;

        $order_id = $request->order_id;
        $orders = Order::where('id', $order_id)->orderBy('id', 'desc')->first();
        $data = $this->repository->getOrderProducts($orders, $type, $this->lang);
        return $this->respond(
            200,
            trans('messages.order.info'),
            $data
        );
    }
}
