<?php

namespace App\Http\Controllers\API;

use App\Driver;
use App\DriverOrder;
use App\Http\Requests\Order\Trip;
use App\Repositories\User\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DriverController extends APIController
{
    protected $repository;
    protected $UserRepository;
    protected $OrderRepository;

    public function __construct(Request $request, UserRepository $UserRepository)
    {
        $this->UserRepository = $UserRepository;


        $this->setLang($request->header('lang'));
    }

    function createTrip(Trip $request)
    {
        $check_user = $this->UserRepository->checkJwtUser($request);
        if (!$check_user) {
            return $this->respondWithError(trans('messages.auth.user_check'));
        }
        $order_number = rand(100000, 999999);
        $add_trip = new DriverOrder();
        $add_trip->user_id = $check_user->id;
        $add_trip->order_number = $order_number;
        $add_trip->sender_lat = $request->sender_lat;
        $add_trip->sender_lng = $request->sender_lng;
        $add_trip->sender_location = $request->sender_location;
        $add_trip->receive_lat = $request->receive_lat;
        $add_trip->receive_lng = $request->receive_lng;
        $add_trip->receive_location = $request->receive_location;
        $add_trip->order_date = NOW();
        $add_trip->save();
        if ($add_trip) {
            return response()->json([
                'status' => 200,
                'message' => trans('messages.order.trip'),
            ]);
        }
    }

    function sendTripToDriver(Request $request)
    {
        $check_user = $this->UserRepository->checkJwtDriver($request);
        if (!$check_user) {
            return $this->respondWithError(trans('messages.auth.user_check'));
        }
        $notification_data = Driver::selectRaw("( FLOOR(6371 * ACOS( COS( RADIANS($check_user->lat) ) * COS( RADIANS( lat ) ) * COS( RADIANS( lng ) - RADIANS($check_user->lng) ) + SIN( RADIANS($check_user->lat) ) * SIN( RADIANS( lat ) ) )) ) distance,
        id,lat,lng,first_name,last_name,car_name,car_color,car_number")
            ->havingRaw("distance <= 50")
            ->where('user_status', 1)
            ->where('status', 1)
            ->get();
        return $notification_data;

    }


    public function confirmOrderFromDriver(Request $request)
    {
        $check_driver = $this->UserRepository->checkJwtDriver($request);
        if (!$check_driver) {
            return $this->respondWithError(trans('messages.auth.user_check'));
        }
        $order_id = $request->order_id;

        $order = DriverOrder::where('id', $order_id)->first();
        $order->order_status = 2;
        $order->driver_id = $check_driver->id;
        if ($order->save()) {
            return response()->json([
                'status' => 200,
                'message' => trans('messages.order.confirm')
            ]);
        }
    }

    function getDistanceLatLng($lat1 , $lon1 , $lat2 , $lon2){
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        }
        else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            return ($miles * 1.609344);

        }
    }

    function location (Request $input)
    {
        $locations = $input->locations;
        $total_distance = 0;
        $waiting_time=0;
        if (count($locations) >= 4 )
        {
            for ($i = 0, $j = 1; $j < count($locations)-2; $i+=2, $j+=2) {
                $distanceKM =  $this->getDistanceLatLng($locations[$i],$locations[$j],$locations[$i+2],$locations[$j+2]);
                $total_distance += $distanceKM;
                $distanceMeter = $distanceKM * 1000;
                if($distanceMeter < 20)
                    $waiting_time += 5;
            }
        }
        dd($total_distance , $waiting_time);



    }


}
