<?php

namespace App\Http\Controllers\API;

use App\Repositories\User\UserRepository;
use App\Shop;
use App\ShopChat;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ChatController extends APIController
{
    protected $userRepository;


    public function __construct(Request $request, UserRepository $userRepository)
    {

        $this->userRepository = $userRepository;

        $this->setLang($request->header('lang'));

    }

    public function index(Request $request)
    {
        $type = $request->type;
        if ($type == 1) {
             $check_login = $this->userRepository->checkJwtUser($request);
            if (!$check_login) {
                return $this->respondWithError(trans('messages.auth.user_check'));
            }
            
                     $all_chats = DB::select("SELECT
	*
FROM
	shop_chats
WHERE
	shop_chats.shop_id = $request->shop_id  
	    and ( ( shop_chats.from = $check_login->id and shop_chats.to = $request->shop_id ) 
   or ( shop_chats.from = $request->shop_id and shop_chats.to = $check_login->id ) )
;");
        } else {
            
             $check_login = $this->userRepository->checkJwtShop($request);
            if (!$check_login) {
                return $this->respondWithError(trans('messages.auth.user_check'));
            }
                    $all_chats = DB::select("SELECT
	*
FROM
	shop_chats
WHERE
	shop_chats.shop_id = $check_login->id  
	    and ( ( shop_chats.from = $check_login->id and shop_chats.to = $request->shop_id ) 
   or ( shop_chats.from = $request->shop_id and shop_chats.to = $check_login->id ) )
;");
        }
  

        $chat_item = [];
        $chat_list = array();
        foreach ($all_chats as $chats) {
                        $user = User::where('id', $chats->from)->orwhere('id', $chats->to)->where('type', 1)->select('id', 'first_name as name', 'user_image')->first();
            $shop = Shop::where('id', $chats->from)->orwhere('id', $chats->to)->where('type', 3)->select('id', 'name_app_' . $this->lang . ' as  name', 'shop_image')->first();
            $chat_item['id'] = $chats->id;
             $chat_item['message'] = $chats->message;
            $chat_item['type'] = $chats->type;
            $chat_item['image'] = !empty($chats->image) ? 'https://em.my-staff.net/images/shopChat/'.$chats->image : null ;
             $chat_item['date'] =  \Carbon\Carbon::parse($chats->created_at)->diffForHumans();
            $chat_list[] = $chat_item;
        }
        if ($chat_list) {
            return response()->json([
                'status' => 200,
                'message' => trans('validation.message'),
                'chats' => $chat_list,
                'user' => $user,
                'shop' => $shop,
            ]);

        }

        return response()->json([
            'status' => 401,
            'message' => trans('validation.no_data'),
        ]);

    }

    public function createShopChat(Request $request)
    {
        $type = $request->type;
        if ($type == 1) {
            $check_login = $this->userRepository->checkJwtUser($request);
            if (!$check_login) {
                return $this->respondWithError(trans('messages.auth.user_check'));
            }
            $create_message = ShopChat::create([
                'from' => $check_login->id,
                'to' => $request->to,
                'shop_id' => $request->shop_id,
                'type' => 1,
                'message' => $request->message,
                'image' => $request->image
            ]);


        } else {

            $check_login = $this->userRepository->checkJwtShop($request);
            if (!$check_login) {
                return $this->respondWithError(trans('messages.auth.user_check'));
            }
            $create_message = ShopChat::create([
                'from' => $check_login->id,
                'to' => $request->to,
                'shop_id' => $check_login->id,
                'type' => 3,
                'message' => $request->message,
                'image' => $request->image
            ]);
        }
      
             $this->sendNotify($request->shop_id,$check_login->id,$type);

        if ($create_message) {
            return response()->json([
                'status' => 200,
                'message' => trans('messages.auth.send message'),
            ]);
        }
        return $this->respondWithError(trans('messages.something_went_wrong'));

    }
    
    
    function sendNotify($shop_id,$from, $type)
    {
        define('API_ACCESS_KEY', 'AAAAJIhBdDE:APA91bHLPrry6EpPfm8cSqvb0Vg5d3pa22K5i_e3uPTKev8JR_7SsVjGktN-paxPXo7k26OYB4oJbWZN6RxkOrQ2KCLvLH_ifqUyfRJow7EG4doyamUVXVlGK8aRnaKPVqIib3md5MoU');
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
     if($type == 1) {
            $send_shop = Shop::where('id', $shop_id)->pluck('firebase_token')->first();

        } else {
            $send_shop = User::where('id', $shop_id)->pluck('firebase_token')->first();

        }


        if ($send_shop != NULL) {
 if($type == 1) {
                $notification = [
                    'title' => 'رساله جديده',
                    'body' => 'لديك رساله جديده من المستخدم فى شات المتجر',
                    'sender_id' => (int)$from,
                    'type'=>6
                ]; 
            }else if($type == 3) {
                $notification = [
                    'title' => 'رساله جديده',
                    'body' => 'لديك رساله جديده من صاحب المتجر',
                    'sender_id' => (int)$from,
                    'type'=>6
                ];
            }

            $extraNotificationData = ["message" => $notification, "moredata" => 'dd'];
            $fcmNotification = [
                'to' => $send_shop, //single token
                'data' => $extraNotificationData
            ];
            $headers = [
                'Authorization: key=' . API_ACCESS_KEY,
                'Content-Type: application/json'
            ];
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $fcmUrl);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
            $result = curl_exec($ch);
            curl_close($ch);
            if ($result) {
                return true;
            } else {
                return false;
            }
        }
    }


}
