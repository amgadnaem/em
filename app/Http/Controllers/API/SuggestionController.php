<?php

namespace App\Http\Controllers\API;

use App\Repositories\User\UserRepository;
use App\Suggestion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class SuggestionController extends APIController
{
    protected $userRepository;


    public function __construct(Request $request, UserRepository $userRepository)
    {

        $this->userRepository = $userRepository;

        $this->setLang($request->header('lang'));

    }

  public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'comment' => 'required',
            ]
        );
        if ($validator->fails()) {
            return $this->respondWithError($validator->errors()->first());
        }
        $type = $request->type;
        if($type == 1 || $type == 2) {
            $auth_login = $this->userRepository->checkJwtUser($request);
        } else {
            $auth_login = $this->userRepository->checkJwtShop($request);
        }

        if (isset($auth_login) && !empty($auth_login)) {
            $suggestions = Suggestion::create([
                'user_id' => $auth_login->id,
                'comment' => $request->comment,
                'title' => $request->title,
                'type' => $request->type,
            ]);

        } else {

            $suggestions = Suggestion::create([
                'user_id' => 0,
                'comment' => $request->comment,
                'title' => $request->title,
                'type' => $request->type,
            ]);
        }


        if ($suggestions) {
            return $this->respondWithMessage(trans('messages.suggestion.added'));
        } else {
            $this->respondWithError(trans('messages.something_went_wrong'));
        }
    }
}
