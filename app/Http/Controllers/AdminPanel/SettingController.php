<?php

namespace App\Http\Controllers\AdminPanel;

use App\Repositories\Setting\SettingRepository;
use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    //

    protected $repository;

    public function __construct(SettingRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $setting = $this->repository->getAll();
        return view('AdminPanel.backend.pages.setting.index', compact('setting'));
    }

    public function edit($setting_id)
    {
        $setting = $this->repository->getAll();
        return view('AdminPanel.backend.pages.setting.edit', compact('setting'));
    }

    public function UpdateSetting(Request $request)
    {
        $this->repository->updateSettings($request->except('_method', '_token', 'range', 'amount'));
        session()->flash('success', __('تم تعديل  البيانات بنجاح'));
        return redirect()->route('settings');

    }

    function socialMedia()
    {
        $socialMedia = Setting::where('status', 2)->orwhere('status',0)->select('id', 'key', 'value','status')->get();
        return view('AdminPanel.backend.pages.setting.socialMedia', compact('socialMedia'));

    }

    //Add city

    function AddSocialMedia(Request $request)
    {
        $validate = $request->validate(
            [
                'key' => 'required',
                'value' => 'required',
            ],
            [
                'key.required' => trans('validation.key'),
                'value.required' => trans('validation.value'),
            ]);
        $add_media = array(
            'key' => $request->key,
            'value' => $request->value,
            'status' => 2
        );

        $insert_media = Setting::create($add_media);
        $insert_media->save();
        if ($insert_media) {
            session()->flash('success', __('تم اضافه بيانات السوشيال ميديا بنجاح'));
            return redirect()->back();
        }

    }

    //Edit city

    public function EditMedia($id, Request $request)
    {
        $media = $this->updateMedia($id, $request->all());
        session()->flash('success', __('تم تعديل بيانات السوشيال ميديا بنجاح'));
        return redirect()->back();
    }

    public function updateMedia($id, $input)
    {
        $edit_media = Setting::where('id', $id)->first();
        $edit_media->key = $input['key'];
        $edit_media->value = $input['value'];
        $edit_media->save();
        return $edit_media;
    }

    public function updateStatusMedia($id, $status)
    {
        $edit_status_media = Setting::where('id', $id)->first();
        $edit_status_media->status = $status;
        $edit_status_media->save();
        session()->flash('success', __('تم تغيير حاله السوشيال ميديا بنجاح'));

    }
}
