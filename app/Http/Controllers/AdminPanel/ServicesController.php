<?php

namespace App\Http\Controllers\AdminPanel;

use App\Service;
use App\Shop;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServicesController extends Controller
{
    // Show services

    function index()
    {
        $services = Service::all();
        return view('AdminPanel.backend.pages.services.index', compact('services'));
    }

    // Update service

    public function updateService($id, Request $request)
    {
        $EditService = Service::whereId($id)->first();
        $EditService->service_name_en = $request->service_name_en;
        $EditService->service_name_ar = $request->service_name_ar;
        $EditService->service_name_kur = $request->service_name_kur;
        $EditService->save();

        if ($request->service_image) {
            $EditService->service_image = $request->service_image;
            $EditService->save();
        }
        session()->flash('success', __('تم تعديل بيانات القسم بنجاح'));
        return redirect()->back();

    }


    function AddService(Request $request)
    {

        $validate = $request->validate(
            [
                'service_name_en' => 'required',
                'service_name_ar' => 'required',
                'service_name_kur' => 'required',
                'type' => 'required',
            ],
            [
                'service_name_en.required' => trans('validation.service_name_en'),
                'service_name_ar.required' => trans('validation.service_name_ar'),
                'service_name_kur.required' => trans('validation.service_name_kur'),
            ]);

        $add_service = array(
            'service_name_en' => $request->service_name_en,
            'service_name_ar' => $request->service_name_ar,
            'service_name_kur' => $request->service_name_kur,
            'type' => $request->type,
        );
        $insert_service = Service::create($add_service);
        $insert_service->save();
        if ($insert_service) {
            if ($request->service_image) {
                $insert_service->service_image = $request->service_image;
                $insert_service->save();
            }
            session()->flash('success', __('تم اضافه قسم  بنجاح'));
            return redirect()->back();
        }
    }

    public function updateStatus($id, $status)
    {
        $status_service = Service::where('id', $id)->first();
        $status_service->status = $status;
        $status_service->save();
        session()->flash('success', __('تم تغيير حاله القسم  بنجاح'));

    }

    function shops_service($service_id)
    {
        $serviceShops = Shop::where('service_id', $service_id)->get();
        $showServices = Service::where('id', $service_id)->pluck('service_name_ar')->first();

        return view('AdminPanel.backend.pages.services.shops', compact('serviceShops', 'showServices'));

    }

    // delete service

    function destroy($id)
    {
        Service::where('id', $id)->delete();
        session()->flash('success', __('تم حذف بيانات القسم بنجاح'));
        return redirect()->back();
    }
}
