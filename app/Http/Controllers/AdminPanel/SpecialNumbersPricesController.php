<?php

namespace App\Http\Controllers\AdminPanel;

use App\SpecialNumberCost;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class SpecialNumbersPricesController extends Controller
{
    // show Prices

    function index()
    {

        $prices = SpecialNumberCost::orderBy('id','desc')->get();
        return view('AdminPanel.backend.pages.specialNumbers.prices', compact('prices'));
    }



    //Add Price
    function AddPrice(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'price' => 'required',
            'number_of_digits' => 'required',
            'description' => 'required|min:2',
        ],[
            'price.required' => 'من فضلك أدخل السعر',
            'number_of_digits.required' => 'من فضلك أدخل عدد الارقام',
            'description.required' => 'من فضلك أدخل الوصف',
            'description.min' => 'من فضلك أدخل الوصف حرفين علي الأقل',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $add_price = new SpecialNumberCost();
        $add_price->price = $request->price;
        $add_price->number_of_digits = $request->number_of_digits;
        $add_price->description = $request->description;
        $add_price->save();
        if ($add_price) {
            session()->flash('success', __('تم اضافه السعر بنجاح'));
            return redirect()->back();
        }
    }

    // Update Price
    public function EditPrice($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'price' => 'required',
            'description' => 'required|min:2',
        ],[
            'price.required' => 'من فضلك أدخل السعر',
            'description.required' => 'من فضلك أدخل الوصف',
            'description.min' => 'من فضلك أدخل الوصف حرفين علي الأقل',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $edit_price = SpecialNumberCost::whereId($id)->first();
        $edit_price->price = $request->price;
        $edit_price->number_of_digits = $request->number_of_digits;
        $edit_price->description = $request->description;
        $edit_price->save();

        session()->flash('success', __('تم تعديل السعر بنجاح'));
        return redirect()->back();
    }

    // delete Price
    function destroy($id)
    {
        SpecialNumberCost::where('id', $id)->delete();
        session()->flash('success', __('تم حذف السعر بنجاح'));
        return redirect()->back();
    }
}
