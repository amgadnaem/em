<?php

namespace App\Http\Controllers\AdminPanel;


use App\City;
use App\Country;
use App\HostProduct;
use App\Order;
use App\PermanentUser;
use App\Product;
use App\Service;
use App\Setting;
use App\Shop;
use App\ShopCategories;
use App\SpecialNumber;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class ShopsController extends Controller
{

    //  Show All shops
    function AllShops()
    {
        $all_shops = Shop::where('shop_status', 1)->with('categories')->get();
        foreach ($all_shops as $shop) {
            $shop->country = Country::where('id',$shop->country_id)->pluck('country_name_ar')->first();
            $shop->city = City::where('id',$shop->city_id)->pluck('city_name_ar')->first();
        }
        $shopRequest = Shop::where('shop_status', 0)->get();
        return view('AdminPanel.backend.pages.shops.index', compact('all_shops', 'shopRequest'));
    }

    //Show Category
    function shop_details($id)
    {

        $CategoryShop = ShopCategories::where('shop_id', $id)->get();
        $PermanentUser = PermanentUser::where('shop_id', $id)->get();
        $product_host = HostProduct::where('to_shop', $id)->get();
        $product_shared = HostProduct::where('from_shop', $id)->get();
        $shop_orders = Order::whereShopId($id)->get();
        return view('AdminPanel.backend.pages.shops.Category', compact('CategoryShop', 'products', 'shop_orders', 'PermanentUser', 'product_host', 'product_shared'));

    }

    function CategoryProducts($category_id)
    {
        $products = Product::where('category_id', $category_id)->get();
        return view('AdminPanel.backend.pages.shops.Products', compact('products'));
    }

    //Show Products Shop from Category

    function ShopProducts($id)
    {
        $CategoryShop = Shop::find($id)->categories()->select('categories.id as id', 'categories.category_name_en as cat_en', 'categories.category_name_ar as cat_ar')->get();

        return view('AdminPanel.backend.pages.shops.Category', compact('CategoryShop'));

    }

    function editShop($id)
    {
        $services = Service::where('status', 1)->get();
        $editShop = Shop::where('id', $id)->first();

        return view('AdminPanel.backend.pages.shops.editShop', compact('editShop', 'services'));

    }

    // Active or dis active shops
    public function updateStatusShop($id, $status)
    {
        $shops = Shop::whereId($id)->first();
        $shops->status = $status;
        $shops->save();
        session()->flash('success', __('تم تغيير فى بيانات المتجر بنجاح'));
        return redirect()->back();

    }

    // accept account shop
    public function acceptShop($id, $status)
    {
        $shops = Shop::whereId($id)->first();
        $shops->shop_status = $status;
        $shops->save();
        session()->flash('success', __('تم قبول طلب المتجر بنجاح'));
        return redirect()->back();

    }


    function index()
    {
        $services = Service::where('status', 1)->get();
        $shipping = Setting::where('key', 'shipping')->pluck('value')->first();
        $countries = Country::where('status', 1)->get();
        $special_numbers = SpecialNumber::where('status', 1)->get();

        return view('AdminPanel.backend.pages.shops.add', compact('shipping', 'services', 'countries', 'special_numbers'));
    }

//Add shops
    function AddShop(Request $request, $shop_image = null, $ID_image = null, $license = null)
    {
        $check_email = Shop::where('phone_number', $request->phone_number)->orwhere('ID_number', $request->ID_number)->first();
        if ($check_email) {
            session()->flash('success', __('رقم الهاتف موجود من قبل'));
            return redirect()->back();
        }
        if (!empty($request->special_number)) {
            $shop_number = $request->special_number;
            $special_number = SpecialNumber::where('id', $request->special_number)->first();
            $special_number->status = 0;
            $special_number->save();
        } else {
            $shop_number = rand(1111, 9999);
        }
        $jwt_token = Str::random(25);
        $password = Hash::make($request->password);
        $add_shop = new Shop();
        $add_shop->shop_number = $shop_number;
        $add_shop->name_app_en = $request->name_app;
        $add_shop->name_app_ar = $request->name_app;
        $add_shop->name_app_kur = $request->name_app;

        $add_shop->name_en = $request->real_name;
        $add_shop->name_ar = $request->real_name;
        $add_shop->name_kur = $request->real_name;

        $add_shop->phone_number = $request->phone_number;
        $add_shop->shop_status = 1;
        QrCode::format('png')->size(500)->generate($request->phone_number, public_path('images/shops/qrCode/' . $request->phone_number . '.png'));
        $add_shop->qr_code = $request->phone_number;
        $add_shop->password = $password;
        $add_shop->country_id = $request->country_id;
        $add_shop->city_id = $request->city_id;
        $add_shop->lat = $request->lat;
        $add_shop->lng = $request->lng;
        $add_shop->location = $request->location;
        $add_shop->type = 3;
        $add_shop->vip = $request->vip;
        $add_shop->Subscribe_end = $request->subscribe_end;
        $add_shop->service_id = $request->service_id;
        $add_shop->shop_image = $request->shop_image;
        $add_shop->ID_image = $request->ID_image;
        $add_shop->ID_number = $request->ID_number;
        $add_shop->shop_license = $request->shop_license;
        $add_shop->desc_ar = $request->description;
        $add_shop->desc_en = $request->description;
        $add_shop->desc_kur = $request->description;
        $add_shop->jwt_token = $jwt_token;
        $add_shop->save();
        if ($add_shop) {
            $add_shop->shop_image = $shop_image;
            $add_shop->save();
        }
        if ($ID_image) {
            $add_shop->ID_image = $ID_image;
            $add_shop->save();
        }
        if ($license) {
            $add_shop->shop_license = $license;
            $add_shop->save();
        }
        //If shop saved successfully, then return true
        if ($add_shop) {
            $add_shop->assignRole('shop');

            session()->flash('success', __('تم اضافه متجر بنجاح'));
            return redirect()->back();
        }
    }

    public function updateShop($id, Request $request)
    {
        $shop = Shop::where('id', $id)->first();
        $password = Hash::make($request->password);
        if (!empty($request->password)) {
            $shop->password = '';
            $shop->password = $password;
        }
        $shop->name_app_en = $request->name_app_en;
        $shop->name_app_ar = $request->name_app_ar;
        $shop->name_app_kur = $request->name_app_kur;
        $shop->Subscribe_end = $request->subscribe_end;
        $shop->service_id = $request->service_id;

        $shop->vip = $request->vip;
        $shop->desc_ar = $request->desc_ar;
        $shop->desc_en = $request->desc_en;
        $shop->desc_kur = $request->desc_kur;
        $shop->save();
        if ($request->shop_image) {
            $shop->shop_image = $request->shop_image;
            $shop->save();
        }

        session()->flash('success', __('تم تعديل البيانات بنجاح'));
        return redirect()->route('shops');
    }

    // edit subscribe date

    function editSubscribe(Request $request, $id)
    {
        $edit_shops = Shops::where('id', $id)->first();
        $edit_branch = Shops::where('parent_id', $id)->first();
        $edit_shops->Subscribe_end = $request->subscribe_end;
        $edit_shops->save();
        if (!empty($edit_branch->parent_id)) {
            $edit_branch->Subscribe_end = $request->subscribe_end;
            $edit_branch->save();
        }

        session()->flash('success', __('تم تعديل اشتراك المتجر بنجاح'));
        return redirect()->back();
    }

    function destroyShop($id)
    {
        Shop::where('id', $id)->delete();
        session()->flash('success', __('تم حذف  المتجر بنجاح'));
        return redirect()->back();
    }
}
