<?php

namespace App\Http\Controllers\AdminPanel;

use App\City;
use App\Country;
use App\Driver;
use App\Helper\AppHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class DriverController extends Controller
{
    function index()
    {
        $drivers = Driver::get();
        $countries = Country::get();
        $data['drivers'] = $drivers;
        $data['countries'] = $countries;
        return view('AdminPanel.backend.pages.drivers.index', compact('data'));
    }

    function getCities(Request $request)
    {
        $cities = City::where('country_id', $request->country_id)->get();
        if (count($cities) > 0) {
            echo '<option value="">اختر اسم المدينه</option>';
            foreach ($cities as $row) {
                echo "<option value='" . $row->id . "'>" . $row->city_name_ar . "</option>";
            }
        } else {
            echo '<option value="">لا يوجد مدن تابعه لهذه الدوله</option>';
        }

    }
    public function create()
    {
        $data['countries'] = Country::get();
        return view('AdminPanel.backend.pages.drivers.create', compact('data'));
    }

    public function store(Request $request)
    {
        $check_phone = Driver::where('phone_number',$request->phone_number)->first();
        if ($check_phone) {
            session()->flash('success', __(' رقم الهاتف موجود من قبل'));
            return redirect()->back();
        }
        $driver_number = rand(111111, 999999);
        $driver = Driver::create(array_merge($request->all(),['jwt_token' => Str::random(25), 'driver_number' => $driver_number]));
//        if ($request->delegate_image) {
//            $AdminAccount->user_image = $request->delegate_image;
//            $AdminAccount->save();
//        }
        if ($driver){
            session()->flash('success', __('تم اضافه السائق بنجاح'));
        }else{
            session()->flash('error', __('حدث خطأ ما برجاء التأكد من البيانات'));
        }
        return redirect()->route('drivers');
    }

    public function edit(Request $request)
    {
        $data['driver'] = Driver::where('id',$request->driver_id)->first();
        $data['countries'] = Country::get();
        return view('AdminPanel.backend.pages.drivers.edit', compact('data'));
    }

    public function update(Request $request)
    {
//        dd($request->all());
        $check_phone = Driver::where('phone_number',$request->phone_number)->where('id','!=',$request->driver_id)->first();
        if ($check_phone) {
            session()->flash('success', __(' رقم الهاتف موجود من قبل'));
            return redirect()->back();
        }
        $driver = Driver::where('id',$request->driver_id)->first();
        if (isset($driver)){
            if ($request->has('driver_image') && is_file($request->driver_image)){
                if (!empty($driver->driver_image)){
                    AppHelper::unlinkFile($driver->getOriginal('driver_image'), 'drivers');
                }
            }
            $driver->update($request->except(['_token','driver_id']));
        }
        if ($driver){
//            session()->flash('success', __('تم تعديل بيانات السائق بنجاح'));
            return redirect()->route('drivers')->with('success', __('تم تعديل بيانات السائق بنجاح'));
        }else{
//            session()->flash('error', __('حدث خطأ ما برجاء التأكد من البيانات'));
            return redirect()->route('drivers')->with('error', __('حدث خطأ ما برجاء التأكد من البيانات'));
        }

    }

    public function changeStatus(Request $request)
    {
        $driver = Driver::where('id',$request->driver_id)->first();
        $driver->status = $request->status;
        if ($driver->save()){
            if ($driver->status == 1){
                return response()->json(['success' => 'تم تفعيل السائق بنجاح']);
            }
            return response()->json(['success' => 'تم إلغاء تفعيل السائق بنجاح']);
        }else{
            return response()->json(['error' => 'حدث خطأ ما !']);
        }
    }

    public function delete(Request $request)
    {
        $driver = Driver::where('id',$request->driver_id)->first();
        if (!empty($driver->getOriginal('driver_image'))){
            AppHelper::unlinkFile($driver->getOriginal('driver_image'), 'drivers');
        }
        if ($driver->delete()){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }
}
