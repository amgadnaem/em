<?php

namespace App\Http\Controllers\AdminPanel;

use App\Driver;
use App\Order;
use App\OrderStatus;
use App\Shop;
use App\Slider;
use App\Suggestion;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    //

    function Dashboard()
    {
        $Users = User::where('type', 1)->where('status', 1)->count();
        $orders = Order::count();
        $allSliders = Slider::where('status', 1)->orderBy('id', 'desc')->get();
        $all_orders = Order::where('order_status', '!=', 0)->get();
        $order_status = OrderStatus::all();
        $delegate = User::where('type', 2)->where('status', 1)->count();
        $shipping = \App\ShippingCompany::where('company_status', 1)->where('status', 1)->count();
        $drivers = Driver::where('user_status', 1)->where('status', 1)->count();
        $shops = Shop::where('status', 1)
            ->where('status', 1)
            ->where("Subscribe_end", '>', Now())
            ->count();
        $users_charts = DB::SELECT("select id, count(*) as count, date(created_at) as date from users WHERE  type = 1 and date(created_at) >= DATE(NOW()) - INTERVAL 7 DAY GROUP BY date(created_at)");
        $delegate_charts = DB::SELECT("select id, count(*) as count, date(created_at) as date from users WHERE  type = 2 and date(created_at) >= DATE(NOW()) - INTERVAL 7 DAY GROUP BY date(created_at)");
        $numbers_Of_Shops = DB::SELECT("select id, count(*) as count, date(created_at) as date from shops WHERE date(created_at) >= DATE(NOW()) - INTERVAL 7 DAY  GROUP BY date(created_at)");
        $suggestions = Suggestion::with('user')->orderBy('id', 'desc')->take(5)->get();


        return view('AdminPanel.backend.pages.home.index')
            ->with('users', $Users)
            ->with('delegate', $delegate)
            ->with('shops', $shops)
            ->with('orders', $orders)
            ->with('allSliders', $allSliders)
            ->with('all_orders', $all_orders)
            ->with('allSliders', $allSliders)
            ->with('users_charts', $users_charts)
            ->with('delegate_charts', $delegate_charts)
            ->with('shipping', $shipping)
            ->with('drivers', $drivers)
            ->with('of_shops', $numbers_Of_Shops)
            ->with('suggestions', $suggestions);

    }
}
