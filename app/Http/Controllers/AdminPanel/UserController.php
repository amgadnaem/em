<?php

namespace App\Http\Controllers\AdminPanel;

use App\Order;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    //Show all User
    function index()
    {
        $all_users = User::where('type', 1)->get();
        return view('AdminPanel.backend.pages.user.index', compact('all_users'));
    }

    //Active or dis active product

    public function UpdateStatus($id, $status)
    {
        $update_account = User::where('id', $id)->first();
        $update_account->status = $status;
        $update_account->save();
        session()->flash('success', __('تم ايقاف حساب المستخدم بنجاح'));
        return redirect()->back();

    }

    // delete User

    function destroy($id)
    {
        User::where('id', $id)->delete();
        session()->flash('success', __('تم حذف بيانات المستخدم بنجاح'));
        return redirect()->back();
    }

    function users_orders($id)
    {
        $all_orders = Order::where('user_id', $id)->where('order_status','>=',1)->get();
        return view('AdminPanel.backend.pages.user.orders', compact('all_orders'));
    }

}
