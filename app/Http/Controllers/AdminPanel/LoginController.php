<?php

namespace App\Http\Controllers\AdminPanel;

use App\Shop;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    //

    function showLogin()
    {
        return view('AdminPanel.login');
    }

    public function login(Request $request)
    {
        $deaActive_account = array('phone_number' => $request->phone_number, 'password' => $request->password, 'status' => 0);


        $admin = array('phone_number' => $request->phone_number, 'password' => $request->password, 'type' => 6);
        $supervisor_admin = array('phone_number' => $request->phone_number, 'password' => $request->password, 'type' => 4);

        $super = array('phone_number' => $request->phone_number, 'password' => $request->password, 'type' => 5);
        $owner_shop = Shop::where('phone_number', $request->phone_number)->where('type', $request->type)->first();
        $shipping_company = "";

        if ($request->type == 2) {
            $shipping_company = \App\ShippingCompany::where('phone_number', $request->phone_number)->first();
        }

        if (!Auth::attempt($deaActive_account)) {
            if (Auth::attempt($admin) || Auth::attempt($supervisor_admin)) {
                Session::flash('loggedin', '1');
                Session::put('frontSession', Auth::user());

                return redirect()->route('admin.dashboard');
            }
            if (Auth::attempt($super)) {
                Session::flash('loggedin', '1');
                Session::put('frontSession', Auth::user()->shop_id);
                Session::put('admin_id', Auth::user()->id);
                Session::put('type', Auth::user()->type);
                return redirect()->route('shop.dashboard');
            } else if ($owner_shop) {
                if (password_verify($request->password, $owner_shop->password)) {
                    Session::flash('loggedin', '1');
                    Session::put('frontSession', $owner_shop->id);
                    Session::put('shop_image', $owner_shop->shop_image);
                    Session::put('shop_name', $owner_shop->name_app_ar);
                    Session::put('service_id', $owner_shop->service_id);
                    Session::put('type', $owner_shop->type);
                    Session::put('phone', $owner_shop->phone_number);
                    return redirect()->route('shop.dashboard');
                } else {
                    session()->flash('failed', __('خطا فى كلمه المرور'));
                    return redirect()->back();
                }
            } else if ($shipping_company) {
                if (password_verify($request->password, $shipping_company->password)) {
                    Session::flash('loggedin', '1');
                    Session::put('company_id', $shipping_company->id);
                    Session::put('type', $shipping_company->type);
                    Session::put('company_image', $shipping_company->company_image);
                    Session::put('company_name', $shipping_company->company_ar);
                    Session::put('phone', $shipping_company->phone_number);
                    return redirect()->route('company.dashboard');
                } else {
                    session()->flash('failed', __('خطا فى كلمه المرور'));
                    return redirect()->back();
                }
            } else {
                session()->flash('failed', __('تاكد من صحه البيانات'));
                return redirect()->back();
            }
        } else {
            session()->flash('failed', __('تم ايقاف حسابك من قبل الادمن'));
            return redirect()->back();

        }

    }

// logout
    public
    function logout()
    {
//        Auth::logout();
        Session::forget('frontSession');
        return redirect('/admin/login');
    }
}
