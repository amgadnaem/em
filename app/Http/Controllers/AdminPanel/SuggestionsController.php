<?php

namespace App\Http\Controllers\AdminPanel;

use App\Suggestion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SuggestionsController extends Controller
{
    // show all suggestions

    function index()
    {

        $suggestions = Suggestion::with('user')->orderBy('id', 'desc')->get();

        return view('AdminPanel.backend.pages.suggestion.index', compact('suggestions'));
    }

    // delete category

    function destroy($id)
    {
        Suggestion::where('id', $id)->delete();
        session()->flash('success', __('تم حذف بيانات الشكوى بنجاح'));
        return redirect()->back();
    }
}
