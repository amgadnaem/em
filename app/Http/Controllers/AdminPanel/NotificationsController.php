<?php

namespace App\Http\Controllers\AdminPanel;

use App\AdminNotification;
use App\Country;
use App\Helper\AppHelper;
use App\Shop;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class NotificationsController extends Controller
{
    public function index()
    {
        $notifications = AdminNotification::get();
        $countries = Country::where('status', 1)->get();
        $data['notifications'] = $notifications;
        $data['countries'] = $countries;
        $data['title'] = 'رسائل النظام';
        $users = User::where('type', 1)->get();
        $shops = Shop::get();
        foreach ($users as $user) {
            $user->type = 'user';
        }
        foreach ($shops as $shop) {
            $shop->type = 'shop';
        }
        $data['users'] = $users;
        $data['shops'] = $shops;
        return view('AdminPanel.backend.pages.adminNotifications.index', compact('data'));
    }

    // get accounts depends on countries
    function accounts_from_countries(Request $request)
    {
        $users = User::where('type', 1)->where('status', 1)->where('country_id', $request->country_id)->orderby('first_name','asc')->get();
        if (count($users) > 0) {
            echo '<option disabled selected value="">اختر المرسل اليه</option>';
            echo '<option  value="users">كل المستخدمين </option>';
            echo '<option value="shops">كل المتاجر</option>';
            echo '<option value="delegates">كل المناديب</option>';
            echo '<option value="shops">كل شركات الشحن</option>';
            echo '<option value="shops">كل السائقيين</option>';
            echo '<optgroup label="المستخدمين">';
            foreach ($users as $row) {
                echo "<option value='" . $row->id . "'>" . $row->first_name . "</option>";
                echo '</optgroup>';
                echo '<hr/>';
            }
        } else {
            echo '<option value="">لا يوجد مستخدمين </option>';
        }
        $shops = Shop::where("Subscribe_end", '>', Now())
            ->where('status', 1)
            ->where('shop_status', 1)
            ->where('country_id', $request->country_id)
            ->orderby('name_app_ar','asc')
            ->get();
        if (count($shops) > 0) {
            echo '<optgroup label="المتاجر">';
            foreach ($shops as $row) {
                echo "<option value='" . $row->id . "'>" . $row->name_ar . "</option>";
                echo '</optgroup>';
                echo '<hr/>';
            }
        } else {
            echo '<option value="">لا يوجد متاجر </option>';
        }
        $delegates = User::where('type', 2)
            ->where('confirm_account', 1)
            ->where('status', 1)
            ->where('country_id', $request->country_id)
            ->orderby('first_name','asc')
            ->get();
        if (count($delegates) > 0) {
            echo '<optgroup label="المناديب">';
            foreach ($delegates as $row) {
                echo "<option value='" . $row->id . "'>" . $row->first_name . "</option>";
                echo '</optgroup>';
                echo '<hr/>';
            }
        } else {
            echo '<option value="">لا يوجد مناديب </option>';
        }

        $shipping_companies = \App\ShippingCompany::where('company_status', 1)
            ->where('country_id', $request->country_id)
            ->where('status', 1)->orderby('company_ar','asc')->get();
        if (count($shipping_companies) > 0) {
            echo '<optgroup label="شركات الشحن">';
            foreach ($shipping_companies as $row) {
                echo "<option value='" . $row->id . "'>" . $row->company_ar . "</option>";
                echo '</optgroup>';
                echo '<hr/>';
            }
        } else {
            echo '<option value="">لا يوجد شركات شحن </option>';
        }


    }


    public function Send(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'message' => 'required',
        ], [
            'message.required' => 'من فضلك أدخل الرد',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $user_type = explode('|', $request->person);
        if ($user_type['0'] == 'users') {

            $users_token = User::where('type', 1)->where('status',1)->where('country_id',$request->country_id)->pluck('firebase_token')->toArray();
//            $users = User::get();
            AppHelper::send_to_user($users_token, $request, '10', "");
//            foreach ($users as $user){
//                Notification::create([
//                    'user_id' => $user->id,
//                    'title' => 'رسالة من مدير النظام',
//                    'body' => $request->message
//                ]);
//            }
            AdminNotification::create([
                'title' => $request->title,
                'message' => $request->message,
                'type' => 0  // All Users
            ]);
        }  if ($user_type['0'] == 'All_delegate') {
        $users_token = User::where('type', 2)->where('status',1)->pluck('firebase_token')->toArray();
//            $users = User::get();
        AppHelper::send_to_user($users_token, $request, '10', "");
//            foreach ($users as $user){
//                Notification::create([
//                    'user_id' => $user->id,
//                    'title' => 'رسالة من مدير النظام',
//                    'body' => $request->message
//                ]);
//            }
        AdminNotification::create([
            'title' => $request->title,
            'message' => $request->message,
            'type' => 0  // All Users
        ]);
    } if ($user_type['0'] == 'shops') {
        $shops_token = Shop::pluck('firebase_token')->where('status',1)->toArray();
        AppHelper::send_to_user($shops_token, $request, '10', "");
//            $shops = Shop::get();
//            foreach ($shops as $shop){
//                Mail::to($shop->email)->send(new ShopMail(['message' => $request->message]));
//                Notification::create([
//                    'user_id' => $user->id,
//                    'title' => 'رسالة من مدير النظام',
//                    'body' => $request->message
//                ]);
//            }
        AdminNotification::create([
            'title' => $request->title,
            'message' => $request->message,
            'type' => 1  // All Shops
        ]);
    }
        if ($user_type[0] != 'shops' && $user_type[0] != 'users' && $request->person[0] != 'delegates') {
        $user = User::where('id', $request->person)->first();

            if($user->type == 1 || $user->type == 2) {
                $user = User::where('id', $request->person)->first();
                AppHelper::send_to_user([$user->firebase_token], $request, '10', "");
                if($user->type == 1) {
                    AdminNotification::create([
                        'user_id' => $user->id,
                        'title' => $request->title,
                        'message' => $request->message,
                        'type' => 2  // User
                    ]);
                } else {
                    AdminNotification::create([
                        'user_id' => $user->id,
                        'title' => $request->title,
                        'message' => $request->message,
                        'type' => 5  // delegate
                    ]);
                }

            } else {

            $shop = Shop::where('id', $request->person)->first();
            AppHelper::send_to_user([$shop->firebase_token], $request, '10', "");
            AdminNotification::create([
                'shop_id' => $shop->id,
                'title' => $request->title,
                'message' => $request->message,
                'type' => 3  // shop
            ]);
        }

    }
        session()->flash('success', __("تم إرسال الرسالة بنجاح"));
        return redirect()->back();
//        }
//        else{
//            session()->flash('error', 'عذراً لا يمكنك الدخول لهذة الصفحة');
//            return redirect()->back();
//        }
    }

    public function destroy($id, Request $request)
    {
//        if (Auth::guard('admin')->user()->can('delete_notifications')){
        $notification_id = $id;
        $notification = AdminNotification::where('id', $notification_id)->first();
        if ($notification) {
            $notification->delete();
            session()->flash('success', __("تم إرسال الرسالة بنجاح"));
            return redirect()->back();
        } else {
            session()->flash('error', __("للأسف حدث خطأ ما"));
            return redirect()->back();
        }
//        }
//        else{
//            session()->flash('error', 'عذراً لا يمكنك الدخول لهذة الصفحة');
//            return redirect()->back();
//        }
    }
}
