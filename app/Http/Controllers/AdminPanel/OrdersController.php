<?php

namespace App\Http\Controllers\AdminPanel;

use App\Order;
use App\OrdersProducts;
use App\OrderStatus;
use App\UserCart;
use App\User;
use App\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrdersController extends Controller
{
    function index()
    {
        $all_orders = Order::where('order_status', '!=', 0)->get();
        $order_status = OrderStatus::all();
        return view('AdminPanel.backend.pages.orders.index', compact('all_orders', 'order_status'));
    }

    function order_details($order_id)
    {
        $order = Order::where('id', $order_id)->with('user', 'products')->first();
        $order_products = OrdersProducts::where('order_id', $order_id)->get();
        return view('AdminPanel.backend.pages.orders.details', compact('order', 'order_products'));

    }


    function ordersSearch(Request $request)
    {
        $orderSearch = Order::whereBetween('order_date', array($request->from, $request->to))->get();
        $order_status = OrderStatus::all();
        return view('AdminPanel.backend.pages.orders.search', compact('orderSearch', 'order_status'));

    }

    function  print($order_id)
    {
        $order = Order::where('id', $order_id)->with('user', 'products')->first();
        $order_products = OrdersProducts::where('order_id', $order_id)->get();

//        $product_extra = UserCart::where('order_id', $order_id)->first();

        return view('AdminPanel.backend.pages.orders.print', compact('order', 'order_products'));

    }

}
