<?php

namespace App\Http\Controllers\AdminPanel;

use App\SpecialNumber;
use App\SpecialNumberCost;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class SpecialNumbersController extends Controller
{
    // show Special Numbers

    function index()
    {

        $prices = SpecialNumberCost::get();
        $special_numbers = SpecialNumber::orderBy('id','desc')->get();
        return view('AdminPanel.backend.pages.specialNumbers.numbers', compact('prices','special_numbers'));
    }



    //Add Special Number
    function Add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'number_id' => 'required|exists:special_number_costs,id',
        ],[
            'number_id.required' => 'من فضلك أدخل السعر',
            'number_id.exists' => 'عذرًا هذا الاختيار غير مسجل لدينا',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $price = SpecialNumberCost::where('id',$request->number_id)->first();
        $length = $price->number_of_digits;
        $previous_numbers = SpecialNumber::whereRaw('LENGTH(special_number) = ?', [$length])->first();
        if ($previous_numbers){
            session()->flash('error', __('لقد تمت إضافة هذة الأرقام '.$price->description.' من قبل '));
            return redirect()->back();
        }
        if ($length == 1){
            for ($i = 1 ; $i <=9 ; $i++){
                $add_special_number = new SpecialNumber();
                $add_special_number->special_number = $i;
                $add_special_number->number_id = $price->id;
                $add_special_number->save();
            }
        }else{
            $numbers_array = $this->generateNumbers($length);
            foreach ($numbers_array as $number){
                $add_special_number = new SpecialNumber();
                $add_special_number->special_number = $number;
                $add_special_number->number_id = $price->id;
                $add_special_number->save();
            }
        }
        session()->flash('success', __('تم اضافه اأرقام المميزة بنجاح'));
        return redirect()->back();
    }


    // delete Special Number
    function destroy($id)
    {
        SpecialNumber::where('id', $id)->delete();
        session()->flash('success', __('تم حذف الرقم المميز بنجاح'));
        return redirect()->back();
    }

    public function generateNumbers($m)
    {
        $numbers = [];
        $k_max = 0;
        $x = 0;
        for ($y = 0; $y < 10; $y++) {
            $k_max = (int)(pow(10,$m-2) * (10 * $y + 1)) /
                (int)(pow(10, $m-1) + $y);
            for ($k = 1; $k <= $k_max; $k++) {
                $x = (int)($y*(pow(10,$m-1)-$k)) / (10 * $k -1);
                if ((int)($y*(pow(10,$m-1)-$k)) % (10 * $k -1) == 0){
                    array_push ($numbers,10 * $x + $y);
                }
            }
        }
        sort($numbers);
        return ($numbers);
    }
}
