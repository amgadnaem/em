<?php

namespace App\Http\Controllers\AdminPanel;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;

class SuperVisorController extends Controller
{
    // get all supervisor depend on main admin

    function index()
    {
        $all_supervisor = User::where('type', 4)->get();
        return view('AdminPanel.backend.pages.supervisor.index', compact('all_supervisor'));
    }

    // add supervisor in main admin
    function AddSupervisor(Request $request)
    {
        $request->validate([
            'permissions' => 'required|min:1'
        ]);
        $check_email = User::where('phone_number', $request->phone_number)->orwhere('email', $request->email)->first();
        if ($check_email) {
            session()->flash('success', __('البريد الالكترونى او رقم الهاتف موجود من قبل'));
            return redirect()->back();
        }

        $request->password = bcrypt($request->password);
        $user_number = rand(111111, 999999);

        $jwt_token = Str::random(25);
        $AdminAccount = User::create([
            'user_number' => $user_number,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'phone_number' => $request->phone_number,
            'password' => $request->password,
            'type' => 4,
            'shop_id' => 'admin',
            'jwt_token' => $jwt_token,
        ]);
        $AdminAccount->givePermissionTo($request->permissions);

        if ($request->super_image) {
            $AdminAccount->user_image = $request->super_image;
            $AdminAccount->save();
        }
        session()->flash('success', __('تم اضافه مشرف بنجاح'));
        return redirect()->back();

    }

    //Active or dis active account supervisor

    public function UpdateStatusSuper($id, $status)
    {
        $supervisor = User::where('id', $id)->first();
        $supervisor->status = $status;
        $supervisor->save();
        session()->flash('success', __('تم ايقاف حساب المشرف بنجاح'));
        return redirect()->back();

    }

    public function EditAccountSupervisor($id, Request $request)
    {
       $this->updateSuperVisor($id, $request->all());

        session()->flash('success', __('تم تعديل بيانات المشرف بنجاح'));
        return redirect()->back();
    }

    public function updateSuperVisor($id, $input)
    {
        $user = User::whereId($id)->first();

        $user->first_name = $input['first_name'];
        $user->last_name = $input['last_name'];
        $user->email = $input['email'];
        $user->phone_number = $input['phone_number'];
        if (isset($input['password'])) {
            $user->password = Hash::make($input['password']);
        }
        if (isset($input['super_image'])) {
            $user->user_image = $input['super_image'];
        }
        $user->save();
        return $user;
    }

    function destroyAccount($id)
    {
        User::where('id', $id)->delete();
        session()->flash('success', __('تم حذف  حساب المشرف بنجاح'));
        return redirect()->back();
    }
}
