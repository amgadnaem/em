<?php

namespace App\Http\Controllers\AdminPanel;

use App\CompanyMessaging;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CompaniesMessagingController extends Controller
{
    public function index()
    {
//        $messages = CompanyMessaging::get();

        $companies = \App\ShippingCompany::with(['messages' => function ($query) {
            $query->where('type',1)->orderBy('id', 'desc');
        }])->get();

        return view('AdminPanel.backend.pages.company_messages.index', compact('companies'));
    }

    public function sendView($id)
    {
        $messages = CompanyMessaging::where('company_id',$id)->get();
        foreach ($messages as $message){
            $message->is_read = 1;
            $message->save();
        }
        $company = \App\ShippingCompany::where('id',$id)->first();

        return view('AdminPanel.backend.pages.company_messages.send', compact('messages','company'));
    }

    public function Send(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:shipping_companies,id',
            'message' => 'required',
        ],[
            'company_id.required' => 'من فضلك اختر الشركة',
            'company_id.exists' => 'عذرًا هذه الشركة غير موجودة لدينا',
            'message.required' => 'من فضلك ادخل الرسالة',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $message = CompanyMessaging::create(array_merge($request->all(),[
            'admin_id' => Auth::user()->id,
            'type' => 0,
            ]));
        $message->load('admin');

//        if ($message){
            return response()->json($message);
//        }else{
//            session()->flash('error', __('خطا في البيانات'));
//            return redirect()->back();
//        }
    }
}
