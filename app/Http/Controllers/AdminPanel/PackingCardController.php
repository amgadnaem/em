<?php

namespace App\Http\Controllers\AdminPanel;

use App\PackingCart;
use App\PackingNumbers;
use App\SellerPoints;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class PackingCardController extends Controller
{
    // packing card

    function index()
    {
        $packing_card = PackingCart::all();
        return view('AdminPanel.backend.pages.packingCard.index', compact('packing_card'));
    }

    function addPackingCard(Request $request)
    {

        $add_card = new PackingCart();
        $add_card->card_name_en = $request->card_name_en;
        $add_card->card_name_ar = $request->card_name_ar;
        $add_card->card_name_kur = $request->card_name_kur;
        $add_card->desc_en = $request->desc_en;
        $add_card->desc_ar = $request->desc_ar;
        $add_card->desc_kur = $request->desc_kur;
        $add_card->points = $request->points;
        $add_card->save();
        if ($add_card) {
            session()->flash('success', __('تم اضافه كرت تعبئه بنجاح'));
            return redirect()->back();
        }
    }

    public function updateStatus($id, $status)
    {
        $status_card = PackingCart::where('id', $id)->first();
        $status_card->status = $status;
        $status_card->save();
        session()->flash('success', __('تم تغيير حاله الكرت بنجاح'));
    }

    // delete Card

    function destroy($id)
    {
        PackingCart::where('id', $id)->delete();
        session()->flash('success', __('تم حذف بيانات الكرت بنجاح'));
        return redirect()->back();
    }

    // packing number

    function packingNumber()
    {

        $packingCards = PackingCart::where('status', 1)->get();
        $packingNumbers = PackingNumbers::where('status', 1)->get();
        return view('AdminPanel.backend.pages.packingCard.packingNumber', compact('packingCards', 'packingNumbers'));
    }

    // addPackingNumber
    function addPackingNumber(Request $request)
    {
        $card_count = $request->card_numbers;
        $card_id = $request->card_id;
        for ($i = 1; $i <= $card_count; $i++) {
            $card_number[$i] = Str::random(16);
            $add_card = new PackingNumbers();
            $add_card->card_id = $card_id;
            $add_card->card_number = $card_number[$i];
            QrCode::format('png')->size(500)->generate($card_number[$i], public_path('images/cards/' . $card_number[$i] . '.png'));
            $add_card->card_image = $card_number[$i] . '.png';
            $add_card->save();
        }
        if ($add_card) {
            session()->flash('success', __('تم اضافه كروت التعبئه بنجاح'));
            return redirect()->back();

        }

    }

    // get expired card

    function expiredCard()
    {
        $expiredPackingNumbers = PackingNumbers::where('status', 0)->get();
        return view('AdminPanel.backend.pages.packingCard.expired', compact('expiredPackingNumbers'));
    }

    function sellerPoints(Request $request)
    {
        $sellerPoints = SellerPoints::where('status', 1)->get();
        return view('AdminPanel.backend.pages.packingCard.sellerPoints', compact('sellerPoints'));
    }

    //Add shops
    function addSellerPoints(Request $request)
    {
        $add_points = new SellerPoints();
        $add_points->seller_name_ar = $request->seller_name_ar;
        $add_points->seller_name_en = $request->seller_name_en;
        $add_points->seller_name_kur = $request->seller_name_kur;
        $add_points->lat = $request->lat;
        $add_points->lng = $request->lng;
        $add_points->location = $request->location;
        $add_points->save();
        //If shop saved successfully, then return true
        if ($add_points) {
            session()->flash('success', __('تم اضافه نقطه بيع  بنجاح'));
            return redirect()->back();
        }
    }

    // delete seller points

    function destroySellerPoints($id)
    {
        SellerPoints::where('id', $id)->delete();
        session()->flash('success', __('تم حذف  نقطه بيع بنجاح'));
        return redirect()->back();
    }
}
