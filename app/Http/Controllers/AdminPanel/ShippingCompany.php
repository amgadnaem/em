<?php

namespace App\Http\Controllers\AdminPanel;

use App\City;
use App\Country;
use App\Order;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class ShippingCompany extends Controller
{


    function getCities(Request $request)
    {
        $cities = City::where('country_id', $request->country_id)->get();
        if (count($cities) > 0) {
            echo '<option value="">اختر اسم المدينه</option>';
            foreach ($cities as $row) {
                echo "<option value='" . $row->id . "'>" . $row->city_name_ar . "</option>";
            }
        } else {
            echo '<option value="">لا يوجد مدن تابعه لهذه الدوله</option>';
        }

    }

    //Show All Shipping
    function allShipping()
    {
        $countries = Country::where('status', 1)->get();
        $showShippingCompany = \App\ShippingCompany::where('company_status', 1)->get();
        $shippingCompanyRequest = \App\ShippingCompany::where('company_status', 0)->get();
        return view('AdminPanel.backend.pages.shippingCompany.index', compact('countries', 'showShippingCompany', 'shippingCompanyRequest'));
    }

    // accept request delegate
    public function confirmAccountCompany($id)
    {
        $delegate = \App\ShippingCompany::where('id', $id)->first();
        $delegate->company_status = 1;
        $delegate->save();
        session()->flash('success', __('تم قبول طلب شركه الشحن بنجاح'));
        return redirect()->back();
    }

    function add_shipping(Request $request)
    {
        $check_phone = \App\ShippingCompany::where('phone_number', $request->phone_number)->first();
        if ($check_phone) {
            session()->flash('success', __(' رقم الهاتف موجود من قبل'));
            return redirect()->back();
        }
        $company_number = rand(111111, 999999);
        $AdminAccount = \App\ShippingCompany::create([
            'type' => $request->type,
            'company_number' => $company_number,
            'company_ar' => $request->company_ar,
            'company_en' => $request->company_en,
            'company_kur' => $request->company_kur,
            'phone_number' => $request->phone_number,
            'password' => bcrypt($request->password),
            'company_status' => 1,
            'country_id' => $request->country_id,
        ]);
        if ($request->company_image) {
            $AdminAccount->company_image = $request->company_image;
            $AdminAccount->save();
        }
        if ($request->license_image) {
            $AdminAccount->license_image = $request->license_image;
            $AdminAccount->save();
        }
        session()->flash('success', __('تم اضافه شركه شحن بنجاح'));
        return redirect()->back();
    }

//Active or dis active account shipping

    public function UpdateStatusCompany($id, $status)
    {
        $delegate = \App\ShippingCompany::where('id', $id)->first();
        $delegate->status = $status;
        $delegate->save();
        session()->flash('success', __('تم ايقاف حساب شركه الشحن بنجاح'));
        return redirect()->back();

    }

    public function EditAccountCompany($id, Request $request)
    {

        $this->updateShipping($id, $request->all());
        session()->flash('success', __('تم تعديل بيانات شركه الشحن بنجاح'));
        return redirect()->back();
    }

    public function updateShipping($id, $input)
    {
        $shipping = \App\ShippingCompany::whereId($id)->first();
        $shipping->company_ar = $input['company_ar'];
        $shipping->company_en = $input['company_en'];
        $shipping->company_kur = $input['company_kur'];
        $shipping->shipping = $input['shipping'];
        if (isset($input['password'])) {
            $shipping->password = Hash::make($input['password']);
        }
        if (isset($input['company_image'])) {
            $shipping->company_image = $input['company_image'];
        }
        $shipping->save();
        return $shipping;
    }

    function destroyCompany($id)
    {
        \App\ShippingCompany::where('id', $id)->delete();
        session()->flash('success', __('تم حذف  حساب شركه الشحن بنجاح'));
        return redirect()->back();
    }

    function delegate_orders($id)
    {
        $OrdersDelegate = Order::where('delegate_id', $id)->where('order_status', '>=', 2)->get();
        return view('AdminPanel.backend.pages.Delegate.Orders', compact('OrdersDelegate'));
    }
}
