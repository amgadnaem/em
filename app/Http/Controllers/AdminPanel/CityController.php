<?php

namespace App\Http\Controllers\AdminPanel;

use App\City;
use App\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CityController extends Controller
{
    // show Countries

    function index()
    {
        $cities = City::all();
        $countries = Country::where('status',1)->get();
        return view('AdminPanel.backend.pages.Country.city', compact('cities','countries'));
    }



    //Add cities
    function AddCity(Request $request)
    {
        $add_city = new City();
        $add_city->city_name_ar = $request->city_name_ar;
        $add_city->country_id = $request->country_id;
        $add_city->city_name_en = $request->city_name_en;
        $add_city->city_name_kur = $request->city_name_kur;
        $add_city->save();
        if ($add_city) {
            session()->flash('success', __('تم اضافه بيانات المدينه بنجاح'));
            return redirect()->back();
        }
    }

    // Update City
    public function EditCity($id, Request $request)
    {
        $edit_city = City::whereId($id)->first();
        $edit_city->city_name_en = $request->city_name_en;
        $edit_city->city_name_ar = $request->city_name_ar;
        $edit_city->city_name_kur = $request->city_name_kur;
        $edit_city->save();

        session()->flash('success', __('تم تعديل بيانات المدينه  بنجاح'));
        return redirect()->back();
    }

    // delete Country
    function destroy($id)
    {
        City::where('id', $id)->delete();
        session()->flash('success', __('تم حذف بيانات المدينه بنجاح'));
        return redirect()->back();
    }
}
