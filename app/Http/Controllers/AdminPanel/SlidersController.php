<?php

namespace App\Http\Controllers\AdminPanel;

use App\Country;
use App\Shop;
use App\Slider;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SlidersController extends Controller
{
    //
    //all banner

    function index()
    {
        $allSliders = Slider::orderBy('country_id','asc')->get();
        foreach ($allSliders as $slider) {
            $slider->country_name  = Country::where('id',$slider->country_id)->pluck('country_name_ar')->first();
            $slider->shop_name  = Shop::where('id',$slider->shop_id)->pluck('name_ar')->first();
        }
        $countries = Country::where('status', 1)->get();
        $shops = Shop::where("Subscribe_end", '>', Now())
            ->where('status', 1)
            ->where('shop_status', 1)
            ->get();
        return view('AdminPanel.backend.pages.sliders.index', compact('allSliders', 'countries', 'shops'));
    }

    //create Banner

    function createSlider(Request $request)
    {
        $add_slider = new Slider();
        $add_slider->country_id = $request->country_id;
        $add_slider->shop_id = $request->shop_id;
        $add_slider->image = $request->slider_image;
        $add_slider->save();
        session()->flash('success', __('تم اضافه الإعلان بنجاح'));
        return redirect()->back();
    }


    public function updateSlider($id, Request $request)
    {
        $slider = Slider::where('id', $id)->first();
        if ($request->slider_image) {
            $slider->slider_image = $request->slider_image;
            $slider->save();
        }
        session()->flash('success', __('تم تعديل صوره الإعلان بنجاح'));
        return redirect()->back();
    }

    function deleteSlider($id)
    {
        Slider::where('id', $id)->delete();
        session()->flash('success', __('تم حذف الإعلان بنجاح'));
        return redirect()->back();
    }
}
