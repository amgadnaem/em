<?php

namespace App\Http\Controllers\AdminPanel;

use App\City;
use App\Country;
use App\Order;
use App\Orders;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class DelegateController extends Controller
{
    // get cities from countries
    function getCities(Request $request)
    {
        $cities = City::where('country_id', $request->country_id)->get();
        if (count($cities) > 0) {
            echo '<option value="">اختر اسم المدينه</option>';
            foreach ($cities as $row) {
                echo "<option value='" . $row->id . "'>" . $row->city_name_ar . "</option>";
            }
        } else {
            echo '<option value="">لا يوجد مدن تابعه لهذه الدوله</option>';
        }

    }

    //Show All Delegate
    function AllDelegate()
    {
        $countries = Country::where('status', 1)->get();
        $ShowDelegate = User::where('type', 2)->where('confirm_account', 1)->get();
        $delegateRequest = User::where('type', 2)->where('confirm_account', 0)->get();
        return view('AdminPanel.backend.pages.Delegate.index', compact('countries', 'ShowDelegate', 'delegateRequest'));
    }

    // accept request delegate
    public function confirmAccountDelegate($id)
    {
        $delegate = User::where('id', $id)->first();
        $delegate->confirm_account = 1;
        $delegate->save();
        session()->flash('success', __('تم قبول طلب المندوب بنجاح'));
        return redirect()->back();
    }

    function add_delegate(Request $request)
    {
        $check_phone = User::where('phone_number',$request->phone_number)->first();
        if ($check_phone) {
            session()->flash('success', __(' رقم الهاتف موجود من قبل'));
            return redirect()->back();
        }
        $user_number = rand(111111, 999999);
        QrCode::format('png')->size(500)->generate($request->phone_number, public_path('images/qrCode/' . $request->phone_number . '.png'));
        $AdminAccount = User::create([
            'user_number' => $user_number,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone_number' =>$request->phone_number,
            'qr_code' => $request->phone_number,
            'password' => bcrypt($request->password),
            'type' => 2,
            'confirm_account' => 1,
            'country_id' => $request->country_id,
            'city_id' => $request->city_id,
            'dob' => $request->dob,
            'jwt_token' => Str::random(25),
        ]);
        if ($request->delegate_image) {
            $AdminAccount->user_image = $request->delegate_image;
            $AdminAccount->save();
        }
        session()->flash('success', __('تم اضافه مندوب بنجاح'));
        return redirect()->back();
    }

//Active or dis active account delegate

    public function UpdateStatus($id, $status)
    {
        $delegate = User::where('id', $id)->first();
        $delegate->status = $status;
        $delegate->save();
        session()->flash('success', __('تم ايقاف حساب المندوب بنجاح'));
        return redirect()->route('delegate');

    }

    public function EditAccount($id, Request $request)
    {
        $check_phone = User::where('phone_number', $request->phone_number)->first();
        if ($check_phone) {
            session()->flash('success', __(' رقم الهاتف موجود من قبل'));
            return redirect()->back();
        }
        $this->updateDelegate($id, $request->all());
        session()->flash('success', __('تم تعديل بيانات المندوب بنجاح'));
        return redirect()->back();
    }

    public function updateDelegate($id, $input)
    {

        $delegate = User::whereId($id)->first();
        $delegate->first_name = $input['first_name'];
        $delegate->last_name = $input['last_name'];
        $delegate->phone_number = $input['phone_number'];
        $delegate->dob = $input['dob'];
        if (isset($input['password'])) {
            $delegate->password = Hash::make($input['password']);
        }
        if (isset($input['delegate_image'])) {
            $delegate->user_image = $input['delegate_image'];
        }
        $delegate->save();
        return $delegate;
    }

    function destroy($id)
    {
        User::where('id', $id)->delete();
        session()->flash('success', __('تم حذف  حساب المندوب بنجاح'));
        return redirect()->back();
    }

    function delegate_order($id)
    {
        $OrdersDelegate = Order::where('delegate_id', $id)->where('order_status', '>=', 2)->get();
        return view('AdminPanel.backend.pages.Delegate.Orders', compact('OrdersDelegate'));
    }

}
