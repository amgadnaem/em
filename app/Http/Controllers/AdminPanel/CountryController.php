<?php

namespace App\Http\Controllers\AdminPanel;

use App\City;
use App\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CountryController extends Controller
{
    // show Countries

    function index()
    {
        $countries = Country::all();
        return view('AdminPanel.backend.pages.Country.index', compact('countries'));
    }

    //Add countries
    function AddCountry(Request $request)
    {
        $add_country = new Country();
        $add_country->country_name_ar = $request->country_name_ar;
        $add_country->country_name_en = $request->country_name_en;
        $add_country->country_name_kur = $request->country_name_kur;
        $add_country->code = $request->code;
        $add_country->tax = $request->tax;
        $add_country->country_image = $request->country_image;
        $add_country->save();
        if ($add_country) {
            session()->flash('success', __('تم اضافه بيانات الدوله بنجاح'));
            return redirect()->back();
        }
    }

    // Update Country
    public function EditCountry($id, Request $request)
    {
        $edit_country = Country::whereId($id)->first();
        $edit_country->country_name_en = $request->country_name_en;
        $edit_country->country_name_ar = $request->country_name_ar;
        $edit_country->country_name_kur = $request->country_name_kur;
        $edit_country->code = $request->code;
        $edit_country->tax = $request->tax;

        $edit_country->save();

        if ($request->country_image) {
            $edit_country->country_image = $request->country_image;
            $edit_country->save();
        }
        session()->flash('success', __('تم تعديل بيانات الدوله  بنجاح'));
        return redirect()->back();
    }

    // delete Country

    function destroy($id)
    {
        Country::where('id', $id)->delete();
        session()->flash('success', __('تم حذف بيانات الدوله بنجاح'));
        return redirect()->back();
    }
}
