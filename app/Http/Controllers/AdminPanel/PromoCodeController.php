<?php

namespace App\Http\Controllers\AdminPanel;

use App\PromoCode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PromoCodeController extends Controller
{
    //
    function index()
    {
        $all_promo = PromoCode::get();
        return view('AdminPanel.backend.pages.promoCode.index', compact('all_promo'));
    }

    function addPromoCode(Request $request)
    {

        $new_promo = new PromoCode();
        $new_promo->code = $request->promo_code;
        $new_promo->valid_from = $request->valid_from . ' ' . date("h:i:sa");

        $new_promo->valid_to = $request->valid_to . ' ' . date("h:i:sa");
        $new_promo->discount_type = 'percentage';
        $new_promo->discount_amount = $request->discount_amount;
        $new_promo->status = 1;
        $new_promo->save();
        session()->flash('success', __('تم اضافه برمو كود بنجاح'));
        return redirect()->back();

    }

    function editPromo($id)
    {
        $edit_promo = PromoCode::whereId($id)->first();
        return view('AdminPanel.backend.pages.promoCode.edit', compact('edit_promo'));
    }

    function updatePromo(Request $request, $id)
    {

        $offer = PromoCode::whereId($id)->first();
        $offer->code = $request->code;
        $offer->valid_from = $request->valid_from . ' ' . date("h:i:sa") ;
        $offer->valid_to = $request->valid_to . ' ' . date("h:i:sa");
        $offer->discount_amount = $request->discount_amount;
        $offer->save();
        if ($offer) {
            session()->flash('success', __('تم تعديل بيانات برمو كود بنجاح'));
            return redirect()->route('promoCode');
        }
    }

    // delete Promo Code

    function destroyPromo($id)
    {
        PromoCode::where('id', $id)->delete();
        session()->flash('success', __('تم حذف برمو كود بنجاح'));
        return redirect()->back();
    }
}
