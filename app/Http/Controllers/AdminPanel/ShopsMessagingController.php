<?php

namespace App\Http\Controllers\AdminPanel;

use App\Shop;
use App\ShopMessaging;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ShopsMessagingController extends Controller
{
    public function index()
    {
//        $messages = ShopMessaging::orderBy('id','desc')->groupBy('shop_id')->get();
        $shops = Shop::with(['messages' => function ($query) {
            $query->where('type',1)->orderBy('id', 'desc');
        }])->get();

        return view('AdminPanel.backend.pages.shop_messages.index', compact('shops'));
    }

    public function sendView($id)
    {
        $messages = ShopMessaging::where('shop_id',$id)->get();
        foreach ($messages as $message){
            $message->is_read = 1;
            $message->save();
        }
        $shop = \App\Shop::where('id',$id)->first();

        return view('AdminPanel.backend.pages.shop_messages.send', compact('messages','shop'));
    }

    public function Send(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'shop_id' => 'required|exists:shops,id',
            'message' => 'required',
        ],[
            'company_id.required' => 'من فضلك اختر المتجر',
            'company_id.exists' => 'عذرًا هذا المتجر غير موجودة لدينا',
            'message.required' => 'من فضلك ادخل الرسالة',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $message = ShopMessaging::create(array_merge($request->all(),[
            'admin_id' => Auth::user()->id,
            'type' => 0,
        ]));
        $message->load('admin');

//        if ($message){
        return response()->json($message);
//        }else{
//            session()->flash('error', __('خطا في البيانات'));
//            return redirect()->back();
//        }
    }
}
