<?php

namespace App\Http\Controllers\AdminPanel;

use App\Country;
use App\ServiceCar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceCarsController extends Controller
{
    public function index(){

        $services=ServiceCar::all();
        $countries=Country::all();
        return view('AdminPanel.backend.pages.servicecars.index', compact('services','countries'));

    }
    public function store(Request $request){
        ServiceCar::create([
            'country_id'=>$request->country_id,
            'start'=>$request->start,
            'wait'=>$request->wait,
            'minimum_fare'=>$request->minimum_fare,
            'service_fees'=>$request->service_fees,
            'cancel_before_fees'=>$request->cancel_before_fees,
            'cancel_after_fees'=>$request->cancel_after_fees
        ]);
        return redirect()->back()->with('success','Added Successfully');
    }
    public function edit(Request $request){
        $service= ServiceCar::whereId($request->service_id)->first();
        $countries=Country::all();
        return view('AdminPanel.backend.pages.servicecars.edit', compact('service','countries'));

    }
    public function update(Request $request){
        $service= ServiceCar::whereId($request->service_id)->first();
        $service->update([
            'country_id'=>$request->country_id,
            'start'=>$request->start,
            'wait'=>$request->wait,
            'minimum_fare'=>$request->minimum_fare,
            'service_fees'=>$request->service_fees,
            'cancel_before_fees'=>$request->cancel_before_fees,
            'cancel_after_fees'=>$request->cancel_after_fees
        ]);
        return redirect()->route('serviceCars')->with('success','Updated Successfully');
    }
    public function delete(Request $request)
    {
        $service = ServiceCar::where('id',$request->service_id)->first();

        if ($service->delete()){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }
}
