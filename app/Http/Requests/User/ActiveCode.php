<?php


namespace App\Http\Requests\User;

use App\Http\Requests\Request;

class ActiveCode extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    public function rules()
    {
        return [
            'activate_code' => 'required',
        ];
    }

    function messages()
    {
        return [

            'activate_code.required' => trans('validation.activate_code'),
        ];
    }
}
