<?php

namespace App\Http\Requests\User;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'phone' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'location' => 'required',
            'lat' => 'required',
            'lng' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => trans('validation.name'),
            'phone.required' => trans('validation.phone'),
            'password.required' => trans('validation.password'),
            'email.required' => trans('validation.email'),
            'location.required' => trans('validation.location'),
            'lat.required' => trans('validation.lat'),
            'lng.required' => trans('validation.lng')
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response($validator->errors()->first()));
    }
}
