<?php

namespace App\Http\Requests\User;

use http\Env\Response;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
              return [

                  'phone' => 'required',
                  'password'=>'required',

              ];

    }

    function messages()
    {
        return [
            'password.required' => trans('validation.password'),
            'phone.required' => trans('validation.phone'),

        ];
    }

    function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response($validator->errors()));
    }
}
