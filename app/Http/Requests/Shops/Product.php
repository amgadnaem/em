<?php

namespace App\Http\Requests\Shops;

use App\Http\Requests\Request;

class Category extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    public function rules()
    {
        return [
            'product_name' => 'required',
            'product_code' => 'required',
            'general_price' => 'required',
            'special_price' => 'required',
            'category_id' => 'required',
            'product_desc' => 'required',
            'product_image' => 'required',
        ];
    }

    function messages()
    {
        return [
            'product_name.required' => trans('validation.product_name'),
            'product_code.required' => trans('validation.product_code'),
            'general_price.required' => trans('validation.general_price'),
            'special_price.required' => trans('validation.special_price'),
            'category_id.required' => trans('validation.category_id'),
            'product_desc.required' => trans('validation.product_desc'),
            'product_image.required' => trans('validation.product_image'),
        ];
    }
}
