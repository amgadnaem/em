<?php

namespace App\Http\Requests\Shops;

use App\Http\Requests\Request;

class Category extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    public function rules()
    {
        return [
            'category_name' => 'required',
            'category_image' => 'required',
        ];
    }

    function messages()
    {
        return [
            'category_name.required' => trans('validation.category_name'),
            'category_image.required' => trans('validation.category_image'),
        ];
    }
}
