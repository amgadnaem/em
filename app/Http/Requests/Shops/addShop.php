<?php

namespace App\Http\Requests\Shops;

use App\Http\Requests\Request;

class addShop extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    public function rules()
    {
        return [
            'real_shop_name' => 'required',
            'shop_name_app' => 'required',
            'phone_number' => 'required|min:6',
            'address' => 'required',
            'lat' => 'required',
            'lng' => 'required',
            'service_id' => 'required',
            'shop_image' => 'required',
            'password' => 'required|min:6',
            'country_id' => 'required',
            'city_id' => 'required',
        ];
    }

    function messages()
    {
        return [
            'real_shop_name.required' => trans('validation.real_shop_name'),
            'shop_name_app.required' => trans('validation.shop_name_app'),
            'phone_number.required' => trans('validation.phone_number'),
            'address.required' => trans('validation.address'),
            'lat.required' => trans('validation.lat'),
            'lng.required' => trans('validation.lng'),
            'service_id.required' => trans('validation.service_id'),
            'shop_image.required' => trans('validation.shop_image'),
            'password.required' => trans('validation.password'),
            'country_id.required' => trans('validation.country_id'),
            'city_id.required' => trans('validation.city_id'),
        ];
    }
}
