<?php

namespace App\Http\Requests\Order;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class ManageSettingsRequest.
 */
class StoreOrderRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'payment_method' => 'required',
            'delivery_address_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'payment_method.required' => trans('validation.payment_method'),
            'delivery_address_id.required' => trans('validation.delivery_address_id'),

        ];
    }


}
