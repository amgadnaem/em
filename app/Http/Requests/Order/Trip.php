<?php

namespace App\Http\Requests\Order;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class ManageSettingsRequest.
 */
class Trip extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sender_lat' => 'required',
            'sender_lng' => 'required',
            'sender_location' => 'required',
            'receive_lat' => 'required',
            'receive_lng' => 'required',
            'receive_location' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'sender_lat.required' => trans('validation.sender_lat'),
            'sender_lng.required' => trans('validation.sender_lng'),
            'sender_location.required' => trans('validation.sender_location'),
            'receive_lat.required' => trans('validation.receive_lat'),
            'receive_lng.required' => trans('validation.receive_lng'),
            'receive_location.required' => trans('validation.receive_location'),
        ];
    }
}
