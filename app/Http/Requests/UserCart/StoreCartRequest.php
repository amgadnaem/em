<?php

namespace App\Http\Requests\UserCart;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;
/**
 * Class ManageSettingsRequest.
 */
class StoreCartRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


        return [
            'product_id' => 'required',


        ];
    }

    public function messages()
    {
        return [

            'product_id.required' => trans('validation.product_id'),

        ];
    }
}
