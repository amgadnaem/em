<?php

namespace App\Http\Requests\UserReview;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class ManageSettingsRequest.
 */
class StoreReviewRequest extends Request
{
    private $comment;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


        return [
            'rate_value' => 'required',
            'product_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'rate_value.required' => trans('validation.rate_value'),
            'product_id.required' => trans('validation.product_id'),
        ];
    }
}
