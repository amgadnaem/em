<?php

namespace App\Http\Requests\Address;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class ManageSettingsRequest.
 */
class StoreAddressRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'address' => trans('validation.address')
        ];
    }
}
