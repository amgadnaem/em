<?php

namespace App\Http\Requests\Seller;

use App\Http\Requests\Request;

class SellerPoints extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    public function rules()
    {
        return [
            'lat' => 'required',
            'lng' => 'required',
        ];
    }

    function messages()
    {
        return [
            'lat.required' => trans('validation.lat'),
            'lng.required' => trans('validation.lng'),
        ];
    }
}
