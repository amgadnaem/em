<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class OrderChat extends Model
{
    public $table = 'order_chats';

    public $fillable = [
        'from', 'to', 'order_id', 'message', 'image','type'
    ];
    public $hidden = [
        'created_at', 'updated_at'
    ];

    public function UserFrom()
    {
        return $this->belongsTo(User::class, 'from');
    }


    public function UserTo()
    {
        return $this->belongsTo(User::class, 'to');
    }

    function getCreatedAtAttribute()
    {
        $start = Carbon::now();
        $end = Carbon::parse($this->attributes['created_at']);
        return $duration = $end->diffForHumans($start);
    }

    public function getImageAttribute($value)
    {
        if ($value) {
            return asset('images/chat/' . $value);
        }
    }

    public function setImageAttribute($value)
    {
        if ($value) {
            $image = time() . rand(1111, 9999) . '.' . $value->getClientOriginalExtension();
            $value->move(public_path('images/chat/'), $image);
            $this->attributes['image'] = $image;
        }
    }
}
