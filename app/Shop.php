<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;


class Shop extends Model
{
     use Notifiable;
    use HasRoles;
    protected $guard = 'shop';

    public $table = 'shops';
    public $fillable = [

        'shop_number', 'name_app_en', 'name_app_ar', 'name_app_kur', 'phone_number',
        'password', 'shop_status', 'type', 'shop_image', 'service_id', 'status','ID_number',
        'location', 'lat', 'lng', 'country_id', 'city_id', 'desc_en', 'desc_ar','vip',
        'desc_kur', 'active_flag', 'Subscribe_end', 'jwt_token', 'firebase_token'
    ];

    function followers()
    {
        return $this->hasMany(ShopFollow::class);
    }


    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function getQrCodeAttribute($value)
    {
        if ($value) {
            return asset('images/shops/qrCode/' . $value . '.png');
        }
    }

    public function getShopImageAttribute($value)
    {
        if ($value) {
            return asset('images/shops/' . $value);
        } else {
            return asset('images/shops/no-shops.jpg');
        }
    }

    public function setShopImageAttribute($value)
    {
        if ($value) {
            $shop_image = time() . rand(1111, 9999) . '.' . $value->getClientOriginalExtension();
            $value->move(public_path('images/shops/'), $shop_image);
            $this->attributes['shop_image'] = $shop_image;
        }
    }


    public function getShopLicenseAttribute($value)
    {
        if ($value) {
            return asset('images/shops/' . $value);
        } else {
            return asset('images/shops/no-shops.jpg');
        }
    }

    public function getIDImageAttribute($value)
    {
        if ($value) {
            return asset('images/delegate/' . $value);
        } else {
            return asset('images/shops/no-shops.jpg');
        }
    }

    public function setShopLicenseAttribute($value)
    {
        if ($value) {
            $img_name2 = time() . rand(1111, 9999) . '.' . $value->getClientOriginalExtension();
            $value->move(public_path('images/shops/'), $img_name2);
            $this->attributes['shop_license'] = $img_name2;
        }
    }

    public function setIDImageAttribute($value)
    {
        if ($value) {
            $img_name1 = time() . rand(1111, 9999) . '.' . $value->getClientOriginalExtension();
            $value->move(public_path('images/shops/'), $img_name1);
            $this->attributes['ID_image'] = $img_name1;
        }
    }

    function categories()
    {
        return $this->hasMany(ShopCategories::class);

    }

    public function get_all_order($shop_id)
    {
        return Order::where('shop_id', $shop_id)->pluck('shop_id')->count();

    }

    public function follow($shop_id)
    {
        return ShopFollow::where('shop_id', $shop_id)->pluck('shop_id')->count();

    }

    public function get_service($service_id)
    {
        return Service::where('id', $service_id)->pluck('service_name_ar')->first();

    }

    public function messages()
    {
        return $this->hasMany(ShopMessaging::class);
    }
}
