<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopFollow extends Model
{
    protected $table = 'shop_follows';
    protected $fillable = ['user_id', 'shop_id'];

    protected $hidden = ['created_at', 'updated_at'];
}
