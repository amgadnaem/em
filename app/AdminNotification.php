<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminNotification extends Model
{
    protected $fillable = ['user_id','shop_id','message','title','type'];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public function shop()
    {
        return $this->belongsTo(Shop::class,'shop_id');
    }
}
