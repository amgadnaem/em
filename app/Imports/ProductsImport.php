<?php

namespace App\Imports;

use App\Product;
use App\Service;
use App\ShopCategories;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToModel;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Validators\ValidationException;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\BeforeImport;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ProductsImport implements ToModel, WithValidation, WithHeadingRow, WithMultipleSheets, WithEvents
{
    use Importable, RegistersEventListeners;

    public static function beforeImport(BeforeImport $event)
    {
        $worksheet = $event->reader->getActiveSheet();
        $highestRow = $worksheet->getHighestRow(); // e.g. 10

        if ($highestRow < 2) {
            $error = \Illuminate\Validation\ValidationException::withMessages([]);
            $failure = new Failure(1, 'rows', [0 => 'Now enough rows!']);
            $failures = [0 => $failure];
            throw new ValidationException($error, $failures);
        }
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $shop_categories = ShopCategories::where('shop_id',Session::get('frontSession'))->pluck('id')->toArray();
        if (!in_array($row['category_id'],$shop_categories)){
            return session()->flash('error', __("من فضلك تأكد من الأقسام"));
//            return null;
        }
        if (isset($row[1]))
        QrCode::format('png')->size(500)->generate($row['qr_code'], public_path('images/products/qrCode/' . $row['qr_code'] . '.png'));
        $product =  new Product([
            'shop_id'    => Session::get('frontSession'),
            'code'     => $row['code'],
            'qr_code'    => $row['qr_code'],
            'name_en'    => $row['name_en'],
            'name_ar'    => $row['name_ar'],
            'name_kur'    => $row['name_kur'],
            'desc_en'    => $row['desc_en'],
            'desc_ar'    => $row['desc_ar'],
            'desc_kur'    => $row['desc_kur'],
            'general_price'    => $row['general_price'],
            'special_price'    => $row['special_price'],
            'hosted_price'    => $row['hosted_price'],
            'hosted'    => (!empty($row['hosted_price'])) ? 1 : 0,
            'product_stock'    => $row['product_stock'],
            'category_id'    => $row['category_id'],
            'product_image'    => $row['image'],
            'type' => Service::where('id', Session::get('service_id'))->pluck('type')->first(),
        ]);
//        if (isset($row['images']) && !empty($row['images'])){
//            $images_array = explode("|", $row['images']);
//            foreach ($images_array as $image){
//                $product->product_image = $image;
//                $product->save();
//            }
//        }

        return $product;

    }

    public function sheets(): array
    {
        return [
            // Select by sheet index
            0 => new ProductsImport(),
        ];
    }

    public function rules(): array
    {
        return [
            'category_id' => 'required',
            '*.category_id' => 'required',
            'code' => 'required|min:1',
            '*.code' => 'required|min:1',
            'qr_code' => 'required|min:1',
            '*.qr_code' => 'required|min:1',
            'name_en' => 'required|min:1',
            '*.name_en' => 'required|min:1',
            'name_ar' => 'required|min:1',
            '*.name_ar' => 'required|min:1',
            'name_kur' => 'required|min:1',
            '*.name_kur' => 'required|min:1',
            'desc_en' => 'required|min:1',
            '*.desc_en' => 'required|min:1',
            'desc_ar' => 'required|min:1',
            '*.desc_ar' => 'required|min:1',
            'desc_kur' => 'required|min:1',
            '*.desc_kur' => 'required|min:1',
            'general_price' => 'required|min:1',
            '*.general_price' => 'required|min:1',
            'special_price' => 'required|min:1',
            '*.special_price' => 'required|min:1',
            'hosted_price' => 'required',
            '*.hosted_price' => 'required',
            'product_stock' => 'required|min:1',
            '*.product_stock' => 'required|min:1',
        ];
    }

    public function customValidationMessages()
    {

        return [
            'code.required' => 'من فضلك ادخل code',
            'qr_code.required' => 'من فضلك ادخل qr code',
            'name_en.required' => 'من فضلك ادخل الإسم باللغة الإنجليزية',
            'name_ar.required' => 'من فضلك ادخل الإسم باللغة العربية',
            'name_kur.required' => 'من فضلك ادخل الإسم باللغة الكردية',
            'desc_en.required' => 'من فضلك ادخل الوصف باللغة الإنجليزية',
            'desc_ar.required' => 'من فضلك ادخل الوصف باللغة العربية',
            'desc_kur.required' => 'من فضلك ادخل الوصف باللغة الكردية',
            'general_price.required' => 'من فضلك ادخل السعر العام',
            'special_price.required' => 'من فضلك ادخل السعر الخاص',
            'hosted_price.required' => 'من فضلك ادخل سعر الإستضافة',
            'product_stock.required' => 'من فضلك ادخل الموجود في المتجر',
        ];
    }
}
