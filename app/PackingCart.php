<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackingCart extends Model
{
    public function getCardImageAttribute($value)
    {
        if ($value) {
            return asset('images/qrCode/' . $value);
        }
    }

    function card($card_id)
    {
        $packing = PackingNumbers::where('card_id', $card_id)->pluck('id')->first();
        return $packing;
    }

    function getPacking($card_id)
    {
        return $card_name = PackingNumbers::where('card_id',$card_id)->where('status',1)->count();

    }

    function expiredPacking($card_id)
    {
        return $card_name = PackingNumbers::where('card_id',$card_id)->where('status',0)->count();

    }


//    public function setCardImageAttribute($value)
//    {
//        if ($value) {
//            $img_name = time() . rand(1111, 9999) . '.' . $value->getClientOriginalExtension();
//            $value->move(public_path('images/qrCode/'), $img_name);
//            $this->attributes['card_image'] = $img_name;
//        }
//    }
}
