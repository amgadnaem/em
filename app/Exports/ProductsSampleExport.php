<?php

namespace App\Exports;

use App\Order;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;

class ProductsSampleExport implements FromView
{
    use Exportable;

    public function view(): View
    {
        return view('exports.products-sample');
    }
}
