<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = ['code', 'name_en', 'name_ar', 'qr_code', 'name_kur', 'desc_en', 'desc_ar', 'desc_kur', 'product_image',
        'general_price', 'special_price', 'hosted_price', 'product_stock', 'is_building', 'is_food', 'is_clothes', 'shop_id', 'category_id', 'status',
    ];
    protected $hidden = ['created_at', 'updated_at'];

    function sizes()
    {
        return $this->belongsToMany(Size::class, 'products_sizes', 'product_id', 'size_id');
    }

    function colors()
    {
        return $this->belongsToMany(ProductColor::class, 'colors', 'product_id', 'size_id');
    }

    public function getProductImageAttribute($value)
    {
        if ($value) {
            return asset('images/products/' . $value);
        } else {
            return asset('images/products/no-product.jpg');
        }
    }

//    public function setQrCodeAttribute($value)
//    {
//        if ($value) {
//            $img_name1 = time() . rand(1111, 9999) . '.' . $value->getClientOriginalExtension();
//            $value->move(public_path('images/products/qrCode/'), $img_name1);
//            $this->attributes['qr_code'] = $img_name1;
//        }
//    }

    public function getQrCodeAttribute($value)
    {
        if ($value) {
            return asset('images/products/qrCode/' . $value . '.png');
        }
    }

    public function setProductImageAttribute($value)
    {
        if ($value && is_file($value)) {
            $img_name = time() . rand(1111, 9999) . '.' . $value->getClientOriginalExtension();
            $value->move(public_path('images/products/'), $img_name);
            $this->attributes['product_image'] = $img_name;
        }else{
            $this->attributes['product_image'] = $value;
        }
    }
}
