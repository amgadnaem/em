<?php

namespace App;
use Carbon\Carbon;


use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    
    
        function getCreatedAtAttribute()
    {
     return  Carbon::parse($this->attributes['created_at'])->diffForHumans();
    }
}
