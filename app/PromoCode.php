<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PromoCode extends Model
{
    //
    protected $fillable = [
        'code',
        'discount_type',
        'discount_amount',
        'valid_times',
        'valid_from',
        'valid_to',
        'status'
    ];

//    public function getValidFromAttribute($value)
//    {
//        return Carbon::parse($value)->toDateString();
//    }
//
//    public function getValidToAttribute($value)
//    {
//        return Carbon::parse($value)->toDateString();
//    }
//
//    public function getDiscountTypeAttribute($value)
//    {
//        return trans('messages.promo.'.$value);
//    }
}
