<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    //

    protected $fillable = ['id', 'size_name'];
    protected $hidden = ['created_at', 'updated_at'];


    function colors()
    {
        return $this->belongsToMany(ProductColor::class, 'colors', 'size_id', 'product_id');
    }

    function products()
    {
        return $this->belongsToMany(Product::class, 'products_sizes', 'size_id', 'product_id');
    }

    public function size($id)
    {
        return Size::whereId($id)->pluck('size_name');
    }
}
