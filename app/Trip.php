<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    protected $fillable = [
        'type','car_id','user_id','driver_id','order_action','canceled_by','rate','feedback','total','google_distance','date','time',
    ];

    public function tripPath()
    {
        return $this->hasMany(TripPath::class)->select('trip_id','type','address','lat','lng');
    }

    public function tripStatus()
    {
        return $this->hasMany(TripStatus::class)->select('id','trip_id','status_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class)->select('id','name','phone','image','rate','payment_type');
    }

    public function driver()
    {
        return $this->belongsTo(Driver::class)->select('id','name','phone','image','rate','car_id','country_id');
    }

    public function getCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->format('d F Y');
    }

    public function getRateAttribute()
    {
        return json_decode($this->attributes['rate'],true);
    }

    public function getFeedbackAttribute()
    {
        return json_decode($this->attributes['feedback'],true);
    }

    function distance($lat1, $lon1, $lat2, $lon2, $unit)
    {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    public function search($search)
    {
        $trips = new Trip();

        $user = User::where(function($q) use($search)
        {
            $q->where('name','like','%'.$search['search'].'%');
        }
        )->first();

        $driver = Driver::where(function($q) use($search)
        {
            $q->where('name','like','%'.$search['search'].'%');
        }
        )->first();

        if($user)
        {
            $trips = $trips->where('user_id',$user->id);
        }

        if($driver)
        {
            $trips = $trips->where('driver_id',$driver->id);
        }

        if($search['select_from'] && $search['select_to'])
        {
            $trips = $trips->where('created_at','>=',$search['select_from'])->where('created_at','<=',Carbon::parse($search['select_to'])->addDays(1));
        }

        if($search['select_service_type'])
        {
            $trips = $trips->where('order_action', $search['select_service_type']);
        }

        $trips = $trips->latest()->paginate(50);

        return $trips;

    }
}
