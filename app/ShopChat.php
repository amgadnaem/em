<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ShopChat extends Model
{
    protected $table = 'shop_chats';
    protected $fillable = ['id', 'from', 'to', 'shop_id', 'type', 'image', 'message'];
    protected $hidden = ['created_at', 'updated_at'];
    function getCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->diffForHumans();
    }

    public function getImageAttribute($value)
    {
        if ($value) {
            return asset('images/shopChat/' . $value);
        }
    }

    public function setImageAttribute($value)
    {
        if ($value) {
            $image = time() . rand(1111, 9999) . '.' . $value->getClientOriginalExtension();
            $value->move(public_path('images/shopChat/'), $image);
            $this->attributes['image'] = $image;
        }
    }

}
