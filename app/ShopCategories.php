<?php

namespace App;

use App\Http\Requests\Request;
use Illuminate\Database\Eloquent\Model;

class ShopCategories extends Model
{
    //
    protected $table = 'shop_categories';
    protected $fillable = ['category_name_en', 'category_name_ar', 'category_name_kur', 'category_image', 'shop_id', 'status'];

    public function shop()
    {
        return $this->belongsTo(Shop::class, 'shop_id');
    }

    public function getCategoryImageAttribute($value)
    {
        if ($value) {
            return asset('images/category/' . $value);
        } else {
            return asset('images/category/no-image.jpg');
        }
    }


    public function setCategoryImageAttribute($value)
    {
        if ($value) {
            $img_name = time() . rand(1111, 9999) . '.' . $value->getClientOriginalExtension();
            $value->move(public_path('images/category/'), $img_name);
            $this->attributes['category_image'] = $img_name;
        }
    }

    function countProducts($category_id)
    {
        return Product::where('category_id', $category_id)->pluck('id')->count();

    }

}
