<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    //

    protected $table = 'services';

    protected $fillable = ['id', 'service_name_en', 'service_name_ar', 'service_name_kur', 'service_image','type', 'status'];

    public function getServiceImageAttribute($value)
    {
        if ($value) {
            return asset('images/services/' . $value);
        } else {

            return asset('images/services/no-service.jpg');
        }
    }

    public function setServiceImageAttribute($value)
    {
        if ($value) {
            $service_image = time() . rand(1111, 9999) . '.' . $value->getClientOriginalExtension();
            $value->move(public_path('images/services/'), $service_image);
            $this->attributes['service_image'] = $service_image;
        }
    }

    function countShop($service_id)
    {
        $shops= Shop::where('service_id', $service_id)->pluck('id')->count();
        return $shops;

    }
}
