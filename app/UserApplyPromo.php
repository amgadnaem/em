<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserApplyPromo extends Model
{
    //

    protected $fillable = [
        'user_id',
        'promo_id',
        'order_id'
    ];



    public function promo_code()
    {
        return $this->belongsTo(PromoCode::class, 'promo_id');
    }
}
