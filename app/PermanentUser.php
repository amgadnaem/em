<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PermanentUser extends Model
{
    protected $table = 'permanent_users';
    protected $fillable = ['user_id', 'shop_id'];
    protected $hidden = ['created_at', 'updated_at'];
}
