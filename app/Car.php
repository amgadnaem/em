<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $fillable = [
        'country_id','price','image','car_name_en','car_name_ar','car_name_kur',
        'car_description_en','car_description_ar','car_description_kur'
    ];
}
