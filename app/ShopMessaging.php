<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ShopMessaging extends Model
{
    protected $fillable = ['admin_id','shop_id','type','message'];

    public function admin()
    {
        return $this->belongsTo(User::class,'admin_id');
    }
    public function shop()
    {
        return $this->belongsTo(Shop::class,'shop_id');
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->formatLocalized('%H:%M %p');
    }
}
