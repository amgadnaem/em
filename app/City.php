<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    function getCountry($country_id)
    {
        return Country::where('id', $country_id)->pluck('country_name_ar')->first();
    }
}
