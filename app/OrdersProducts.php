<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrdersProducts extends Model
{
    public $table = 'orders_products';

    public $fillable = ['product_id', 'order_id', 'order_qty', 'price', 'size_id', 'color_id'];


    public function test()
    {
        return $this->hasMany(Product::class, 'id');
    }

    function getProductName($product_id)
    {
        return Product::where('id', $product_id)->pluck('name_ar')->first();
    }

    function getProductPrice($product_id)
    {
        return Product::where('id', $product_id)->pluck('general_price')->first();

    }

    function getOrdersDetails($id)
    {
        return Product::where('id', $id)->pluck('product_image')->first();

    }

    function getSizeName($size_id)
    {
        return Size::where('id', $size_id)->pluck('size_name')->first();
    }

    function getProductDetails($size_id)
    {
        return ProductColorsSize::where('size_id', $size_id)->pluck('general_price')->first();
    }




    function sizeName($size_id)
    {

        $product_size = ProductColorsSize::where('size_id', $size_id)->pluck('size_id')->first();
        return Size::where('id', $product_size)->pluck('size_name')->first();
    }
}
