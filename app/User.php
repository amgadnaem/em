<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';

    protected $fillable = [
        'user_number', 'first_name', 'last_name', 'phone_number', 'password', 'user_image', 'confirm_account','dob','type', 'qr_code',
        'user_status','shop_id', 'lat', 'lng', 'location', 'country_id', 'city_id', 'status', 'jwt_token', 'firebase_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];



    public function getUserImageAttribute($value)
    {
        if ($value) {
            return asset('images/profile/' . $value);
        } else {
            return asset('images/profile/user.png');
        }
    }

    public function setUserImageAttribute($value)
    {
        if ($value) {
            $img_name = time() . rand(1111, 9999) . '.' . $value->getClientOriginalExtension();
            $value->move(public_path('images/profile/'), $img_name);
            $this->attributes['user_image'] = $img_name;
        }
    }

    public function getQrCodeAttribute($value)
    {
        if ($value) {
            return asset('images/qrCode/' . $value . '.png');
        }
    }
    function Orders_user($user_id)
    {
        return Order::where('user_id', $user_id)->pluck('id')->count();
    }

    function delegate_orders($delegate_id)
    {
        return Order::where('delegate_id', $delegate_id)->pluck('id')->count();

    }




}
