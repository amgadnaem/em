<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CompanyMessaging extends Model
{
    protected $fillable = ['admin_id','company_id','type','message'];

    public function admin()
    {
        return $this->belongsTo(User::class,'admin_id');
    }
    public function company()
    {
        return $this->belongsTo(ShippingCompany::class,'company_id');
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->formatLocalized('%H:%M %p');
    }
}
