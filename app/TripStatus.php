<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripStatus extends Model
{
    protected $fillable = [
        'trip_id','status_id'
    ];

    public function status()
    {
        return $this->belongsTo(AllStatus::class)->select('id','name');
    }
}
