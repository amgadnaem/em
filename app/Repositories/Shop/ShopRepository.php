<?php

namespace App\Repositories\Shop;

use App\City;
use App\Address;
use App\Country;
use App\Driver;
use App\Helper\ConstantHelper;
use App\Helper\AppHelper;
use App\Repositories\Address\AddressRepository;
use App\Shop;
use App\Service;
use App\ShopFavorite;
use App\ShopFollow;
use Illuminate\Http\Request;
use App\User;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use SimpleSoftwareIO\QrCode\Facades\QrCode;


class ShopRepository extends BaseRepository
{
    public $model;

    public function __construct(Shop $model)
    {
        $this->model = $model;
    }

    public function checkIfAccountExists($phone)
    {
        return User::where('phone_number', $phone)->first();
    }

    public function checkIfEmailExists($email)
    {
        $user = User::where('email', $email)->first();
        if ($user) {
            return 'true';
        }
        return 'false';
    }


    public function createShop1($input)
    {

        $jwt_token = Str::random(25);
        $user_number = rand(111111, 999999);
        $input->password = Hash::make($input->password);
        $array = array(
            'user_number' => $user_number,
            'first_name' => $input->first_name,
            'last_name' => $input->last_name,
            'password' => $input->password,
            'phone_number' => $input->phone_number,
            'country_id' => $input->country_id,
            'city_id' => $input->city_id,
            'jwt_token' => $jwt_token,
            'lat' => $input->lat,
            'lng' => $input->lng,
            'location' => $input->location,
        );
        //If user saved successfully, then return true
        if ($user = User::create($array)) {

            $this->userVerificationCode($user);

            return ['user_id' => $user->id, 'jwt_token' => $jwt_token];
        }

        return false;
    }

    public function createShop($input, $shop_image = null)
    {
        $jwt_token = Str::random(25);
        $shop_number = rand(1111, 9999);
        $input->password = Hash::make($input->password);
        $add_shop = new Shop();
        $add_shop->name_app_en = $input->real_shop_name;
        $add_shop->name_app_ar = $input->real_shop_name;
        $add_shop->name_app_kur = $input->real_shop_name;
        $add_shop->name_en = $input->shop_name_app;
        $add_shop->name_ar = $input->shop_name_app;
        $add_shop->name_kur = $input->shop_name_app;
        $add_shop->phone_number = $input->phone_number;
        QrCode::size(500)->format('png')->generate($input->phone_number, public_path('images/shops/qrCode/' . $input->phone_number . '.png'));
            // QrCode::format('png')->merge('https://em.my-staff.net/images/logo/qr1.png', 0.4, true)
            // ->size(500)->errorCorrection('H')
            // ->generate($input->phone_number, public_path('images/shops/qrCode/' . $input->phone_number . '.png'));
        $add_shop->qr_code = $input->phone_number;
        $add_shop->password = $input->password;
        $add_shop->country_id = $input->country_id;
        $add_shop->city_id = $input->city_id;
        $add_shop->lat = $input->lat;
        $add_shop->lng = $input->lng;
        $add_shop->location = $input->address;
        $add_shop->type = 3;
        $add_shop->service_id = $input->service_id;
        $add_shop->desc_en = $input->shop_desc;
        $add_shop->desc_ar = $input->shop_desc;
        $add_shop->desc_kur = $input->shop_desc;
        $add_shop->shop_image = $input->shop_image;
        $add_shop->jwt_token = $jwt_token;
        $add_shop->firebase_token = $input->firebase_token;
        $add_shop->save();
        if ($add_shop) {
            $add_shop->shop_image = $shop_image;
            $add_shop->save();
        }
            
        //If user saved successfully, then return true
        if ($add_shop) {
          $address = new Address();
            $address->shop_id = $add_shop->id;
            $address->lat = $input->lat;
            $address->lng = $input->lng;
            $address->location = $input->address;
            $address->type = 3;
            $address->save();
            
            return $add_shop;
      
        }

        return false;
    }


    public function userVerificationCode($user)
    {
        $rand_number = rand(1111, 9999);
        //using api sms send notification to user->phone
        $user->activate_code = $rand_number;
        $user->save();
    }


    public function verifyAccount($input)
    {
        $code = $input->activate_code;
        $phone = $input->phone;
        $user = User::where('activate_code', $code)->first();

        if ($user) {
            $user->user_status = 1;
            $user->activate_code = '';
            $user->save();
            return $user;
        }
        return false;
    }


    public function checkaccount($input)
    {
        $code = $input->activate_code;
        $user = User::where('activate_code', $code)->first();
        if ($user) {

            return $user;
        }
        return false;
    }

    public function checkJwtUser($input)
    {
        $jwt = AppHelper::getKeyHeader($input, ConstantHelper::$jwt, "");
//        $jwt = $input['jwt'];
        $check_account = User::where('jwt_token', $jwt)->first();
        return $check_account;

    }

// check jwt shop
    public function checkJwtShop($input)
    {
        $jwt = AppHelper::getKeyHeader($input, ConstantHelper::$jwt, "");
//        $jwt = $input['jwt'];
        $check_account = Shop::where('jwt_token', $jwt)->first();
        return $check_account;

    }

//check jwt driver
    public function checkJwtDriver($input)
    {
        $jwt = AppHelper::getKeyHeader($input, ConstantHelper::$jwt, "");
//        $jwt = $input['jwt'];
        $check_account = Driver::where('jwt_token', $jwt)->first();
        return $check_account;

    }


// CheckIfCodeExists

    public function checkIfPhoneExists($input)
    {
        //check using jwt_token
        $user = User::where('phone_number', $input['phone_number'])->first();
        return $user;
    }

    // get all shops
    // function getShopDetails1($shops, $user = null, $lang = "en")
    // {
    //     $shop_item = [];
    //     $shop_list = [];
    //     foreach ($shops as $shop) {
    //         $shop_item['id'] = $shop->id;
    //         $shop_item['shop_number'] = '#'.$shop->shop_number;
    //         if ($lang == 'ar') {
    //             $shop_item['shop_name'] = $shop->name_app_ar;
    //             $shop_item['description'] = $shop->desc_ar;
    //         } else if ($lang == 'kur') {
    //             $shop_item['shop_name'] = $shop->name_app_kur;
    //             $shop_item['description'] = $shop->desc_kur;
    //         } else {
    //             $shop_item['shop_name'] = $shop->name_app_en;
    //             $shop_item['description'] = $shop->desc_en;
    //         }
    //         $shop_item['shop_image'] = $shop->shop_image;
    //         $shop_item['vip'] = !empty($shop->vip) ? true : false;
    //         $shop_item['secret_password'] = !empty($shop->secret_password) ? true : false;
    //         $shop_item['followers_count'] = ShopFollow::where('shop_id', $shop->id)->count();
    //         if ($user) {
    //             $shop_item['isFavorite'] = ShopFavorite::
    //             where('user_id', $user['id'])
    //                 ->where('shop_id', $shop->id)
    //                 ->get()
    //                 ->count();
    //             $shop_item['isFavorite'] =  $shop_item['isFavorite'] ? true : false;

    //             $shop_item['isFollow'] = ShopFollow::
    //             where('user_id', $user['id'])
    //                 ->where('shop_id', $shop->id)
    //                 ->get()
    //                 ->count();
    //             $shop_item['isFollow'] =  $shop_item['isFollow'] ? true : false;

    //         }
    //         $shop_list[] = $shop_item;
    //     }
    //     return $shop_list;
    // }
    
        // get all shops
    function getShopDetails($shops, $type, $lang = "en", $user = null)
    {
        $shop_item = [];
        $shop_list = [];
        foreach ($shops as $shop) {
            $shop_item['id'] = $shop->id;
            $shop_item['shop_number'] = '#' . $shop->shop_number;
            if ($lang == 'ar') {
                $shop_item['shop_name'] = $shop->name_app_ar;
                $shop_item['description'] = $shop->desc_ar;
            } else if ($lang == 'kur') {
                $shop_item['shop_name'] = $shop->name_app_kur;
                $shop_item['description'] = $shop->desc_kur;
            } else {
                $shop_item['shop_name'] = $shop->name_app_en;
                $shop_item['description'] = $shop->desc_en;
            }
            $shop_item['shop_image'] = $shop->shop_image;
            $shop_item['vip'] = !empty($shop->vip) ? true : false;
            $shop_item['secret_password'] = !empty($shop->secret_password) ? true : false;
            $shop_item['followers_count'] = ShopFollow::where('shop_id', $shop->id)->count();
            if ($type == 3) {
                $shop_item['type'] = Service::where('id', $shop->service_id)->pluck('type')->first();
            }
            if ($user && $type == 1) {
                $shop_item['isFavorite'] = ShopFavorite::
                where('user_id', $user['id'])
                    ->where('shop_id', $shop->id)
                    ->get()
                    ->count();
                $shop_item['isFavorite'] = $shop_item['isFavorite'] ? true : false;

                $shop_item['isFollow'] = ShopFollow::
                where('user_id', $user['id'])
                    ->where('shop_id', $shop->id)
                    ->get()
                    ->count();
                $shop_item['isFollow'] = $shop_item['isFollow'] ? true : false;
                    $data['country_id'] = $user['country_id'];
        $data['country'] = Country::where('id', $user['country_id'])->pluck('country_name_' . $lang . ' as country_name')->first();
        $data['city_id'] = City::where('id', $user['city_id'])->pluck('city_name_' . $lang . ' as city_name')->first();

            }
            $shop_list[] = $shop_item;
        }
        return $shop_list;
    }


    // get all users
    function getAllUsers($users)
    {
        $user_count = [];
        $user_list = [];
        foreach ($users as $user) {
            $user_count['user_id'] = $user->id;
            $user_count['first_name'] = $user->first_name . $user->last_name;
            $user_count['user_image'] = $user->user_image;
            $user_count['phone_number'] = $user->phone_number;
            $user_list[] = $user_count;
        }
        return $user_list;
    }

    // get all permanent users
    function getAllPermanentUsers($users)
    {
        $user_list = [];
        foreach ($users as $user) {
            $user_count = User::where('id', $user->user_id)->select('first_name', 'last_name', 'user_image', 'phone_number')->first();
            $user_count['user_id'] = $user->user_id;
            $user_list[] = $user_count;
        }
        return $user_list;
    }

    public function updatePasswordProfile($input)
    {
        $updated = false;
        $user = User::whereId($input['user_id'])->first();
        if (Hash::check($input['old_password'], $user->password)) {
            $user->password = Hash::make($input['new_password']);
            $updated = $user->save();
        } else {
            return -1; // if old password is wrong
        }

        //If user saved successfully, then return true
        if ($updated) {
            return true;
        }
        return false;
    }

    ///ChangeLanguage
    public function switchUserLanguage($input)
    {

        $lang = $input['lang'];
        //If user saved successfully, then return true
        if ($lang) {
            return true;
        }
        return false;
    }

//Logout
    public function logoutUser($input)
    {
        $user = User::whereId($input['user_id'])->first();
        $user->firebase_token = null;
        //If firebase_token cleared successfully, then return true
        if ($user->save()) {
            return true;
        }
        return false;
    }


//updateProfile
    public function update($input, $user_image)
    {
        $checkaccount = $this->checkJwtUser($input);
        if (!$checkaccount) {
            return $this->respondWithError(trans('messages.auth.user_check'));
        }

        $user = User::whereId($checkaccount->id)->first();

        $user->name = $input['name'];
        $user->lat = $input['lat'];
        $user->lng = $input['lng'];
        $user->location = $input['location'];
        $user->phone = $input['phone'];

        if ($user_image) {
            $user->user_image = $user_image;
        }
        //If user saved successfully, then return true
        if ($user->save()) {
            $data = $this->getLoggedUserDetails($user);
            return $data;
        }

        return false;
    }


    public function getAllUserAddress($user_id)
    {
        $addresses = Address::where('user_id', $user_id)->latest()->get();
        return $addresses;
    }


    public function getAllAddressDetails($addresses)
    {
        $address_list = [];
        $address_item = [];
        foreach ($addresses as $address) {
            $address_item['id'] = $address->id;
            $address_item['name'] = $address->name;
            $address_item['phone'] = $address->phone;
            $address_item['location'] = $address->location;
            $address_item['lat'] = $address->lat;
            $address_item['lng'] = $address->lng;
            $address_list[] = $address_item;
        }
        return $address_list;
    }


}
