<?php

namespace App\Repositories\Address;

use App\Address;
use App\Repositories\BaseRepository;
use App\User;


/**
 * Class NotificationRepository.
 */
class AddressRepository extends BaseRepository
{

    /**
     * related model of this repositery.
     *
     * @var object
     */
    public $model;


    public function __construct(Address $model)
    {
        $this->model = $model;
    }


    public function getAllUserAddress($check_user)
    {
        $addresses = Address::where('user_id', $check_user->id)->latest()->get();
        return $addresses;
    }

    public function create($input, $check_user)
    {
        $addAddress = array(
            'user_id' => $check_user->id,
            'address' => $input['address']
        );
        if (Address::create($addAddress)) {
            return true;
        }
        return false;
    }

    public function update($input, $check_user)
    {
        $address = Address::whereUserId($check_user->id)->whereId($input['address_id'])->first();
        $address->address = $input['address'];
        if ($address->save()) {
            return true;
        }
        return false;
    }

    public function delete($input)
    {
        if (Address::destroy($input)) {
            return true;
        }
        return false;
    }

    public function getAllAddressDetails($addresses)
    {
        $address_list = [];
        $address_item = [];
        foreach ($addresses as $address) {
            $address_item['id'] = $address->id;
            $address_item['name'] = User::where('id', $address->user_id)->pluck('name')->first();
            $address_item['address'] = $address->address;
            $address_list[] = $address_item;
        }
        return $address_list;
    }
}
