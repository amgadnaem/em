<?php

namespace App\Repositories\User;

use App\Address;
use App\City;
use App\Country;
use App\Driver;
use App\Helper\ConstantHelper;
use App\Helper\AppHelper;
use App\Repositories\Address\AddressRepository;
use App\Service;
use App\ShippingCompany;
use App\Shop;
use App\UserPoints;
use Illuminate\Http\Request;
use App\User;
use App\PermanentUser;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use SimpleSoftwareIO\QrCode\Facades\QrCode;


class UserRepository extends BaseRepository
{
    public $model;


    public function __construct(User $model)
    {
        $this->model = $model;

    }

    public function getLoggedUserDetails($user, $lang)
    {
        $data['id'] = $user['id'];
        $data['user_number'] = $user['user_number'];
        $data['first_name'] = $user['first_name'];
        $data['last_name'] = $user['last_name'];
        $data['phone_number'] = $user['phone_number'];
        $data['user_image'] = $user['user_image'];
        $data['qr_code'] = $user['qr_code'];
        $data['lat'] = $user['lat'];
        $data['lng'] = $user['lng'];
        $data['location'] = $user['location'];
        $data['city_id'] = (int)$user['city_id'];
        $data['country_id'] = $user['country_id'];
        $data['points'] = UserPoints::where('user_id', $user['id'])->pluck('value')->first();
        if (empty($data['points'])) {
            $data['points'] = 0;
        } else {
            $data['points'];
        }
        $data['country'] = Country::where('id', $user['country_id'])->pluck('country_name_' . $lang . ' as country_name')->first();
        $data['city_name'] = City::where('id', $user['city_id'])->pluck('city_name_' . $lang . ' as city_name')->first();
        $data['address_id'] = Address::where('user_id', $user['id'])->pluck('id')->first();

        $data['jwt_token'] = $user['jwt_token'];
        return $data;
    }


    public function getLoggedShopDetails($user, $lang)
    {
        $data['id'] = $user['id'];
        $data['shop_number'] = $user['shop_number'];
        $data['name_app'] = $user['name_app_' . $lang];
          $data['name'] = $user['name__' . $lang];
        $data['phone_number'] = $user['phone_number'];
        $data['shop_image'] = $user['shop_image'];
        $data['qr_code'] = $user['qr_code'];
        $data['lat'] = $user['lat'];
        $data['lng'] = $user['lng'];
        $data['location'] = $user['location'];
        $data['service_id'] = $user['service_id'];
        $data['vip'] = !empty($user->vip) ? true : false;
         $data['secret_password'] = !empty($user->secret_password) ? true : false;

        $data['shop_service'] = Service::where('id',$user['service_id'])->pluck('type')->first();

          $data['points'] = UserPoints::where('user_id', $user['id'])->where('type',3)->pluck('value')->first();
        if (empty($data['points'])) {
            $data['points'] = 0;
        } else {
            $data['points'];
        }
        if($lang == 'ar') {
            $data['description'] = $user['desc_en'];
        } elseif ($lang == 'ar') {
            $data['description'] = $user['desc_ar'];
        } else {
            $data['description'] = $user['desc_kur'];
        }
        $data['jwt_token'] = $user['jwt_token'];
          $data['address_id'] = Address::where('user_id', 4)->pluck('id')->first();


        $data['country'] = Country::where('id', $user['country_id'])->select('id','country_name_' . $lang . ' as country_name')->first();
        $data['city'] = City::where('id', $user['city_id'])->select('id','city_name_' . $lang . ' as city_name')->first();
        return $data;
    }


     // get login company
    public function getLoggedCompanyDetails($company, $lang)
    {
        $data['id'] = $company['id'];
        $data['user_number'] = $company['company_number'];
        if ($lang == 'ar') {
            $data['name'] = $company['company_ar'];
        } else if ($lang == 'kur') {
            $data['name'] = $company['company_kur'];
        } else {
            $data['name'] = $company['company_en'];
        }
        $data['phone_number'] = $company['phone_number'];
        $data['company_image'] = $company['company_image'];
        $data['lat'] = $company['lat'];
        $data['lng'] = $company['lng'];
        $data['location'] = $company['location'];
        $data['points'] = UserPoints::where('user_id', $company['id'])->pluck('value')->first();
        if (empty($data['points'])) {
            $data['points'] = 0;
        } else {
            $data['points'];
        }
        $data['country'] = Country::where('id', $company['country_id'])->pluck('country_name_' . $lang . ' as country_name')->first();
        $data['city_id'] = City::where('id', $company['city_id'])->pluck('city_name_' . $lang . ' as city_name')->first();
        $data['jwt_token'] = $company['jwt_token'];
        return $data;
    }

    public function checkIfAccountExists($phone)
    {
        return User::where('phone_number', $phone)->first();
    }

    public function checkIfShopExists($phone)
    {
        return Shop::where('phone_number', $phone)->first();
    }

    public function checkIfEmailExists($email)
    {
        $user = User::where('email', $email)->first();
        if ($user) {
            return 'true';
        }
        return 'false';
    }


    public function create($input)
    {


        $jwt_token = Str::random(25);
        $user_number = rand(111111, 999999);
        $input->password = Hash::make($input->password);
        $qr_code = QrCode::format('png')->size(500)->generate($input->phone_number, public_path('images/qrCode/' . $input->phone_number . '.png'));
        //  $qr_code = QrCode::format('png')->merge('https://em.my-staff.net/images/logo/qr1.png', 0.5, true)
        //     ->size(500)->errorCorrection('H')
        //     ->generate($input->phone_number, public_path('images/qrCode/' . $input->phone_number . '.png'));
        $array = array(
            'user_number' => $user_number,
            'first_name' => $input->first_name,
            'last_name' => $input->last_name,
            'password' => $input->password,
            'phone_number' => $input->phone_number,
            'qr_code' => $input->phone_number,
            'type' => $input->type,
            'country_id' => $input->country_id,
            'city_id' => $input->city_id,
            'jwt_token' => $jwt_token,
            'lat' => $input->lat,
            'lng' => $input->lng,
            'location' => $input->location,
             'firebase_token' => $input->firebase_token,
        );
        //If user saved successfully, then return true
        if ($user = User::create($array)) {
             $address = Address::create([
                'user_id' => $user->id,
                'lat' => $input->lat,
                'lng' => $input->lng,
                'location' => $input->location,
            ]);
            $address->save();
        return $user;

        }

        return false;
    }


      // create account delegate

    public function createDelegate($input, $delegate_image = null)
    {
        $jwt_token = Str::random(25);
        $user_number = rand(111111, 999999);
        $input->password = Hash::make($input->password);

        QrCode::format('png')->size(500)->generate($input->phone_number, public_path('images/qrCode/' . $input->phone_number . '.png'));
        $array = array(
            'user_number' => $user_number,
            'first_name' => $input->first_name,
            'last_name' => $input->last_name,
            'phone_number' => $input->phone_number,
            'password' => $input->password,
            'qr_code' => $input->phone_number,
            'type' => $input->type,
            'user_image' => $input->image,
            'country_id' => $input->country_id,
            'city_id' => $input->city_id,
            'dob' => $input->dob,
            'confirm_account' => 0,
            'lat' => $input->lat,
            'lng' => $input->lng,
            'location' => $input->location,
            'jwt_token' => $jwt_token,
            'firebase_token' => $input->firebase_token,

        );
        //If delegate saved successfully, then return true
        if ($delegate = User::create($array)) {
            if ($delegate_image) {
                $delegate->user_image = $delegate_image;
                $delegate->save();
            }
            return $delegate;
        }

        return false;
    }

    // create account delegate

    public function createShipping($input, $company_image = null, $license_image = null)
    {
        $jwt_token = Str::random(25);
        $user_number = rand(111111, 999999);
        $input->password = Hash::make($input->password);

        QrCode::format('png')->size(500)->generate($input->phone_number, public_path('images/qrCode/' . $input->phone_number . '.png'));
        $array = array(
            'company_number' => $user_number,
            'company_en' => $input->first_name,
            'company_ar' => $input->first_name,
            'company_kur' => $input->first_name,
            'phone_number' => $input->phone_number,
            'password' => $input->password,
            'qr_code' => $input->phone_number,
            'company_image' => $input->image,
            'license_image' => $input->license_image,
            'country_id' => $input->country_id,
            'city_id' => $input->city_id,
            'company_status' => 0,
            'jwt_token' => $jwt_token,
             'firebase_token' => $input->firebase_token,

        );
        //If delegate saved successfully, then return true
        if ($company = ShippingCompany::create($array)) {
            if ($company_image) {
                $company->company_image = $company_image;
                $company->save();
            }
            if ($license_image) {
                $company->license_image = $license_image;
                $company->save();
            }
            return $company;
        }

        return false;
    }

    public function createDriver($input)
    {

        $jwt_token = Str::random(25);
        $driver_number = rand(111111, 999999);
        $input->password = Hash::make($input->password);

        $add_driver = new Driver();
        $add_driver->driver_number = $driver_number;
        $add_driver->first_name = $input->first_name;
        $add_driver->last_name = $input->last_name;
        $add_driver->dob = $input->dob;
        $add_driver->phone_number = $input->phone_number;
        $add_driver->car_name = $input->car_name;
        $add_driver->car_color = $input->car_color;
        $add_driver->car_number = $input->car_number;
        $add_driver->country_id = $input->country_id;
        $add_driver->city_id = $input->city_id;
        $add_driver->lat = $input->lat;
        $add_driver->lng = $input->lng;
        $add_driver->location = $input->location;
        $add_driver->jwt_token = $jwt_token;
        $add_driver->firebase_token = $input->firebase_token;

        $add_driver->save();

//        $array = array(
//            'driver_number' => $driver_number,
//            'first_name' => $input->first_name,
//            'last_name' => $input->last_name,
//            'dob' => $input->dob,
//            'phone_number' => $input->phone_number,
//            'car_name' => $input->car_name,
//            'car_color' => $input->car_color,
//            'car_number' => $input->car_number,
//            'country_id' => $input->country_id,
//            'city_id' => $input->city_id,
//            'lat' => $input->lat,
//            'lng' => $input->lng,
//            'location' => $input->location,
//            'jwt_token' => $jwt_token,
//
//        );
        //If user saved successfully, then return true
        if ($add_driver) {

           return $add_driver;

        }

        return false;
    }


    public function userVerificationCode($user)
    {
        $rand_number = rand(1111, 9999);

        //using api sms send notification to user->phone
        $user->activate_code = $rand_number;
        $user->save();
    }


    public function verifyAccount($input)
    {
        $code = $input->activate_code;
        $phone = $input->phone;
        $user = User::where('activate_code', $code)->first();
        if ($user) {
            $user->user_status = 1;
            $user->activate_code = '';
            $user->save();
            return $user;
        }
        return false;
    }

    public function verifyAccountShop($input)
    {
        $code = $input->activate_code;
        $shop = Shop::where('shop_number', $code)->first();
        if ($shop) {
            $shop->shop_status = 1;
            $shop->save();
            return $shop;
        }
        return false;
    }

    public function updateAccountShops($input, $license = null, $image_id = null, $lang = "en")
    {
        $update_shop = Shop::where('id', $input->shop_id)->first();
        $update_shop->ID_number = $input->ID_number;
        $update_shop->firebase_token = $input->firebase_token;
        //If shop saved successfully, then return true
        if ($image_id) {
            $update_shop->ID_image = $image_id;
            $update_shop->save();
        }
        if ($license) {
            $update_shop->shop_license = $license;
            $update_shop->save();
        }

        //If user saved successfully, then return true
        if ($update_shop->save()) {
            $data = $this->getLoggedShopDetails($update_shop, $lang);
            return $data;
        }

        return false;


    }


    public function checkaccount($input)
    {
        $code = $input->activate_code;
        $user = User::where('activate_code', $code)->first();
        if ($user) {

            return $user;
        }
        return false;
    }

    public function checkJwtUser($input)
    {
        $jwt = AppHelper::getKeyHeader($input, ConstantHelper::$jwt, "");
//        $jwt = $input['jwt'];
        return User::where('jwt_token', $jwt)->first();

    }

// check jwt shop
    public function checkJwtShop($input)
    {
        $jwt = AppHelper::getKeyHeader($input, ConstantHelper::$jwt, "");
//        $jwt = $input['jwt'];
        $check_account = Shop::where('jwt_token', $jwt)->first();
        return $check_account;

    }

//check jwt driver
    public function checkJwtDriver($input)
    {
        $jwt = AppHelper::getKeyHeader($input, ConstantHelper::$jwt, "");
//        $jwt = $input['jwt'];
        $check_account = Driver::where('jwt_token', $jwt)->first();
        return $check_account;

    }

// CheckIfCodeExists

    public function checkIfPhoneExists($input)
    {
        //check using jwt_token
        $user = User::where('phone_number', $input['phone_number'])->first();
        return $user;
    }

  // get all users
    function getAllUsers($users,$shop_owner)
    {
        $user_count = [];
        $user_list = [];
        foreach ($users as $user) {
            $user_count['user_id'] = $user->id;
            $user_count['first_name'] = $user->first_name . $user->last_name;
            $user_count['user_image'] = $user->user_image;
            $user_count['phone_number'] = $user->phone_number;
            $permanent = PermanentUser::where('user_id', $user->id)->where('shop_id', $shop_owner->id)->pluck('user_id')->first();
            if ($permanent) {
                $user_count['isPermanent'] = true;
            } else {
                $user_count['isPermanent'] = false;

            }
            $user_list[] = $user_count;
        }
        return $user_list;
    }

    // get all permanent users
    function getAllPermanentUsers($users)
    {
        $user_list = [];
        foreach ($users as $user) {
            $user_count = User::where('id', $user->user_id)->select('first_name', 'last_name', 'user_image', 'phone_number')->first();
            $user_count['user_id'] = $user->user_id;
            $user_list[] = $user_count;
        }
        return $user_list;
    }

    public function updatePasswordProfile($input)
    {
        $updated = false;
        $user = User::whereId($input['user_id'])->first();
        if (Hash::check($input['old_password'], $user->password)) {
            $user->password = Hash::make($input['new_password']);
            $updated = $user->save();
        } else {
            return -1; // if old password is wrong
        }

        //If user saved successfully, then return true
        if ($updated) {
            return true;
        }
        return false;
    }

    ///ChangeLanguage
    public function switchUserLanguage($input)
    {

        $lang = $input['lang'];
        //If user saved successfully, then return true
        if ($lang) {
            return true;
        }
        return false;
    }

//Logout
    public function logoutUser($input)
    {
        $user = User::whereId($input['user_id'])->first();
        $user->firebase_token = null;
        //If firebase_token cleared successfully, then return true
        if ($user->save()) {
            return true;
        }
        return false;
    }


//updateProfile
    public function update($input, $user_image)
    {
        $checkaccount = $this->checkJwtUser($input);
        if (!$checkaccount) {
            return $this->respondWithError(trans('messages.auth.user_check'));
        }

        $user = User::whereId($checkaccount->id)->first();

        $user->name = $input['name'];
        $user->lat = $input['lat'];
        $user->lng = $input['lng'];
        $user->location = $input['location'];
        $user->phone = $input['phone'];

        if ($user_image) {
            $user->user_image = $user_image;
        }
        //If user saved successfully, then return true
        if ($user->save()) {
            $data = $this->getLoggedUserDetails($user);
            return $data;
        }

        return false;
    }


    public function getAllUserAddress($user_id)
    {
        $addresses = Address::where('user_id', $user_id)->latest()->get();
        return $addresses;
    }


    public function getAllAddressDetails($addresses)
    {
        $address_list = [];
        $address_item = [];
        foreach ($addresses as $address) {
            $address_item['id'] = $address->id;
            $address_item['name'] = $address->name;
            $address_item['phone'] = $address->phone;
            $address_item['location'] = $address->location;
            $address_item['lat'] = $address->lat;
            $address_item['lng'] = $address->lng;
            $address_list[] = $address_item;
        }
        return $address_list;
    }


}
