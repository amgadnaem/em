<?php

namespace App\Repositories\UserFavorite;

use App\ProductFavorite;
use App\Repositories\BaseRepository;
use App\Repositories\Product\ProductRepository;
use App\UserFavorite;


/**
 * Class NotificationRepository.
 */
class UserFavoriteRepository extends BaseRepository
{

    /**
     * related model of this repositery.
     *
     * @var object
     */
    public $productRepository;


    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function getUserFavorites($favorites, $lang = "en", $user = null)
    {

        $favorites_item = [];
        $favorites_list = [];
        foreach ($favorites as $favorite) {
            $favorites_item = $this->productRepository->getProductFavorites($favorite->product, $lang, $user);
            $favorites_list[] = $favorites_item;
        }
        return $favorites_list;
    }


    public function getShopFavorites($favorites, $lang = "en", $user = null)
    {

        $favorites_item = [];
        $favorites_list = [];
        foreach ($favorites as $favorite) {
            $favorites_item = $this->productRepository->getShopFavorites($favorite->shop, $lang, $user);
            $favorites_list[] = $favorites_item;
        }
        return $favorites_list;
    }

    public function create($input)
    {
        if (ProductFavorite::create($input)) {
            return true;
        }
        return false;
    }

    public function delete($input)
    {
        if (ProductFavorite::destroy($input)) {
            return true;
        }
        return false;
    }

}
