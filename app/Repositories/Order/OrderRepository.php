<?php

namespace App\Repositories\Order;

use App\Address;
use App\Notification;
use App\Order;
use App\OrdersProducts;
use App\OrderStatus;
use App\PackingCart;
use App\Product;
use App\ProductColor;
use App\Repositories\BaseRepository;
use App\Repositories\Product\ProductRepository;
use App\ShippingCompany;
use App\Shop;
use App\Size;
use App\User;
use App\UserApplyPromo;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * Class NotificationRepository.
 *
 *
 */
class OrderRepository extends BaseRepository
{

    public $model;
    public $productRepository;


    public function __construct(Order $model, ProductRepository $productRepository)
    {
        $this->model = $model;
        $this->productRepository = $productRepository;
    }

    function sendOrderToDelegate($user_exist, $shop_id, $order_id)
    {
        define('API_ACCESS_KEY', 'AAAAJIhBdDE:APA91bHLPrry6EpPfm8cSqvb0Vg5d3pa22K5i_e3uPTKev8JR_7SsVjGktN-paxPXo7k26OYB4oJbWZN6RxkOrQ2KCLvLH_ifqUyfRJow7EG4doyamUVXVlGK8aRnaKPVqIib3md5MoU');
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $location_user = User::where('id', $user_exist->id)->select('lat', 'lng')->first();
        $notification_data = User::selectRaw("( FLOOR(6371 * ACOS( COS( RADIANS($location_user->lat) ) * COS( RADIANS( users.lat ) ) * COS( RADIANS( users.lng ) - RADIANS($location_user->lng) ) + SIN( RADIANS($location_user->lat) ) * SIN( RADIANS( users.lat ) ) )) ) as distance, users.id as id,users.lat,users.first_name,users.firebase_token as firebase_token")
            ->havingRaw("distance <= 50")
            ->where('users.type', 2)
            ->where('users.confirm_account', 1)
            ->where('users.status', 1)
            ->whereNotIn('id', DB::table('orders')->select('orders.delegate_id')->where('orders.order_status', '=', 2)->orwhere('orders.order_status', '=', 3))
            ->pluck('firebase_token')->toArray();

        $notification_data1 = User::selectRaw("( FLOOR(6371 * ACOS( COS( RADIANS($location_user->lat) ) * COS( RADIANS( users.lat ) ) * COS( RADIANS( users.lng ) - RADIANS($location_user->lng) ) + SIN( RADIANS($location_user->lat) ) * SIN( RADIANS( users.lat ) ) )) ) as distance, users.id as id,users.lat,users.first_name,users.firebase_token as firebase_token")
            ->havingRaw("distance <= 50")
            ->where('users.type', 2)
            ->where('users.confirm_account', 1)
            ->where('users.status', 1)
            ->whereNotIn('id', DB::table('orders')->select('orders.delegate_id')->where('orders.order_status', '=', 2)->orwhere('orders.order_status', '=', 3))
            ->pluck('id')->toArray();

        if ($notification_data != NULL) {
            $notification = [
                'title' => 'طلب جديد',
                'body' => 'تم ارسال طلب من قبل المستخدم',
                'type' => '4',
                'order_id' => $order_id
            ];
            $extraNotificationData = ["message" => $notification, "moredata" => 'dd'];
            $fcmNotification = [
                'registration_ids' => $notification_data, //single token
                'data' => $extraNotificationData
            ];
            foreach ($notification_data1 as $data) {
                $notification = new Notification();
                $notification->from = $user_exist->id;
                $notification->to = $data;
                $notification->title_en = 'New Order';
                $notification->message_en = 'Send New order from User';
                $notification->title_ar = 'طلب جديد';
                $notification->message_ar = 'تم ارسال طلب من قبل المستخدم';
                $notification->title_kur = 'طلب جديد';
                $notification->message_kur = 'تم ارسال طلب من قبل المستخدم';
                $notification->type = 1;
                $notification->flag = 2;
                $notification->save();
            }
            $this->sendOrderToShop($user_exist, $shop_id, $order_id);
            $headers = [
                'Authorization: key=' . API_ACCESS_KEY,
                'Content-Type: application/json'
            ];
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $fcmUrl);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
            $result = curl_exec($ch);
            curl_close($ch);
            if ($result) {
                return true;
            } else {
                return false;
            }
        }
    }

    // send order to shop

    function sendOrderToShop($user_exist, $shop_id, $order_id)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $send_shop = Shop::where('id', $shop_id)->pluck('firebase_token')->first();
        if ($send_shop != NULL) {
            $notification = [
                'title' => 'طلب جديد',
                'body' => 'تم ارسال طلب من قبل المستخدم',
                'type' => '4',
                'order_id' => $order_id
            ];
            $extraNotificationData = ["message" => $notification, "moredata" => 'dd'];
            $fcmNotification = [
                'to' => $send_shop, //single token
                'data' => $extraNotificationData
            ];
            $notification = new Notification();
            $notification->from = $user_exist->id;
            $notification->to = $shop_id;
            $notification->title_en = 'New Order';
            $notification->message_en = 'Send New order from User';
            $notification->title_ar = 'طلب جديد';
            $notification->message_ar = 'تم ارسال طلب من قبل المستخدم';
            $notification->title_kur = 'طلب جديد';
            $notification->message_kur = 'تم ارسال طلب من قبل المستخدم';
            $notification->type = 1;
            $notification->flag = 3;
            $notification->save();
            $headers = [
                'Authorization: key=' . API_ACCESS_KEY,
                'Content-Type: application/json'
            ];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $fcmUrl);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
            $result = curl_exec($ch);
            curl_close($ch);
            if ($result) {
                return true;
            } else {
                return false;
            }

        }
    }

    function sendOrderToShipping($user_exist, $shop_id, $order_id, $shipping)
    {
        define('API_ACCESS_KEY', 'AAAAJIhBdDE:APA91bHLPrry6EpPfm8cSqvb0Vg5d3pa22K5i_e3uPTKev8JR_7SsVjGktN-paxPXo7k26OYB4oJbWZN6RxkOrQ2KCLvLH_ifqUyfRJow7EG4doyamUVXVlGK8aRnaKPVqIib3md5MoU');

        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $send_shop = Shop::where('id', $shop_id)->pluck('firebase_token')->first();
        $send_shipping = ShippingCompany::where('id', $shipping)->pluck('firebase_token')->first();
        if ($send_shop != NULL) {
            $notification = [
                'title' => 'طلب جديد',
                'body' => 'تم ارسال طلب من قبل المستخدم',
                'type' => '4',
                'order_id' => $order_id
            ];
            $extraNotificationData = ["message" => $notification, "moredata" => 'dd'];
            $fcmNotification = [
                'to' => $send_shop, //single token
                'data' => $extraNotificationData
            ];
            if ($shop_id) {
                $notification = new Notification();
                $notification->from = $user_exist->id;
                $notification->to = $shop_id;
                $notification->title_en = 'New Order';
                $notification->message_en = 'Send New order from User';
                $notification->title_ar = 'طلب جديد';
                $notification->message_ar = 'تم ارسال طلب من قبل المستخدم';
                $notification->title_kur = 'طلب جديد';
                $notification->message_kur = 'تم ارسال طلب من قبل المستخدم';
                $notification->type = 1;
                $notification->flag = 3;
                $notification->save();
            }
            if ($shipping) {
                $notification = new Notification();
                $notification->from = $user_exist->id;
                $notification->to = $shipping;
                $notification->title_en = 'New Order';
                $notification->message_en = 'Send New order from User';
                $notification->title_ar = 'طلب جديد';
                $notification->message_ar = 'تم ارسال طلب من قبل المستخدم';
                $notification->title_kur = 'طلب جديد';
                $notification->message_kur = 'تم ارسال طلب من قبل المستخدم';
                $notification->type = 1;
                $notification->flag = 4;
                $notification->save();
            }
            $headers = [
                'Authorization: key=' . API_ACCESS_KEY,
                'Content-Type: application/json'
            ];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $fcmUrl);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
            $result = curl_exec($ch);
            curl_close($ch);
            if ($result) {
                return true;
            } else {
                return false;
            }

        }
    }


    public
    function getSubtotal($cart_items)
    {
        foreach ($cart_items as $cart_item) {
            $product_price[] = $cart_item['qty'] * $cart_item['purchase_price'];
        }
        $subtotal = array_sum($product_price);
        return $subtotal;
    }

    public
    function delete($input)
    {
        if (Order::destroy($input)) {
            return true;
        }
        return false;
    }

    public
    function getOrderByID($order_id)
    {
        return Order::whereId($order_id)->first();
    }


    public
    function ifOrderHasPromoCode($order_id)
    {
        $checkCode = UserApplyPromo::with('promo_code')->where('order_id', $order_id)->first();
        return $checkCode;
    }

    public
    function getDiscount($order_id, $promo_code)
    {

        $order = $this->getOrderByID($order_id);
        if ($promo_code->discount_type == 'fixed') {
            $order->subtotal_fees = $order->subtotal_fees - $promo_code->discount_amount;
        } elseif ($promo_code->disount_type == 'percentage') {
            $order->subtotal_fees = $order->subtotal_fees * (100 * $promo_code->discount_amount);
        }
        if ($order->subtotal_fees < 0) {
            $order->subtotal_fees = 0;
        }
        return [
            'total_fees' => (string)($order->subtotal_fees),
            'new_subtotal' => (string)($order->subtotal_fees)
        ];
    }

    public
    function updateQtyToStock($cart_item)
    {
        if ($cart_item->product->product_stock == 0) {
            return 1;
        }
        if ($cart_item->product->product_stock < $cart_item->qty) {
            $cart_item->qty = $cart_item->product->product_stock;
            $cart_item->save();
        }
        return 0;
    }

    public
    function getAllOrdersList($orders)
    {
        $orders_list = [];
        $order_item = [];
        foreach ($orders as $order) {
            $order_item['id'] = $order->id;
            $order_item['order_number'] = $order->order_number;
            $order_item['order_date'] = $order->order_date;
            $order_item['total_fees'] = $order->total_fees;
            $order_item['order_status'] = $order->order_status;
            $orders_list[] = $order_item;
        }
        return $orders_list;
    }

    public
    function getOrderProducts($order,$type, $lang = "en")
    {

        $order_item = [];
        $order_list = [];
        if($type == 1 || $type == 3) {
            if (!empty($order['delegate_id']) || empty($order['delegate_id'])) {

                $order_item['delegate'] = User::where('id', $order['delegate_id'])->select('id', 'user_image', 'first_name', 'phone_number', 'lat', 'lng', 'location')->first();
            } else if (!empty($order['shipping_id'])) {
                $order_item['delegate'] = $this->shippingDetails($order['shipping_id']);
            }
        }

if($type == 2 || $type == 3) {
    if ($order->type == 3) {
        $order_item['user'] = Shop::where('id', $order['shop_id'])->select('id', 'shop_number', 'shop_image', 'name_app_' . $lang . ' as name')->first();
    } else {
        $order_item['user'] = User::where('id', $order['user_id'])->select('id', 'phone_number', 'user_image', 'first_name as name', 'last_name as name','lat','lng','location')->first();
    }
    $order_item['address'] = Address::where('id', $order['delivery_address_id'])->select('id', 'lat', 'lng', 'location')->first();

}

        $order_item['status'] = $order['order_status'];
        $order_item['status'] = OrderStatus::where('id', $order['order_status'])->select('id', 'status_name_' . $lang . ' as status')->first();
        $order_item['order_number'] = $order['order_number'];
        $order_item['subtotal_fees'] = (double)$order['subtotal_fees'];
        $order_item['tax_fees'] = $order['tax_fees'];
        $order_item['shipping_fees'] = $order['shipping_fees'];
        if ($order['type'] == 4) {
            $order_item['products'] = $this->getProducts($order['id'], $lang);
        } else {
            $order_item['products'] = $this->getProducts($order['id'], $lang);
            $card = PackingCart::where('id', $order->packing_id)->select('card_name_' . $lang . ' as card_name', 'desc_' . $lang . ' as desc')->first();
            $order_item['card_name'] = $card->card_name;
            $order_item['description'] = $card->desc;
        }

        return $order_item;
    }

    function shippingDetails($shipping)
    {
        $shipping = ShippingCompany::where('id', $shipping)->first();
        $order_item['user_image'] = $shipping['company_image'];
        $order_item['first_name'] = $shipping['company_ar'];
        $order_item['phone_number'] = $shipping['phone_number'];
        return $order_item;


    }

    public
    function getProducts($order_id, $lang)
    {

        $product_list = [];
        $product_item = [];
        $order_products = OrdersProducts::where('order_id', $order_id)->get();
        foreach ($order_products as $order) {
            $product_item = Product::whereId($order->product_id)->select('product_image', 'name_' . $lang . ' as product_name', 'desc_' . $lang . ' as description')->first();
            $product_item['order_qty'] = $order->order_qty;
            $product_item['price'] = $order->price;
            $product_item['product_id'] = $order->product_id;
            if(!empty($order->size_id) && !empty($order->color_id)) {
                $product_item['size'] = Size::where('id',$order->size_id)->pluck('size_name')->first();
                $product_item['color'] = ProductColor::where('id',$order->color_id)->pluck('color')->first();
            } elseif (!empty($order->size_id)) {
                $product_item['size'] = Size::where('id',$order->size_id)->pluck('size_name')->first();
            }

            $product_item['rate'] = $this->productRepository->getProductRate($order->product_id);


//            $explode_check_additions = explode(',', $order->additions);
//            $product_item['additions'] = Additions::whereIn('id', $explode_check_additions)->pluck('name');
//            $product_item['without_extra'] = $order->without_extra;
//            $product_item['total_price'] = $order->price * $order->order_qty;
            $product_list[] = $product_item;
        }
        return $product_list;
    }


}
