<?php

namespace App\Repositories\Category;

use App\Category;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\SubCategory;
use Illuminate\Support\Facades\Hash;

/**
 * Class NotificationRepository.
 */
class CategoryRepository extends BaseRepository
{

    /**
     * related model of this repositery.
     *
     * @var object
     */
    public $model;

    public function __construct(Category $model)
    {

        $this->model = $model;
    }

    public function getAllCategoryDetailsWSub($categories, $lang)
    {
        $categories_list = [];
        $category_item = [];
        foreach ($categories as $category) {
            $category_item['id'] = $category->id;
            if ($lang == 'ar') {
                $category_item['category_name'] = $category->category_name_ar;
            } else {
                $category_item['category_name'] = $category->category_name_en;
            }
            $category_item['category_image'] = $category->category_image;
            $subCategories = SubCategory::where('category_id', $category->id)->get();
            $category_item['subcategory'] = array();
            foreach ($subCategories as $subCategory) {
                $val['id'] = $subCategory->id;
                if ($lang == 'ar') {
                    $val['subcategory_name'] = $subCategory->category_name_ar;
                } else {
                    $val['subcategory_name'] = $subCategory->category_name_en;
                }
                array_push($category_item['subcategory'], $val);
            }
            $categories_list[] = $category_item;
        }
        return $categories_list;
    }
}
