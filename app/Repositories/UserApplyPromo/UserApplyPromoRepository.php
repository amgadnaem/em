<?php

namespace App\Repositories\UserApplyPromo;


use App\Repositories\BaseRepository;
use App\Repositories\Order\OrderRepository;
use App\UserApplyPromo;


/**
 * Class NotificationRepository.
 */
class UserApplyPromoRepository extends BaseRepository
{

    /**
     * related model of this repositery.
     *
     * @var object
     */
    public $model;
    public $orderRepository;

    public function __construct(UserApplyPromo $model, OrderRepository $orderRepository)
    {
        $this->model = $model;
        $this->orderRepository = $orderRepository;
    }

    public function getNumberOfUses($user_id, $promo_id)
    {
        return $this->model->where(['user_id' => $user_id, 'promo_id' => $promo_id])->count();
    }

    public function create($user, $promo_code, $price)
    {
        $input['user_id'] = $user;
        $input['promo_id'] = $promo_code->id;
        $discounted_price = $this->orderRepository->getDiscount($input['user_id'], $promo_code->id, $price);
        //If promo code added successfully, then return true
        if ($user = UserApplyPromo::updateOrCreate($input)) {
            return $discounted_price;
        }

        return false;
    }

}
