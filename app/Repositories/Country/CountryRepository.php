<?php

namespace App\Repositories\Country;

use App\Category;
use App\City;
use App\Country;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\SubCategory;
use Illuminate\Support\Facades\Hash;

/**
 * Class NotificationRepository.
 */
class CountryRepository extends BaseRepository
{


    public $model;

    public function __construct(Country $model)
    {

        $this->model = $model;
    }

    public function getAllCountry($countries, $lang)
    {
        $countries_list = [];
        $country_item = [];
        foreach ($countries as $country) {
            $country_item['id'] = $country->id;
            if ($lang == 'ar') {
                $country_item['country_name'] = $country->country_name_ar;
            } elseif ($lang == 'en') {
                $country_item['country_name'] = $country->country_name_en;
            } else {
                $country_item['country_name'] = $country->country_name_en;
            }
            $country_item['country_code'] = $country->code;
            $country_item['country_image'] = $country->country_image;
            $cities = City::where('country_id', $country->id)->get();
            $country_item['cities'] = array();
            foreach ($cities as $city) {
                $val['id'] = $city->id;
                if ($lang == 'ar') {
                    $val['city_name'] = $city->city_name_ar;
                } elseif ($lang == 'en') {
                    $val['city_name'] = $city->city_name_en;
                } else {
                    $val['city_name'] = $city->city_name_en;

                }
                array_push($country_item['cities'], $val);
            }
            $countries_list[] = $country_item;
        }
        return $countries_list;
    }
}
