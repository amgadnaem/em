<?php

namespace App\Repositories\Setting;

use App\Setting;
use App\Models\UserFavorite\UserFavorite;
use App\Repositories\BaseRepository;
use App\Repositories\Product\ProductRepository;
use Hamcrest\Core\Set;


/**
 * Class NotificationRepository.
 */
class SettingRepository extends BaseRepository
{

    /**
     * related model of this repositery.
     *
     * @var object
     */
    public $model;


    public function __construct(Setting $model)
    {
        $this->model = $model;
    }

    public function getAll()
    {
        $settings = Setting::get();
        $setting_list = [];
        foreach ($settings as $setting) {
            $setting_list[$setting->key] = $setting->value;
        }
        return (object)$setting_list;
    }

    public function getSettingByKey($key){
        $value = Setting::where('key',$key)->pluck('value')->first();
        return $value;
    }

    public function updateSettings($input){
        if($this->flushSettings(array_keys($input))){
            foreach ($input as $key=>$val){
                Setting::create([
                    'key' => $key,
                    'value' => $val
                ]);
            }
            return true;
        }
        return false;
    }

    public function flushSettings($input){
        Setting::whereIn('key',$input)->delete();
        return true;
    }



}
