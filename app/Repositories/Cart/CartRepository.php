<?php

namespace App\Repositories\Cart;


use App\Country;
use App\PermanentUser;
use App\Product;
use App\ProductColor;
use App\ProductColorsSize;
use App\ProductSize;
use App\Repositories\BaseRepository;
use App\Repositories\Product\ProductRepository;
use App\Setting;
use App\ShippingCompany;
use App\Shop;
use App\Size;
use App\UserCart;

/**
 * Class NotificationRepository.
 *
 *
 */
class CartRepository extends BaseRepository
{

    public $model;
    public $productRepository;

    public function __construct(UserCart $model, ProductRepository $productRepository)
    {
        $this->model = $model;
        $this->productRepository = $productRepository;
    }


    public function getCartItemDetails($user, $lang = "en")
    {
        $items = $this->model->where('user_id', $user->id)
            ->with('product')
            ->get();

        $cart_item = [];
        $items_cart = array();
        $total = 0;
        foreach ($items as $item) {
            $product = Product::where('id', $item->product_id)->select('type', 'hosted', 'shop_id', 'general_price', 'special_price')->first();
//            $product_size = ProductSize::where('id', $item->size_id)->select('size_id')->first();
            $size_name = Size::where('id', $item->size_id)->select('size_name')->first();
            $cart_item = $size_name;
            $permanent = PermanentUser::where('user_id', $user->id)->where('shop_id', $product['shop_id'])->pluck('id')->first();
            if (!empty($item->size_id) && !empty($item->color_id)) {
                $cart_item['size_id'] = $item->size_id;
                $cart_item['color_id'] = $item->color_id;
                $cart_item['color'] = ProductColor::where('id', $item->color_id)->pluck('color')->first();
                if (($product['hosted'] == 2) && ($product['type'] == 1 || $product['type'] == 3 || $product['type'] == 4)) {
                    $cart_item['price'] = $product['general_price'];
                    $cart_item['total_price'] = $item->qty * $product['general_price'];
                } else {
                    if ($permanent) {
                        $cart_item['price'] = $product['special_price'];
                        $cart_item['total_price'] = $item->qty * $product['special_price'];
                    } else {
                        $cart_item['price'] = $product['general_price'];
                        $cart_item['total_price'] = $item->qty * $product['general_price'];
                    }
                }

            } else if (!empty($item->size_id)) {
                $cart_item['size_id'] = $item->size_id;
                $product_price1 = ProductColorsSize::where('size_id', $item->size_id)->where('product_id', $item->product_id)->select('general_price', 'special_price')->first();
                if ($permanent) {
                    $cart_item['price'] = $product_price1['special_price'];
                    $cart_item['total_price'] = $item->qty * $product_price1['special_price'];
                } else {
                    $cart_item['price'] = $product_price1['general_price'];
                    $cart_item['total_price'] = $item->qty * $product_price1['general_price'];
                }
            } else {

                $product_price = Product::where('id', $item->product_id)->where('id', $item->product_id)->select('hosted', 'general_price', 'special_price')->first();
                if (($product_price->hosted == 2) && ($product_price->type == 3 || $product_price->type == 4)) {
                    $cart_item['price'] = $product['general_price'];
                    $cart_item['total_price'] = $item->qty * $product['general_price'];
                } else {
                    if ($permanent) {
                        $cart_item['price'] = $product['special_price'];
                        $cart_item['total_price'] = $item->qty * $product['special_price'];
                    } else {
                        $cart_item['price'] = $product['general_price'];
                        $cart_item['total_price'] = $item->qty * $product['general_price'];
                    }
                }
            }

            if ($lang == 'en') {
                $cart_item['product_name'] = $item->product->name_en;
                $cart_item['product_desc'] = $item->product->desc_en;
            } else if ($lang == 'ar') {
                $cart_item['product_name'] = $item->product->name_ar;
                $cart_item['product_desc'] = $item->product->desc_ar;
            } else {
                $cart_item['product_name'] = $item->product->name_kur;
                $cart_item['product_desc'] = $item->product->desc_kur;
            }
            $cart_item['qty'] = $item->qty;
            $cart_item['product_id'] = $item->product->id;
            $cart_item['product_image'] = $item->product->product_image;
            $cart_item['id'] = $item->id;
            $cart_item['rate'] = $this->productRepository->getProductRate($item->product_id);
            array_push($items_cart, $cart_item);
            $total += $cart_item['total_price'];
//            $tax = Setting::where('key', 'tax')->pluck('value')->first();
//            $shipping = Setting::where('key', 'shipping')->pluck('value')->first();
            $shop_country = Shop::where('id', $product->shop_id)->pluck('country_id')->first();
            if ($user->country_id == $shop_country) {
                $GLOBALS['country_tax'] = Country::where('id',$user->country_id)->pluck('tax')->first();
            } else {
                $GLOBALS['country_tax'] = Country::where('id',$shop_country)->pluck('tax')->first();
            }
        }
        return [
            'items' => $items_cart,
            'sub_total' => (string)$total,
            'TaX' => $GLOBALS['country_tax'] . "%",
            'shipping' => Setting::where('key', 'shipping')->pluck('value')->first(),
        ];
    }

    public
    function getSubtotal($items)
    {
        foreach ($items as $cart_item) {
            $product_price[] = $cart_item->qty * $cart_item->product->price_after;
        }
        return array_sum($product_price);
    }

    public
    function create($input)
    {
        if ($input['qty'] > $this->productRepository->getProductStock($input['product_id'])) {
            return false;
        }
        if (UserCart::create($input)) {
            return true;
        }
        return false;
    }

    public function createProduct($input, $type)
    {
        $data['size_id'] = $input['size_id'];
        $data['color_id'] = !empty($input['color_id']) ? $input['color_id'] : null;
        if ($type == 1) {
            $stock = ProductColorsSize::where('product_id', $input['product_id'])->where('size_id', $input['size_id'])->where('color_id', $input['color_id'])->pluck('stock')->first();
            if ($input['qty'] > $stock) {
                return false;
            }
        }
        if (UserCart::create($input)) {
            return true;
        }
        return false;
    }

    public
    function update($input)
    {
        $cart_item = UserCart::where('id', $input['cart_item_id'])->first();
        $cart_item->size_id = !empty($input['size_id']) ? $input['size_id'] : null;
        $cart_item->color_id = !empty($input['color_id']) ? $input['color_id'] : null;
        $cart_item->qty = $input['qty'];

        if ($cart_item->save()) {
            return true;
        }
        return false;
    }

    public
    function delete($input)
    {
        if (UserCart::destroy($input)) {
            return true;
        }
        return false;
    }

    public
    function deleteAllCartItems($check_user)
    {
        $deleted = UserCart::where('user_id', $check_user->id)->delete();
        if ($deleted) {
            return true;
        }
        return false;
    }


    public
    function updateQtyToStock($cart_item)
    {
        if ($cart_item->product->product_stock == 0) {
            return 1;
        }
        if ($cart_item->product->product_stock < $cart_item->qty) {
            $cart_item->qty = $cart_item->product->product_stock;
            $cart_item->save();
        }
        return 0;
    }

    public
    function checkIfProductExists($user_id, $product_id)
    {
        $check = UserCart::where('user_id', $user_id)->where('product_id', $product_id)->exists();
        return $check;
    }

    public
    function checkIfProductSizeExists($user_id, $product_id, $size_id, $color_id)
    {
        $check = UserCart::where('user_id', $user_id)->where('product_id', $product_id)->where('size_id', $size_id)->where('color_id', $color_id)->exists();
        return $check;
    }

    public
    function checkIfProductFoodExists($user_id, $product_id, $size_id)
    {
        $check = UserCart::where('user_id', $user_id)->where('product_id', $product_id)->where('size_id', $size_id)->exists();
        return $check;
    }


    public
    function checkIfProductClothes($product_id)
    {
        $check = Product::where('id', $product_id)->select('type')->first();
        return $check;
    }

    public
    function checkIfProductBuilding($product_id)
    {
        $check = Product::where('id', $product_id)->select('type')->first();

        return $check;
    }
}
