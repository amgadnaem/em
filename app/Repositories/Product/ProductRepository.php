<?php

namespace App\Repositories\Product;

use App\PermanentUser;
use App\Product;
use App\HostProduct;
use App\ProductColor;
use App\ProductColorsSize;
use App\ProductFavorite;
use App\ProductSize;
use App\Repositories\BaseRepository;
use App\Service;
use App\Shop;
use App\ShopFavorite;
use App\ShopFollow;
use App\Size;
use App\UserReview;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\Paginator;
use SimpleSoftwareIO\QrCode\Facades\QrCode;


class ProductRepository extends BaseRepository
{

    public $model;
    public $userFavorite;
    public $userReview;
    public $productImage;


    public function __construct(Product $model)
    {
        $this->model = $model;

    }

    public function getAllProductsDetailPaginate($products, $lang, $user_id = null)
    {
        $product_list = [];
        $product_item = [];
        foreach ($products as $product) {
            $product_item['id'] = $product->id;
            if ($lang == 'ar') {
                $product_item['product_name'] = $product->name_ar;
                $product_item['desc_name'] = $product->desc_ar;
            } else if ($lang == 'en') {
                $product_item['product_name'] = $product->name_en;
                $product_item['desc_name'] = $product->desc_en;
            } else {
                $product_item['product_name'] = $product->name_kur;
                $product_item['desc_name'] = $product->desc_kur;
            }
            $permanent = PermanentUser::where('user_id', $user_id['id'])->where('shop_id', $product->shop_id)->pluck('id')->first();
            if ($permanent) {
                $product_item['price'] = $product->special_price;
            } else {
                $product_item['price'] = $product->general_price;
            }
            $type = Shop::where('id',$product->shop_id)->pluck('service_id')->first();
            $product_item['type'] = Service::where('id',$type)->pluck('type')->first();
                 if ($product->hosted == 2) {
                $hosted_product = HostProduct::where('product_id', $product->id)->where('to_shop', $product->shop_id)->pluck('hosted_product_id')->first();
                $product_item['product_image'] = Product::where('id', $hosted_product)->pluck('product_image')->first();
            } else {
                $product_item['product_image'] = $product->product_image;
            }
            $product_item['hosted_price'] = !empty($product->hosted_price) ? $product->hosted_price : null;
            $product_item['product_image'] = $product->product_image;
            $product_item['qrCode'] = $product->qr_code;
            if ($user_id) {
                $product_item['user_favorite'] = ProductFavorite::
                where('user_id', $user_id['id'])
                    ->where('product_id', $product->id)
                    ->get()
                    ->count();
            }
            $product_item['rate'] = $this->getProductRate($product->id);
            $product_list[] = $product_item;
        }
        return $product_list;

    }

     // function get all product

    function getAllProducts($products, $lang = "en")
    {
        $product_list = [];
        $product_item = [];
        foreach ($products as $product) {
            $product_item['id'] = $product->id;
            if ($lang == 'ar') {
                $product_item['product_name'] = $product->name_ar;
                $product_item['desc_name'] = $product->desc_ar;
            } else if ($lang == 'en') {
                $product_item['product_name'] = $product->name_en;
                $product_item['desc_name'] = $product->desc_en;
            } else {
                $product_item['product_name'] = $product->name_kur;
                $product_item['desc_name'] = $product->desc_kur;
            }
            $product_item['product_image'] = $product->product_image;
            $product_item['price'] = $product->general_price;
            $product_item['rate'] = $this->getProductRate($product->id);
            $product_list[] = $product_item;
        }
        return $product_list;
    }

    // get product details

    function getProductShopDetails($product, $lang = "en")
    {
        $product_list = [];
        $product_item = [];
        $product_item['id'] = $product->id;
        if ($lang == 'ar') {
            $product_item['product_name'] = $product->name_ar;
            $product_item['desc_name'] = $product->desc_ar;
        } else if ($lang == 'en') {
            $product_item['product_name'] = $product->name_en;
            $product_item['desc_name'] = $product->desc_en;
        } else {
            $product_item['product_name'] = $product->name_kur;
            $product_item['desc_name'] = $product->desc_kur;
        }
        $product_item['product_image'] = $product->product_image;
        $product_item['general_price'] = $product->general_price;
        $product_item['special_price'] = $product->special_price;
        $product_item['hosted_price'] = $product->hosted_price;
        $product_item['qrCode'] = $product->qr_code;
        $product_item['category_id'] = $product->category_id;
        $product_item['code'] = $product->code;
        $product_item['rate'] = $this->getProductRate($product->id);
        $product_list[] = $product_item;

        return $product_list;

    }

    function getProductDetails($product, $lang, $user_id = null)
    {
        $product_item['product_id'] = $product->id;
        if ($lang == 'ar') {
            $product_item['product_name'] = $product->name_ar;
            $product_item['desc_name'] = $product->desc_ar;
        } else if ($lang == 'en') {
            $product_item['product_name'] = $product->name_en;
            $product_item['desc_name'] = $product->desc_en;
        } else {
            $product_item['product_name'] = $product->name_kur;
            $product_item['desc_name'] = $product->desc_kur;
        }
        if ( ($product->hosted == 2 ) && ($product->type == 1 || $product->type == 3 || $product->type == 4 ) ) {
            $product_item['price'] = $product->general_price;
        } else {
            $permanent = PermanentUser::where('user_id', $user_id['id'])->where('shop_id', $product->shop_id)->pluck('id')->first();
            if ($permanent) {
                $product_item['price'] = $product->special_price;
            } else {
                $product_item['price'] = $product->general_price;
            }
        }
          $product_item['hosted_price'] = !empty($product->hosted_price) ? $product->hosted_price : null;

        $product_item['hosted'] = !empty($product->hosted_price) ? true : false;

        $product_item['product_image'] = $product->product_image;
        $product_item['qrCode'] = $product->qr_code;

        if ($user_id) {
            $product_item['user_favorite'] = ProductFavorite::
            where('user_id', $user_id['id'])
                ->where('product_id', $product->id)
                ->get()
                ->count();

            $product_item['user_rate'] = UserReview::
            where('user_id', $user_id['id'])
                ->where('product_id', $product->id)
                ->pluck('rate_value')
                ->first();
        }
        $product_item['rate'] = $this->getProductRate($product->id);

            if ($product->type == 1) {
            $product_item['type'] = 1;
            $product_item['product_size'] = $this->getProductSizes($product->id, $user_id);
        } else if ($product->type == 2) {
            $product_item['type'] = 2;
            $product_item['product_size'] = $this->getProductSizesFood($product->id, $user_id);
        } else if ($product->type == 3) {
            $product_item['type'] = 3;
        } else {
            $product_item['type'] = 4;
        }
        return $product_item;
    }

    public function getProductSizesFood($product_id, $user_id)
    {
        $sizes = ProductColorsSize::where('product_id', $product_id)->get();
        $add_items = [];
        $add_list = [];
        foreach ($sizes as $SizeDetails) {
            $add_items['size_id'] = $SizeDetails->size_id;
            $add_items['size_name'] = Size::where('id', $SizeDetails->size_id)->pluck('size_name')->first();
            $shop_id = Product::where('id', $product_id)->pluck('shop_id')->first();
            $permanent = PermanentUser::where('user_id', $user_id['id'])->where('shop_id', $shop_id)->pluck('id')->first();
            if ($permanent) {
                $add_items['price'] = ProductColorsSize::where('product_id', $product_id)->where('size_id',$SizeDetails->size_id)->pluck('special_price')->first();
            } else {
                $add_items['price'] = ProductColorsSize::where('product_id', $product_id)->where('size_id',$SizeDetails->size_id)->pluck('general_price')->first();

            }

            $add_list[] = $add_items;
        }
        return $add_list;
    }

   public function getProductSizes($product_id, $user_id)
    {
        $sizes = ProductColorsSize::where('product_id', $product_id)->get();
        $add_items = [];
        $add_list = [];
        foreach ($sizes as $SizeDetails) {
            $add_items['size_id'] = $SizeDetails->size_id;
            $add_items['size_name'] = Size::where('id', $SizeDetails->size_id)->pluck('size_name')->first();
              $products = Product::where('id', $product_id)->first();
            if ($products['hosted'] == 2 && ($products['type'] == 1 || $products['type'] == 3 || $products['type'] == 4 ) ) {
                $add_items['price'] = $products->general_price;
            } else {
                $shop_id = Product::where('id', $product_id)->pluck('shop_id')->first();
                $permanent = PermanentUser::where('user_id', $user_id['id'])->where('shop_id', $shop_id)->pluck('id')->first();
                if ($permanent) {
                    $add_items['price'] = Product::where('id', $product_id)->pluck('special_price')->first();
                } else {
                    $add_items['price'] = Product::where('id', $product_id)->pluck('general_price')->first();
                }
            }
            $add_items['product_colors'] = $this->getColors($SizeDetails->color_id);


            $add_list[] = $add_items;
        }
        return $add_list;
    }

    public function getColors($size_id)
    {
        $sizes = ProductColor::where('id', $size_id)->select('id', 'color')->get();
        return $sizes;
    }

    public function getProductFavorites($product, $lang = "en", $user_id = null)
    {

        $product_item = [];
        $product_item['id'] = $product['id'];
        if ($lang == 'ar') {
            $product_item['product_name'] = $product['name_ar'];
            $product_item['desc_name'] = $product['desc_ar'];
        } else if ($lang == 'en') {
            $product_item['product_name'] = $product->name_en;
            $product_item['desc_name'] = $product->desc_en;
        } else {
            $product_item['product_name'] = $product->name_kur;
            $product_item['desc_name'] = $product->desc_kur;
        }


        if ( ($product['hosted'] == 2 ) && ($product['type'] == 1 || $product['type'] == 3 || $product['type'] == 4) ) {
            $product_item['price'] = $product->general_price;
            $hosted_product = HostProduct::where('product_id', $product->id)->where('to_shop', $product->shop_id)->pluck('hosted_product_id')->first();
            $product_item['product_image'] = Product::where('id', $hosted_product)->pluck('product_image')->first();
        } else {
            $product_item['product_image'] = $product['product_image'];

            $permanent = PermanentUser::where('user_id', $user_id['id'])->where('shop_id', $product['shop_id'])->pluck('id')->first();
            if ($permanent) {
                $product_item['price'] = $product->special_price;
            } else {
                $product_item['price'] = $product->general_price;
            }
        }
        $product_item['type'] = $product->type;

        $product_item['rate'] = $this->getProductRate($product['id']);

        return $product_item;
    }


    //showAllProducts to user
    public function getShopFavorites($shop, $lang = "en", $user_id = null)
    {

        $shop_item = [];
        $shop_item['id'] = $shop['id'];
        $shop_item['shop_number'] = '#' . $shop['shop_number'];
        if ($lang == 'ar') {
            $shop_item['shop_name'] = $shop['name_app_ar'];
            $shop_item['description'] = $shop['desc_ar'];
        } else if ($lang == 'kur') {
            $shop_item['shop_name'] = $shop['name_app_kur'];
            $shop_item['description'] = $shop['desc_kur'];
        } else {
            $shop_item['shop_name'] = $shop['name_app_en'];
            $shop_item['description'] = $shop['desc_en'];
        }
        $shop_item['shop_image'] = $shop['shop_image'];
        $shop_item['vip'] = !empty($shop->vip) ? true : false;
        $shop_item['secret_password'] = !empty($shop->secret_password) ? true : false;
        $shop_item['followers_count'] = ShopFollow::where('shop_id', $shop['id'])->count();
        if ($user_id) {
            $shop_item['isFavorite'] = ShopFavorite::
            where('user_id', $user_id['id'])
                ->where('shop_id', $shop['id'])
                ->get()
                ->count();
            $shop_item['isFavorite'] = $shop_item['isFavorite'] ? true : false;

            $shop_item['isFollow'] = ShopFollow::
            where('user_id', $user_id['id'])
                ->where('shop_id', $shop['id'])
                ->get()
                ->count();
            $shop_item['isFollow'] = $shop_item['isFollow'] ? true : false;

        }

        return $shop_item;
    }


    public function getProductRate($product_id)
    {
        $avg_rate = UserReview::where('product_id', $product_id)->avg('rate_value');
        $result = round($avg_rate, 1);
        return (string)$result;
    }


    public function getProductPrice($product_id)
    {
        $price = $this->model->where('product_id', $product_id)->pluck('price')->first();
        return $price;
    }


    public function getProductStock($product_id)
    {
        $stock = $this->model->where('id', $product_id)->pluck('product_stock')->first();
        return $stock;
    }

    public function getProductById($product_id)
    {
        $product = $this->model->whereId($product_id)->first();
        $product_details = $this->getProductDetails($product);
        return $product_details;
    }

    public function delete($input)
    {
        if (Product::delete($input)) {
            return true;
        }
        return false;
    }
}
