<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackingCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packing_carts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('card_name_en');
            $table->string('card_name_ar');
            $table->string('card_name_kur');
            $table->string('points');
            $table->longText('desc_en')->nullable();
            $table->longText('desc_ar')->nullable();
            $table->longText('desc_kur')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packing_carts');
    }
}
