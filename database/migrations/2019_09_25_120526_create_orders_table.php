<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('order_number');
            $table->bigInteger('user_id')->unsigned();
            $table->integer('delegate_id')->nullable();
            $table->integer('delivery_method')->nullable();
            $table->integer('shipping_id')->nullable();
            $table->bigInteger('shop_id')->nullable()->unsigned();
            $table->string('subtotal_fees');
            $table->string('shipping_fees');
            $table->string('tax_fees');
            $table->integer('delivery_address_id')->nullable();
            $table->integer('payment_method')->nullable();
            $table->date('order_date');
            $table->integer('order_status')->default(1);
            $table->integer('cancel_order')->default(0);
            $table->integer('used_promo');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('shop_id')->references('id')->on('shops')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
