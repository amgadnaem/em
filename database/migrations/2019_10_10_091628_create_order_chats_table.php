<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_chats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('from')->unsigned();
            $table->bigInteger('to')->unsigned();
            $table->bigInteger('order_id')->unsigned();
            $table->string('message');
            $table->integer('type');
            $table->string('image')->nullable();
            $table->foreign('from')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('to')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('order_id')->references('id')->on('orders')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_chats');
    }
}
