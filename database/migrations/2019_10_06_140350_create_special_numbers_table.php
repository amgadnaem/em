<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialNumbersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('special_numbers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('special_number');
            $table->bigInteger('number_id')->unsigned();
            $table->integer('status')->default(1);
            $table->foreign('number_id')->references('id')->on('special_number_costs')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('special_numbers');
    }
}
