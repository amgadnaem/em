<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('name_en');
            $table->string('name_ar');
            $table->string('name_kur');
            $table->string('desc_en');
            $table->string('desc_ar');
            $table->string('desc_kur');
            $table->string('product_image')->nullable();
            $table->string('general_price');
            $table->string('special_price')->nullable();
            $table->string('hosted_price')->nullable();
            $table->integer('product_stock')->default(0);
            $table->integer('is_building')->default(0);
            $table->integer('is_food')->default(0);
            $table->integer('is_clothes')->default(0);
            $table->bigInteger('shop_id')->nullable()->unsigned();
            $table->bigInteger('category_id')->nullable()->unsigned();
            $table->integer('status')->default(1);
            $table->foreign('category_id')->references('id')->on('shop_categories')->onDelete('cascade');
            $table->foreign('shop_id')->references('id')->on('shops')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
