<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_fees', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('city_from')->unsigned()->nullable();
            $table->bigInteger('city_to')->unsigned()->nullable();
            $table->bigInteger('country_from')->unsigned()->nullable();
            $table->bigInteger('country_to')->unsigned()->nullable();
            $table->bigInteger('company_id')->unsigned();
            $table->string('shipping_fees')->nullable();
            $table->foreign('city_from')->references('id')->on('cities')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('city_to')->references('id')->on('cities')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('country_from')->references('id')->on('countries')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('country_to')->references('id')->on('countries')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('company_id')->references('id')->on('shipping_companies')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_fees');
    }
}
