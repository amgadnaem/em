<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('type',['urgent','scheduled']);
            $table->bigInteger('car_id')->unsigned()->nullable();
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('driver_id')->unsigned()->nullable();
            $table->tinyInteger('order_action')->default(0)->comment('1 completed, 2 canceled');
            $table->enum('canceled_by',['','user','driver']);
            $table->text('feedback')->nullable();
            $table->float('user_rate')->default(0);
            $table->float('driver_rate')->default(0);
            $table->float('total')->default(0);
            $table->string('google_distance')->nullable();
            $table->date('date')->nullable();
            $table->time('time')->nullable();
            $table->timestamps();

            if(Schema::hasTable('cars'))
            {
                $table->foreign('car_id')->references('id')->on('cars');
            }

            if(Schema::hasTable('users'))
            {
                $table->foreign('user_id')->references('id')->on('users');
            }

            if(Schema::hasTable('drivers'))
            {
                $table->foreign('driver_id')->references('id')->on('drivers');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips');
    }
}
