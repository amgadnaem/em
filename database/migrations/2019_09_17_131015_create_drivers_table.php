<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('car_name');
            $table->string('car_color');
            $table->string('car_number');
            $table->integer('driver_number')->unique();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('phone_number');
            $table->string('password')->nullable();
            $table->string('driver_image')->nullable();
            $table->string('activate_code')->nullable();
            $table->integer('user_status')->default(0);
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->longText('location')->nullable();
            $table->integer('country_id');
            $table->integer('city_id');
            $table->date('dob');
            $table->integer('status')->default(1);
            $table->string('jwt_token')->unique();
            $table->longText('firebase_token')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
