<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDescriptionToSpecialNumberCosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('special_number_costs', function (Blueprint $table) {
            $table->string('number_of_digits')->nullable()->after('price');
            $table->string('description')->nullable()->after('number_of_digits');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('special_number_costs', function (Blueprint $table) {
            //
        });
    }
}
