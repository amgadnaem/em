<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_number')->unique();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('phone_number');
            $table->string('password');
            $table->string('user_image')->nullable();
            $table->string('activate_code')->nullable();
            $table->integer('user_status')->default(0);
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->longText('location')->nullable();
            $table->integer('country_id');
            $table->integer('city_id');
            $table->integer('status')->default(1);
            $table->string('jwt_token')->unique();
            $table->longText('firebase_token')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
