<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('order_number')->nullable();
            $table->bigInteger('user_id')->unsigned();
            $table->integer('driver_id')->nullable();
            $table->string('sender_lat');
            $table->string('sender_lng');
            $table->string('sender_location');
            $table->string('receive_lat');
            $table->string('receive_lng');
            $table->string('receive_location');
            $table->string('cost');
            $table->date('order_date')->nullable();
            $table->integer('order_status')->default(0);
            $table->integer('cancel_order')->default(0);
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_orders');
    }
}
