<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_cars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('country_id')->unsigned();
            $table->float('start')->default(0)->comment('start counter');
            $table->float('wait')->default(0)->comment('price waiting time');
            $table->float('minimum_fare')->default(0);
            $table->float('service_fees')->default(0);
            $table->double('cancel_before_fees', 8, 2)->default(0);
            $table->double('cancel_after_fees', 8, 2)->default(0);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_cars');
    }
}
