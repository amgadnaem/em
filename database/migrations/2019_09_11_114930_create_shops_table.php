<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('shop_number')->unique();
            $table->string('name_app_en');
            $table->string('name_app_ar')->nullable();
            $table->string('name_app_kur')->nullable();
            $table->string('name_en')->nullable();
            $table->string('name_ar')->nullable();
            $table->string('name_kur')->nullable();
            $table->string('phone_number');
            $table->string('qr_code');
            $table->integer('shop_status')->default(0);
            $table->string('activate_code')->nullable();
            $table->string('ID_number')->nullable()->unique();
            $table->string('password');
            $table->string('secret_password')->nullable();
            $table->integer('vip')->nullable();
            $table->string('type')->default(3);
            $table->string('shop_image')->nullable();
            $table->string('ID_image')->nullable();
            $table->string('shop_license')->nullable();
            $table->bigInteger('service_id')->unsigned();
            $table->integer('status')->default(1);
            $table->text('location')->nullable();
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->integer('country_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->longText('desc_en');
            $table->longText('desc_ar');
            $table->longText('desc_kur')->nullable();
            $table->integer('active_flag')->default(1);
            $table->date('Subscribe_end')->nullable();
            $table->foreign('service_id')->references('id')->on('services')->onUpdate('cascade')->onDelete('cascade');
            $table->string('jwt_token')->unique();
            $table->longText('firebase_token')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}
