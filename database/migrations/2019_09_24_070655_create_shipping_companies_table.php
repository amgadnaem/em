<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('type')->default(1);
            $table->string('company_number')->nullable();
            $table->string('company_en');
            $table->string('company_ar');
            $table->string('company_kur');
            $table->string('phone_number')->unique();
            $table->string('password');
            $table->string('license_image')->nullable();
            $table->string('company_image')->nullable();
            $table->integer('company_status')->default(0);
            $table->bigInteger('country_id')->unsigned();
            $table->integer('status')->default(1);
            $table->foreign('country_id')->references('id')->on('countries')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_companies');
    }
}
