<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellerPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seller_points', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('seller_name_en')->nullable();
            $table->string('seller_name_ar')->nullable();
            $table->string('seller_name_kur')->nullable();
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->longText('location')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seller_points');
    }
}
