<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */


    'auth' => [
        'account_exists' => 'Phone Number Already Exists',
        'register' => 'Register Successfully',
        'login' => 'Login Successfully',
        'password' => 'Incorrect Password',
        'email' => 'incorrect email',
        'secret_password' => 'make Secret Password Successfully',
        'no_secret_password' => 'de Active Secret Password Successfully',

        'phone' => 'incorrect phone number',
        'phone_password' => 'data incorrect',
        'phone_exist' => 'Phone Number exist',
        'phone_not_exist' => 'Phone Number Not exist',
        'account_suspended' => 'Your Account is Suspended by The Administrator',
        'wrong_activate_code' => 'Wrong Activation Code, Please Check and Try Again',
        'activated_successfully' => 'Account Activated Successfully',
        'active_account' => 'Please Active Account',
        'phone_not_exists' => 'No Account Found with this Phone Number, Please Try Again',
        'message_sent' => 'Check Your Message For Activation Code ',
        'message' => 'You Must Enter Your Message',
        'send message' => 'Send Message Successfully',
        'check_phone' => 'correct phone number',
        'forgetPassword' => 'Password Updated Successfully',
        'delegate_confirm' => 'until Dont Accept account from admin ',
        'user_check' => 'Please Login first',
        'countryInfo' => 'Show data Countries',
        'list' => 'Users List',
        'location' => 'Edit Location Successfully',
        'addUser' => 'add User to permanent users successfully',
        'removedUser' => 'remove User from permanent users successfully',
        'permanentUsers' => 'Permanent Users lists',

    ],
       'new_phone' => [
        'add_phone' => 'Add New Phone Number  Successfully',
        'edit_phone' => 'Update Phone Number  Successfully',
        'delete_phone' => 'Delete Phone Number  Successfully',
        'phones_list' => 'Phones Number List',
    ],

       'Packing' => [
        'list' => 'Carts List',
        'sellerPoints' => 'Show Places of Seller Points',
        'noSeller' => 'Sorry Not find places of seller points in this region',
        'add' => 'add Numbers of Points to account successfully',
        'check' => 'Wrong Code, Please Check and Try Again',
        'check_points' => 'Sorry number of points that want to send bigger than your points',
        'send' => 'pull numbers of points that send  from accounts',
        'check_code' => 'The code you entered has already been used',
        'waiting' => 'send request and waiting for respond on your request',
        'borrow_points' => 'Sorry number of points that want to borrow Not Available in this acoount',
        'accept' => 'Accept Points Successfully',
        'reject' => 'reject points',
    ],


    'shops' => [
        'list' => 'Shops List',
        'numbers_list' => 'Special Numbers List',
        'sendNumber' => 'Special Numbers Sent',
        'categoryList' => 'Categories List',
        'no' => 'No Shops found in this city',
        'addCategory' => 'Add Category successfully',


    ],

    'category' => [
        'list' => 'Category List',
        'no' => 'No Categories found in this Shops',
    ],


    'suggestion' => [
        'added' => 'Suggestion Submitted Successfully'
    ],

    'favorites' => [
        'list' => 'Favorites List',
         'added' => 'Product Added Successfully',
        'addedShop' => 'Shop Added Successfully',
        'removedShop' => 'Shop Removed Successfully',
        'removed' => 'Product Removed Successfully',
    ],
    'review' => [
        'list' => 'Favorites List',
        'added' => 'Review Added Successfully',
        'edit' => 'Rate Edit Successfully',
        'removed' => 'Product Removed Successfully'
    ],

    'cart' => [
        'list' => 'Product Cart Items List',
        'shipping' => 'Shipping Companies List',
        'noShipping' => 'No Shipping Companies ',

        'added' => 'Product Added Successfully',
        'removed' => 'Product Removed Successfully',
        'updated' => 'Product Updated Successfully',
        'messing_details' => 'Something Went Wrong, Please Try Again Later',
        'over_available_stock' => 'Over The Available Stock',
        'already_in_cart' => 'Product Already in Your Cart',
        'building' => 'Product Can not Add to Cart contact us owner',

    ],

    'something_went_wrong' => 'Something Went Wrong, Please Try Again Later',

    'profile' => [
        'user_details' => 'User Details',
        'added' => 'User Details Added Successfully',
        'updated' => 'User Details Updated Successfully',
        'password_updated' => 'Password Updated Successfully',
        'wrong_old_password' => 'Wrong Old Password',
        'lang_updated' => 'The Language Was Updated Successfully',
        'old_password_same_as_new' => 'Your Old Password is The Same as New Password',
        'info' => 'User Information',
        'logged_out' => 'User Logged Out',
    ],

    'order' => [
        'list' => 'Orders List',
        'PreviousList' => 'Previous Orders List',
        'added' => 'Order Submitted Successfully',
        'cancel' => 'Order Canceled Successfully',
        'updated' => 'Order Updated Successfully ',
        'missing_details' => '',
        'info' => 'Order Information',
        'completed' => 'Order Completed Successfully',

        'unconfirmed_order' => 'Unconfirmed Order',
        'new_order' => 'New Order',
        'processing' => 'Processing',
        'delivered' => 'Delivered',
        'trip' => 'Send Order Successfully',
        'DetailsTransfer' => 'Order Details',
        'missing_info' => 'Missing Info',
        'confirm' => 'Accept Order Successfully',
        'update' => 'updated  Order Status  Successfully',
        'address order' => 'Updated Address Successfully',

    ],

   'products' => [
        'list' => 'Products List',
        'host_list' => 'Hosted Products List',
        'shared_list' => 'Shared Products List',
        'page' => 'Product Page',
        'add' => 'Product Added Successfully',
        'edit' => 'Product Edit Successfully',
        'delete' => 'Product delete Successfully',
        'already_in_host' => 'Product Already as Hosted',
    ],


    'follow' => [

        'added' => 'follow Added Successfully',
        'removed' => 'follow Removed Successfully'
    ],

  'notification' => [

        'show' => 'Show Notification',
        'added' => 'Active Notification Service Successfully',
        'removed' => 'disActive Notification Service Successfully',
        'nodata' => 'No Notification',
         'accounts' => 'Shop Account',

    ],

    'promo_code' => [
        'info' => 'PromoCode Success',
        'not_exists_or_expired' => 'Promo Code Doesn\'t Exist or Expired',
        'added' => 'Promotion Added Successfully',
        'used_before' => 'Promotion Code Has Been Used Before'
    ],
    'points' => [
        'redeemed' => 'Points has been Redeemed Successfully',
        'removed' => 'Points Removed Successfully'
    ],
    
       'chats' => [
        'list' => 'chats List',
        'add' => 'Add Package Successfully',
    ],

];
