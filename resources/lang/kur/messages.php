<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */


    'auth' => [
        'account_exists' => 'Hejmara mobîl jixwe heye',
        'register' => 'Têkeve bi serfirazî bi Kurdî',
        'key' => 'البريد الالكترونى او اسم المستخدم غير صحيح',
        'check_email' => 'البريد الالكترونى غير صحيح',
        'login' => 'تسجيل الدخول بنجاح',
        'password' => 'كلمه المرور خاطئه',
        'secret_password' => 'تم عمل باسورد سرى للمتجر بنجاح',
        'no_secret_password' => 'تم الغاء باسورد سرى للمتجر',
        'email' => 'البريد الالكترونى غير صحيح',
        'phone' => 'رقم الهاتف  غير صحيح',
        'phone_password' => 'تاكد من صحه البيانات',
        'phone_exist' => 'رقم الهاتف  موجود',
        'phone_not_exist' => 'رقم الهاتف غير  موجود',
        'account_suspended' => 'تم ايقاف حسابك من قبل الادارة',
        'wrong_activate_code' => 'كود تفعيل خاطئ،حاول مرة اخرى',
        'activated_successfully' => 'تم تفعيل الحساب بنجاح',
        'active_account' => 'من فضلك فعل حسابك',
        'phone_not_exists' => 'لا يوجد حساب بهذا الرقم، تأكد من الرقم و حاول مره اخرى',
        'message_sent' => 'تفقد كود التفعيل في الرسائل',
        'message' => 'من فضلك ادخل رسالتك',
        'send message' => 'تم ارسال رسالتك بنجاح',
        'delegate_confirm' => 'لم يتم قبول طلبك من الادمن حتى الان',
        'user_check' => 'برجاء عمل تسجيل دخول',
//        'user_check' => 'المستخدم غير موجود',
        'list' => 'Users List',
        'forgetPassword' => 'تم تغير الباسورد بنجاح',
        'countryInfo' => 'Show data Countries',
        'addUser' => 'add User to permanent users successfully',
        'removedUser' => 'remove User from permanent users successfully',
        'permanentUsers'=>'Permanent Users lists',
    ],
    'something_went_wrong' => 'حدث خطأ ما، برجاء المحاولة فيما بعد',

    'new_phone' => [
        'add_phone' => 'تم اضافه رقم موبايل جديد للمتجر',
        'edit_phone' => 'تم تعديل رقم موبايل  للمتجر',
        'delete_phone' => 'تم حذف رقم موبايل للمتجر',
        'phones_list' => 'قائمه ارقام تليفونات المتجر',
    ],
    'shops' => [
        'list' => 'Shops List',
        'categoryList' => 'Categories List',
        'no' => 'No Shops found in this city',
        'addCategory' => 'Add Category successfully',
        'edit' => 'Product Edit Successfully',
    ],

    'category' => [
        'list' => 'Category List',
        'no' => 'No Categories found in this Shops',
    ],

    'favorites' => [
        'list' => 'قائمة المفضلات',
        'added' => 'تم الاضافة للمفضلة',
        'removed' => 'تم حذف المنتج من المفضلة'
    ],

    'MainPage' => [
        'all' => 'الصفحه الرئيسيه'
    ],

    'SubCategories' => [
        'list' => 'عرض الاقسام الفرعيه'
    ],

    'cart' => [
        'list' => 'قائمة سلة المشتريات',
        'added' => 'تم اضافة المنتج للسلة',
        'removed' => 'تم حذف المنتج من السلة',
        'updated' => 'تم تعديل المنتج في السلة',
        'missing_details' => 'حدث خطأ ما, برجاء المحاولة لاحقا',
        'over_available_stock' => 'الكمية المطلوبة غير متاحة',
        'already_in_cart' => 'المنتج موجود بالفعل في سلة طلباتك',
    ],

    'review' => [
        'list' => 'قائمة التعليقات',
        'added' => 'تم اضافة التقيم',
        'removed' => 'تم حذف المنتج من المفضلة'
    ],

    'suggestion' => [
        'added' => 'تم تسجيل ارسال اقتراحك بنجاح'
    ],

    'profile' => [
        'user_details' => 'بيانات المستخدم',
        'added' => 'تم تسجيل بنجاح',
        'updated' => 'تم تعديل البيانات بنجاح',
        'password_updated' => 'تم تعديل كلمة المرور بنجاح',
        'wrong_old_password' => 'كلمة المرور القديمة غير صحيحة',
        'lang_updated' => 'تم تعديل اللغة بنجاح',
        'missing_details' => 'حدث خطأ ما, برجاء المحاولة لاحقا',
        'old_password_same_as_new' => 'كلمة المرور الجديدة يجب ان تكون مختلفة ان سابقتها',
        'info' => 'معلومات المستخدم',
        'logged_out' => 'تم تسجيل الخروج',
    ],
    'address' => [
        'list' => 'قائمة العناوين',
        'added' => 'تم اضافة عنوان جديد',
        'removed' => 'تم حذف العنوان',
        'updated' => 'تم تعديل العنوان',
        'missing_details' => 'حدث خطأ ما, برجاء المحاولة لاحقا',
    ],

    'order' => [
        'list' => 'قائمة الطلبات',
        'PreviousList' => 'قائمة الطلبات السابقه',
        'added' => 'تم تسجيل طلبك بنجاح',
        'cancel' => 'تم حذف الطلب بنجاح',
        'updated' => 'تم تعديل الطلب بنجاح',
        'missing_details' => 'حدث خطأ ما, برجاء المحاولة لاحقا',
        'info' => 'معلومات الطلب',
        'completed' => 'تم انهاء الطلب بنجاح',
        'confirm' => 'تم قبول الطلب بنجاح',
        'updated' => 'تم تغيير حاله الطلب',
        'addressorder' => 'تم تعديل عنوانك',


        'unconfirmed_order' => 'طلب لم بتم تأكيده',
        'new_order' => 'طلب جديد',
        'processing' => 'طلب جاري العمل عليه',
        'missing_info' => 'لم يتم توفير المعلومات المطلوبة',
    ],

    'products' => [
        'list' => 'قائمة المنتجات',
        'offers' => 'قائمة العروض',
        'page' => 'صفحة المنتج',
        'delete' => 'Product delete Successfully',
    ],

    'follow' => [
        'added' => 'تم عمل متابعه للمحل',
        'removed' => 'تم حذف المتابعه للمحل'
    ],
    'notification' => [

        'show' => 'عرض الاشعارات',
        'nodata' => 'لا توجد الاشعارات',
    ],

    'promo_code' => [
        'not_exists_or_expired' => 'كود الخصم خاطئ او تم انتهاء العرض',
        'added' => 'تم اضافة الكود بنجاح',
        'used_before' => 'تم استخدام هذا الكود من قبل'
    ],


];
