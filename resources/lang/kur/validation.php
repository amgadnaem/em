<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'first_name' => 'yekem_name pêdivî ye',
    'last_name' => 'paşîn_name pêdivî ye',
    'password' => 'şîfreya hewce ye',
    'country_id' => 'navê welatê pêwîst',
    'city_id' => 'navê bajêr pêwîst e',

    'activate_code' => 'كود التفعيل مطلوب',
    'comment' => 'الكومنت مطلوب',
    'product_id' => 'اسم المنتج  مطلوب',
    'size' => 'حجم المنتج  مطلوب',
    'newPassword' => 'كلمه المرور الجديده مطلوبه',
    'confirmPassword' => 'تاكيد كلمه المرور مطلوبه',
    'message' => 'عرض البيانات بنجاح',
    'no_data' => 'لا يوجد بيانات',
    'license' => 'برجاء ادخال رقم الرخصه',
    'delegate_id' => 'برجاء ادخال رقم البطاقه',

    'exists' => 'كود ال JWT غير صحيح',
    'lang' => 'اللغه مطلوبه',
    'payment_method' => 'يجب عليك اختيار طريقه الدفع',
    'delivery_address_id' => 'يجب عليك اختيار  العنوان',
    'order_id' => 'رقم الطلب مطلوب',

    /*   */
    'city_name_en' => 'اسم المدينه مطلوب باللغه الانجليزيه',
    'city_name_ar' => 'اسم المدينه مطلوب باللغه العربيه',


    'min' => [
        'numeric' => 'كلمه المرور يجب ان تحتوي على الاقل على 6 حروف',

    ],

    // shops
    'category_name' => 'Please Enter Category name',
    'category_image' => 'Please Enter Category Image',
    'product_name' => 'Please Enter Product Name',
    'product_code' => 'Please Enter Product Code',
    'general_price' => 'Please Enter General Price',
    'special_price' => 'Please Enter Special Price',
    'category_id' => 'Please Enter Category Name',
    'product_desc' => 'Please Enter Product Description',
    'product_image' => 'Please Enter Product Image',
    'hosted_price' => 'Please Enter Hosted Price',
    'real_shop_name' => 'Real Shop Name Required',
    'shop_name_app' => 'Shop Name App Required',
    'phone_number' => 'Phone Number Required',
    'lat' => 'lat  Required',
    'lng' => 'lng  Required',
    'address' => 'address Required',
    'service_id' => 'Category Name Required',
    'shop_image' => 'Shop Image Required',


];
