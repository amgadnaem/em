@extends('AdminPanel.backend.layouts.default')
@section('content')

    <!-- Right sidebar Ends-->

    <div class="page-body">
        <div class="container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <button class="btn btn-primary" type="button" data-toggle="modal"
                                    data-target="#exampleModalgetbootstrap" data-whatever="@getbootstrap">اضافه قسم
                                للمتاجر
                            </button>
                            <div class="modal fade" id="exampleModalgetbootstrap" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">اضافه قسم للمتاجر</h5>
                                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <form class="needs-validation" novalidate=""
                                                  action="{{route('AddCategory')}}"
                                                  method="POST">
                                                {{ method_field('POST') }}
                                                {{ csrf_field() }}
                                                <div class="form-group">

                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">اسم القسم</label>
                                                    <input class="form-control" name="category_name" id="category_name"
                                                           type="text"
                                                           placeholder="ادخل اسم القسم"
                                                           required="">
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('category_name') }}</div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn btn-primary" type="submit">اضافه</button>
                                                    <button class="btn btn-secondary" type="button"
                                                            data-dismiss="modal">اغلاق
                                                    </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    @if( count($categories) > 0)
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display dataTable" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>اسم القسم</th>
                                        <th>الاكشن</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($categories as $cat)
                                        <tr>

                                            <td>{{ $cat->category_name }}</td>
                                            <td>
                                                <div class="media-body ">
                                                    <label class="switch">
                                                        <input type="checkbox"
                                                               {{ $cat->status == 1 ? 'checked' : '' }}
                                                               onchange="change_status_category({{ $cat->id }},{{ $cat->status }})"><span
                                                            class="switch-state"></span>

                                                    </label>
                                                </div>


                                            </td>


                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    @else

                        <h4 class="text-center"> لا توجد اقسام مضافه للمتاجر</h4>
                    @endif

                </div>
            </div>
        </div>
    </div>
    </div>



    <script>

        function change_status_category(id, value) {
            if (value == 0) {
                value = 1;
            } else {
                value = 0;
            }
            axios.get('updateCategory/' + id + '/' + value)
                .then(function (response) {
                    // alert(response.data.category_status);
                    location.reload();
                })
                .catch(function (error) {
                    console.log(error);
                    alert(error);
                });
        };


    </script>

@endsection








