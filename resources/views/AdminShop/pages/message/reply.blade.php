@extends('AdminPanel.backend.layouts.shop')
@section('content')
    <!-- Right sidebar Ends-->
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">

                    </div>
                    <!-- Bookmark Start-->
                    <div class="col">
                        <div class="bookmark pull-right">
                        </div>
                    </div>
                    <!-- Bookmark Ends-->
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">

                <div class="col call-chat-body">
                    <div class="card">
                        <div class="card-body p-0">
                            <div class="row chat-box">
                                <!-- Chat right side start-->
                                <div class="col pr-0 chat-right-aside">
                                    <!-- chat start-->
                                    <div class="chat">
                                        <!-- chat-header start-->
                                        <div class="chat-header clearfix"><img class="rounded-circle"
                                                                               src="{{ !empty($reply->owner->shop_image) ? $reply->owner->shop_image : nul
                                                                               }}"
                                                                               alt="">
                                            <div class="about">
                                                <div class="name">{{ !empty($reply->owner->shop_image) ? $reply->owner->shop_image : nul
                                                                               }}</div>

                                            </div>
                                        </div>
                                        <!-- chat-header end-->
                                        <div class="chat-history chat-msg-box custom-scrollbar">
                                            <ul>
                                                @foreach($body as $reply_message)
                                                    @if(($reply_message->from == Session::get('frontSession') ) )
                                                        <li>
                                                            <div class="message other-message ">
                                                                <img
                                                                    class="rounded-circle float-left chat-user-img img-30"
                                                                    src="{{ $reply_message->owner['shop_image'] }}"
                                                                    alt="">
                                                                <div class="message-data text-right"><span
                                                                        class="message-data-time">{{$reply_message->created_at->diffForHumans()}}</span>
                                                                </div>
                                                                {{ $reply_message->body }}
                                                            </div>
                                                        </li>
                                                    @else
                                                        <li class="clearfix">
                                                            <div class="message my-message  pull-right"><img
                                                                    class="rounded-circle float-left chat-user-img img-30"
                                                                    src="{{ \Illuminate\Support\Facades\Auth::user()->user_image }}"
                                                                    alt="">
                                                                <div class="message-data"><span
                                                                        class="message-data-time">{{$reply_message->created_at->diffForHumans()}}</span>
                                                                </div>
                                                                {{ $reply_message->body }}
                                                            </div>
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div>
                                        <!-- end chat-history-->
                                        <form action="{{ route('createShopMessage') }}" method="post">
                                            {{ method_field('POST') }}
                                            {{ csrf_field() }}
                                            <div class="chat-message clearfix">
                                                <div class="row">
                                                    <div class="col-xl-12 d-flex">
                                                        <div class="smiley-box bg-primary">
                                                            <div class="picker"><img
                                                                    src="{{ asset('images/smiley.png') }}"
                                                                    alt=""></div>
                                                        </div>

                                                        <div class="input-group text-box">
                                                            <input class="form-control input-txt-bx"
                                                                   style="display: none"
                                                                   type="text" name="shop_id"
                                                                   value="{{ $reply->owner->id}}"
                                                                   placeholder="رد على الرساله">

                                                            <input class="form-control input-txt-bx"
                                                                   id="message-to-send"
                                                                   type="text" name="send_message"
                                                                   placeholder="رد على الرساله" required>
                                                            <div class="input-group-append">
                                                                <button class="btn btn-primary" type="submit">ارسال
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                        <!-- end chat-message-->
                                        <!-- chat end-->
                                        <!-- Chat right side ends-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
{{--@section('script')--}}
{{--    <script type="text/javascript">--}}
{{--        $.ajaxSetup({--}}
{{--            headers: {--}}
{{--                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
{{--            }--}}
{{--        });--}}
{{--        $('.chat_list').click(function () {--}}
{{--            $('.chat_list').removeClass('active_chat');--}}
{{--            $(this).addClass('active_chat');--}}
{{--            var user_id = $(this).attr('data-user-id');--}}
{{--            $('.msg_send_btn').attr('data-user-id', user_id);--}}

{{--            var data = {user_id};--}}

{{--            function update() {--}}
{{--                $.ajax({--}}
{{--                    type: "POST",--}}
{{--                    url: '',--}}
{{--                    data: data,--}}
{{--                    dataType: 'json',--}}
{{--                    success: function (data) {--}}
{{--                        var msgHistory = $('.msg_history');--}}
{{--                        msgHistory.empty();--}}
{{--                        $.each(data, function (k, v) {--}}
{{--                            var dt = new Date(v.created_at);--}}
{{--                            var time = moment(dt).format('HH:mm A');--}}
{{--                            var month = moment(dt).format("D MMMM");--}}
{{--                            if (v.user == 'admin') {--}}
{{--                                msgHistory.append('<div class="outgoing_msg">' +--}}
{{--                                    '<div class="sent_msg">' +--}}
{{--                                    '<p>' + v.body +--}}
{{--                                    '</p>' +--}}
{{--                                    '<span class="time_date"> ' + time + ' | ' + month + '</span> </div>' +--}}
{{--                                    '</div>');--}}
{{--                            } else {--}}
{{--                                msgHistory.append('<div class="incoming_msg">' +--}}
{{--                                    '<div class="incoming_msg_img"> <img src="' + v.owner.user_image + '" alt="user-image"> </div>' +--}}
{{--                                    '<div class="received_msg">' +--}}
{{--                                    '<div class="received_withd_msg">' +--}}
{{--                                    '<p>' + v.body +--}}
{{--                                    '</p>' +--}}
{{--                                    '<span class="time_date">' + time + ' | ' + month + '</span></div>' +--}}
{{--                                    '</div>' +--}}
{{--                                    '</div>');--}}
{{--                            }--}}
{{--                        });--}}
{{--                        $(msgHistory).scrollTop(1000);--}}
{{--                    }, beforeSend: function () {--}}
{{--                        var msgHistory = $('.msg_history');--}}
{{--                        msgHistory.empty();--}}
{{--                        $('.loader').show();--}}
{{--                    },--}}
{{--                    complete: function () {--}}
{{--                        $('.loader').hide();--}}
{{--                    }--}}

{{--                });--}}
{{--            }--}}

{{--            setTimeout(update, 1000);--}}


{{--        });--}}
{{--        $('.msg_send_btn').click(function () {--}}
{{--            var user_id = $('.msg_send_btn').attr('data-user-id');--}}
{{--            var body = $('.write_msg').val();--}}
{{--            var data = {user_id, body};--}}
{{--            if (user_id) {--}}
{{--                $.ajax({--}}
{{--                    type: "POST",--}}
{{--                    url: '',--}}
{{--                    data: data,--}}
{{--                    success: function (data) {--}}
{{--                        if (data.status) {--}}
{{--                            var dt = new Date(data.message.created_at);--}}
{{--                            var time = moment(dt).format('HH:mm A');--}}
{{--                            var month = moment(dt).format("D MMMM");--}}
{{--                            var msgHistory = $('.msg_history');--}}
{{--                            msgHistory.append('<div class="outgoing_msg">' +--}}
{{--                                '<div class="sent_msg">' +--}}
{{--                                '<p>' + data.message.body +--}}
{{--                                '</p>' +--}}
{{--                                '<span class="time_date"> ' + time + ' | ' + month + '</span> </div>' +--}}
{{--                                '</div>');--}}
{{--                            $(msgHistory).scrollTop(msgHistory.scrollHeight);--}}
{{--                            $('.write_msg').val('');--}}
{{--                            $('#latest-' + user_id).text(data.message.body);--}}

{{--                        }--}}
{{--                    }--}}
{{--                });--}}
{{--            }--}}
{{--        });--}}

{{--    </script>--}}

