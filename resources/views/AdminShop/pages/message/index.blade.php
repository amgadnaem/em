
@extends('AdminPanel.backend.layouts.shop')
@section('content')

    <!-- Right sidebar Ends-->

    <div class="page-body">
        <div class="container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <button class="btn btn-primary" type="button" data-toggle="modal"
                                    data-target="#messages" data-whatever="@test">ارسال رساله للمتاجر
                            </button>
                            <div class="modal fade" id="messages" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">ارسال رساله</h5>
                                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <form class="needs-validation" novalidate=""
                                                  action="{{route('createAdminMessage')}}"
                                                  method="POST">
                                                {{ method_field('POST') }}
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <label for="category_id"
                                                           class="page-header-left"> المتاجر</label>
                                                    <select name="shop_id"
                                                            class="form-control"
                                                            required>
                                                        <option value="">اختر</option>
                                                        @foreach($all_shops as $shop)
                                                            <option
                                                                value="{{ $shop->id }}">{{ $shop->shop_name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('shop_id') }}</div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left"> محتوى
                                                        الرساله</label>
                                                    <input class="form-control" name="send_message"
                                                           type="text"
                                                           placeholder="ادخل محتوى الرساله"
                                                           required="">
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('send_message') }}</div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn btn-primary" type="submit">ارسال</button>
                                                    <button class="btn btn-secondary" type="button"
                                                            data-dismiss="modal">اغلاق
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            @if( count($inbox_users) > 0)
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="display dataTable" id="basic-1">
                                            <thead>
                                            <tr>
                                                <th>صوره المتجر</th>
                                                <th>اسم المتجر</th>
                                                <th>الرساله</th>
                                                <th>وقت الرساله</th>
                                                <th>الاكشن</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($inbox_users as $inbox_user)
                                                <tr>
                                                    <td><img src=" {{$inbox_user->owner->shop_image}}"
                                                             style="width: 50px"
                                                             alt="">
                                                    </td>
                                                    <td>{{$inbox_user->owner->shop_name}} </td>
                                                    <td>{{$inbox_user->body}}</td>
                                                    <td>{{$inbox_user->created_at->diffForHumans()}}</td>
                                                    <td>
                                                        <a href="{{ route('messages.reply',['id'=> $inbox_user->owner->id]) }}">
                                                            <i class="fa icofont-social-facebook-messenger"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            @else
                                <h4 class="text-center"> لا توجد رسائل </h4>
                            @endif

                        </div>
                    </div>
                </div>

            </div>
        </div>
        {{--        <div class="row">--}}
        {{--            <div class="col-sm-12">--}}
        {{--                <div class="card">--}}
        {{--                    @if( count($categories) > 0)--}}
        {{--                        <div class="card-body">--}}
        {{--                            <div class="table-responsive">--}}
        {{--                                <table class="display dataTable" id="basic-1">--}}
        {{--                                    <thead>--}}
        {{--                                    <tr>--}}
        {{--                                        <th>اسم القسم الرئيسى</th>--}}
        {{--                                        <th>اسم القسم الفرعى</th>--}}
        {{--                                        @if (auth()->user()->hasPermissionTo('تعديل_قسم'))--}}
        {{--                                            <th>الاكشن</th>--}}
        {{--                                        @endif--}}
        {{--                                    </tr>--}}
        {{--                                    </thead>--}}
        {{--                                    <tbody>--}}
        {{--                                    @foreach($categories as $cat)--}}
        {{--                                        <tr>--}}
        {{--                                            <td>{{ $cat->service($cat->service_id) }}</td>--}}
        {{--                                            <td>{{ $cat->category_name }}</td>--}}
        {{--                                            @if (auth()->user()->hasPermissionTo('تعديل_قسم'))--}}
        {{--                                                <td>--}}
        {{--                                                    <div class="media-body ">--}}
        {{--                                                        <label class="switch">--}}
        {{--                                                            <input type="checkbox"--}}
        {{--                                                                   {{ $cat->status == 1 ? 'checked' : '' }}--}}
        {{--                                                                   onchange="change_status_category({{ $cat->id }},{{ $cat->status }})"><span--}}
        {{--                                                                class="switch-state"></span>--}}
        {{--                                                        </label>--}}
        {{--                                                    </div>--}}
        {{--                                                </td>--}}
        {{--                                            @endif--}}
        {{--                                        </tr>--}}
        {{--                                    @endforeach--}}
        {{--                                    </tbody>--}}
        {{--                                </table>--}}
        {{--                            </div>--}}
        {{--                        </div>--}}

        {{--                    @else--}}

        {{--                        <h4 class="text-center"> لا توجد اقسام مضافه للمتاجر</h4>--}}
        {{--                    @endif--}}

        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </div>--}}
    </div>


@endsection


















