@extends('AdminPanel.backend.layouts.shop')
@section('content')


    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDPN_XufKy-QTSCB68xFJlqtUjHQ8m6uUY&callback=initialize">
    </script>
    <!-- Right sidebar Ends-->
    <div class="page-body">

        <!-- Default ordering (sorting) Starts-->
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h3>تعديل بيانات متجر </h3>
                </div>
            </div>
        </div>


        <!--map div-->
        <div id="map"></div>

        <!--our form-->

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">

                            <form class="needs-validation" novalidate=""
                                  action="{{route('shop.update',['id'=> $show_shop->id]) }}"
                                  method="POST" enctype="multipart/form-data">
                                {{ method_field('POST') }}
                                {{ csrf_field() }}

                                <div class="form-row">

                                    <div class="col-md-4 mb-3">
                                        <label for="validationCustom04"> اسم المتجر فى التطبيق باللغه
                                            العربيه</label>
                                        <input class="form-control" type="text" name="name_app_ar"
                                               value="{{$show_shop->name_app_ar}}"
                                               required>
                                        <div class="invalid-feedback">{{ $errors->first('name_app_ar') }}.</div>
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="validationCustom04">اسم المتجر فى التطبيق باللغه
                                            الانجليزيه</label>
                                        <input class="form-control" type="text" name="name_app_en"
                                               value="{{$show_shop->name_app_en}}"
                                               required>
                                        <div class="invalid-feedback">{{ $errors->first('name_app_en') }}.</div>
                                    </div>

                                    <div class="col-md-4 mb-3">
                                        <label for="validationCustom04">اسم المتجر فى التطبيق باللغه الكرديه</label>
                                        <input class="form-control" type="text" name="name_app_kur"
                                               value="{{$show_shop->name_app_kur}}"
                                               required>
                                        <div class="invalid-feedback">{{ $errors->first('name_app_kur') }}.</div>
                                    </div>

                                    <div class="col-md-4 mb-3">
                                        <label for="validationCustom04"> اسم المتجر الحقيقى باللغه
                                            العربيه</label>
                                        <input class="form-control" type="text" name="name_ar"
                                               value="{{$show_shop->name_ar}}"
                                               required>
                                        <div class="invalid-feedback">{{ $errors->first('name_ar') }}.</div>
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="validationCustom04">اسم المتجر الحقيقى باللغه
                                            الانجليزيه</label>
                                        <input class="form-control" type="text" name="name_en"
                                               value="{{$show_shop->name_en}}"
                                               required>
                                        <div class="invalid-feedback">{{ $errors->first('name_en') }}.</div>
                                    </div>

                                    <div class="col-md-4 mb-3">
                                        <label for="validationCustom04">اسم المتجر الحقيقى باللغه الكرديه</label>
                                        <input class="form-control" type="text" name="name_kur"
                                               value="{{$show_shop->name_kur}}"
                                               required>
                                        <div class="invalid-feedback">{{ $errors->first('name_kur') }}.</div>
                                    </div>

                                    <div class="col-md-4 mb-3">
                                        <label for="validationCustom04"> وصف عن المتجر بالغه العربيه</label>
                                        <textarea class="form-control" name="desc_ar"
                                                  required>{{ $show_shop->desc_ar }}</textarea>
                                        <div class="invalid-feedback">{{ $errors->first('desc_ar') }}.</div>
                                    </div>

                                    <div class="col-md-4 mb-3">
                                        <label for="validationCustom04"> وصف عن المتجر باللغه الانجليزيه</label>
                                        <textarea class="form-control" name="desc_en"
                                                  required>{{ $show_shop->desc_en }}</textarea>
                                        <div class="invalid-feedback">{{ $errors->first('desc_en') }}.</div>
                                    </div>

                                    <div class="col-md-4 mb-3">
                                        <label for="validationCustom04"> وصف عن المتجر بالغه الكرديه</label>
                                        <textarea class="form-control" name="desc_kur"
                                                  required>{{ $show_shop->desc_kur }}</textarea>
                                        <div class="invalid-feedback">{{ $errors->first('desc_kur') }}.</div>
                                    </div>

                                    <div class="col-md-4 mb-3">
                                        <label for="validationCustom04">تفاصيل العنوان</label>
                                        <textarea class="form-control" name="location"
                                                  value="{{$show_shop->location}}"
                                                  required>{{$show_shop->location}}</textarea>
                                        <div class="invalid-feedback">{{ $errors->first('address') }}.</div>
                                    </div>


                                    <div class="col-md-6 mb-3" style="display: none">
                                        <label for="validationCustom04">Latitude</label>
                                        <input class="form-control" {{ old('lat') }} name="lat" id="lat"
                                               id="validationCustom04" type="text" value="{{$show_shop->lat}}"
                                               placeholder="Enter lat"
                                               required="">
                                        <div class="invalid-feedback">{{ $errors->first('lat') }}.</div>
                                    </div>


                                    <div class="col-md-6 mb-3" style="display: none">
                                        <label for="validationCustom04">Longitude</label>
                                        <input class="form-control" {{ old('lng') }} name="lng" id="lng"
                                               id="validationCustom04" type="text" value="{{$show_shop->lng}}"
                                               placeholder="Enter lng"
                                               required="">
                                        <div class="invalid-feedback">{{ $errors->first('lng') }}.</div>
                                    </div>

                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom05">صوره المتجر</label>
                                        <div class="custom-file">
                                            <input class="custom-file-input" id="shop_image" type="file"
                                                   name="shop_image">
                                            <label class="custom-file-label" for="validatedCustomFile">اختر صوره</label>
                                            <div class="invalid-feedback">{{ $errors->first('shop_image') }}</div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <img src="{{ $show_shop->shop_image }}"
                                             style="width: 100px"
                                             class="img-thumbnail image-preview" alt="">
                                    </div>
                                </div>
                                @if ( (!empty($roles) ) || ( auth()->user()->hasPermissionTo('تعديل_متجر') ) )

                                    @if (empty(Session::get('parent_id')))
                                        <button class="btn btn-primary" type="submit">تعديل بيانات متجر</button>
                                    @endif
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">

            var map = new google.maps.Map(document.getElementById('map'), {
                center: {
                    lat: {{ $show_shop->lat }},
                    lng: {{ $show_shop->lng }}
                },
                zoom: 10
            });
            var marker = new google.maps.Marker({
                position: {
                    lat: {{ $show_shop->lat }},
                    lng: {{ $show_shop->lng }}
                },
                map: map,
                draggable: true
            });

            google.maps.event.addListener(marker, 'position_changed', function () {
                var lat = marker.getPosition().lat();
                var lng = marker.getPosition().lng();

                $('#lat').val(lat);
                $('#lng').val(lng);
            });

        </script>


        @endsection

    </div>







