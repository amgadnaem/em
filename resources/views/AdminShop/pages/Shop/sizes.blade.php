@extends('AdminPanel.backend.layouts.shop')
@section('content')

    <!-- Right sidebar Ends-->
    <div class="page-body">
        <div class="container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">

                        <div class="page-header-left">
                            <button class="btn btn-primary" type="button" data-toggle="modal"
                                    data-target="#sizes" data-whatever="@sizes">اضافه مقاس للمنتج
                            </button>
                            <div class="modal fade" id="sizes" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">اضافه مقاس للمنتج</h5>
                                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <form class="needs-validation" novalidate=""
                                                  action="{{route('addSizes',['id'=>$product->id])}}"
                                                  method="POST" enctype="multipart/form-data">
                                                {{ method_field('POST') }}
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="page-header-left">المقاسات</label>
                                                    <select name="size_id"
                                                            class="form-control"
                                                            required>
                                                        <option value="">اختر</option>
                                                        @foreach($all_sizes as $sizes)
                                                            <option
                                                                value="{{ $sizes->id }}">{{ $sizes->size_name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('size_id') }}</div>
                                                </div>

                                                @if($type != 2)
                                                    <div class="form-group">
                                                        <label for="validationCustom05"
                                                               class="page-header-left">الالوان</label>
                                                        <select name="color_id"
                                                                class="form-control"
                                                                required>
                                                            <option value="">اختر</option>
                                                            @foreach($all_colors as $colors)

                                                                <option value="{{ $colors->id }}"
                                                                        style="background-color: {{$colors->color}}">{{ $colors->color }}</option>
                                                            @endforeach
                                                        </select>
                                                        <div
                                                            class="invalid-feedback">{{ $errors->first('color_id') }}</div>
                                                    </div>
                                                @endif
                                                @if($type == 2)

                                                    <div class="form-group">
                                                        <label
                                                            class="col-form-label page-header-left">السعر العام
                                                        </label>
                                                        <input class="form-control"
                                                               name="general_price"
                                                               type="text" required>
                                                        <div
                                                            class="invalid-feedback">{{ $errors->first('general_price') }}</div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label
                                                            class="col-form-label page-header-left">
                                                            السعر الخاص
                                                        </label>
                                                        <input class="form-control"
                                                               name="special_price"
                                                               type="text" required>
                                                        <div
                                                            class="invalid-feedback">{{ $errors->first('special_price') }}</div>
                                                    </div>
                                                @endif


                                                <div class="form-group">
                                                    <label
                                                        class="col-form-label page-header-left">
                                                        كميه مخزون
                                                        المنتج</label>
                                                    <input class="form-control"
                                                           name="product_stock"
                                                           type="text" required>
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('product_stock') }}</div>
                                                </div>


                                                <div class="modal-footer">
                                                    <button class="btn btn-primary" type="submit">
                                                        اضافه
                                                    </button>
                                                    <button class="btn btn-secondary" type="button"
                                                            data-dismiss="modal">اغلاق
                                                    </button>
                                                </div>
                                            </form>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    @if( count($all_product_colors) > 0)
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display dataTable" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>اسم المنتج</th>
                                        <th>مقاس المنتج</th>
                                        @if($type == 2)
                                            <th> السعر العام</th>
                                            <th> السعر الخاص</th>
                                        @endif
                                        @if($type != 2)
                                            <th> اللون</th>
                                        @endif
                                        <th>مخزون</th>
                                        <th>الاكشن</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($all_product_colors as $sizes)
                                        <tr>
                                            <td>{{ $sizes->product->name_ar }}</td>
                                            <td>{{ $sizes->size_name($sizes->size_id) }}</td>
                                            @if($type == 2)
                                                <td>{{ $sizes->general_price }}</td>
                                                <td>{{ $sizes->special_price }}</td>
                                            @endif
                                            @if($type != 2)
                                                <td style="background-color: {{$sizes->color_name($sizes->color_id)}}">{{ $sizes->color_name($sizes->color_id) }}</td>
                                            @endif
                                            <td>{{ $sizes->stock }}</td>
                                            <td>
                                                <button class="btn btn-primary" type="button" data-toggle="modal"
                                                        data-target="#{{ $sizes->id }}"
                                                        data-whatever="@category"><i class="fa fa-edit"></i>
                                                </button>

                                                <form action="{{ route('destroySize', $sizes->id) }}"
                                                      method="post" style="display: inline-block">
                                                    {{ csrf_field() }}
                                                    {{ method_field('delete') }}
                                                    <button type="submit" class="btn btn-danger delete btn-sm"><i
                                                            class="fa fa-trash"></i>
                                                    </button>
                                                </form>

                                                <div class="modal fade" id="{{ $sizes->id }}" tabindex="-1"
                                                     role="dialog"
                                                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title"> تعديل مقاس
                                                                    {{ $sizes->product->name_ar }} </h5>
                                                                <button class="close" type="button" data-dismiss="modal"
                                                                        aria-label="Close">
                                                                    <span aria-hidden="true">×</span></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form class="needs-validation" novalidate=""
                                                                      action="{{route('updateSizes',['id'=>$sizes->id])}}"
                                                                      method="POST" enctype="multipart/form-data">
                                                                    {{ method_field('POST') }}
                                                                    {{ csrf_field() }}
                                                                    <div class="form-group">
                                                                        <label for="category_id"
                                                                               class="page-header-left">المقاسات</label>
                                                                        <select
                                                                            name="size_id"
                                                                            class="form-control"
                                                                            required>

                                                                            @foreach($all_sizes as $size)
                                                                                @if($size->id == $sizes->size_id )
                                                                                    <option value="{{ $size->id }}"
                                                                                            selected>{{ $size->size_name }}</option>
                                                                                @else
                                                                                    <option
                                                                                        value="{{ $size->id }}">{{ $size->size_name }}</option>
                                                                                @endif
                                                                            @endforeach

                                                                        </select>
                                                                        <div
                                                                            class="invalid-feedback">{{ $errors->first('size_id') }}</div>
                                                                    </div>
                                                                    @if($type != 2)
                                                                        <div class="form-group">
                                                                            <label for="category_id"
                                                                                   class="page-header-left">الالوان</label>
                                                                            <select
                                                                                name="color_id"
                                                                                class="form-control"
                                                                                required>

                                                                                @foreach($all_colors as $colors)
                                                                                    @if($colors->id == $sizes->color_id )
                                                                                        <option
                                                                                            value="{{ $colors->id }}"
                                                                                            selected>{{ $colors->color }}</option>
                                                                                    @else
                                                                                        <option
                                                                                            value="{{ $colors->id }}"
                                                                                            style="background-color: {{$colors->color}}">{{ $colors->color }}</option>
                                                                                    @endif
                                                                                @endforeach
                                                                            </select>
                                                                            <div
                                                                                class="invalid-feedback">{{ $errors->first('color_id') }}</div>
                                                                        </div>
                                                                    @endif

                                                                    @if($type == 2)

                                                                        <div class="form-group">
                                                                            <label
                                                                                class="col-form-label page-header-left">السعر
                                                                                العام
                                                                            </label>
                                                                            <input class="form-control"
                                                                                   value="{{ $sizes->special_price }}"

                                                                                   name="general_price"
                                                                                   type="text" required>
                                                                            <div
                                                                                class="invalid-feedback">{{ $errors->first('general_price') }}</div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label
                                                                                class="col-form-label page-header-left">
                                                                                السعر الخاص
                                                                            </label>
                                                                            <input class="form-control"
                                                                                   value="{{ $sizes->special_price }}"
                                                                                   name="special_price"
                                                                                   type="text" required>
                                                                            <div
                                                                                class="invalid-feedback">{{ $errors->first('special_price') }}</div>
                                                                        </div>
                                                                    @endif

                                                                    <div class="form-group">
                                                                        <label
                                                                            class="col-form-label page-header-left">
                                                                            كميه مخزون
                                                                            المنتج</label>
                                                                        <input class="form-control"
                                                                               name="product_stock"
                                                                               value="{{ $sizes->stock }}"
                                                                               type="text" required>
                                                                        <div
                                                                            class="invalid-feedback">{{ $errors->first('product_stock') }}</div>
                                                                    </div>

                                                                    <div class="modal-footer">
                                                                        <button class="btn btn-primary" type="submit">
                                                                            تعديل
                                                                        </button>
                                                                        <button class="btn btn-secondary" type="button"
                                                                                data-dismiss="modal">اغلاق
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @else
                        <h4 class="text-center"> لا توجد مقاسات مضافه </h4>
                    @endif

                </div>
            </div>
        </div>
    </div>



    <script>

        function change_status_category(id, value) {
            if (value == 0) {
                value = 1;
            } else {
                value = 0;
            }
            axios.get('updateStatusCategory/' + id + '/' + value)
                .then(function (response) {
                    // alert(response.data.category_status);
                    location.reload();
                })
                .catch(function (error) {
                    console.log(error);
                    alert(error);
                });
        };


    </script>

@endsection