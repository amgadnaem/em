@extends('AdminPanel.backend.layouts.shop')
@section('content')

    <div class="page-body">

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    @if( count($products) > 0)
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display dataTable" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>QR Code</th>
                                        <th>صوره المنتج</th>
                                        <th>كود المنتج</th>
                                        <th>اسم المنتج</th>
                                        <th>السعر العام</th>
                                        <th>السعر الخاص</th>
                                        @if ( (!empty($roles) ) || ( auth()->user()->hasPermissionTo('تعديل_منتجات') ) )
                                            @if (empty($shop_id))
                                                <th>حاله المنتج</th>
                                                <th>الاكشن</th>
                                            @endif
                                        @endif
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($products as $product)
                                        <tr>
                                            <td>
                                                <img
                                                    src="data:image/png;base64, {!! base64_encode(\SimpleSoftwareIO\QrCode\Facades\QrCode::format('png')->size(100)->generate($product->code)) !!} ">
                                            </td>
                                            <td><img src=" {{ $product->product_image }}" style="width: 50px"></td>
                                            <td>{{ $product->code }}</td>

                                            <td>{{ $product->name_ar }}</td>
                                            <td>{{ $product->general_price }}</td>
                                            <td>{{ $product->special_price }}</td>
                                            @if ( (!empty($roles) ) || ( auth()->user()->hasPermissionTo('تعديل_منتجات') ) )
                                                @if (empty($shop_id))
                                                    <td>
                                                        <div class="media-body ">
                                                            <label class="switch">
                                                                <input type="checkbox"
                                                                       {{ $product->status == 1 ? 'checked' : '' }}
                                                                       onchange="update_status_product({{ $product->id }},{{ $product->status }})"><span
                                                                    class="switch-state"></span>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                @if($product->type == 1 || $product->type == 2)
                                                            <a class="btn btn-primary btn-group-sm"
                                                               href="{{ route('addProductColor',['id'=> $product->id]) }}"
                                                               data-whatever="@test"><i class="fa fa-plus"></i>
                                                            </a>
                                                        @endif
                                                        <button class="btn btn-primary" type="button"
                                                                data-toggle="modal"
                                                                data-target="#{{ $product->id }}"
                                                                data-whatever="@edit"><i class="fa fa-edit"></i>
                                                        </button>

                                                        <form action="{{ route('deleteProduct', $product->id) }}"
                                                              method="post" style="display: inline-block">
                                                            {{ csrf_field() }}
                                                            {{ method_field('delete') }}
                                                            <button type="submit" class="btn btn-danger delete btn-sm">
                                                                <i
                                                                    class="fa fa-trash"></i>
                                                            </button>
                                                        </form>

                                                        <div class="modal fade" id="{{ $product->id }}" tabindex="-1"
                                                             role="dialog"
                                                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title"> تعديل منتج
                                                                            {{ $product->name_ar }} </h5>

                                                                        <button class="close" type="button"
                                                                                data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">×</span></button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <form class="needs-validation" novalidate=""
                                                                              action="{{route('editProducts',['id'=>$product->id])}}"
                                                                              method="POST"
                                                                              enctype="multipart/form-data">
                                                                            {{ method_field('POST') }}
                                                                            {{ csrf_field() }}

                                                                            <div class="form-group">
                                                                                <label
                                                                                    class="col-form-label page-header-left">كود
                                                                                    المنتج
                                                                                </label>
                                                                                <input class="form-control"
                                                                                       name="product_code"
                                                                                       required
                                                                                       type="text"
                                                                                       value="{{ $product->code }}">
                                                                                <div
                                                                                    class="invalid-feedback">{{ $errors->first('product_code') }}</div>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label
                                                                                    class="col-form-label page-header-left">اسم
                                                                                    المنتج بالعربيه
                                                                                </label>
                                                                                <input class="form-control"
                                                                                       name="name_ar"
                                                                                       required
                                                                                       type="text"
                                                                                       value="{{ $product->name_ar }}">
                                                                                <div
                                                                                    class="invalid-feedback">{{ $errors->first('name_ar') }}</div>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label
                                                                                    class="col-form-label page-header-left">اسم
                                                                                    المنتج بالانجليزيه
                                                                                </label>
                                                                                <input class="form-control"
                                                                                       name="name_en"
                                                                                       required
                                                                                       type="text"
                                                                                       value="{{ $product->name_en }}">
                                                                                <div
                                                                                    class="invalid-feedback">{{ $errors->first('name_en') }}</div>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label
                                                                                    class="col-form-label page-header-left">اسم
                                                                                    المنتج بالكرديه
                                                                                </label>
                                                                                <input class="form-control"
                                                                                       name="name_kur"
                                                                                       required
                                                                                       type="text"
                                                                                       value="{{ $product->name_kur }}">
                                                                                <div
                                                                                    class="invalid-feedback">{{ $errors->first('name_kur') }}</div>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label
                                                                                    class="col-form-label page-header-left">السعر
                                                                                    العام</label>
                                                                                <input class="form-control"
                                                                                       name="general_price"
                                                                                       type="text"
                                                                                       value="{{ $product->general_price }}"
                                                                                       required>
                                                                                <div
                                                                                    class="invalid-feedback">{{ $errors->first('general_price') }}</div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label
                                                                                    class="col-form-label page-header-left">السعر
                                                                                    الخاص</label>
                                                                                <input class="form-control"
                                                                                       name="special_price"
                                                                                       type="text"
                                                                                       value="{{ $product->special_price }}"
                                                                                       required>
                                                                                <div
                                                                                    class="invalid-feedback">{{ $errors->first('special_price') }}</div>
                                                                            </div>
                                                                            
                                                                            @if($product->type == 4 ) 
                                                                                        <div class="form-group">
                                                                                    <label
                                                                                        class="col-form-label page-header-left">
                                                                                        الكميه</label>
                                                                                    <input class="form-control"
                                                                                           name="product_stock"
                                                                                           type="text"
                                                                                           value="{{ $product->product_stock }}">
                                                                                    <div
                                                                                        class="invalid-feedback">{{ $errors->first('product_stock') }}</div>
                                                                                </div>
                                                                                @endif
                                                                            
                                                                                                                                                            @if($product->type != 2)

                                                                            <div class="form-group">
                                                                                <label
                                                                                    class="col-form-label page-header-left">السعر
                                                                                    للاستضافه</label>
                                                                                <input class="form-control"
                                                                                       name="hosted_price"
                                                                                       type="text"
                                                                                       value="{{ $product->hosted_price }}">
                                                                                <div
                                                                                    class="invalid-feedback">{{ $errors->first('hosted_price') }}</div>
                                                                            </div>
@endif

                                                                            <div class="form-group">
                                                                                <label for="validationCustom04"
                                                                                       class="page-header-left">وصف
                                                                                    المنتج بالعربيه</label>
                                                                                <textarea class="form-control"
                                                                                          name="desc_ar"
                                                                                          value="{{ $product->desc_ar }}"
                                                                                          required>وصف للمنتج</textarea>
                                                                                <div
                                                                                    class="invalid-feedback">{{ $errors->first('desc_ar') }}
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label for="validationCustom04"
                                                                                       class="page-header-left">وصف
                                                                                    المنتج بالانجليزيه</label>
                                                                                <textarea class="form-control"
                                                                                          name="desc_en"
                                                                                          value="{{ $product->desc_en }}"
                                                                                          required>وصف للمنتج</textarea>
                                                                                <div
                                                                                    class="invalid-feedback">{{ $errors->first('desc_en') }}
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label for="validationCustom04"
                                                                                       class="page-header-left">وصف
                                                                                    المنتج بالكرديه</label>
                                                                                <textarea class="form-control"
                                                                                          name="desc_kur"
                                                                                          value="{{ $product->desc_kur }}"
                                                                                          required>وصف للمنتج</textarea>
                                                                                <div
                                                                                    class="invalid-feedback">{{ $errors->first('desc_kur') }}
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="validationCustom05"
                                                                                       class="page-header-left">صوره
                                                                                    المنتج</label>
                                                                                <label for="file-input"
                                                                                       class="image-upload-label">
                                                                                    <img alt="upload-service-image"
                                                                                         name="product_image"
                                                                                         src="{{$product->product_image}}"
                                                                                         class="thumb"
                                                                                         style="width: 100px"/>
                                                                                </label>
                                                                                <div class="custom-file">
                                                                                    <input class="custom-file-input"
                                                                                           id="product_image"
                                                                                           type="file"
                                                                                           name="product_image">
                                                                                    <label class="custom-file-label"
                                                                                           for="validatedCustomFile">اختر
                                                                                        صوره</label>
                                                                                    <div
                                                                                        class="invalid-feedback">{{ $errors->first('product_image') }}</div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="modal-footer">
                                                                                <button class="btn btn-primary"
                                                                                        type="submit">
                                                                                    تعديل
                                                                                </button>
                                                                                <button class="btn btn-secondary"
                                                                                        type="button"
                                                                                        data-dismiss="modal">اغلاق
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </td>
                                                @endif
                                            @endif
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @else
                        <h4 class="text-center"> لا توجد منتجات مضافه </h4>
                    @endif

                </div>
            </div>
        </div>
    </div>
    <script>

        function update_status_product(id, value) {
            if (value == 0) {
                value = 1;
            } else {
                value = 0;
            }
            axios.get('updateProduct/' + id + '/' + value)
                .then(function (response) {
                    // alert(response.data.category_status);
                    location.reload();
                })
                .catch(function (error) {
                    console.log(error);
                    alert(error);
                });
        };


    </script>
@endsection

















