@extends('AdminPanel.backend.layouts.shop')
@section('content')

    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>تعديل البروفايل </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="edit-profile">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title mb-0">البروفايل الشخصى</h4>
                                <div class="card-options"><a class="card-options-collapse" href=""
                                                             data-toggle="card-collapse"><i
                                            class="fe fe-chevron-up"></i></a><a class="card-options-remove"
                                                                                href=""
                                                                                data-toggle="card-remove"><i
                                            class="fe fe-x"></i></a></div>
                            </div>
                            @if(\Illuminate\Support\Facades\Session::get('type') == 3)
                                <div class="card-body">
                                    <form class="needs-validation" novalidate=""
                                          method="post"
                                          action="{{ route('editProfileShop',['id'=> Session::get('frontSession'),'type'=>\Illuminate\Support\Facades\Session::get('type')]) }}"
                                          enctype="multipart/form-data">
                                        {{ method_field('POST') }}
                                        {{ csrf_field() }}
                                        <div class="row mb-2">
                                            <div class="col-auto"><img class="img-70 rounded-circle" alt=""
                                                                       src="{{ $shop_admin->shop_image }}">
                                            </div>
                                            <div class="col">
                                                <h3 class="mb-1">{{ $shop_admin->shop_name }}</h3>
                                                @if($shop_admin->type == 3)
                                                    <p class="mb-4">مدير المتجر</p>
                                                @else
                                                    <p class="mb-4">مشرف المتجر</p>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label"> الاسم</label>
                                            <input class="form-control" name="admin_name"
                                                   value="{{ $shop_admin->name_ar }}">
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label"> رقم الهاتف</label>
                                            <input class="form-control" name="admin_phone"
                                                   value="{{ $shop_admin->phone_number }}" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="validationCustom05" class="page-header-left">الصوره
                                                الشخصيه</label>
                                            <div class="custom-file">
                                                <input class="custom-file-input" type="file"
                                                       name="admin_image">
                                                <label class="custom-file-label" for="validatedCustomFile">اختر
                                                    صوره</label>
                                                <div
                                                    class="invalid-feedback">{{ $errors->first('admin_image') }}</div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <img src="{{ $shop_admin->shop_image }}"
                                                 style="width: 100px"
                                                 class="img-thumbnail image-preview" alt="">
                                        </div>
                                        <div class="form-footer">
                                            <button class="btn btn-primary btn-block">تعديل</button>
                                        </div>
                                    </form>
                                </div>
                            @else
                                <div class="card-body">
                                    <form class="needs-validation" novalidate=""
                                          method="post" action="{{ route('editProfileShop',['id'=> Auth::user()->id,'type'=>\Illuminate\Support\Facades\Auth::user()->type]) }}"
                                          enctype="multipart/form-data">
                                        {{ method_field('POST') }}
                                        {{ csrf_field() }}
                                        <div class="row mb-2">
                                            <div class="col-auto"><img class="img-70 rounded-circle" alt=""
                                                                       src="{{ Auth::user()->user_image }}">
                                            </div>
                                            <div class="col">
                                                <h3 class="mb-1">{{ Auth::user()->username }}</h3>
                                                @if(Auth::user()->type == 3)
                                                    <p class="mb-4">مدير المتجر</p>
                                                @else  <p class="mb-4">مشرف المتجر</p>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label"> الاسم</label>
                                            <input class="form-control" name="admin_name"
                                                   placeholder="your-email@domain.com"
                                                   value="{{ Auth::user()->first_name }}">
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label"> رقم الهاتف</label>
                                            <input class="form-control" name="admin_phone"
                                                   value="{{ Auth::user()->phone_number }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="validationCustom05" class="page-header-left">الصوره
                                                الشخصيه</label>
                                            <div class="custom-file">
                                                <input class="custom-file-input" type="file"
                                                       name="admin_image">
                                                <label class="custom-file-label" for="validatedCustomFile">اختر
                                                    صوره</label>
                                                <div
                                                    class="invalid-feedback">{{ $errors->first('admin_image') }}</div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <img src="{{ Auth::user()->user_image }}"
                                                 style="width: 100px"
                                                 class="img-thumbnail image-preview" alt="">
                                        </div>
                                        <div class="form-footer">
                                            <button class="btn btn-primary btn-block">تعديل</button>
                                        </div>
                                    </form>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
