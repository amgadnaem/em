@extends('AdminPanel.backend.layouts.shop')
@section('content')

    <div class="page-body">
        <div class="container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">

                        <div class="page-header-left">
                            <!--<button class="btn btn-primary" type="button" data-toggle="modal"-->
                            <!--        data-target="#exampleModalgetbootstrap" data-whatever="@getbootstrap"> اضافه عرض-->
                            <!--    لمنتج-->
                            <!--</button>-->
                            <div class="modal fade" id="exampleModalgetbootstrap" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">اضافه العروض للمنتجات </h5>
                                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <form class="needs-validation" novalidate=""
                                                  action="{{route('AddOffersShop')}}"
                                                  method="POST" enctype="multipart/form-data">
                                                {{ method_field('POST') }}
                                                {{ csrf_field() }}

                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="page-header-left">المنتجات</label>
                                                    <select name="product_id" id="product_id"
                                                            class="form-control"
                                                            required>
                                                        <option value=""> اختر</option>
                                                        @foreach($shop_products as $product)
                                                            <option
                                                                    value="{{ $product->id }}">{{ $product->product_name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div
                                                            class="invalid-feedback">{{ $errors->first('product_id') }}</div>
                                                </div>


                                                <div class="form-group">
                                                    <label class="page-header-left">تاريخ بدايه
                                                        العرض </label>
                                                    <input class="datepicker-here form-control digits" name="from_date"
                                                           type="text" required
                                                           data-language="en"
                                                           data-multiple-dates-separator=", " data-position="top left"
                                                           placeholder="تاريخ بدايه العرض " autocomplete="true">
                                                    <div class="invalid-feedback">{{ $errors->first('from_date') }}.
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="page-header-left">تاريخ نهايه
                                                        العرض </label>
                                                    <input class="datepicker-here form-control digits" name="to_date"
                                                           type="text" required
                                                           data-language="en"
                                                           name="to_date"
                                                           data-multiple-dates-separator=", " data-position="top left"
                                                           placeholder="تاريخ نهايه العرض " autocomplete="true">
                                                    <div class="invalid-feedback">{{ $errors->first('to_date') }}.</div>
                                                </div>


                                                <div class="form-group">
                                                    <label class="col-form-label page-header-left">نسبه العرض</label>
                                                    <input class="form-control" name="discount_price"
                                                           type="text"
                                                           placeholder="ادخل نسبه العرض " required>
                                                    <div
                                                            class="invalid-feedback">{{ $errors->first('discount_price') }}</div>
                                                </div>

                                                <div class="modal-footer">
                                                    <button class="btn btn-primary" type="submit">اضافه</button>
                                                    <button class="btn btn-secondary" type="button"
                                                            data-dismiss="modal">اغلاق
                                                    </button>

                                            </form>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <script>

        function update_status_product(id, value) {
            if (value == 0) {
                value = 1;
            } else {
                value = 0;
            }
            axios.get('updateProduct/' + id + '/' + value)
                .then(function (response) {
                    // alert(response.data.category_status);
                    location.reload();
                })
                .catch(function (error) {
                    console.log(error);
                    alert(error);
                });
        };


    </script>
@endsection

















