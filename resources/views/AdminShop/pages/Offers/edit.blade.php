@extends('AdminPanel.backend.layouts.shop')
@section('content')


    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>تعديل بيانات {{ $edit_offers->product->product_name }}</h3>
                            <div class="col-md-6">

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">


                        <div class="card-body">

                            <form class="needs-validation" novalidate=""
                                  action="{{route('offers.update',['id'=> $edit_offers->id]) }}"
                                  method="POST" enctype="multipart/form-data">
                                {{ method_field('POST') }}
                                {{ csrf_field() }}

                                <div class="form-row">


                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom04">تاريخ بدايه العرض</label>
                                        <input class="datepicker-here form-control digits"
                                               type="text" required
                                               data-language="en"
                                               name="from_date" value="{{$edit_offers->from_date}}"
                                               data-multiple-dates-separator=", " data-position="bottom left"
                                               placeholder="تاريخ بدايه العرض" autocomplete="true">
                                        <div class="invalid-feedback">{{ $errors->first('from_date') }}.</div>
                                    </div>


                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom04">تاريخ نهايه العرض</label>
                                        <input class="datepicker-here form-control digits"
                                               type="text" required
                                               data-language="en"
                                               name="to_date" value="{{$edit_offers->to_date}}"
                                               data-multiple-dates-separator=", " data-position="bottom left"
                                               placeholder="تاريخ نهايه العرض" autocomplete="true">
                                        <div class="invalid-feedback">{{ $errors->first('to_date') }}.</div>
                                    </div>

                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom05">نسبه العرض</label>
                                        <input class="form-control" name="discounted_price"
                                               type="text" value="{{$edit_offers->discounted_price}}"
                                               placeholder="نسبه العرض"
                                               required="">

                                        <div class="invalid-feedback">{{ $errors->first('discounted_price') }}</div>
                                    </div>


                                </div>
                                <input type="submit" class="btn btn-success" value="تعديل">
                            </form>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

    <!-- END PAGE CONTENT WRAPPER -->
@endsection








