@extends('AdminPanel.backend.layouts.shop')
@section('content')

    <!-- Right sidebar Ends-->
    <div class="page-body">
        @if (empty($shop_id)) {

        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>اضافه عروض للمتجر</h3>
                            <div class="col-md-6">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    @endif

    <!-- Container-fluid starts-->
        <div class="container-fluid">
            @if (empty($shop_id)) {
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">


                        <div class="card-body">

                            <form class="needs-validation" novalidate="" action="{{route('createOfferProduct')}}"
                                  method="POST" enctype="multipart/form-data">
                                {{ method_field('POST') }}
                                {{ csrf_field() }}

                                <div class="form-row">

                                    <div class="col-md-6 mb-3">
                                        <label for="product_id">اسم المنتج</label><select
                                            name="product_id" id="product_id"
                                            class="form-control"
                                            required>
                                            <option value="">اختر</option>
                                            @foreach($shop_products as $products)
                                                <option
                                                    value="{{ $products->id }}">{{ $products->product_name }}</option>
                                            @endforeach
                                        </select>
                                        <div class="invalid-feedback">{{ $errors->first('product_id') }}</div>
                                    </div>

                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom05">صوره العرض</label>
                                        <div class="custom-file">
                                            <input class="custom-file-input" id="offer_image" type="file"
                                                   name="offer_image">
                                            <label class="custom-file-label" for="validatedCustomFile">اختر صوره</label>
                                            <div class="invalid-feedback">{{ $errors->first('offer_image') }}</div>
                                        </div>
                                    </div>


                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom04">تاريخ بدايه العرض</label>
                                        <input class="datepicker-here form-control digits"
                                               type="text" required
                                               data-language="en"
                                               name="from_date"
                                               data-multiple-dates-separator=", " data-position="top left"
                                               placeholder="تاريخ بدايه العرض" autocomplete="true">
                                        <div class="invalid-feedback">{{ $errors->first('from_date') }}.</div>
                                    </div>


                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom04">تاريخ نهايه العرض</label>
                                        <input class="datepicker-here form-control digits"
                                               type="text" required
                                               data-language="en"
                                               name="to_date"
                                               data-multiple-dates-separator=", " data-position="top left"
                                               placeholder="تاريخ نهايه العرض" autocomplete="true">
                                        <div class="invalid-feedback">{{ $errors->first('to_date') }}.</div>
                                    </div>

                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom05">نسبه العرض</label>
                                        <input class="form-control" name="discounted_price"
                                               type="text"
                                               placeholder="نسبه العرض"
                                               required="">

                                        <div class="invalid-feedback">{{ $errors->first('discounted_price') }}</div>
                                    </div>


                                </div>

                                <button class="btn btn-primary" type="submit">اضف</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
            @endif

            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        @if( count($hot_offers) > 0)
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="display dataTable" id="basic-1">
                                        <thead>
                                        <tr>
                                            <th> صوره العرض</th>
                                            <th>اسم المنتج</th>
                                            <th>تاريخ بدايه العرض</th>
                                            <th>تاريخ نهايه العرض</th>
                                            <th>نسبه العرض</th>
                                            @if (empty($shop_id)) {
                                            <th>الاكشن</th>
                                            @endif
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($hot_offers as $offers)
                                            <tr>
                                                <td><img src="{{ $offers->offer_image }}" style="width: 50px"></td>
                                                <td>{{ $offers->product->product_name }}</td>
                                                <td>{{ $offers->from_date }}</td>
                                                <td>{{ $offers->to_date }}</td>
                                                <td>{{ $offers->discounted_price }}</td>
                                                @if (empty($shop_id)) {

                                                <td>
                                                    <a href="{{ route('offers.edit', $offers->id) }}"
                                                       class="btn btn-primary"><i class="fa fa-edit"></i>
                                                    </a>
                                                    <form action="{{ route('deleteOffer', $offers->id) }}"
                                                          method="post" style="display: inline-block">
                                                        {{ csrf_field() }}
                                                        {{ method_field('delete') }}
                                                        <button type="submit" class="btn btn-danger delete btn-sm"><i
                                                                class="fa fa-trash"></i>
                                                        </button>
                                                    </form>
                                                </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @else
                            <h4 class="text-center"> لا يوجد عروض لديك</h4>
                        @endif
                    </div>
                </div>
            </div>

        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection







