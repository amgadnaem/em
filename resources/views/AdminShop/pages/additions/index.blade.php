@extends('AdminPanel.backend.layouts.shop')
@section('content')

    <!-- Right sidebar Ends-->

    <div class="page-body">
        <div class="container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">

                        <div class="page-header-left">
                            <button class="btn btn-primary" type="button" data-toggle="modal"
                                    data-target="#additions" data-whatever="@test">اضافات
                                للمنتجات

                            </button>
                            <div class="modal fade" id="additions" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">الاضافات</h5>
                                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <form class="needs-validation" novalidate=""
                                                  action="{{route('additions')}}"
                                                  method="POST">
                                                {{ method_field('POST') }}
                                                {{ csrf_field() }}
                                                <div class="form-group">

                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">اسم الاضافه</label>
                                                    <input class="form-control" name="addition_name"
                                                           type="text"
                                                           placeholder="ادخل اسم الاضافه"
                                                           required="">
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('addition_name') }}</div>
                                                </div>

                                                <div class="form-group">

                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">السعر</label>
                                                    <input class="form-control" name="price"
                                                           type="text"
                                                           placeholder="ادخل السعر"
                                                           required="">
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('price') }}</div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn btn-primary" type="submit">اضافه</button>
                                                    <button class="btn btn-secondary" type="button"
                                                            data-dismiss="modal">اغلاق
                                                    </button>
                                                </div>

                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    @if( count($additions) > 0)
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display dataTable" id="basic-1">
                                    <thead>
                                    <tr>

                                        <th>اسم الاضافه</th>
                                        <th>السعر</th>
                                        <th>الاكشن</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($additions as $addition)
                                        <tr>

                                            <td>{{ $addition->name }}</td>
                                            <td>{{ $addition->price }}</td>
                                            <td>
                                                <button class="btn btn-primary" type="button" data-toggle="modal"
                                                        data-target="#{{ $addition->id }}"
                                                        data-whatever="@test"><i class="fa fa-edit"></i>
                                                </button>

                                                <form action="{{ route('deleteAddition', $addition->id) }}"
                                                      method="post" style="display: inline-block">
                                                    {{ csrf_field() }}
                                                    {{ method_field('delete') }}
                                                    <button type="submit" class="btn btn-danger delete btn-sm"><i
                                                            class="fa fa-trash"></i>
                                                    </button>
                                                </form>

                                                <div class="modal fade" id="{{ $addition->id }}" tabindex="-1"
                                                     role="dialog"
                                                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title"> تعديل اضافه
                                                                    {{ $addition->name }} </h5>

                                                                <button class="close" type="button" data-dismiss="modal"
                                                                        aria-label="Close">
                                                                    <span aria-hidden="true">×</span></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form class="needs-validation" novalidate=""
                                                                      action="{{route('EditAddition',['id'=>$addition->id])}}"
                                                                      method="POST" enctype="multipart/form-data">
                                                                    {{ method_field('POST') }}
                                                                    {{ csrf_field() }}


                                                                    <div class="form-group">
                                                                        <label class="col-form-label page-header-left">اسم
                                                                            الاشافه</label>
                                                                        <input class="form-control" name="addition_name"
                                                                               required
                                                                               type="text"
                                                                               value="{{ $addition->name }}">
                                                                        <div
                                                                            class="invalid-feedback">{{ $errors->first('addition_name') }}</div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-form-label page-header-left">السعر
                                                                        </label>
                                                                        <input class="form-control" name="price"
                                                                               type="text"
                                                                               value="{{ $addition->price }}"
                                                                               placeholder="ادخل سعر الاضافه" required>
                                                                        <div
                                                                            class="invalid-feedback">{{ $errors->first('price') }}</div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button class="btn btn-primary" type="submit">
                                                                            تعديل
                                                                        </button>
                                                                        <button class="btn btn-secondary" type="button"
                                                                                data-dismiss="modal">اغلاق
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @else
                        <h4 class="text-center"> لا توجد اضافات مضافه </h4>
                    @endif

                </div>
            </div>
        </div>
    </div>


@endsection








