@extends('AdminPanel.backend.layouts.shop')
@section('content')

    <!-- Right sidebar Ends-->
    <div class="page-body">

        <!-- Default ordering (sorting) Starts-->
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h6>إضافة متعددة للصور </h6>
                </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <form action="{{route('addBulkProductsImages')}}" class="dropzone" method="post">
                                @csrf
                                <div class="dz-message" data-dz-message><h4>من فضلك قم بإختيار صور المنتجات</h4></div>
                                <div class="fallback">
                                    <input name="file" type="file" multiple />
                                </div>
                            </form>
                        </div>
                    </div>
            </div>
        </div>


    </div>
@endsection









