@extends('AdminPanel.backend.layouts.shop')
@section('content')
    <!-- Right sidebar Ends-->
    <div class="page-body">

        <!-- Default ordering (sorting) Starts-->
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h3>اضافه منتج </h3>
                </div>
            </div>
        </div>

        <!--our form-->

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <form class="needs-validation" novalidate="" action="{{route('addProduct')}}"
                                  method="POST" enctype="multipart/form-data">
                                {{ method_field('POST') }}
                                {{ csrf_field() }}
                                <div class="modal-body">
                                    <div class="form-row">

                                        <div class="col-md-4 mb-3">
                                            <label for="validationCustom04">اسم المنتج بالعربيه</label>
                                            <input class="form-control" type="text" name="name_ar"
                                                   required>
                                            <div class="invalid-feedback">{{ $errors->first('name_ar') }}.</div>
                                        </div>

                                        <div class="col-md-4 mb-3">
                                            <label for="validationCustom04">اسم المنتج بالانجليزيه</label>
                                            <input class="form-control" type="text" name="name_en"
                                                   required>
                                            <div class="invalid-feedback">{{ $errors->first('name_en') }}.</div>
                                        </div>

                                        <div class="col-md-4 mb-3">
                                            <label for="validationCustom04">اسم المنتج بالكرديه</label>
                                            <input class="form-control" type="text" name="name_kur"
                                                   required>
                                            <div class="invalid-feedback">{{ $errors->first('name_kur') }}.</div>
                                        </div>

                                        <div class="col-md-4 mb-3">
                                            <label for="validationCustom04"> السعر العام</label>
                                            <input class="form-control" type="text" name="general_price"
                                                   required>
                                            <div class="invalid-feedback">{{ $errors->first('general_price') }}.</div>
                                        </div>

                                        <div class="col-md-4 mb-3">
                                            <label for="validationCustom04"> السعر الخاص</label>
                                            <input class="form-control" type="text" name="special_price"
                                                   required>
                                            <div class="invalid-feedback">{{ $errors->first('special_price') }}.</div>
                                        </div>
                                        @if($shop_type != 2)
                                        <div class="col-md-4 mb-3">
                                            <label for="validationCustom04"> السعر للاستضافه</label>
                                            <input class="form-control" type="text" name="hosted_price">
                                            <div class="invalid-feedback">{{ $errors->first('hosted_price') }}.</div>
                                        </div>
                                        @endif
                                        <div class="col-md-4 mb-3">
                                            <label for="validationCustom04"> كود المنتج</label>
                                            <input class="form-control" type="text" name="product_code"
                                                   required>
                                            <div class="invalid-feedback">{{ $errors->first('product_code') }}.</div>
                                        </div>

                                        <div class="col-md-4 mb-3">
                                            <label for="validationCustom04"> الكميه</label>
                                            <input class="form-control" type="text" name="product_stock">
                                            <div class="invalid-feedback">{{ $errors->first('product_stock') }}.</div>
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="validationCustom05">صوره المنتج</label>
                                            <div class="custom-file">
                                                <input class="custom-file-input" type="file"
                                                       name="product_image">
                                                <label class="custom-file-label" for="validatedCustomFile">اختر
                                                    صوره</label>
                                                <div
                                                    class="invalid-feedback">{{ $errors->first('product_image') }}</div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="validationCustom05">الاقسام</label>
                                            <select name="category_id" class="form-control" required>
                                                <option value="">اختر</option>
                                                @foreach($categories as $category)
                                                    <option
                                                        value="{{ $category->id }}">{{ $category->category_name_ar }}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">{{ $errors->first('category_id') }}</div>
                                        </div>
                                        @if($shop_type != 3 && $shop_type != 4)

                                        <div class="col-md-4 mb-3">
                                            <label for="validationCustom05"
                                                   class="page-header-left"> اختر مقاس </label>
                                            <select name="size_id"
                                                    class="form-control"
                                                    required>
                                                <option value="">اختر</option>
                                                @foreach($sizes as $size)
                                                    <option
                                                        value="{{ $size->id }}">{{ $size->size_name }}</option>
                                                @endforeach
                                            </select>
                                            <div
                                                class="invalid-feedback">{{ $errors->first('size_id') }}</div>
                                        </div>

                                        @if($shop_type != 2)

                                        <div class="col-md-4 mb-3">
                                            <label for="validationCustom05"
                                                   class="page-header-left"> اختر لون </label>
                                            <select name="color_id"
                                                    class="form-control"
                                                    required>
                                                <option value="">اختر</option>
                                                @foreach($colors as $color)
                                                    <option value="{{$color->id}}"
                                                            style=background-color:{{$color->color}};>{{ $color->color }}</option>
                                                @endforeach
                                            </select>
                                            <div
                                                class="invalid-feedback">{{ $errors->first('color_id') }}</div>
                                        </div>
                                        @endif
                                        @endif

                                        <div class="col-md-4 mb-3">
                                            <label for="validationCustom04"> وصف عن المنتج بالعربيه</label>
                                            <textarea class="form-control" name="desc_ar"
                                                      required></textarea>
                                            <div class="invalid-feedback">{{ $errors->first('desc_ar') }}.</div>
                                        </div>

                                        <div class="col-md-4 mb-3">
                                            <label for="validationCustom04"> وصف عن المنتج بالانجليزيه</label>
                                            <textarea class="form-control" name="desc_en"
                                                      required></textarea>
                                            <div class="invalid-feedback">{{ $errors->first('desc_en') }}.</div>
                                        </div>


                                        <div class="col-md-4 mb-3">
                                            <label for="validationCustom04"> وصف عن المنتج بالكرديه</label>
                                            <textarea class="form-control" name="desc_kur"
                                                      required></textarea>
                                            <div class="invalid-feedback">{{ $errors->first('desc_kur') }}.</div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <button class="btn btn-primary" type="submit">اضافه منتج</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
