@extends('AdminPanel.backend.layouts.shop')
@section('content')

    <!-- Right sidebar Ends-->
    <div class="page-body">

        <!-- Default ordering (sorting) Starts-->
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h6>إضافة متعددة للمنتجات </h6>
                </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <a class="btn btn-square btn-success float-left" href="{{route('exportProductsSample')}}" title="ملف كمثال"> تحميل ملف عينة </a>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            {{--                        <a class="btn btn-square btn-success float-right" href="{{route('addBulk')}}" title="إضافة"> إضافة متعددة للمنتجات </a>--}}
                            <form action="{{ route('addBulk') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="file" name="file" class="form-control" required accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                                <br>
                                <button class="btn btn-success">إضافة متعددة للمنتجات</button>
                            </form>
                        </div>
                    </div>
            </div>
        </div>


    </div>
@endsection









