@extends('AdminPanel.backend.layouts.default')
@section('content')
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <!-- Right sidebar Ends-->
    <div class="page-body">

        <!-- Default ordering (sorting) Starts-->
            @if (!empty($roles)  || auth()->user()->hasPermissionTo('عرض_طلبات'))

            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4>تفاصيل الطلب :{{ $order->order_number }} </h4>
                        <h4> اجمالى الطلب :{{ $order->subtotal_fees }} </h4>
                        <h4> اسم المسخدم: {{ !empty($order->user->first_name) ? $order->user->first_name : null }} </h4>
      
                    </div>
                    @if( count($order_products) > 0)
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display dataTable" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>صوره المنتج</th>
                                        <th>اسم المنتج</th>
                                        <th>سعر المنتج للوحده</th>
                                        <th>كميه المنتج</th>
                                        <th>اجمالى سعر المنتج</th>
                                        <th>مقاس المنتج</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($order_products as $details)
                                        <tr>
                                            <td><img src=" {{ $details->getOrdersDetails($details->product_id) }}"
                                                     style="width: 50px"
                                                     alt=""></td>
                                            <td>{{ $details->getProductName($details->product_id) }}</td>
                                            @if(!empty($details->size))
                                                <td>
                                                    {{ $details->getProductDetails($details->size) }}
                                                </td>
                                                <td>    {{ $details->order_qty }}</td>
                                                <td>
                                                    {{ $details->price    }}
                                                </td>
                                                <td>
                                                    {{ $details->sizeName($details->size)    }}
                                                </td>
                                            @else
                                                <td>
                                                    {{ $details->getProductPrice($details->product_id) }}
                                                </td>
                                                <td>    {{ $details->order_qty }}</td>
                                                <td>
                                                    {{ $details->getProductPrice($details->product_id)  }}
                                                </td>
                                                <td>
                                                    لا يوجد مقاس للمنتج
                                                </td>
                                            @endif


                                            {{--                                            <td>{{ $orders->order_number }}</td>--}}
                                            {{--                                            <td>{{ $orders->subtotal_fees }}</td>--}}
                                            {{--                                            <td>{{ $orders->order_date }}</td>--}}
                                            {{--                                            <td>{{ !empty($orders->user->name) ? $orders->user->name : null }}</td>--}}

                                            {{--                                            <td><p><a href="{{$orders->address['google_location']}}" target="_blank">العنوان--}}
                                            {{--                                                        على الخريطه</a></p></td>--}}

                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    @else
                        <h4 class="text-center">لا توجد طلبات </h4>
                    @endif
                </div>
            </div>
        @endif
    </div>

@endsection







