@extends('AdminPanel.backend.layouts.shop')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>رسائل مدير النظام</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col call-chat-body">
                    <div class="card">
                        <div class="card-body p-0">
                            <div class="row chat-box">
                                <!-- Chat right side start-->
                                <div class="col pr-0 chat-right-aside">
                                    <!-- chat start-->
                                    <div class="chat">
                                        <!-- chat-header start-->
                                        <div class="chat-header clearfix">
                                            <img class="rounded-circle" src="{{asset('images/user/7.jpg')}}" alt="">
                                            <div class="about">
                                                <div class="name"> مدير النظام  </div>
{{--                                                <div class="status digits">Last Seen 3:55 PM</div>--}}
                                            </div>
                                        </div>
                                        <!-- chat-header end-->
                                        <div class="chat-history chat-msg-box custom-scrollbar" id="chat-history">
                                            <ul class="message-container">
                                                @foreach($messages as $message)
                                                    @if($message->type == 1)
                                                        <li>
                                                            <div class="message my-message"><img class="rounded-circle float-left chat-user-img img-30" src="{{$message->shop->shop_image}}" alt="">
                                                                <div class="message-data text-right"><span class="message-data-time">{{$message->created_at}}</span></div> {{$message->message}}
                                                            </div>
                                                        </li>
                                                    @else
                                                        <li class="clearfix">
                                                            <div class="message other-message pull-right"><img class="rounded-circle float-right chat-user-img img-30" src="{{ asset('images/user/7.jpg') }}" alt="">
                                                                <div class="message-data"><span class="message-data-time">{{$message->created_at}}</span></div>  {{$message->message}}
                                                            </div>
                                                        </li>
                                                    @endif
                                                @endforeach
{{--                                                <li class="clearfix">--}}
{{--                                                    <div class="message other-message pull-right"><img class="rounded-circle float-right chat-user-img img-30" src="../../images/user/12.png" alt="">--}}
{{--                                                        <div class="message-data"><span class="message-data-time">10:14 am</span></div>                                                            Well I am not sure. The rest of the team--}}
{{--                                                    </div>--}}
{{--                                                </li>--}}
{{--                                                <li>--}}
{{--                                                    <div class="message my-message mb-0"><img class="rounded-circle float-left chat-user-img img-30" src="../../images/user/3.png" alt="">--}}
{{--                                                        <div class="message-data text-right"><span class="message-data-time">10:20 am</span></div>                                                            Actually everything was fine. I'm very excited to show this to our team.--}}
{{--                                                    </div>--}}
{{--                                                </li>--}}
                                            </ul>
                                        </div>
                                        <!-- end chat-history-->
                                        <div class="chat-message clearfix">
                                            <form id="send" action="" method="post">
                                                @csrf
                                            <div class="row">
                                                <div class="col-xl-12 d-flex">
                                                    <div class="input-group text-box">
                                                            <input class="form-control input-txt-bx" id="message-to-send" type="text" name="message-to-send" placeholder="نص الرسالة">
                                                            <div class="input-group-append">
                                                                <button class="btn btn-primary" type="submit">إرسال</button>
                                                            </div>
                                                    </div>
                                                </div>
                                            </div>
                                            </form>
                                        </div>
                                        <!-- end chat-message-->
                                        <!-- chat end-->
                                        <!-- Chat right side ends-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            var element = document.getElementById("chat-history");
            element.scrollTop = element.scrollHeight;
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#send').on('submit', function(e) {
            e.preventDefault();
            var message = $('#message-to-send').val();
            $.ajax({
                type: "post",
                url: '{{route('AdminShopMessagesView')}}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {message:message},
                success: function( data ) {
                    $('.message-container').last().append('<li><div class="message my-message"><img class="rounded-circle float-left chat-user-img img-30" src="'+data.shop.shop_image+'" alt="">\n' +
                        '                                                                <div class="message-data text-right"><span class="message-data-time">'+data.created_at+'</span></div> '+data.message+'' +
                        '                                                            </div></li>');
                    $('#message-to-send').val('');
                    var element = document.getElementById("chat-history");
                    element.scrollTop = element.scrollHeight;
                }
            });
        });
    </script>

@endsection