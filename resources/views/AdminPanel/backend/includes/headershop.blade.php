<body main-theme-layout="rtl">
<!-- Loader starts-->
<div class="loader-wrapper">
    <div class="loader bg-white">
        <div class="whirly-loader"></div>
    </div>
</div>
<!-- Loader ends-->
<!-- page-wrapper Start-->
<div class="page-wrapper">
    <!-- Page Header Start-->
    <div class="page-main-header">
        <div class="main-header-right row">
            <div class="main-header-left d-lg-none">
                <div class="logo-wrapper"><a><img src="{{ asset('images/logo.png') }}"
                                                  alt=""></a>
                </div>
            </div>
            <div class="mobile-sidebar d-block">
                <div class="media-body text-right switch-sm">
                    <label class="switch"><a><i id="sidebar-toggle"
                                                data-feather="align-left"></i></a></label>
                </div>
            </div>
            <div class="nav-right col p-0">
                <ul class="nav-menus">

                    <li><a href=""></a></li>
                    <li class="onhover-dropdown">
                        @if(\Illuminate\Support\Facades\Session::get('type') == 3)
                            <div class="media align-items-center"><img
                                    class="align-self-center pull-right img-50 rounded-circle"
                                    src="{{\Illuminate\Support\Facades\Session::get('shop_image') }}" alt="header-user">
                                <div class="dotted-animation"><span class="animate-circle"></span><span
                                        class="main-circle"></span></div>
                            </div>
                        @else
                            <div class="media align-items-center"><img
                                    class="align-self-center pull-right img-50 rounded-circle"
                                    src="{{Auth::user()->user_image }}" alt="header-user">
                                <div class="dotted-animation"><span class="animate-circle"></span><span
                                        class="main-circle"></span></div>
                            </div>
                        @endif
                        <ul class="profile-dropdown onhover-show-div">
                            <li><a href="{{ route('profileShop') }}"><i data-feather="user"></i> البروفايل</a></li>
                            <li><a href="{{ route('admin.logout') }}"><i data-feather="log-out"></i> تسجيل الخروج</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <div class="d-lg-none mobile-toggle pull-right"><i data-feather="more-horizontal"></i></div>
            </div>

        </div>
    </div>
    <!-- Page Header Ends                              -->
    <!-- Page Body Start-->
    <div class="page-body-wrapper">
        <!-- Page Sidebar Start-->
        <div class="page-sidebar">
            <div class="main-header-left d-none d-lg-block">
                <div class="logo-wrapper" style="display: table;margin: 0 auto"><a href=""><img
                            style="margin: 50px auto;display: table;"
                            src="{{ asset('images/logo.png') }}"
                            alt=""></a>
                </div>
            </div>
            <div class="sidebar custom-scrollbar">
                @if(\Illuminate\Support\Facades\Session::get('type') == 3)
                    <div class="sidebar-user text-center">
                        <div><img class="img-60 rounded-circle"
                                  src="{{ \Illuminate\Support\Facades\Session::get('shop_image') }}"
                                  alt="#">
                            <div class="profile-edit"><a href="{{ route('profileShop') }}" target="_blank"><i
                                        data-feather="edit"></i></a></div>
                        </div>
                        <h6 class="mt-3 f-14">{{ \Illuminate\Support\Facades\Session::get('shop_name') }}</h6>
                        <p class="mb-4">مدير المتجر</p>
                    </div>
                @else
                    <div class="sidebar-user text-center">
                        <div><img class="img-60 rounded-circle"
                                  src="{{ \Illuminate\Support\Facades\Auth::user()->user_image }}"
                                  alt="#">
                            <div class="profile-edit"><a href="{{ route('profileShop') }}" target="_blank"><i
                                        data-feather="edit"></i></a></div>
                        </div>
                        <h6 class="mt-3 f-14">{{ Auth::user()->first_name }}</h6>
                        <p class="mb-4">مشرف المتجر</p>

                    </div>
                @endif
                <ul class="sidebar-menu">
                    <li><a class="sidebar-header" href="{{ route('shop.dashboard') }}"><i
                                data-feather="home"></i><span>الصفحه الرئيسيه</span></a>
                    </li>
{{--                    @if ( (!empty($roles) ) || ( auth()->user()->hasPermissionTo('عرض_مشرف') ) )--}}
                        <li><a class="sidebar-header" href="#"><i
                                    data-feather="user-plus"></i><span>المشرفين</span><span
                                    class="badge badge-pill badge-primary"></span><i
                                    class="fa fa-angle-right pull-right"></i></a>
                            <ul class="sidebar-submenu">
                                <li><a href="{{ route('SupervisorShop') }}"><i
                                            class="fa fa-user-plus"></i><span> اضافه مشرفين </span></a></li>
                            </ul>
                        </li>
{{--                    @endif--}}

                    <li><a class="sidebar-header" href="#"><i
                                data-feather="home"></i><span>المتجر</span>
                            <i class="fa fa-angle-right pull-right"></i></a>
                        <ul class="sidebar-submenu">
{{--                            @if ( (!empty($roles) ) || ( auth()->user()->hasPermissionTo('تعديل_متجر') ) )--}}
                                <li><a href="{{ route('shop.index') }}"><i
                                            class="fa fa-edit">&nbsp;&nbsp;</i><span>تعديل بيانات المتجر</span></a>
                                </li>
{{--                            @endif--}}
{{--                            @if ( (!empty($roles) ) || ( auth()->user()->hasPermissionTo('عرض_قسم') ) )--}}

                                <li><a href="{{ route('ShopCategories') }}"><i
                                            class="fa fa-plus">&nbsp;&nbsp;</i><span>اقسام المتجر</span></a>
                                </li>
{{--                            @endif--}}
{{--                            @if ( (!empty($roles) ) || ( auth()->user()->hasPermissionTo('اضافه_منتجات') ) )--}}
                                <li>
                                    <a href="{{ route('products') }}"><i class="fa fa-plus">&nbsp;&nbsp;</i><span>المنتجات</span></a>
                                </li>
{{--                                @if ( (!empty($roles) ) || ( auth()->user()->hasPermissionTo('اضافه_منتجات') ) )--}}
{{--                                @endif--}}
{{--                            @endif--}}
{{--                            @if ( (!empty($roles) ) || ( auth()->user()->hasPermissionTo('عرض_طلبات') ) )--}}

                                <li><a href="{{ route('Orders') }}"><i
                                            class="fa fa-eye">&nbsp;&nbsp;</i><span>الطلبات</span></a>
                                </li>
{{--                            @endif--}}
                        </ul>
                    </li>

{{--                    <li><a class="sidebar-header" href="#"><i--}}
{{--                                data-feather="user-plus"></i><span>الرسائل</span><span--}}
{{--                                class="badge badge-pill badge-primary"></span><i--}}
{{--                                class="fa fa-angle-right pull-right"></i></a>--}}
{{--                        <ul class="sidebar-submenu">--}}
{{--                            <li><a href="#"><i--}}
{{--                                        class="fa fa-user-plus">&nbsp;&nbsp;</i><span> الرسائل </span></a></li>--}}
{{--                        </ul>--}}
{{--                    </li>--}}
                    <li><a class="sidebar-header" href="{{ route('AdminShopMessagesView') }}"><i
                                    data-feather="message-square"></i><span>رسائل النظام</span></a>
                    </li>

                </ul>
            </div>
        </div>
    @yield('content')

    <!-- Page Sidebar Ends-->

        <!-- Right sidebar Ends-->


    </div>
</div>
