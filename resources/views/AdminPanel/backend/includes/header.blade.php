<body main-theme-layout="rtl">
<!-- Loader starts-->
<div class="loader-wrapper">
    <div class="loader bg-white">
        <div class="whirly-loader"></div>
    </div>
</div>
<!-- Loader ends-->
<!-- page-wrapper Start-->
<div class="page-wrapper">
    <!-- Page Header Start-->
    <div class="page-main-header">
        <div class="main-header-right row">
            <div class="main-header-left d-lg-none">
                <div class="logo-wrapper"><a><img src="{{ asset('images/logo/em.png') }}"
                                                  alt=""></a>
                </div>
            </div>
            <div class="mobile-sidebar d-block">
                <div class="media-body text-right switch-sm">
                    <label class="switch"><a><i id="sidebar-toggle"
                                                data-feather="align-left"></i></a></label>
                </div>
            </div>
            <div class="nav-right col p-0">
                <ul class="nav-menus">

                    <li><a href=""></a></li>
                    <li class="onhover-dropdown">
                        <div class="media align-items-center"><img
                                class="align-self-center pull-right img-50 rounded-circle"
                                src="{{ Auth::user()->user_image }}" alt="header-user">
                            <div class="dotted-animation"><span class="animate-circle"></span><span
                                    class="main-circle"></span></div>
                        </div>
                        <ul class="profile-dropdown onhover-show-div p-20">
                            <li><a data-toggle="modal" href="#" data-target="#{{ Auth::user()->id }}"><i data-feather="user"></i> البروفايل</a></li>
                                                      <li><a href="{{ route('admin.logout') }}"><i data-feather="log-out"></i> تسجيل الخروج</a>

                        </ul>
                    </li>
                </ul>
                <div class="d-lg-none mobile-toggle pull-right"><i data-feather="more-horizontal"></i></div>
            </div>

        </div>
    </div>
    <!-- Page Header Ends                              -->
    <!-- Page Body Start-->
    <div class="page-body-wrapper">
        <!-- Page Sidebar Start-->
        <div class="page-sidebar">
            <div class="main-header-left d-none d-lg-block">
                <div class="logo-wrapper" style="display: table;margin: 0 auto"><a href=""><img
                            style="margin: 17px auto;display: table;"
                            src="{{ asset('images/logo/logo.png') }}"
                            alt=""></a>
                </div>
            </div>
            <div class="sidebar custom-scrollbar">
                <div class="sidebar-user text-center">
                    <div><img class="img-60 rounded-circle" src="{{ Auth::user()->user_image }}"
                              alt="#">
                        <div class="profile-edit"><a data-toggle="modal" href="#" data-target="#{{ Auth::user()->id }}"><i
                                    data-feather="edit"></i></a></div>
                    </div>
                    <h6 class="mt-3 f-14"></h6>
                    @if(Auth::user()->id == 1 )
                        <p>مدير النظام</p>
                    @else
                        <p>مشرف النظام</p>
                    @endif
                </div>
                <ul class="sidebar-menu">
                    <li><a class="sidebar-header" href="{{ route('admin.dashboard') }}"><i
                                data-feather="home"></i><span>الصفحه الرئيسيه</span></a>
                    </li>

                    <li><a class="sidebar-header" href=""><i
                                data-feather="users"></i><span>المستخدمين</span>
                            <i class="fa fa-angle-right pull-right"></i></a>
                        <ul class="sidebar-submenu">
                            <li><a href="{{ route('users') }}"><i
                                        class="fa fa-eye"></i><span>&nbsp;&nbsp; المستخدميين</span></a>
                            </li>
                        </ul>
                    </li>

                    <li><a class="sidebar-header" href=""><i
                                data-feather="users"></i><span>المشرفيين</span>
                            <i class="fa fa-angle-right pull-right"></i></a>
                        <ul class="sidebar-submenu">
                            <li><a href="{{ route('supervisor') }}"><i
                                        class="fa fa-plus"></i><span>&nbsp;&nbsp;المشرفيين</span></a>
                            </li>
                        </ul>
                    </li>

                    @if (auth()->user()->hasPermissionTo('عرض_مندوب'))
                        <li><a class="sidebar-header" href=""><i
                                    data-feather="users"></i><span>المناديب</span>
                                <i class="fa fa-angle-right pull-right"></i></a>
                            <ul class="sidebar-submenu">
                                <li><a href="{{ route('delegate') }}"><i
                                            class="fa fa-plus"></i><span>&nbsp;&nbsp;اضافه مناديب</span></a>
                                </li>
                            </ul>
                        </li>
                    @endif
{{--                    @if (auth()->user()->hasPermissionTo('عرض_مندوب'))--}}
                        <li><a class="sidebar-header" href="{{route('drivers')}}"><i
                                    data-feather="users"></i><span>السائقين</span>
                                <i class="fa fa-angle-right pull-right"></i></a>
                        </li>
{{--                    @endif--}}
{{--                    @if (auth()->user()->hasPermissionTo('عرض_مندوب'))--}}
                        <li><a class="sidebar-header" href="{{route('serviceCars')}}"><i
                                    data-feather="dollar-sign"></i><span>رسوم الخدمة</span>
                                <i class="fa fa-angle-right pull-right"></i></a>
                        </li>

{{--                    @endif--}}

                    @if (auth()->user()->hasPermissionTo('عرض_مندوب'))
                        <li><a class="sidebar-header" href=""><i
                                    data-feather="users"></i><span>شركات الشحن</span>
                                <i class="fa fa-angle-right pull-right"></i></a>
                            <ul class="sidebar-submenu">
                                <li><a href="{{ route('shippingCompany') }}"><i
                                            class="fa fa-plus"></i><span>&nbsp;&nbsp; شركات الشحن</span></a>
                                </li>
                            </ul>
                        </li>
                    @endif

                    @if (auth()->user()->hasPermissionTo('عرض_سلايدر'))
                        <li><a class="sidebar-header" href=""><i
                                    data-feather="sliders"></i><span> البانر الاعلانى</span>
                                <i class="fa fa-angle-right pull-right"></i></a>
                            <ul class="sidebar-submenu">
                                <li><a href="{{ route('sliders') }}"><i
                                            class="fa fa-plus"></i><span>&nbsp;&nbsp;اضافه بانر</span></a>
                                </li>
                            </ul>
                        </li>
                    @endif


                    <li><a class="sidebar-header" href=""><i
                                data-feather="home"></i><span>الخدمات الاساسيه</span>
                            <i class="fa fa-angle-right pull-right"></i></a>
                        <ul class="sidebar-submenu">

                            <li><a href="{{ route('services') }}"><i
                                        class="fa fa-plus"></i><span> الاقسام الرئيسه</span></a>
                            </li>
                        </ul>
                    </li>

                    @if (auth()->user()->hasPermissionTo('عرض_متجر'))
                        <li><a class="sidebar-header" href=""><i
                                    data-feather="home"></i><span>المتاجر</span>
                                <i class="fa fa-angle-right pull-right"></i></a>
                            <ul class="sidebar-submenu">
                                @if (auth()->user()->hasPermissionTo('عرض_متجر'))

                                    <li><a href="{{ route('shops') }}"><i
                                                class="fa fa-eye"></i><span> كل المتاجر</span></a>
                                    </li>
                                @endif
                                @if (auth()->user()->hasPermissionTo('اضافه_متجر'))

                                    <li><a href="{{ route('AddShops') }}"><i
                                                class="fa fa-plus"></i>&nbsp;&nbsp;<span>اضافه متاجر</span></a>
                                    </li>
                                @endif

                                {{--                                @if (auth()->user()->hasPermissionTo('عرض_قسم'))--}}

                                {{--                                    <li><a href="{{ route('categories') }}"><i--}}
                                {{--                                                class="fa fa-circle"></i><span>اقسام المتجر</span></a>--}}
                                {{--                                    </li>--}}
                                {{--                                @endif--}}


                            </ul>
                        </li>
                    @endif

                    <li><a class="sidebar-header" href=""><i
                                data-feather="save"></i><span>كروت التعبئه</span>
                            <i class="fa fa-angle-right pull-right"></i></a>
                        <ul class="sidebar-submenu">
                            <li><a class="sidebar-header" href="{{ route('packingCard') }}"><i
                                        class="fa fa-plus"></i><span> اضافه كرت تعبئه</span></a>
                            </li>
                            <li><a class="sidebar-header" href="{{ route('packingNumber') }}"><i
                                        class="fa fa-plus"></i><span> اضافه ارقام كروت تعبئه</span></a>
                            </li>
                            <li><a class="sidebar-header" href="{{ route('expiredCard') }}"><i
                                        class="fa fa-plus"></i><span> كروت تم استخدامها</span></a>
                            </li>
                            <li><a class="sidebar-header" href="{{ route('sellerPoints') }}"><i
                                        class="fa fa-plus"></i><span> نقط بيع كروت</span></a>
                            </li>
                        </ul>
                    </li>


                    <li><a class="sidebar-header" href=""><i
                                data-feather="shopping-bag"></i><span>الارقام المميزه</span>
                            <i class="fa fa-angle-right pull-right"></i></a>
                        <ul class="sidebar-submenu">
                            <li><a href="{{ route('prices') }}"><i
                                            class="fa fa-plus"></i><span>&nbsp;&nbsp;أسعار الارقام المميزه</span></a>
                            </li>
                            <li><a href="{{route('specialNumbers')}}"><i
                                        class="fa fa-plus"></i><span>&nbsp;&nbsp;الارقام المميزه</span></a>
                            </li>
                        </ul>
                    </li>


                    {{--                        <li><a class="sidebar-header" href=""><i--}}
                    {{--                                        data-feather="home"></i><span>الطلبات</span>--}}
                    {{--                                <i class="fa fa-angle-right pull-right"></i></a>--}}
                    {{--                            <ul class="sidebar-submenu">--}}
                    {{--                                <li><a href="{{ route('Orders') }}"><i--}}
                    {{--                                                class="fa fa-circle"></i><span>الطلبات</span></a>--}}
                    {{--                                </li>--}}
                    {{--                            </ul>--}}
                    {{--                        </li>--}}

                    <li><a class="sidebar-header" href=""><i
                                    data-feather="message-square"></i><span>الرسائل</span>
                            <i class="fa fa-angle-right pull-right"></i></a>
                        <ul class="sidebar-submenu">
                            <li>
                                <a href="{{route('companiesMessages')}}">
                                    <i class="fa fa-envelope"></i><span>&nbsp;&nbsp;رسائل الشركات</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('shopsMessages')}}">
                                    <i class="fa fa-envelope"></i><span>&nbsp;&nbsp;رسائل المتاجر</span>
                                </a>
                            </li>
                        </ul>
                    </li>


                    <li><a class="sidebar-header" href=""><i
                                data-feather="settings"></i><span>الاعدادت</span>
                            <i class="fa fa-angle-right pull-right"></i></a>
                        <ul class="sidebar-submenu">
                            <li><a class="sidebar-header" href="{{ route('settings') }}"><i
                                        data-feather="settings"></i><span>الاعدادات</span></a>
                            </li>
                            <li><a class="sidebar-header" href="{{ route('socialMedia') }}"><i
                                        data-feather="chrome"></i><span>السوشيال ميديا</span></a>
                            </li>
                            <li><a class="sidebar-header" href="{{route('promoCode')}}"><i
                                        data-feather="chrome"></i><span>اضافه برمو كود</span></a>
                            </li>
                        </ul>
                    </li>

                    <li><a class="sidebar-header" href="{{ route('suggestions') }}"><i
                                data-feather="eye"></i><span>الشكاوى والمقترحات</span></a>
                    </li>
                    <li><a class="sidebar-header" href="{{ route('adminNotifications') }}"><i
                                    data-feather="eye"></i><span>الإشعارات</span></a>
                    </li>

                    {{--                        <li><a class="sidebar-header" href="{{ route('cities') }}"><i--}}
                    {{--                                        data-feather="map"></i><span>المدن</span></a>--}}
                    {{--                        </li>--}}

                    <li><a class="sidebar-header" href=""><i
                                data-feather="users"></i><span>الدول</span>
                            <i class="fa fa-angle-right pull-right"></i></a>
                        <ul class="sidebar-submenu">
                            <li><a href="{{ route('countries') }}"><i
                                        class="fa fa-plus"></i><span>&nbsp;&nbsp;اضافه دوله</span></a>
                            </li>
                            <li><a href="{{ route('cities') }}"><i
                                        class="fa fa-plus"></i><span>&nbsp;&nbsp;اضافه مدينه</span></a>
                            </li>
                        </ul>
                    </li>


                    {{--                    <li><a class="sidebar-header" href="{{ route('notifications') }}"><i--}}
                    {{--                                    data-feather="eye"></i><span>الاشعارات</span></a>--}}
                    {{--                    </li>--}}


                    {{--                    <li><a class="sidebar-header" href="{{ route('settings') }}"><i--}}
                    {{--                                data-feather="home"></i><span>الاعدادات</span></a>--}}
                    {{--                    </li>--}}

                </ul>
            </div>
        </div>


    @yield('content')


    <!-- Page Sidebar Ends-->

        <!-- Right sidebar Ends-->


        <!-- footer start-->
        <div class="modal fade" id="{{ Auth::user()->id }}" tabindex="-1"
             role="dialog"
             aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"> تعديل حساب
                            {{ Auth::user()->first_name }} {{ Auth::user()->last_name }} </h5>

                        <button class="close" type="button"
                                data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <form class="needs-validation" novalidate=""
                              action="{{route('EditAccountSupervisor',['id'=>Auth::user()->id])}}"
                              method="POST"
                              enctype="multipart/form-data">
                            {{ method_field('POST') }}
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label
                                        class="col-form-label page-header-left">الاسم الاول
                                </label>
                                <input class="form-control"
                                       name="first_name"
                                       required
                                       type="text"
                                       value="{{ Auth::user()->first_name }}">
                                <div
                                        class="invalid-feedback">{{ $errors->first('first_name') }}</div>
                            </div>

                            <div class="form-group">
                                <label
                                        class="col-form-label page-header-left">  الاسم الاخير
                                </label>
                                <input class="form-control"
                                       name="last_name"
                                       required
                                       type="text"
                                       value="{{ Auth::user()->last_name }}">
                                <div
                                        class="invalid-feedback">{{ $errors->first('last_name') }}</div>
                            </div>

                            <div class="form-group">
                                <label
                                        class="col-form-label page-header-left">كلمه
                                    المرور</label>
                                <input class="form-control"
                                       name="password"
                                       type="password"
                                >
                                <div
                                        class="invalid-feedback">{{ $errors->first('password') }}</div>
                            </div>

                            <div class="form-group">
                                <label
                                        class="col-form-label page-header-left">رقم
                                    الهاتف
                                </label>
                                <input class="form-control" name="phone_number"
                                       type="text"
                                       value="{{ Auth::user()->phone_number }}"
                                       required>
                                <div
                                        class="invalid-feedback">{{ $errors->first('phone_number') }}</div>
                            </div>


                            <div class="form-group">
                                <label
                                        class="col-form-label page-header-left">
                                    البريد الالكترونى</label>
                                <input class="form-control"
                                       name="email"
                                       type="text"
                                       value="{{ Auth::user()->email }}">
                                <div
                                        class="invalid-feedback">{{ $errors->first('email') }}</div>
                            </div>


                            <div class="form-group">
                                <label for="validationCustom05"
                                       class="page-header-left">صوره
                                    الحساب</label>
                                <label for="file-input"
                                       class="image-upload-label">
                                    <img alt="upload-service-image"
                                         src="{{Auth::user()->user_image}}"
                                         class="thumb"
                                         style="width: 100px"/>
                                </label>
                                <div class="custom-file">
                                    <input class="custom-file-input"
                                           type="file"
                                           name="super_image">
                                    <label class="custom-file-label"
                                           for="validatedCustomFile">اختر
                                        صوره</label>
                                    <div
                                            class="invalid-feedback">{{ $errors->first('super_image') }}</div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-primary"
                                        type="submit">
                                    تعديل
                                </button>
                                <button class="btn btn-secondary"
                                        type="button"
                                        data-dismiss="modal">اغلاق
                                </button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    
    </div>
</div>
