<!-- latest jquery-->
<script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
<!-- Bootstrap js-->
<script src="{{asset('js/bootstrap/popper.min.js')}}"></script>
<script src="{{asset('js/bootstrap/bootstrap.js')}}"></script>
<!-- feather icon js-->
<script src="{{asset('js/icons/feather-icon/feather.min.js')}}"></script>
<script src="{{asset('js/icons/feather-icon/feather-icon.js')}}"></script>
<!-- Sidebar jquery-->
<script src="{{asset('js/chat-menu.js')}}"></script>

<script src="{{asset('js/sidebar-menu.js')}}"></script>
<script src="{{asset('js/config.js')}}"></script>

<link rel="stylesheet" href="{{ asset('js/noty/noty.css') }}">

<script src="{{ asset('js/noty/noty.min.js') }}"></script>

<!-- Plugins JS start-->


<script src="{{ asset('js/datatable/datatables/jquery.dataTables.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.js"></script>

{{--<script src="{{ asset('js/datatable/datatables/datatable.custom.js')}}"></script>--}}

<script>
    $('#basic-1').dataTable({
        "language": {
            "sEmptyTable":     "ليست هناك بيانات متاحة في الجدول",
            "sLoadingRecords": "جارٍ التحميل...",
            "sProcessing": "جارٍ التحميل...",
            "sLengthMenu": "أظهر _MENU_ مدخلات",
            "sZeroRecords": "لم يعثر على أية سجلات",
            "sInfo": "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
            "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجل",
            "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
            "sInfoPostFix": "",
            "sSearch": "ابحث:",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "الأول",
                "sPrevious": "السابق",
                "sNext": "التالي",
                "sLast": "الأخير"
            },
            "oAria": {
                "sSortAscending":  ": تفعيل لترتيب العمود تصاعدياً",
                "sSortDescending": ": تفعيل لترتيب العمود تنازلياً"
            }
        }
    });


    $('#basic-2').dataTable({
        "language": {
            "sProcessing": "جارٍ التحميل...",
            "sLengthMenu": "أظهر _MENU_ مدخلات",
            "sZeroRecords": "لم يعثر على أية سجلات",
            "sInfo": "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
            "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجل",
            "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
            "sInfoPostFix": "",
            "sSearch": "ابحث:",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "الأول",
                "sPrevious": "السابق",
                "sNext": "التالي",
                "sLast": "الأخير"
            }
        }
    });
    $('#basic-3').dataTable({
        "language": {
            "sProcessing": "جارٍ التحميل...",
            "sLengthMenu": "أظهر _MENU_ مدخلات",
            "sZeroRecords": "لم يعثر على أية سجلات",
            "sInfo": "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
            "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجل",
            "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
            "sInfoPostFix": "",
            "sSearch": "ابحث:",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "الأول",
                "sPrevious": "السابق",
                "sNext": "التالي",
                "sLast": "الأخير"
            }
        }
    });

    $('#basic-4').dataTable({
        "language": {
            "sProcessing": "جارٍ التحميل...",
            "sLengthMenu": "أظهر _MENU_ مدخلات",
            "sZeroRecords": "لم يعثر على أية سجلات",
            "sInfo": "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
            "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجل",
            "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
            "sInfoPostFix": "",
            "sSearch": "ابحث:",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "الأول",
                "sPrevious": "السابق",
                "sNext": "التالي",
                "sLast": "الأخير"
            }
        }
    });

    $('#basic-5').dataTable({
        "language": {
            "sProcessing": "جارٍ التحميل...",
            "sLengthMenu": "أظهر _MENU_ مدخلات",
            "sZeroRecords": "لم يعثر على أية سجلات",
            "sInfo": "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
            "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجل",
            "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
            "sInfoPostFix": "",
            "sSearch": "ابحث:",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "الأول",
                "sPrevious": "السابق",
                "sNext": "التالي",
                "sLast": "الأخير"
            }
        }
    });


</script>

<script src="{{ asset('js/chart/google/google-chart-loader.js')}}"></script>
<script src="{{ asset('js/chart/google/google-chart.js')}}"></script>

<script src="{{asset('js/prism/prism.min.js')}}"></script>
<script src="{{asset('js/clipboard/clipboard.min.js')}}"></script>
<script src="{{asset('js/counter/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('js/counter/jquery.counterup.min.js')}}"></script>
<script src="{{asset('js/counter/counter-custom.js')}}"></script>
<script src="{{asset('js/custom-card/custom-card.js')}}"></script>




<script src="{{asset('js/notify/bootstrap-notify.min.js')}}"></script>
{{--<script src="{{asset('js/dashboard/default.js')}}"></script>--}}
{{--<script src="{{asset('js/notify/index.js')}}"></script>--}}

{{--<script src="{{asset('js/select2/select2.full.min.js')}}"></script>--}}
{{--<script src="{{asset('js/select2/select2-custom.js')}}"></script>--}}

<script src="{{ asset('js/datepicker/date-picker/datepicker.js')}}"></script>
<script src="{{ asset('js/datepicker/date-picker/datepicker.en.js')}}"></script>
<script src="{{ asset('js/datepicker/date-picker/datepicker.custom.js')}}"></script>


<script src="{{ asset('js/time-picker/jquery-clockpicker.min.js')}}"></script>
<script src="{{ asset('js/time-picker/highlight.min.js')}}"></script>
<script src="{{ asset('js/time-picker/clockpicker.js')}}"></script>

{{--<script src="{{asset('js/typeahead/handlebars.js')}}"></script>--}}
{{--<script src="{{asset('js/typeahead/typeahead.bundle.js')}}"></script>--}}
{{--<script src="{{asset('js/typeahead/typeahead.custom.js')}}"></script>--}}
<script src="{{asset('js/select2/select2.full.min.js')}}"></script>
<script src="{{asset('js/select2/select2-custom.js')}}"></script>
<script src="{{asset('js/form-validation-custom.js')}}"></script>

<script src="{{asset('js/chat-menu.js')}}"></script>
<script src="{{ asset('js/print.js') }}"></script>
<script src="{{ asset('js/p.js') }}"></script>
<script src="{{asset('js/height-equal.js')}}"></script>
<script src="{{asset('js/tooltip-init.js')}}"></script>

<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="{{asset('js/script.js')}}"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
{{--<script src="{{asset('js/theme-customizer/customizer.js')}}"></script>--}}
<!-- Plugin used-->
<script src="{{ asset('js/toastr.min.js') }}"></script>
<script src="{{ asset('js/sweetalert.min.js') }}"></script>

{{--<script type="text/javascript">--}}
{{--    toastr.options = {--}}
{{--        "progressBar": true,--}}
{{--        "positionClass": "toast-bottom-right",--}}
{{--        toastClass: 'alert',--}}
{{--        iconClasses: {--}}
{{--            error: 'alert-error',--}}
{{--            info: 'alert-info',--}}
{{--            success: 'alert-success',--}}
{{--            warning: 'alert-warning'--}}
{{--        }--}}
{{--    };--}}
{{--</script>--}}
{{--<script type="text/javascript">--}}
{{--    @if(session()->has('success'))--}}
{{--    toastr.success("{{session()->get('success')}}");--}}
{{--    @elseif(session()->has('error'))--}}
{{--    toastr.error("{{session()->get('error')}}");--}}
{{--    @endif--}}
{{--</script>--}}




@if (session('success'))

    <script>
        new Noty({
            type: 'success',
            layout: 'bottomRight',
            text: "{{ session('success') }}",
            timeout: 2000,
            killer: true
        }).show();
    </script>

@endif

@if (session('error'))

    <script>
        new Noty({
            type: 'error',
            layout: 'bottomRight',
            text: "{{ session('error') }}",
            timeout: 2000,
            killer: true
        }).show();
    </script>

@endif

<script>

    (function ($) {
        $('.btnprn').printPage();


        $(document).ready(function () {

            $("#country").change(function () {
                const country_id = $(this).val();
                if (country_id !== '') {
                    $.ajax
                    ({
                        type: "GET",
                        url: "{{route('accounts-from-countries')}}",
                        data: "country_id=" + encodeURIComponent(country_id),
                        success: function (option) {
                            $("#persons").html(option);
                        }
                    });
                } else {
                    $("#persons").html("<option value=''>اختر اسم المرسل إليه</option>");
                }
                return false;
            });

            $("#country_id").change(function () {
                const country_id = $(this).val();
                if (country_id !== '') {
                    $.ajax
                    ({
                        type: "GET",
                        url: "{{route('getCities')}}",
                        data: "country_id=" + encodeURIComponent(country_id),
                        success: function (option) {
                            $("#city_id").html(option);
                        }
                    });
                } else {
                    $("#city_id").html("<option value=''>اختر اسم الدوله</option>");
                }
                return false;
            });





            //delete
            $('.delete').click(function (e) {

                var that = $(this);
                e.preventDefault();

                var n = new Noty({
                    text: "تاكيد الحذف",
                    type: "error",
                    layout: 'bottomRight',
                    killer: true,
                    buttons: [
                        Noty.button("نعم", 'btn btn-success mr-2', function () {
                            that.closest('form').submit();
                        }),
                        Noty.button("لا", 'btn btn-primary mr-2', function () {
                            n.close();
                        })
                    ]
                });

                n.show();

            });//end of delete

        });
    })(jQuery);
    //end of ready


    @if(isset($users_charts))
    if ($("#users").length > 0) {
        var data = google.visualization.arrayToDataTable([

            ["Element", "users", {role: "style"}],
                @if(isset($users_charts))
                @foreach($users_charts as $test)
            [" {{$test->date  }} ", {{ $test->count }} , "#1D2D44"],
            @endforeach
            @endif
        ]);
        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            {
                calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation"
            },
            2]);
        var options = {
            width: '100%',
            height: 400,
            bar: {groupWidth: "95%"},
            legend: {position: "none"},
        };
        var chart = new google.visualization.ColumnChart(document.getElementById("users"));
        chart.draw(view, options);
    }
    @endif

        @if(isset($delegate_charts))
    if ($("#users").length > 0) {
        var data = google.visualization.arrayToDataTable([

            ["Element", "users", {role: "style"}],
                @if(isset($delegate_charts))
                @foreach($delegate_charts as $test)
            [" {{$test->date  }} ", {{ $test->count }} , "#1D2D44"],
            @endforeach
            @endif
        ]);
        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            {
                calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation"
            },
            2]);
        var options = {
            width: '100%',
            height: 400,
            bar: {groupWidth: "95%"},
            legend: {position: "none"},
        };
        var chart = new google.visualization.ColumnChart(document.getElementById("delegate"));
        chart.draw(view, options);
    }
    @endif

        @if(isset($of_shops))
    if ($("#shops").length > 0) {
        var data = google.visualization.arrayToDataTable([

            ["Element", "shops", {role: "style"}],
                @if(!empty($of_shops))
                @foreach($of_shops as $test)

            ["{{ $test->date }}", {{ $test->count }}  , "#1D2D44"],

            @endforeach
            @endif

        ]);
        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            {
                calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation"
            },
            2]);
        var options = {
            width: '100%',
            height: 400,
            bar: {groupWidth: "95%"},
            legend: {position: "none"},
        };
        var chart = new google.visualization.ColumnChart(document.getElementById("shops"));
        chart.draw(view, options);
    }
    @endif
</script>
