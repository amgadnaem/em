@extends('AdminPanel.backend.layouts.default')
@section('content')


    <div class="page-body">
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>تعديل سائق</h5>
                        </div>
                        <form class="needs-validation" method="POST" action="{{route('updateDriver')}}" enctype="multipart/form-data" novalidate="">
                            @csrf
                            <input type="hidden" name="driver_id" value="{{$data['driver']->id}}"/>
                            <div class="card-body">
                                <div class="form-group">
                                    <input  type="hidden" class="form-control"  name="lng" value="{{isset($data['driver']->lat) ? $data['driver']->lat : ""}}" id="userlat" placeholder="Enter lat ">
                                </div>
                                <div class="form-group">
                                    <input  type="hidden" class="form-control"  name="lat" value="{{isset($data['driver']->lng) ? $data['driver']->lng : ""}}" id="userlng" placeholder="Enter long">
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <div id="userlocation" style="width:100%;height:350px"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="autocomplete">البحث عن عنوان</label>
                                            <input type="text" class="form-control" id="autocomplete" placeholder="البحث عن عنوان" >                                    </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="validationCustom05"
                                                   class="col-form-label page-header-left">إسم السيارة</label>
                                            <input class="form-control" value="{{isset($data['driver']->car_name) ? $data['driver']->car_name : ""}}"
                                                   name="car_name" id="validationCustomCarname"
                                                   type="text"
                                                   placeholder="ادخل إسم السيارة"
                                                   aria-describedby="inputGroupPrepend"
                                                   required="">
                                            <div class="invalid-feedback">{{ $errors->first('car_name') }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="validationCustom05"
                                                   class="col-form-label page-header-left">لون السيارة</label>
                                            <input class="form-control"
                                                   name="car_color" id="validationCustomCarcolor"
                                                   type="color"
                                                   value="{{isset($data['driver']->car_color) ? $data['driver']->car_color: ""}}"
                                                   placeholder="ادخل لون السيارة"
                                                   aria-describedby="inputGroupPrepend"
                                                   required="">
                                            <div class="invalid-feedback">{{ $errors->first('car_color') }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="validationCustom05"
                                                   class="col-form-label page-header-left">رقم السيارة</label>
                                            <input class="form-control" value="{{isset($data['driver']->car_number) ? $data['driver']->car_number: ""}}"
                                                   name="car_number" id="validationCustomCarnumber"
                                                   type="text"
                                                   placeholder="ادخل رقم السيارة"
                                                   aria-describedby="inputGroupPrepend"
                                                   required="">
                                            <div class="invalid-feedback">{{ $errors->first('car_number') }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="validationCustom05"
                                                   class="col-form-label page-header-left">الاسم الاول</label>
                                            <input class="form-control" value="{{isset($data['driver']->first_name) ? $data['driver']->first_name : ""}}"
                                                   name="first_name" id="validationCustomUsername"
                                                   type="text"
                                                   placeholder="ادخل الاسم الاول"
                                                   aria-describedby="inputGroupPrepend"
                                                   required="">
                                            <div class="invalid-feedback">{{ $errors->first('first_name') }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="validationCustom05"
                                                   class="col-form-label page-header-left">الاسم الاخير</label>
                                            <input class="form-control" value="{{isset($data['driver']->last_name) ? $data['driver']->last_name : ""}}"
                                                   name="last_name"
                                                   id="validationCustomUsername"
                                                   type="text"
                                                   placeholder="ادخل الاسم الاخير"
                                                   aria-describedby="inputGroupPrepend"
                                                   required="">
                                            <div class="invalid-feedback">{{ $errors->first('last_name') }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="validationCustom05"
                                                   class="col-form-label page-header-left">رقم الهاتف</label>
                                            <input class="form-control" value="{{isset($data['driver']->phone_number) ? $data['driver']->phone_number : ""}}"
                                                   name="phone_number" id="validationCustomPhonenumber"
                                                   type="text"
                                                   placeholder="ادخل رقم الهاتف"
                                                   aria-describedby="inputGroupPrepend"
                                                   required="">
                                            <div class="invalid-feedback">{{ $errors->first('phone_number') }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="validationCustom05"
                                                   class="col-form-label page-header-left">كلمه المرور</label>
                                            <input class="form-control" name="password" id="validationCustom05"
                                                   type="password"
                                                   placeholder="ادخل كلمه المرور في حالة أردت تغييرها"
                                            >
                                            <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="text-center">
                                        <img id="image-display" width="100px" height="100px" class="profile-user-img img-fluid img-circle" style="border: 1px solid #e5eaff;"
                                             alt="Admin profile picture" src="{{$data['driver']->driver_image}}" >
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="validationCustom05" class="col-form-label page-header-left">صوره السائق</label>
                                            <div class="custom-file">
                                                <input class="custom-file-input" id="image"
                                                       type="file"
                                                       name="driver_image">
                                                <label class="custom-file-label"
                                                       for="image">اختر</label>
                                                <div class="invalid-feedback">تحميل صوره</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="validationCustom05"
                                                   class="col-form-label page-header-left">تاريخ الميلاد</label>
                                            <input class="form-control" name="dob" value="{{isset($data['driver']->dob) ? $data['driver']->dob : ""}}"
                                                   id="validationCustomUsername"
                                                   type="date"
                                                   placeholder="ادخل تاريخ الميلاد"
                                                   aria-describedby="inputGroupPrepend"
                                                   required="">
                                            <div class="invalid-feedback">{{ $errors->first('dob') }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="validationCustom05" class="page-header-left"> اسم الدوله</label>
                                            <select name="country_id" id="country_id"
                                                    class="form-control"
                                                    required>
                                                <option value="">اختر</option>
                                                @foreach($data['countries'] as $country)
                                                    <option value="{{ $country->id }}" @if ($country->id == $data['driver']->country_id)
                                                    selected
                                                            @endif>{{ $country->country_name_ar }}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">{{ $errors->first('country_id') }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="validationCustom05" class="page-header-left"> اسم المدينه</label>
                                            <select name="city_id" id="city_id"
                                                    class="form-control"
                                                    required>
                                                @foreach($data['driver']->country->cities as $city)
                                                    {{--                                                <option value="">اختر اسم الدوله اولا</option>--}}
                                                    <option value="{{ $city->id }}" @if ($city->id == $data['driver']->city_id)
                                                    selected
                                                            @endif>{{ $city->city_name_ar }}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">{{ $errors->first('city_id') }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-primary" type="submit">حفط</button>
                                <input class="btn btn-light" type="reset" value="إلغاء">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#image-display').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#image").change(function() {
            readURL(this);
        });
    </script>
    <script>
        var marker = null;
        var placeSearch, autocomplete;
        function initMap() {
            autocomplete =
                new google.maps.places.Autocomplete((document.getElementById('autocomplete')),
                    {types: ['geocode']});
            var map = new google.maps.Map(document.getElementById('userlocation'), {
                zoom: 7,
                center: {lat: {{$data['driver']->lat}}, lng: {{$data['driver']->lng}} }
            });
            var MaekerPos = new google.maps.LatLng({{$data['driver']->lat}} , {{$data['driver']->lng}});
            marker = new google.maps.Marker({
                position: MaekerPos,
                map: map
            });
            autocomplete.addListener('place_changed', function(){
                placeMarkerAndPanTo(autocomplete.getPlace().geometry.location, map);
                document.getElementById("userlat").value=autocomplete.getPlace().geometry.location.lat();
                document.getElementById("userlng").value=autocomplete.getPlace().geometry.location.lng();
            });
            map.addListener('click', function(e) {
                placeMarkerAndPanTo(e.latLng, map);
                document.getElementById("userlat").value=e.latLng.lat();
                document.getElementById("userlng").value=e.latLng.lng();
            });
        }
        function placeMarkerAndPanTo(latLng, map) {
            map.setZoom(9);
            marker.setPosition(latLng);
            map.panTo(latLng);
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDPN_XufKy-QTSCB68xFJlqtUjHQ8m6uUY&libraries=places&callback=initMap">
    </script>
    <script>


        $('.driver_status').on('change.bootstrapSwitch', function(e) {
            var driver_id = $(this).attr('data-driver-id');
            var status = "";
            if (e.target.checked == true){
                var status = 1;
            }else{
                var status = 0;
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "{{URL::route('changeDriverStatus')}}",
                data: {
                    driver_id: driver_id,
                    status: status,
                    "_token": "{{ csrf_token() }}"
                },
                success: function(response){
                    toastr.success(response.success);
                },
                error: function(jqXHR){
                    toastr.error(jqXHR.responseJSON.message);
                }
            });
        });

        function confirm_delegate(id) {
            axios.get('confirmAccountDriver/' + id)
                .then(function (response) {
                    location.reload();
                })
                .catch(function (error) {
                    console.log(error);
                    alert(error);
                });
        };


    </script>

@endsection
