@extends('AdminPanel.backend.layouts.default')
@section('content')


    <div class="page-body">
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>إضافة سائق</h5>
                        </div>
                        <form class="needs-validation" method="POST" action="{{route('storeDriver')}}" enctype="multipart/form-data" novalidate="">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <input  type="hidden" class="form-control"  name="lng" value="" id="userlat" placeholder="Enter lat ">
                                </div>
                                <div class="form-group">
                                    <input  type="hidden" class="form-control"  name="lat" value="" id="userlng" placeholder="Enter long">
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <div id="userlocation" style="width:100%;height:350px"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="autocomplete">البحث عن عنوان</label>
                                            <input type="text" class="form-control" id="autocomplete" placeholder="البحث عن عنوان" >                                    </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="validationCustom05"
                                                   class="col-form-label page-header-left">إسم السيارة</label>
                                            <input class="form-control" value="{{ old('car_name') }}"
                                                   name="car_name" id="validationCustomCarname"
                                                   type="text"
                                                   placeholder="ادخل إسم السيارة"
                                                   aria-describedby="inputGroupPrepend"
                                                   required="">
                                            <div class="invalid-feedback">{{ $errors->first('car_name') }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="validationCustom05"
                                                   class="col-form-label page-header-left">لون السيارة</label>
                                            <input class="form-control"
                                                   name="car_color" id="validationCustomCarcolor"
                                                   type="color"
                                                   value="#ff0000"
                                                   placeholder="ادخل لون السيارة"
                                                   aria-describedby="inputGroupPrepend"
                                                   required="">
                                            <div class="invalid-feedback">{{ $errors->first('car_color') }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="validationCustom05"
                                                   class="col-form-label page-header-left">رقم السيارة</label>
                                            <input class="form-control" value="{{ old('car_number') }}"
                                                   name="car_number" id="validationCustomCarnumber"
                                                   type="text"
                                                   placeholder="ادخل رقم السيارة"
                                                   aria-describedby="inputGroupPrepend"
                                                   required="">
                                            <div class="invalid-feedback">{{ $errors->first('car_number') }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="validationCustom05"
                                                   class="col-form-label page-header-left">الاسم الاول</label>
                                            <input class="form-control" value="{{ old('first_name') }}"
                                                   name="first_name" id="validationCustomUsername"
                                                   type="text"
                                                   placeholder="ادخل الاسم الاول"
                                                   aria-describedby="inputGroupPrepend"
                                                   required="">
                                            <div class="invalid-feedback">{{ $errors->first('first_name') }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="validationCustom05"
                                                   class="col-form-label page-header-left">الاسم الاخير</label>
                                            <input class="form-control" value="{{ old('last_name') }}"
                                                   name="last_name"
                                                   id="validationCustomUsername"
                                                   type="text"
                                                   placeholder="ادخل الاسم الاخير"
                                                   aria-describedby="inputGroupPrepend"
                                                   required="">
                                            <div class="invalid-feedback">{{ $errors->first('last_name') }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="validationCustom05"
                                                   class="col-form-label page-header-left">رقم الهاتف</label>
                                            <input class="form-control" value="{{ old('phone_number') }}"
                                                   name="phone_number" id="validationCustomPhonenumber"
                                                   type="text"
                                                   placeholder="ادخل رقم الهاتف"
                                                   aria-describedby="inputGroupPrepend"
                                                   required="">
                                            <div class="invalid-feedback">{{ $errors->first('phone_number') }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="validationCustom05"
                                                   class="col-form-label page-header-left">كلمه المرور</label>
                                            <input class="form-control" name="password" id="validationCustom05"
                                                   type="password"
                                                   placeholder="ادخل كلمه المرور"
                                                   required="">
                                            <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="validationCustom05" class="col-form-label page-header-left">صوره السائق</label>
                                            <div class="custom-file">
                                                <input class="custom-file-input" id="validatedCustomFile"
                                                       type="file"
                                                       name="driver_image">
                                                <label class="custom-file-label"
                                                       for="validatedCustomFile">اختر</label>
                                                <div class="invalid-feedback">تحميل صوره</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="validationCustom05"
                                                   class="col-form-label page-header-left">تاريخ الميلاد</label>
                                            <input class="form-control" name="dob" value="{{ old('dob') }}"
                                                   id="validationCustomUsername"
                                                   type="date"
                                                   placeholder="ادخل تاريخ الميلاد"
                                                   aria-describedby="inputGroupPrepend"
                                                   required="">
                                            <div class="invalid-feedback">{{ $errors->first('dob') }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="validationCustom05" class="page-header-left"> اسم الدوله</label>
                                            <select name="country_id" id="country_id"
                                                    class="form-control"
                                                    required>
                                                <option value="">اختر</option>
                                                @foreach($data['countries'] as $country)
                                                    <option
                                                            value="{{ $country->id }}">{{ $country->country_name_ar }}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">{{ $errors->first('country_id') }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="validationCustom05" class="page-header-left"> اسم المدينه</label>
                                            <select name="city_id" id="city_id"
                                                    class="form-control"
                                                    required>
                                                <option value="">اختر اسم الدوله اولا</option>
                                            </select>
                                            <div class="invalid-feedback">{{ $errors->first('city_id') }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-primary" type="submit">حفط</button>
                                <input class="btn btn-light" type="reset" value="إلغاء">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        var marker = null;
        var placeSearch, autocomplete;
        function initMap() {
            autocomplete =
                new google.maps.places.Autocomplete((document.getElementById('autocomplete')),
                    {types: ['geocode']});
            var map = new google.maps.Map(document.getElementById('userlocation'), {
                zoom: 7,
                center: {lat: 26.719517, lng: 29.2161655 }
            });
            var MaekerPos = new google.maps.LatLng(0 , 0);
            marker = new google.maps.Marker({
                position: MaekerPos,
                map: map
            });
            autocomplete.addListener('place_changed', function(){
                placeMarkerAndPanTo(autocomplete.getPlace().geometry.location, map);
                document.getElementById("userlat").value=autocomplete.getPlace().geometry.location.lat();
                document.getElementById("userlng").value=autocomplete.getPlace().geometry.location.lng();
            });
            map.addListener('click', function(e) {
                placeMarkerAndPanTo(e.latLng, map);
                document.getElementById("userlat").value=e.latLng.lat();
                document.getElementById("userlng").value=e.latLng.lng();
            });
        }
        function placeMarkerAndPanTo(latLng, map) {
            map.setZoom(9);
            marker.setPosition(latLng);
            map.panTo(latLng);
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDPN_XufKy-QTSCB68xFJlqtUjHQ8m6uUY&libraries=places&callback=initMap">
    </script>
    <script>


        $('.driver_status').on('change.bootstrapSwitch', function(e) {
            var driver_id = $(this).attr('data-driver-id');
            var status = "";
            if (e.target.checked == true){
                var status = 1;
            }else{
                var status = 0;
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "{{URL::route('changeDriverStatus')}}",
                data: {
                    driver_id: driver_id,
                    status: status,
                    "_token": "{{ csrf_token() }}"
                },
                success: function(response){
                    toastr.success(response.success);
                },
                error: function(jqXHR){
                    toastr.error(jqXHR.responseJSON.message);
                }
            });
        });

        function confirm_delegate(id) {
            axios.get('confirmAccountDriver/' + id)
                .then(function (response) {
                    location.reload();
                })
                .catch(function (error) {
                    console.log(error);
                    alert(error);
                });
        };


    </script>

@endsection
