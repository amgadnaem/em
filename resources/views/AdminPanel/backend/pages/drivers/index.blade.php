@extends('AdminPanel.backend.layouts.default')
@section('content')


    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">

                        <div class="page-header-left">
{{--                            @if (auth()->user()->hasPermissionTo('اضافه_مندوب'))--}}
{{--                                <button class="btn btn-primary" type="button" data-toggle="modal"--}}
{{--                                        data-target="#AddDelegate" data-whatever="@delegate">اضافه سائق--}}
{{--                                </button>--}}
                            <a class="btn btn-square btn-primary" href="{{route('createDriver')}}" title="إضافة"> إضافة سائق </a> &nbsp; &nbsp;

                            {{--                            @endif--}}
{{--                            <div class="modal fade" id="AddDelegate" tabindex="-1" role="dialog"--}}
{{--                                 aria-labelledby="exampleModalLabel" aria-hidden="true">--}}
{{--                                <div class="modal-dialog" role="document">--}}
{{--                                    <div class="modal-content">--}}
{{--                                        <div class="modal-header">--}}
{{--                                            <h5 class="modal-title">اضافه سائق</h5>--}}
{{--                                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">--}}
{{--                                                <span aria-hidden="true">×</span></button>--}}
{{--                                        </div>--}}
{{--                                        <div class="modal-body">--}}
{{--                                            <form class="needs-validation" novalidate=""--}}
{{--                                                  action="{{route('addDriver')}}"--}}
{{--                                                  method="POST" enctype="multipart/form-data">--}}
{{--                                                {{ method_field('POST') }}--}}
{{--                                                {{ csrf_field() }}--}}
{{--                                                <div class="form-group">--}}
{{--                                                    <input  type="hidden" class="form-control"  name="lng" value="" id="userlat" placeholder="Enter lat ">--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group">--}}
{{--                                                    <input  type="hidden" class="form-control"  name="lat" value="" id="userlng" placeholder="Enter long">--}}
{{--                                                </div>--}}
{{--                                                <div class="row">--}}
{{--                                                    <div class="col">--}}
{{--                                                        <div class="form-group">--}}
{{--                                                            <div id="userlocation" style="width:100%;height:350px"></div>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group">--}}
{{--                                                    <label for="validationCustom05"--}}
{{--                                                           class="col-form-label page-header-left">إسم السيارة</label>--}}
{{--                                                    <input class="form-control" value="{{ old('car_name') }}"--}}
{{--                                                           name="car_name" id="validationCustomCarname"--}}
{{--                                                           type="text"--}}
{{--                                                           placeholder="ادخل إسم السيارة"--}}
{{--                                                           aria-describedby="inputGroupPrepend"--}}
{{--                                                           required="">--}}
{{--                                                    <div class="invalid-feedback">{{ $errors->first('car_name') }}</div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group">--}}
{{--                                                    <label for="validationCustom05"--}}
{{--                                                           class="col-form-label page-header-left">لون السيارة</label>--}}
{{--                                                    <input class="form-control"--}}
{{--                                                           name="car_color" id="validationCustomCarcolor"--}}
{{--                                                           type="color"--}}
{{--                                                           value="#ff0000"--}}
{{--                                                           placeholder="ادخل لون السيارة"--}}
{{--                                                           aria-describedby="inputGroupPrepend"--}}
{{--                                                           required="">--}}
{{--                                                    <div class="invalid-feedback">{{ $errors->first('car_color') }}</div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group">--}}
{{--                                                    <label for="validationCustom05"--}}
{{--                                                           class="col-form-label page-header-left">رقم السيارة</label>--}}
{{--                                                    <input class="form-control" value="{{ old('car_number') }}"--}}
{{--                                                           name="car_number" id="validationCustomCarnumber"--}}
{{--                                                           type="text"--}}
{{--                                                           placeholder="ادخل رقم السيارة"--}}
{{--                                                           aria-describedby="inputGroupPrepend"--}}
{{--                                                           required="">--}}
{{--                                                    <div class="invalid-feedback">{{ $errors->first('car_number') }}</div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group">--}}
{{--                                                    <label for="validationCustom05"--}}
{{--                                                           class="col-form-label page-header-left">رقم السائق</label>--}}
{{--                                                    <input class="form-control" value="{{ old('driver_number') }}"--}}
{{--                                                           name="driver_number" id="validationCustomDrivernumber"--}}
{{--                                                           type="text"--}}
{{--                                                           placeholder="ادخل رقم السائق"--}}
{{--                                                           aria-describedby="inputGroupPrepend"--}}
{{--                                                           required="">--}}
{{--                                                    <div class="invalid-feedback">{{ $errors->first('driver_number') }}</div>--}}
{{--                                                </div>--}}

{{--                                                <div class="form-group">--}}
{{--                                                    <label for="validationCustom05"--}}
{{--                                                           class="col-form-label page-header-left">الاسم الاول</label>--}}
{{--                                                    <input class="form-control" value="{{ old('first_name') }}"--}}
{{--                                                           name="first_name" id="validationCustomUsername"--}}
{{--                                                           type="text"--}}
{{--                                                           placeholder="ادخل الاسم الاول"--}}
{{--                                                           aria-describedby="inputGroupPrepend"--}}
{{--                                                           required="">--}}
{{--                                                    <div class="invalid-feedback">{{ $errors->first('first_name') }}</div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group">--}}
{{--                                                    <label for="validationCustom05"--}}
{{--                                                           class="col-form-label page-header-left">الاسم الاخير</label>--}}
{{--                                                    <input class="form-control" value="{{ old('last_name') }}"--}}
{{--                                                           name="last_name"--}}
{{--                                                           id="validationCustomUsername"--}}
{{--                                                           type="text"--}}
{{--                                                           placeholder="ادخل الاسم الاخير"--}}
{{--                                                           aria-describedby="inputGroupPrepend"--}}
{{--                                                           required="">--}}
{{--                                                    <div class="invalid-feedback">{{ $errors->first('last_name') }}</div>--}}
{{--                                                </div>--}}

{{--                                                <div class="form-group">--}}
{{--                                                    <label for="validationCustom05"--}}
{{--                                                           class="col-form-label page-header-left">رقم الهاتف</label>--}}
{{--                                                    <input class="form-control" value="{{ old('phone_number') }}"--}}
{{--                                                           name="phone_number" id="validationCustomPhonenumber"--}}
{{--                                                           type="text"--}}
{{--                                                           placeholder="ادخل رقم الهاتف"--}}
{{--                                                           aria-describedby="inputGroupPrepend"--}}
{{--                                                           required="">--}}
{{--                                                    <div class="invalid-feedback">{{ $errors->first('phone_number') }}</div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group">--}}
{{--                                                    <label for="validationCustom05"--}}
{{--                                                           class="col-form-label page-header-left">كلمه المرور</label>--}}
{{--                                                    <input class="form-control" name="password" id="validationCustom05"--}}
{{--                                                           type="password"--}}
{{--                                                           placeholder="ادخل كلمه المرور"--}}
{{--                                                           required="">--}}
{{--                                                    <div class="invalid-feedback">{{ $errors->first('password') }}</div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group">--}}
{{--                                                    <label for="validationCustom05" class="col-form-label page-header-left">صوره السائق</label>--}}
{{--                                                    <div class="custom-file">--}}
{{--                                                        <input class="custom-file-input" id="validatedCustomFile"--}}
{{--                                                               type="file"--}}
{{--                                                               name="driver_image">--}}
{{--                                                        <label class="custom-file-label"--}}
{{--                                                               for="validatedCustomFile">اختر</label>--}}
{{--                                                        <div class="invalid-feedback">تحميل صوره</div>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group">--}}
{{--                                                    <label for="validationCustom05"--}}
{{--                                                           class="col-form-label page-header-left">تاريخ الميلاد</label>--}}
{{--                                                    <input class="form-control" name="dob" value="{{ old('dob') }}"--}}
{{--                                                           id="validationCustomUsername"--}}
{{--                                                           type="date"--}}
{{--                                                           placeholder="ادخل تاريخ الميلاد"--}}
{{--                                                           aria-describedby="inputGroupPrepend"--}}
{{--                                                           required="">--}}
{{--                                                    <div class="invalid-feedback">{{ $errors->first('dob') }}</div>--}}
{{--                                                </div>--}}


{{--                                                <div class="form-group">--}}
{{--                                                    <label for="validationCustom05" class="page-header-left"> اسم الدوله</label>--}}
{{--                                                    <select name="country_id" id="country_id"--}}
{{--                                                            class="form-control"--}}
{{--                                                            required>--}}
{{--                                                        <option value="">اختر</option>--}}
{{--                                                        @foreach($data['countries'] as $country)--}}
{{--                                                            <option--}}
{{--                                                                    value="{{ $country->id }}">{{ $country->country_name_ar }}</option>--}}
{{--                                                        @endforeach--}}
{{--                                                    </select>--}}
{{--                                                    <div class="invalid-feedback">{{ $errors->first('country_id') }}</div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group">--}}
{{--                                                    <label for="validationCustom05" class="page-header-left"> اسم المدينه</label>--}}
{{--                                                    <select name="city_id" id="city_id"--}}
{{--                                                            class="form-control"--}}
{{--                                                            required>--}}
{{--                                                        <option value="">اختر اسم الدوله اولا</option>--}}
{{--                                                    </select>--}}
{{--                                                    <div class="invalid-feedback">{{ $errors->first('city_id') }}</div>--}}
{{--                                                </div>--}}
{{--                                                <div class="modal-footer">--}}
{{--                                                    <button class="btn btn-primary" type="submit">اضافه</button>--}}
{{--                                                    <button class="btn btn-secondary" type="button"--}}
{{--                                                            data-dismiss="modal">اغلاق--}}
{{--                                                    </button>--}}
{{--                                                </div>--}}

{{--                                            </form>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>عدد السائقين {{ count($data['drivers']) }}</h5>
                        </div>
                        @if( count($data['drivers']) > 0)
{{--                            @if (auth()->user()->hasPermissionTo('عرض_مندوب'))--}}
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="display dataTable" id="basic-1">
                                            <thead>
                                            <tr>
                                                <th>QR Code</th>
                                                <th>صوره</th>
                                                <th>الاسم</th>
                                                <th>رقم الهاتف</th>
                                                <th>رقم ال ID</th>
{{--                                                @if (auth()->user()->hasPermissionTo('تعديل_مندوب'))--}}
                                                    <th>حاله الحساب</th>
{{--                                                @endif--}}
{{--                                                <th>الطلبات المنفذه</th>--}}
                                                <th>الاكشن</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($data['drivers'] as $driver)
                                                <tr>
                                                    <td><img
                                                            src="data:image/png;base64, {!! base64_encode(\SimpleSoftwareIO\QrCode\Facades\QrCode::format('png')->size(100)->generate($driver->phone_number)) !!} ">
                                                    </td>
                                                    <td><img src="{{ $driver->driver_image }}" style="width: 50px"></td>
                                                    <td>{{ $driver->first_name }}  {{ $driver->last_name }}</td>
                                                    <td>{{ $driver->phone_number }}</td>
                                                    <td>{{ $driver->driver_number }}</td>
{{--                                                    @if (auth()->user()->hasPermissionTo('تعديل_مندوب'))--}}
                                                        <td>
                                                            <div class="media-body ">
{{--                                                                <label class="switch">--}}
{{--                                                                    <input type="checkbox"--}}
{{--                                                                           {{ $driver->status == 1 ? 'checked' : '' }}><span--}}
{{--                                                                            class="switch-state driver_status" data-driver-id="{{$driver->id}}"></span>--}}
{{--                                                                </label>--}}
                                                                <label class="switch">
                                                                    <input type="checkbox" id="status" class=" driver_status" data-driver-id="{{$driver->id}}" name="status" @if($driver->status == 1) checked @endif><span class="switch-state"></span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                    <td>
                                                        <a title="تعديل" href="{{route('editDriver',['driver_id' => $driver->id])}}"><i width="20" height="20" color="blue" data-feather="edit"></i></a> &nbsp;
                                                        <a onclick='return deleteDelegate({{$driver->id}})' title="حذف" data-id="{{$driver->id}}" href="#"><i width="20" height="20" color="red" data-feather="trash-2"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                        @else
                            <h4 class="text-center"> لا يوجد سائقين لديك</h4>
                        @endif
                    </div>
                </div>
            </div>




        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>


    <script>
        function deleteDelegate(driver_id)
        {
            var id = driver_id;
            swal({
                title: 'هل أنت متأكد!',
                text: 'عفواً لا يمكنك التراجع عن هذا الأمر',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'حذف',
                cancelButtonText: 'تراجع'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('deleteDriver')}}",
                        dataType: 'json',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: {driver_id:id,"_token": "{{ csrf_token() }}"},

                        success: function(response){
                            window.location.reload();
                            toastr.success(response.success);
                        },
                        error: function() {
                            swal(
                                'يوجد خطأ ما',
                                'من فضلك حاول مرة أخري',
                                'خطأ'
                            )
                        }
                    });
                }
            });
            //
        }
    </script>


    <script>
        var marker = null;
        var placeSearch, autocomplete;
        function initMap() {
            autocomplete =
                new google.maps.places.Autocomplete((document.getElementById('autocomplete')),
                    {types: ['geocode']});
            var map = new google.maps.Map(document.getElementById('userlocation'), {
                zoom: 7,
                center: {lat: 26.719517, lng: 29.2161655 }
            });
            var MaekerPos = new google.maps.LatLng(0 , 0);
            marker = new google.maps.Marker({
                position: MaekerPos,
                map: map
            });
            autocomplete.addListener('place_changed', function(){
                placeMarkerAndPanTo(autocomplete.getPlace().geometry.location, map);
                document.getElementById("userlat").value=autocomplete.getPlace().geometry.location.lat();
                document.getElementById("userlng").value=autocomplete.getPlace().geometry.location.lng();
            });
            map.addListener('click', function(e) {
                placeMarkerAndPanTo(e.latLng, map);
                document.getElementById("userlat").value=e.latLng.lat();
                document.getElementById("userlng").value=e.latLng.lng();
            });
        }
        function placeMarkerAndPanTo(latLng, map) {
            map.setZoom(9);
            marker.setPosition(latLng);
            map.panTo(latLng);
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDPN_XufKy-QTSCB68xFJlqtUjHQ8m6uUY&libraries=places&callback=initMap">
    </script>
    <script>


        $('.driver_status').on('change.bootstrapSwitch', function(e) {
            var driver_id = $(this).attr('data-driver-id');
            var status = "";
            if (e.target.checked == true){
                var status = 1;
            }else{
                var status = 0;
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "{{URL::route('changeDriverStatus')}}",
                data: {
                    driver_id: driver_id,
                    status: status,
                    "_token": "{{ csrf_token() }}"
                },
                success: function(response){
                    toastr.success(response.success);
                },
                error: function(jqXHR){
                    toastr.error(jqXHR.responseJSON.message);
                }
            });
        });

        function confirm_delegate(id) {
            axios.get('confirmAccountDriver/' + id)
                .then(function (response) {
                    location.reload();
                })
                .catch(function (error) {
                    console.log(error);
                    alert(error);
                });
        };


    </script>

@endsection
