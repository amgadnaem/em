@extends('AdminPanel.backend.layouts.default')
@section('content')


    <div class="page-body">
        <div class="container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">
                        @if (auth()->user()->hasPermissionTo('اضافه_سلايدر'))

                            <div class="page-header-left">
                                <button class="btn btn-primary" type="button" data-toggle="modal"
                                        data-target="#AddSlider" data-whatever="@delegate">اضافه اعلان
                                </button>
                                @endif
                                <div class="modal fade" id="AddSlider" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">

                                                <h5 class="modal-title">اضافه اعلان</h5>
                                                <button class="close" type="button" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">×</span></button>
                                            </div>
                                            <div class="modal-body">
                                                <form class="needs-validation" novalidate=""
                                                      action="{{ route('createSlider') }}"
                                                      method="POST" enctype="multipart/form-data">
                                                    {{ method_field('POST') }}
                                                    {{ csrf_field() }}

                                                    <div class="form-group">
                                                        <label for="validationCustom05"
                                                               class="page-header-left"> اسم الدوله</label>
                                                        <select name="country_id"
                                                                class="form-control"
                                                                required>
                                                            <option value="">اختر</option>
                                                            @foreach($countries as $country)
                                                                <option
                                                                    value="{{ $country->id }}">{{ $country->country_name_ar }}</option>
                                                            @endforeach
                                                        </select>
                                                        <div
                                                            class="invalid-feedback">{{ $errors->first('country_id') }}</div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="validationCustom05"
                                                               class="page-header-left"> اسم متجر</label>
                                                        <select name="shop_id"
                                                                class="form-control"
                                                                required>
                                                            <option value="">اختر</option>
                                                            @foreach($shops as $shop)
                                                                <option
                                                                    value="{{ $shop->id }}">{{ $shop->name_ar }}</option>
                                                            @endforeach
                                                        </select>
                                                        <div
                                                            class="invalid-feedback">{{ $errors->first('shop_id') }}</div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="validationCustom05"
                                                               class="col-form-label page-header-left">صور</label>
                                                        <div class="custom-file">
                                                            <input class="custom-file-input" id="validatedCustomFile"
                                                                   type="file"
                                                                   name="slider_image" required>
                                                            <label class="custom-file-label"
                                                                   for="validatedCustomFile">اختر</label>
                                                            <div class="invalid-feedback">تحميل صوره</div>
                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <button class="btn btn-primary" type="submit">اضافه</button>
                                                        <button class="btn btn-secondary" type="button"
                                                                data-dismiss="modal">اغلاق
                                                        </button>
                                                    </div>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>

                </div>
            </div>


            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        @if( count($allSliders) > 0)
                            @if (auth()->user()->hasPermissionTo('عرض_سلايدر'))
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="display dataTable" id="basic-1">
                                            <thead>
                                            <tr>
                                                <th> صوره الاعلان</th>
                                                <th>الدوله التابع لها الاعلان</th>
                                                <th>المتجر التابع لها الاعلان</th>
                                                <th>الاكشن</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($allSliders as $slider)
                                                <tr>
                                                    <td><img src="{{ $slider->image }}" style="width: 50px"></td>
                                                    <td>{{$slider->country_name}}</td>
                                                    <td>{{$slider->shop_name}}</td>
                                                    <td>
                                                        @if (auth()->user()->hasPermissionTo('تعديل_سلايدر'))

                                                            <button class="btn btn-primary" type="button"
                                                                    data-toggle="modal"
                                                                    data-target="#{{ $slider->id }}"
                                                                    data-whatever="@slider"><i class="fa fa-edit"></i>
                                                            </button>
                                                        @endif
                                                        @if (auth()->user()->hasPermissionTo('حذف_سلايدر'))
                                                            <form action="{{ route('deleteSlider', $slider->id) }}"
                                                                  method="post" style="display: inline-block">
                                                                {{ csrf_field() }}
                                                                {{ method_field('delete') }}
                                                                <button type="submit"
                                                                        class="btn btn-danger delete btn-sm"><i
                                                                        class="fa fa-trash"></i>
                                                                </button>
                                                            </form>
                                                        @endif
                                                        <div class="modal fade" id="{{ $slider->id }}" tabindex="-1"
                                                             role="dialog"
                                                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title"> تعديل الصوره
                                                                        </h5>
                                                                        <button class="close" type="button"
                                                                                data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">×</span></button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <form class="needs-validation" novalidate=""
                                                                              action="{{route('EditSlider',['id'=>$slider->id])}}"
                                                                              method="POST"
                                                                              enctype="multipart/form-data">
                                                                            {{ method_field('POST') }}
                                                                            {{ csrf_field() }}

                                                                            <div class="form-group">
                                                                                <label for="validationCustom05"
                                                                                       class="page-header-left">صوره
                                                                                    السلايدر</label>
                                                                                <label for="file-input"
                                                                                       class="image-upload-label">
                                                                                    <img alt="upload-Slider-image"
                                                                                         name="slider_image"
                                                                                         src="{{$slider->image}}"
                                                                                         class="thumb"
                                                                                         style="width: 100px"/>
                                                                                </label>
                                                                                <div class="custom-file">
                                                                                    <input class="custom-file-input"
                                                                                           type="file"
                                                                                           name="slider_image">
                                                                                    <label class="custom-file-label"
                                                                                           for="validatedCustomFile">اختر
                                                                                        صوره</label>
                                                                                    <div
                                                                                        class="invalid-feedback">{{ $errors->first('slider_image') }}</div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button class="btn btn-primary"
                                                                                        type="submit">
                                                                                    تعديل
                                                                                </button>
                                                                                <button class="btn btn-secondary"
                                                                                        type="button"
                                                                                        data-dismiss="modal">اغلاق
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            @endif
                        @else
                            <h4 class="text-center"> لا يوجد اعلانات مضافه </h4>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection








