@extends('AdminPanel.backend.layouts.default')
@section('content')
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <div class="page-body">
        <div class="container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            @if (auth()->user()->hasPermissionTo('اضافه_منتجات'))

                                <button class="btn btn-primary" type="button" data-toggle="modal"
                                        data-target="#product" data-whatever="@product"> اضافه منتجات
                                </button>
                            @endif
                            <div class="modal fade" id="product" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">اضافه منتج </h5>
                                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <form class="needs-validation" novalidate=""
                                                  action="{{route('AddProducts')}}"
                                                  method="POST" enctype="multipart/form-data">
                                                {{ method_field('POST') }}
                                                {{ csrf_field() }}

                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="page-header-left">الاقسام الرئيسيه</label>
                                                    <select name="category_id"
                                                            class="form-control"
                                                            required>
                                                        <option value=""> اختر</option>
                                                        @foreach($all_categories as $category)
                                                            <option
                                                                    value="{{ $category->id }}">{{ $category->category_name_ar }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div
                                                            class="invalid-feedback">{{ $errors->first('category_name') }}</div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="page-header-left">الاقسام الفرعيه</label>
                                                    <select name="subcategory_id"
                                                            class="form-control"
                                                            required>
                                                        <option value=""> اختر</option>
                                                        @foreach($all_SubCategories as $subCategory)
                                                            <option
                                                                    value="{{ $subCategory->id }}">{{ $subCategory->category_name_ar }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div
                                                            class="invalid-feedback">{{ $errors->first('subcategory_id') }}</div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="page-header-left"> البراندات</label>
                                                    <select name="brand_id"
                                                            class="form-control"
                                                            required>
                                                        <option value=""> اختر</option>
                                                        @foreach($all_brands as $brands)
                                                            <option
                                                                    value="{{ $brands->id }}">{{ $brands->brand_name_ar }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div
                                                            class="invalid-feedback">{{ $errors->first('brand_id') }}</div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-form-label page-header-left"> اسم المنتج
                                                        بالعربيه</label>
                                                    <input class="form-control" name="product_name_ar" required
                                                           type="text"
                                                           placeholder="ادخل اسم المنتج باللغه العربيه">
                                                    <div
                                                            class="invalid-feedback">{{ $errors->first('product_name_ar') }}</div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-form-label page-header-left">اسم المنتج
                                                        بالانجليزيه</label>
                                                    <input class="form-control" name="product_name_en" required
                                                           type="text"
                                                           placeholder="ادخل اسم المنتج باللغه الانجليزيه">
                                                    <div
                                                            class="invalid-feedback">{{ $errors->first('product_name_en') }}</div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-form-label page-header-left">هل المنتج لديه
                                                        مقاسات</label>
                                                    <div
                                                            class="form-group page-header-left m-checkbox-inline mb-0 custom-radio-ml">
                                                        <div class="radio radio-primary">
                                                            <input id="check1" type="radio" name="is_size"
                                                                   value="1">
                                                            <label class="mb-0" for="check1">نعم</label>
                                                        </div>
                                                        <div class="radio radio-primary">
                                                            <input id="check2" type="radio" name="is_size"
                                                                   value="0" checked>
                                                            <label class="mb-0" for="check2">لا</label>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-form-label page-header-left">سعر المنتج
                                                        قبل</label>
                                                    <input class="form-control" name="price_before"
                                                           type="text"
                                                           placeholder="ادخل سعر المنتج قبل">
                                                    <div
                                                            class="invalid-feedback">{{ $errors->first('price_before') }}</div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-form-label page-header-left">سعر المنتج
                                                        بعد</label>
                                                    <input class="form-control" name="price_after"
                                                           type="text"
                                                           placeholder="ادخل سعر المنتج بعد">
                                                    <div
                                                            class="invalid-feedback">{{ $errors->first('price_after') }}</div>
                                                </div>


                                                <div class="form-group">
                                                    <label class="col-form-label page-header-left"> كميه مخزون
                                                        المنتج</label>
                                                    <input class="form-control" name="product_stock"
                                                           type="text"
                                                           placeholder="ادخل  كميه محزون المنتج">
                                                    <div
                                                            class="invalid-feedback">{{ $errors->first('product_stock') }}</div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="validationCustom04" class="page-header-left">وصف
                                                        للمنتج بالعربيه</label>
                                                    <textarea class="form-control" name="desc_ar"
                                                              required></textarea>
                                                    <div
                                                            class="invalid-feedback">{{ $errors->first('desc_ar') }}

                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="validationCustom04" class="page-header-left">وصف
                                                        للمنتج بالانجليزيه</label>
                                                    <textarea class="form-control" name="desc_en"
                                                              required></textarea>
                                                    <div
                                                            class="invalid-feedback">{{ $errors->first('desc_en') }}

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="validationCustom05" class="page-header-left">صوره
                                                        اساسيه المنتج</label>
                                                    <div class="custom-file">
                                                        <input class="custom-file-input" type="file"
                                                               name="product_image">
                                                        <label class="custom-file-label" for="validatedCustomFile">اختر
                                                            صوره</label>
                                                        <div
                                                                class="invalid-feedback">{{ $errors->first('product_image') }}</div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="validationCustom05" class="page-header-left">صور
                                                        المنتج</label>
                                                    <div class="custom-file">
                                                        <input class="custom-file-input" id="product_image" type="file"
                                                               name="product_images[]" multiple="multiple">
                                                        <label class="custom-file-label" for="validatedCustomFile">اختر
                                                            صوره</label>
                                                        <div
                                                                class="invalid-feedback">{{ $errors->first('product_image') }}</div>
                                                    </div>
                                                </div>

                                                <div class="modal-footer">
                                                    <button class="btn btn-primary" type="submit">اضافه</button>
                                                    <button class="btn btn-secondary" type="button"
                                                            data-dismiss="modal">اغلاق
                                                    </button>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    @if( count($all_products) > 0)
                        @if (auth()->user()->hasPermissionTo('عرض_منتجات'))

                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="display dataTable" id="basic-1">
                                        <thead>
                                        <tr>
                                            <th>صوره المنتج</th>
                                            <th>رقم المنتج</th>
                                            <th>اسم المنتج</th>
                                            <th>تقييم المنتج</th>
                                            <th>السعر قبل</th>
                                            <th>السعر بعد</th>
                                            <th>مخزن</th>
                                            <th>القسم الاساسى</th>
                                            <th>حاله المنتج</th>
                                            <th>الاكشن</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($all_products as $product)
                                            <tr>
                                                <td><img src=" {{ $product->product_image }}" style="width: 50px"
                                                         alt=""></td>
                                                <td>{{ $product->id }}</td>
                                                <td>{{ $product->product_name_ar }}</td>
                                                <td>{{ $product->getRateValue($product->id) }}</td>
                                                <td>{{ $product->price_before }}</td>
                                                <td>{{ $product->price_after }}</td>
                                                <td>{{ $product->product_stock }}</td>
                                                <td>{{ $product->getCategory($product->category_id) }}</td>
                                                <td>
                                                    <div class="media-body ">
                                                        <label class="switch">
                                                            <input type="checkbox"
                                                                   {{ $product->status == 1 ? 'checked' : '' }}
                                                                   onchange="update_status_product({{ $product->id }},{{ $product->status }})"><span
                                                                    class="switch-state"></span>
                                                        </label>
                                                    </div>
                                                </td>
                                                <td>
                                                    @if($product->is_size == 1)
                                                        <a class="btn btn-primary btn-group-sm"
                                                           href="{{ route('sizes',['id'=> $product->id]) }}"
                                                           data-whatever="@test"><i class="fa fa-plus"></i>
                                                        </a>
                                                    @endif
                                                    @if (auth()->user()->hasPermissionTo('تعديل_منتجات'))

                                                        <button class="btn btn-primary" type="button"
                                                                data-toggle="modal"
                                                                data-target="#{{ $product->id }}"
                                                                data-whatever="@test"><i class="fa fa-edit"></i>
                                                        </button>
                                                    @endif
                                                    @if (auth()->user()->hasPermissionTo('حذف_منتجات'))
                                                        <form action="{{ route('deleteProduct', $product->id) }}"
                                                              method="post" style="display: inline-block">
                                                            {{ csrf_field() }}
                                                            {{ method_field('delete') }}
                                                            <button type="submit" class="btn btn-danger delete btn-sm">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </form>
                                                    @endif

                                                    <div class="modal fade" id="{{ $product->id }}" tabindex="-1"
                                                         role="dialog"
                                                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title"> تعديل منتج
                                                                        {{ $product->product_name_ar }} </h5>

                                                                    <button class="close" type="button"
                                                                            data-dismiss="modal"
                                                                            aria-label="Close">
                                                                        <span aria-hidden="true">×</span></button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <form class="needs-validation" novalidate=""
                                                                          action="{{route('EditProducts',['id'=>$product->id])}}"
                                                                          method="POST" enctype="multipart/form-data">
                                                                        {{ method_field('POST') }}
                                                                        {{ csrf_field() }}

                                                                        <div
                                                                        <div class="form-group">
                                                                            <label for="validationCustom05"
                                                                                   class="page-header-left">الاقسام
                                                                                الرئيسيه</label>
                                                                            <select name="category_id"
                                                                                    class="form-control"
                                                                                    required>

                                                                                @foreach($all_categories as $category)
                                                                                    @if($category->id == $product->category_id)
                                                                                        <option
                                                                                                value="{{ $category->id }}"
                                                                                                selected>{{ $category->category_name_ar }}</option>
                                                                                    @else
                                                                                        <option
                                                                                                value="{{ $category->id }}">{{ $category->category_name_ar }}</option>
                                                                                    @endif
                                                                                @endforeach
                                                                            </select>
                                                                            <div
                                                                                    class="invalid-feedback">{{ $errors->first('category_name') }}</div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label for="validationCustom05"
                                                                                   class="page-header-left">الاقسام
                                                                                الفرعيه</label>
                                                                            <select name="subcategory_id"
                                                                                    class="form-control"
                                                                                    required>

                                                                                @foreach($all_SubCategories as $subCategory)
                                                                                    @if( $subCategory->id == $product->subcategory_id)
                                                                                        <option
                                                                                                value="{{ $subCategory->id }}"
                                                                                                selected>{{ $subCategory->category_name_ar }}</option>
                                                                                    @else
                                                                                        <option
                                                                                                value="{{ $subCategory->id }}">{{ $subCategory->category_name_ar }}</option>
                                                                                    @endif
                                                                                @endforeach
                                                                            </select>
                                                                            <div
                                                                                    class="invalid-feedback">{{ $errors->first('subcategory_id') }}</div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label for="validationCustom05"
                                                                                   class="page-header-left">
                                                                                البراندات</label>
                                                                            <select name="brand_id"
                                                                                    class="form-control"
                                                                                    required>
                                                                                @foreach($all_brands as $brands)
                                                                                    @if($brands->id == $product->brand_id)
                                                                                        <option
                                                                                                value="{{ $brands->id }}"
                                                                                                selected>{{ $brands->brand_name_ar }}</option>
                                                                                    @else
                                                                                        <option
                                                                                                value="{{ $brands->id }}">{{ $brands->brand_name_ar }}</option>
                                                                                    @endif
                                                                                @endforeach
                                                                            </select>
                                                                            <div
                                                                                    class="invalid-feedback">{{ $errors->first('brand_id') }}</div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label
                                                                                    class="col-form-label page-header-left">
                                                                                اسم المنتج
                                                                                بالعربيه</label>
                                                                            <input class="form-control"
                                                                                   name="product_name_ar" required
                                                                                   type="text"
                                                                                   value="{{$product->product_name_ar}}"
                                                                                   placeholder="ادخل اسم المنتج باللغه العربيه">
                                                                            <div
                                                                                    class="invalid-feedback">{{ $errors->first('product_name_ar') }}</div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label
                                                                                    class="col-form-label page-header-left">اسم
                                                                                المنتج
                                                                                بالانجليزيه</label>
                                                                            <input class="form-control"
                                                                                   name="product_name_en" required
                                                                                   type="text"
                                                                                   value="{{$product->product_name_en}}"
                                                                                   placeholder="ادخل اسم المنتج باللغه الانجليزيه">
                                                                            <div
                                                                                    class="invalid-feedback">{{ $errors->first('product_name_en') }}</div>
                                                                        </div>
                                                                        @if($product->is_size == 0)
                                                                            <div class="form-group">
                                                                                <label
                                                                                        class="col-form-label page-header-left">سعر
                                                                                    المنتج
                                                                                    قبل</label>
                                                                                <input class="form-control"
                                                                                       name="price_before"
                                                                                       type="text"
                                                                                       value="{{ $product->price_before }}"
                                                                                       placeholder="ادخل سعر المنتج قبل">
                                                                                <div
                                                                                        class="invalid-feedback">{{ $errors->first('price_before') }}</div>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label
                                                                                        class="col-form-label page-header-left">سعر
                                                                                    المنتج
                                                                                    بعد</label>
                                                                                <input class="form-control"
                                                                                       name="price_after"
                                                                                       type="text"
                                                                                       value="{{ $product->price_after }}"
                                                                                       placeholder="ادخل سعر المنتج بعد">
                                                                                <div
                                                                                        class="invalid-feedback">{{ $errors->first('price_after') }}</div>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label
                                                                                        class="col-form-label page-header-left">
                                                                                    كميه مخزون
                                                                                    المنتج</label>
                                                                                <input class="form-control"
                                                                                       name="product_stock"
                                                                                       type="text"
                                                                                       value="{{ $product->product_stock }}">
                                                                                <div
                                                                                        class="invalid-feedback">{{ $errors->first('product_stock') }}</div>
                                                                            </div>
                                                                        @endif

                                                                        <div class="form-group">
                                                                            <label for="validationCustom04"
                                                                                   class="page-header-left">وصف
                                                                                للمنتج بالعربيه</label>
                                                                            <textarea class="form-control"
                                                                                      name="desc_ar"
                                                                                      required>{{ $product->desc_ar }}</textarea>
                                                                            <div
                                                                                    class="invalid-feedback">{{ $errors->first('desc_ar') }}

                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label for="validationCustom04"
                                                                                   class="page-header-left">وصف
                                                                                للمنتج بالانجليزيه</label>
                                                                            <textarea class="form-control"
                                                                                      name="desc_en"
                                                                                      required>{{ $product->desc_en }}</textarea>
                                                                            <div
                                                                                    class="invalid-feedback">{{ $errors->first('desc_en') }}

                                                                            </div>
                                                                        </div>


                                                                        <div class="form-group">
                                                                            <label for="validationCustom05"
                                                                                   class="page-header-left">صوره
                                                                                الاساسيه
                                                                                للمنتج</label>
                                                                            <label for="file-input"
                                                                                   class="image-upload-label">
                                                                                <img alt="upload-service-image"
                                                                                     name="product_image"
                                                                                     src="{{$product->product_image}}"
                                                                                     class="thumb"
                                                                                     style="width: 100px"/>
                                                                            </label>
                                                                            <div class="custom-file">
                                                                                <input class="custom-file-input"
                                                                                       id="product_image" type="file"
                                                                                       name="product_image">
                                                                                <label class="custom-file-label"
                                                                                       for="validatedCustomFile">اختر
                                                                                    صوره</label>
                                                                                <div
                                                                                        class="invalid-feedback">{{ $errors->first('product_image') }}</div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label for="validationCustom05"
                                                                                   class="page-header-left">صور الفرعيه
                                                                                للمنتج</label>
                                                                            <label for="file-input"
                                                                                   class="image-upload-label">
                                                                                @foreach($product->getMultiImage($product->id) as $images)
                                                                                    <img alt="upload-service-image"
                                                                                         name="product_images[]"
                                                                                         multiple="multiple"
                                                                                         src="{{$images}}"
                                                                                         class="thumb"
                                                                                         style="width: 100px"/>
                                                                                @endforeach
                                                                            </label>
                                                                            <div class="custom-file">
                                                                                <input class="custom-file-input"
                                                                                       id="product_image" type="file"
                                                                                       name="product_images[]"
                                                                                       multiple="multiple">
                                                                                <label class="custom-file-label"
                                                                                       for="validatedCustomFile">اختر
                                                                                    صوره</label>
                                                                                <div
                                                                                        class="invalid-feedback">{{ $errors->first('product_images') }}</div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="modal-footer">
                                                                            <button class="btn btn-primary"
                                                                                    type="submit">
                                                                                تعديل
                                                                            </button>
                                                                            <button class="btn btn-secondary"
                                                                                    type="button"
                                                                                    data-dismiss="modal">اغلاق
                                                                            </button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @endif
                    @else
                        <h4 class="text-center"> لا توجد منتجات مضافه </h4>
                    @endif

                </div>
            </div>
        </div>

    </div>
    <script>

        function update_status_product(id, value) {
            if (value == 0) {
                value = 1;
            } else {
                value = 0;
            }
            axios.get('updateProduct/' + id + '/' + value)
                .then(function (response) {
                    // alert(response.data.category_status);
                    location.reload();
                })
                .catch(function (error) {
                    console.log(error);
                    alert(error);
                });
        };


    </script>
@endsection

















