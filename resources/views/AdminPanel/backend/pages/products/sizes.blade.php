@extends('AdminPanel.backend.layouts.default')
@section('content')

    <!-- Right sidebar Ends-->
    <div class="page-body">
        <div class="container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">

                        <div class="page-header-left">
                            <button class="btn btn-primary" type="button" data-toggle="modal"
                                    data-target="#sizes" data-whatever="@sizes">اضافه مقاس للمنتج
                            </button>
                            <div class="modal fade" id="sizes" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">اضافه مقاس للمنتج</h5>
                                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <form class="needs-validation" novalidate=""
                                                  action="{{route('AddSizes',['id'=>$product->id])}}"
                                                  method="POST" enctype="multipart/form-data">
                                                {{ method_field('POST') }}
                                                {{ csrf_field() }}
     <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="page-header-left">المقاسات</label>
                                                    <select name="size_id"
                                                            class="form-control"
                                                            required>
                                                        <option value="">اختر</option>
                                                        @foreach($all_sizes as $sizes)
                                                            <option
                                                                value="{{ $sizes->id }}">{{ $sizes->size }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('sizes') }}</div>
                                                </div>

                                                <div class="form-group">
                                                    <label
                                                        class="col-form-label page-header-left">سعر
                                                        المنتج
                                                        قبل</label>
                                                    <input class="form-control"
                                                           name="price_before"
                                                           type="text"
                                                           placeholder="ادخل سعر المنتج قبل"
                                                           required>
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('price_before') }}</div>
                                                </div>


                                                <div class="form-group">
                                                    <label
                                                        class="col-form-label page-header-left">سعر
                                                        المنتج
                                                        بعد</label>
                                                    <input class="form-control"
                                                           name="price_after"
                                                           type="text"
                                                           placeholder="ادخل سعر المنتج بعد"
                                                           required>
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('price_after') }}</div>
                                                </div>

                                                <div class="form-group">
                                                    <label
                                                        class="col-form-label page-header-left">
                                                        كميه مخزون
                                                        المنتج</label>
                                                    <input class="form-control"
                                                           name="product_stock"
                                                           type="text" required>
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('product_stock') }}</div>
                                                </div>


                                                <div class="modal-footer">
                                                    <button class="btn btn-primary" type="submit">
                                                        اضافه
                                                    </button>
                                                    <button class="btn btn-secondary" type="button"
                                                            data-dismiss="modal">اغلاق
                                                    </button>
                                                </div>
                                            </form>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    @if( count($product_sizes) > 0)
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display dataTable" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>اسم المنتج</th>
                                        <th>مقاس المنتج</th>
                                        <th>السعر قبل</th>
                                        <th>السعر بعد</th>
                                        <th>مخزون</th>
                                        <th>الاكشن</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($product_sizes as $sizes)
                                        <tr>
                                            <td>{{ $sizes->product->product_name_ar }}</td>
                                            <td>{{ $sizes->size->size }}</td>
                                            <td>{{ $sizes->price_before }}</td>
                                            <td>{{ $sizes->price_after }}</td>
                                            <td>{{ $sizes->stock }}</td>
                                            <td>
                                                <button class="btn btn-primary" type="button" data-toggle="modal"
                                                        data-target="#{{ $sizes->id }}"
                                                        data-whatever="@category"><i class="fa fa-edit"></i>
                                                </button>

                                                <form action="{{ route('destroySize', $sizes->id) }}"
                                                      method="post" style="display: inline-block">
                                                    {{ csrf_field() }}
                                                    {{ method_field('delete') }}
                                                    <button type="submit" class="btn btn-danger delete btn-sm"><i
                                                            class="fa fa-trash"></i>
                                                    </button>
                                                </form>

                                                <div class="modal fade" id="{{ $sizes->id }}" tabindex="-1"
                                                     role="dialog"
                                                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title"> تعديل مقاس
                                                                    {{ $sizes->product->product_name_ar }} </h5>

                                                                <button class="close" type="button" data-dismiss="modal"
                                                                        aria-label="Close">
                                                                    <span aria-hidden="true">×</span></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form class="needs-validation" novalidate=""
                                                                      action="{{route('updateSizes',['id'=>$sizes->id])}}"
                                                                      method="POST" enctype="multipart/form-data">
                                                                    {{ method_field('POST') }}
                                                                    {{ csrf_field() }}
                                                                    <div class="form-group">
                                                                        <label for="category_id"
                                                                               class="page-header-left">المقاسات</label>

                                                                        <select
                                                                            name="size_id"
                                                                            class="form-control"
                                                                            required>
                                                                            <option value="{{ $sizes->size_id }}"
                                                                                    selected> {{ $sizes->size->size }}</option>
                                                                            <option value="1">50</option>
                                                                            <option value="2">100</option>
                                                                            <option value="3">150</option>
                                                                            <option value="4">200</option>
                                                                        </select>
                                                                        <div
                                                                            class="invalid-feedback">{{ $errors->first('sizes') }}</div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label
                                                                            class="col-form-label page-header-left">سعر
                                                                            المنتج
                                                                            قبل</label>
                                                                        <input class="form-control"
                                                                               name="price_before"
                                                                               type="text"
                                                                               value="{{ $sizes->price_before }}"
                                                                               placeholder="ادخل سعر المنتج قبل"
                                                                               required>
                                                                        <div
                                                                            class="invalid-feedback">{{ $errors->first('price_before') }}</div>
                                                                    </div>


                                                                    <div class="form-group">
                                                                        <label
                                                                            class="col-form-label page-header-left">سعر
                                                                            المنتج
                                                                            بعد</label>
                                                                        <input class="form-control"
                                                                               name="price_after"
                                                                               type="text"
                                                                               value="{{ $sizes->price_after }}"
                                                                               placeholder="ادخل سعر المنتج بعد"
                                                                               required>
                                                                        <div
                                                                            class="invalid-feedback">{{ $errors->first('price_after') }}</div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label
                                                                            class="col-form-label page-header-left">
                                                                            كميه مخزون
                                                                            المنتج</label>
                                                                        <input class="form-control"
                                                                               name="stock"
                                                                               value="{{ $sizes->stock }}"
                                                                               type="text" required>
                                                                        <div
                                                                            class="invalid-feedback">{{ $errors->first('product_stock') }}</div>
                                                                    </div>

                                                                    <div class="modal-footer">
                                                                        <button class="btn btn-primary" type="submit">
                                                                            تعديل
                                                                        </button>
                                                                        <button class="btn btn-secondary" type="button"
                                                                                data-dismiss="modal">اغلاق
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @else
                        <h4 class="text-center"> لا توجد مقاسات مضافه </h4>
                    @endif

                </div>
            </div>
        </div>
    </div>



    <script>

        function change_status_category(id, value) {
            if (value == 0) {
                value = 1;
            } else {
                value = 0;
            }
            axios.get('updateStatusCategory/' + id + '/' + value)
                .then(function (response) {
                    // alert(response.data.category_status);
                    location.reload();
                })
                .catch(function (error) {
                    console.log(error);
                    alert(error);
                });
        };


    </script>

@endsection








