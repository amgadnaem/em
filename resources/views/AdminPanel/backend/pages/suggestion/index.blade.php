@extends('AdminPanel.backend.layouts.default')
@section('content')

    <!-- Right sidebar Ends-->
    <div class="page-body">

        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4>عرض الشكاوى والمقترحات</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="display" id="basic-1">
                            <thead>
                            <tr>
                                <th>تطبيق صاحب الشكوى</th>
                                <th> رقم الموبايل</th>
                                <th>اسم صاحب الشكوى</th>
                                <th>عنوان الشكوى</th>
                                <th>الشكوى او الاقتراح</th>
                                <th> حذف الشكوى</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($suggestions as $suggestion)
                                <tr>
                                    @if($suggestion->type == 1)
                                        <td>مستخدم</td>
                                            <td>{{ !empty($suggestion->user->phone_number) ? $suggestion->user->phone_number : null }}</td>
                                        <td>{{ !empty($suggestion->user->first_name) ? $suggestion->user->first_name : 'بدون اسم'  }}</td>

                                    @elseif($suggestion->type == 3)
                                        <td>متجر</td>
                                  
                                          <td>{{ !empty($suggestion->shop->phone_number) ? $suggestion->shop->phone_number : null }}</td>
                                          
                                        <td>{{ !empty($suggestion->shop->name_app_ar) ? $suggestion->shop->name_app_ar : 'بدون اسم'  }}</td>
                                    @else
                                        <td>مندوب او سائق</td>
                                        <td>{{ !empty($suggestion->user->phone_number) ? $suggestion->user->phone_number : null }}</td>
                                        <td>{{ !empty($suggestion->user->first_name) ? $suggestion->user->first_name : 'بدون اسم'  }}</td>
                                    @endif
                                    <td>{{ $suggestion->title }}</td>
                                    <td>{{ $suggestion->comment }}</td>
                                    <td>
                                        <form action="{{ route('SuggestionDestroy', $suggestion->id) }}"
                                              method="post" style="display: inline-block">
                                            {{ csrf_field() }}
                                            {{ method_field('delete') }}
                                            <button type="submit"
                                                    class="btn btn-danger delete btn-sm"><i
                                                    class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>

                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

















