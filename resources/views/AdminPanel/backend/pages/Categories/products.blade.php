@extends('AdminPanel.backend.layouts.default')
@section('content')
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4>عرض المنتجات </h4>
                    </div>
                    @if( count($products) > 0)
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display dataTable" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>صوره المنتج</th>
                                        <th>رقم المنتج</th>
                                        <th>اسم المنتج</th>
                                        <th>تقييم المنتج</th>
                                        <th>السعر قبل</th>
                                        <th>السعر بعد</th>
                                        <th>مخزن</th>
                                        <th>القسم الاساسى</th>
                                        <th>حاله المنتج</th>
                                        <th>الاكشن</th>
                                    </tr>
                                    <tbody>
                                    @foreach($products as $product)
                                        <tr>
                                            <td><img src=" {{ $product->product_image }}" style="width: 50px" alt="">
                                            </td>
                                            <td>{{ $product->id }}</td>
                                            <td>{{ $product->product_name_en }}</td>
                                            <td>{{ $product->getRateValue($product->id) }}</td>
                                            <td>{{ $product->price_before }}</td>
                                            <td>{{ $product->price_after }}</td>
                                            <td>{{ $product->product_stock }}</td>
                                            <td>{{ $product->getCategory($product->category_id) }}</td>
                                            <td>
                                                <div class="media-body ">
                                                    <label class="switch">
                                                        <input type="checkbox"
                                                               {{ $product->status == 1 ? 'checked' : '' }}
                                                               onchange="update_status_product({{ $product->id }},{{ $product->status }})"><span
                                                            class="switch-state"></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                @if($product->is_size == 1)
                                                    <a class="btn btn-primary btn-group-sm"
                                                       href="{{ route('sizes',['id'=> $product->id]) }}"
                                                       data-whatever="@test"><i class="fa fa-plus"></i>
                                                    </a>
                                                @endif

                                                <button class="btn btn-primary" type="button" data-toggle="modal"
                                                        data-target="#{{ $product->id }}"
                                                        data-whatever="@test"><i class="fa fa-edit"></i>
                                                </button>

                                                <form action="{{ route('deleteProduct', $product->id) }}"
                                                      method="post" style="display: inline-block">
                                                    {{ csrf_field() }}
                                                    {{ method_field('delete') }}
                                                    <button type="submit" class="btn btn-danger delete btn-sm"><i
                                                            class="fa fa-trash"></i>
                                                    </button>
                                                </form>

                                                <div class="modal fade" id="{{ $product->id }}" tabindex="-1"
                                                     role="dialog"
                                                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title"> تعديل منتج
                                                                    {{ $product->product_name_ar }} </h5>

                                                                <button class="close" type="button" data-dismiss="modal"
                                                                        aria-label="Close">
                                                                    <span aria-hidden="true">×</span></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form class="needs-validation" novalidate=""
                                                                      action="{{route('EditProducts',['id'=>$product->id])}}"
                                                                      method="POST" enctype="multipart/form-data">
                                                                    {{ method_field('POST') }}
                                                                    {{ csrf_field() }}

                                                                    <div class="form-group">
                                                                        <label for="validationCustom05"
                                                                               class="page-header-left">الاقسام
                                                                            الرئيسيه</label>
                                                                        <select name="category_id" id="category_id"
                                                                                class="form-control"
                                                                                required>
                                                                            <option
                                                                                value="{{ $product->category_id }}"> {{ $product->getCategory($product->category_id) }}</option>
                                                                        </select>
                                                                        <div
                                                                            class="invalid-feedback">{{ $errors->first('category_name') }}</div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label for="validationCustom05"
                                                                               class="page-header-left">الاقسام
                                                                            الفرعيه</label>
                                                                        <select name="subcategory_id"
                                                                                class="form-control"
                                                                                required>
                                                                            <option
                                                                                value="{{ $product->id }}"> {{ $product->getSubCategory($product->subcategory_id) }}</option>
                                                                        </select>
                                                                        <div
                                                                            class="invalid-feedback">{{ $errors->first('subcategory_id') }}</div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label for="validationCustom05"
                                                                               class="page-header-left">
                                                                            البراندات</label>
                                                                        <select name="brand_id"
                                                                                class="form-control"
                                                                                required>
                                                                            <option
                                                                                value="{{ $product->id }}"> {{ $product->getBrand($product->brand_id) }}</option>
                                                                        </select>
                                                                        <div
                                                                            class="invalid-feedback">{{ $errors->first('brand_id') }}</div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-form-label page-header-left">
                                                                            اسم المنتج
                                                                            بالعربيه</label>
                                                                        <input class="form-control"
                                                                               name="product_name_ar" required
                                                                               type="text"
                                                                               value="{{$product->product_name_ar}}"
                                                                               placeholder="ادخل اسم المنتج باللغه العربيه">
                                                                        <div
                                                                            class="invalid-feedback">{{ $errors->first('product_name_ar') }}</div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-form-label page-header-left">اسم
                                                                            المنتج
                                                                            بالانجليزيه</label>
                                                                        <input class="form-control"
                                                                               name="product_name_en" required
                                                                               type="text"
                                                                               value="{{$product->product_name_en}}"
                                                                               placeholder="ادخل اسم المنتج باللغه الانجليزيه">
                                                                        <div
                                                                            class="invalid-feedback">{{ $errors->first('product_name_en') }}</div>
                                                                    </div>
                                                                    @if($product->is_size == 0)
                                                                        <div class="form-group">
                                                                            <label
                                                                                class="col-form-label page-header-left">سعر
                                                                                المنتج
                                                                                قبل</label>
                                                                            <input class="form-control"
                                                                                   name="price_before"
                                                                                   type="text"
                                                                                   value="{{ $product->price_before }}"
                                                                                   placeholder="ادخل سعر المنتج قبل">
                                                                            <div
                                                                                class="invalid-feedback">{{ $errors->first('price_before') }}</div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label
                                                                                class="col-form-label page-header-left">سعر
                                                                                المنتج
                                                                                بعد</label>
                                                                            <input class="form-control"
                                                                                   name="price_after"
                                                                                   type="text"
                                                                                   value="{{ $product->price_after }}"
                                                                                   placeholder="ادخل سعر المنتج بعد">
                                                                            <div
                                                                                class="invalid-feedback">{{ $errors->first('price_after') }}</div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label
                                                                                class="col-form-label page-header-left">
                                                                                كميه مخزون
                                                                                المنتج</label>
                                                                            <input class="form-control"
                                                                                   name="product_stock"
                                                                                   type="text"
                                                                                   value="{{ $product->product_stock }}">
                                                                            <div
                                                                                class="invalid-feedback">{{ $errors->first('product_stock') }}</div>
                                                                        </div>
                                                                    @endif

                                                                    <div class="form-group">
                                                                        <label for="validationCustom04"
                                                                               class="page-header-left">وصف
                                                                            للمنتج بالعربيه</label>
                                                                        <textarea class="form-control" name="desc_ar"
                                                                                  required>{{ $product->desc_ar }}</textarea>
                                                                        <div
                                                                            class="invalid-feedback">{{ $errors->first('desc_ar') }}

                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label for="validationCustom04"
                                                                               class="page-header-left">وصف
                                                                            للمنتج بالانجليزيه</label>
                                                                        <textarea class="form-control" name="desc_en"
                                                                                  required>{{ $product->desc_en }}</textarea>
                                                                        <div
                                                                            class="invalid-feedback">{{ $errors->first('desc_en') }}

                                                                        </div>
                                                                    </div>


                                                                    <div class="form-group">
                                                                        <label for="validationCustom05"
                                                                               class="page-header-left">صوره الاساسيه
                                                                            للمنتج</label>
                                                                        <label for="file-input"
                                                                               class="image-upload-label">
                                                                            <img alt="upload-service-image"
                                                                                 name="product_image"
                                                                                 src="{{$product->product_image}}"
                                                                                 class="thumb"
                                                                                 style="width: 100px"/>
                                                                        </label>
                                                                        <div class="custom-file">
                                                                            <input class="custom-file-input"
                                                                                   id="product_image" type="file"
                                                                                   name="product_image">
                                                                            <label class="custom-file-label"
                                                                                   for="validatedCustomFile">اختر
                                                                                صوره</label>
                                                                            <div
                                                                                class="invalid-feedback">{{ $errors->first('product_image') }}</div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label for="validationCustom05"
                                                                               class="page-header-left">صور الفرعيه
                                                                            للمنتج</label>
                                                                        <label for="file-input"
                                                                               class="image-upload-label">
                                                                            @foreach($product->getMultiImage($product->id) as $images)
                                                                                <img alt="upload-service-image"
                                                                                     name="product_images[]"
                                                                                     multiple="multiple"
                                                                                     src="{{$images}}"
                                                                                     class="thumb"
                                                                                     style="width: 100px"/>
                                                                            @endforeach
                                                                        </label>
                                                                        <div class="custom-file">
                                                                            <input class="custom-file-input"
                                                                                   id="product_image" type="file"
                                                                                   name="product_images[]"
                                                                                   multiple="multiple">
                                                                            <label class="custom-file-label"
                                                                                   for="validatedCustomFile">اختر
                                                                                صوره</label>
                                                                            <div
                                                                                class="invalid-feedback">{{ $errors->first('product_images') }}</div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="modal-footer">
                                                                        <button class="btn btn-primary" type="submit">
                                                                            تعديل
                                                                        </button>
                                                                        <button class="btn btn-secondary" type="button"
                                                                                data-dismiss="modal">اغلاق
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @else
                        <h4 class="text-center"> لا توجد منتجات مضافه </h4>
                    @endif

                </div>
            </div>
        </div>

    </div>
    <script>

        function update_status_product(id, value) {
            if (value == 0) {
                value = 1;
            } else {
                value = 0;
            }
            axios.get('updateProduct/' + id + '/' + value)
                .then(function (response) {
                    // alert(response.data.category_status);
                    location.reload();
                })
                .catch(function (error) {
                    console.log(error);
                    alert(error);
                });
        };


    </script>
@endsection

















