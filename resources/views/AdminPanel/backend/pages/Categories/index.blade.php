@extends('AdminPanel.backend.layouts.default')
@section('content')

    <!-- Right sidebar Ends-->
    <div class="page-body">
        <div class="container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">

                        <div class="page-header-left">
                            @if (auth()->user()->hasPermissionTo('اضافه_اقسام'))

                                <button class="btn btn-primary" type="button" data-toggle="modal"
                                        data-target="#category" data-whatever="@category">اضافه قسم رئيسى
                                </button>
                            @endif
                            <div class="modal fade" id="category" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">اضافه قسم رئيسى</h5>
                                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <form class="needs-validation" novalidate=""
                                                  action="{{route('AddCategory')}}"
                                                  method="POST" enctype="multipart/form-data">
                                                {{ method_field('POST') }}
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">اسم القسم
                                                        بالانجليزيه</label>
                                                    <input class="form-control" name="category_name_en"
                                                           id="category_name_en"
                                                           type="text"
                                                           placeholder="ادخل اسم القسم بالانجليزيه"
                                                           required="">
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('category_name_en') }}</div>
                                                </div>
                                                <span class="text-danger page-header-left"
                                                      style="color: red;">{{$errors->first('category_name_en')}}</span>
                                                <div class="form-group">

                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">اسم القسم
                                                        بالعربى</label>
                                                    <input class="form-control" name="category_name_ar"
                                                           id="category_name_ar"
                                                           type="text"
                                                           placeholder="ادخل اسم القسم بالعربيه"
                                                           required="">
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('category_name_ar') }}</div>
                                                </div>
                                                <span class="text-danger page-header-left"
                                                      style="color: red;">{{$errors->first('category_name_ar')}}</span>

                                                <div class="form-group">
                                                    <label for="validationCustom05" class="page-header-left">صوره
                                                        القسم</label>
                                                    <div class="custom-file">
                                                        <input class="custom-file-input" type="file"
                                                               name="category_image">
                                                        <label class="custom-file-label" for="validatedCustomFile">اختر
                                                            صوره</label>
                                                        <div
                                                            class="invalid-feedback">{{ $errors->first('category_image') }}</div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn btn-primary" type="submit">اضافه</button>
                                                    <button class="btn btn-secondary" type="button"
                                                            data-dismiss="modal">اغلاق
                                                    </button>
                                                </div>
                                            </form>

                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    @if( count($categories) > 0)
                        @if (auth()->user()->hasPermissionTo('عرض_اقسام'))

                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="display dataTable" id="basic-1">
                                        <thead>
                                        <tr>
                                            <th>صوره القسم</th>
                                            <th>اسم القسم بالانجليزيه</th>
                                            <th>اسم القسم بالعربيه</th>
                                            <th>عدد المنتجات</th>
                                            <th>حاله القسم</th>
                                            <th>الاكشن</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($categories as $category)
                                            <tr>
                                                <td><img style="width: 50px" src= {{ $category->category_image }} ></td>
                                                <td>{{ $category->category_name_en }}</td>
                                                <td>{{ $category->category_name_ar }}</td>
                                                <td>{{ $category->countProduct($category->id) }}</td>

                                                <td>
                                                    <div class="media-body ">
                                                        <label class="switch">
                                                            <input type="checkbox"
                                                                   {{ $category->status == 1 ? 'checked' : '' }}
                                                                   onchange="change_status_category({{ $category->id }},{{ $category->status }})"><span
                                                                class="switch-state"></span>

                                                        </label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <a class="btn btn-primary btn-group-sm"
                                                       href="{{ route('category_products',['id'=> $category->id]) }}"
                                                       data-whatever="@test"><i class="fa fa-eye"></i>
                                                    </a>
                                                    @if (auth()->user()->hasPermissionTo('تعديل_اقسام'))

                                                        <button class="btn btn-primary" type="button"
                                                                data-toggle="modal"
                                                                data-target="#{{ $category->id }}"
                                                                data-whatever="@category"><i class="fa fa-edit"></i>
                                                        </button>
                                                        @endif
                                                        @if (auth()->user()->hasPermissionTo('حذف_اقسام'))

                                                            <form action="{{ route('CategoryDestroy', $category->id) }}"
                                                                  method="post" style="display: inline-block">
                                                                {{ csrf_field() }}
                                                                {{ method_field('delete') }}
                                                                <button type="submit"
                                                                        class="btn btn-danger delete btn-sm"><i
                                                                        class="fa fa-trash"></i>
                                                                </button>
                                                            </form>
                                                        @endif

                                                        <div class="modal fade" id="{{ $category->id }}" tabindex="-1"
                                                             role="dialog"
                                                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title"> تعديل قسم
                                                                            {{ $category->category_name_ar }} </h5>

                                                                        <button class="close" type="button"
                                                                                data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">×</span></button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <form class="needs-validation" novalidate=""
                                                                              action="{{route('updateCategory',['id'=>$category->id])}}"
                                                                              method="POST"
                                                                              enctype="multipart/form-data">
                                                                            {{ method_field('POST') }}
                                                                            {{ csrf_field() }}

                                                                            <div class="form-group">
                                                                                <label for="validationCustom05"
                                                                                       class="col-form-label page-header-left">اسم
                                                                                    القسم
                                                                                    بالانجليزيه</label>
                                                                                <input class="form-control"
                                                                                       name="category_name_en"
                                                                                       id="category_name_en"
                                                                                       type="text"
                                                                                       value="{{$category->category_name_en}}"
                                                                                       placeholder="ادخل اسم القسم بالانجليزيه"
                                                                                       required="">
                                                                                <div
                                                                                    class="invalid-feedback">{{ $errors->first('category_name_en') }}</div>
                                                                            </div>
                                                                            <span class="text-danger page-header-left"
                                                                                  style="color: red;">{{$errors->first('category_name_en')}}</span>
                                                                            <div class="form-group">

                                                                                <label for="validationCustom05"
                                                                                       class="col-form-label page-header-left">اسم
                                                                                    القسم
                                                                                    بالعربى</label>
                                                                                <input class="form-control"
                                                                                       name="category_name_ar"
                                                                                       id="category_name_ar"
                                                                                       type="text"
                                                                                       value="{{$category->category_name_ar}}"
                                                                                       placeholder="ادخل اسم القسم بالعربيه"
                                                                                       required="">
                                                                                <div
                                                                                    class="invalid-feedback">{{ $errors->first('category_name_ar') }}</div>
                                                                            </div>
                                                                            <span class="text-danger page-header-left"
                                                                                  style="color: red;">{{$errors->first('category_name_ar')}}</span>

                                                                            <div class="form-group">
                                                                                <label for="validationCustom05"
                                                                                       class="page-header-left">صوره
                                                                                    القسم</label>
                                                                                <label for="file-input"
                                                                                       class="image-upload-label">
                                                                                    <img alt="upload-service-image"
                                                                                         src="{{$category->category_image}}"
                                                                                         class="thumb"
                                                                                         style="width: 100px"/>
                                                                                </label>
                                                                                <div class="custom-file">
                                                                                    <input class="custom-file-input"
                                                                                           type="file"
                                                                                           name="category_image">
                                                                                    <label class="custom-file-label"
                                                                                           for="validatedCustomFile">اختر
                                                                                        صوره</label>
                                                                                    <div
                                                                                        class="invalid-feedback">{{ $errors->first('category_image') }}</div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="modal-footer">
                                                                                <button class="btn btn-primary"
                                                                                        type="submit">
                                                                                    تعديل
                                                                                </button>
                                                                                <button class="btn btn-secondary"
                                                                                        type="button"
                                                                                        data-dismiss="modal">اغلاق
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @endif
                    @else
                        <h4 class="text-center"> لا توجد اقسام مضافه </h4>
                    @endif

                </div>
            </div>
        </div>
    </div>



    <script>

        function change_status_category(id, value) {
            if (value == 0) {
                value = 1;
            } else {
                value = 0;
            }
            axios.get('updateStatusCategory/' + id + '/' + value)
                .then(function (response) {
                    // alert(response.data.category_status);
                    location.reload();
                })
                .catch(function (error) {
                    console.log(error);
                    alert(error);
                });
        };


    </script>

@endsection








