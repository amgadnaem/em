@extends('AdminPanel.backend.layouts.default')
@section('content')


    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            @if (auth()->user()->hasPermissionTo('اضافه_مندوب'))
                                <button class="btn btn-primary" type="button" data-toggle="modal"
                                        data-target="#AddShipping" data-whatever="@delegate">اضافه شركه شحن
                                </button>
                            @endif
                            <div class="modal fade" id="AddShipping" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">اضافه شركه شحن</h5>
                                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <form class="needs-validation" novalidate=""
                                                  action="{{route('addShipping')}}"
                                                  method="POST" enctype="multipart/form-data">
                                                {{ method_field('POST') }}
                                                {{ csrf_field() }}

                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left"> اسم الشركه
                                                        بالعربيه</label>
                                                    <input class="form-control" value="{{ old('company_ar') }}"
                                                           name="company_ar" id="validationCustomUsername"
                                                           type="text"
                                                           placeholder="ادخل اسم الشركه بالعربيه "
                                                           aria-describedby="inputGroupPrepend"
                                                           required="">
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('company_ar') }}</div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left"> اسم الشركه
                                                        بالانجليزيه</label>
                                                    <input class="form-control" value="{{ old('company_en') }}"
                                                           name="company_en" id="validationCustomUsername"
                                                           type="text"
                                                           placeholder="ادخل اسم الشركه بالانجليزيه "
                                                           aria-describedby="inputGroupPrepend"
                                                           required="">
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('company_en') }}</div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left"> اسم الشركه
                                                        بالكرديه</label>
                                                    <input class="form-control" value="{{ old('company_kur') }}"
                                                           name="company_kur" id="validationCustomUsername"
                                                           type="text"
                                                           placeholder="ادخل اسم الشركه بالكرديه"
                                                           aria-describedby="inputGroupPrepend"
                                                           required="">
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('company_kur') }}</div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="validationCustom04"
                                                           class="col-form-label page-header-left">ادخل رقم
                                                        الهاتف</label>
                                                    <input class="form-control"
                                                           {{ old('phone_number') }} name="phone_number"
                                                           id="validationCustom04" type="text"
                                                           placeholder="ادخل رقم الموبايل"
                                                           required="">
                                                    <div class="invalid-feedback">{{ $errors->first('phone_number') }}.
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">كلمه المرور</label>
                                                    <input class="form-control" name="password" id="validationCustom05"
                                                           type="password"
                                                           placeholder="ادخل كلمه المرور"
                                                           required="">
                                                    <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="page-header-left"> نوع شركه الشحن</label>
                                                    <select name="type"
                                                            class="form-control"
                                                            required>
                                                        <option value="">اختر</option>
                                                        <option value="1">شركه شحن محليه</option>
                                                        <option value="2">شركه شحن دوليه</option>
                                                    </select>
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('country_id') }}</div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="page-header-left"> اسم الدوله</label>
                                                    <select name="country_id" id="country_id"
                                                            class="form-control"
                                                            required>
                                                        <option value="">اختر</option>
                                                        @foreach($countries as $country)
                                                            <option
                                                                value="{{ $country->id }}">{{ $country->country_name_ar }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('country_id') }}</div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">صوره الشركه</label>
                                                    <div class="custom-file">
                                                        <input class="custom-file-input" id="validatedCustomFile"
                                                               type="file"
                                                               name="company_image">
                                                        <label class="custom-file-label"
                                                               for="validatedCustomFile">اختر</label>
                                                        <div class="invalid-feedback">تحميل صوره</div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">صوره الرخصه
                                                        للشركه</label>
                                                    <div class="custom-file">
                                                        <input class="custom-file-input" id="validatedCustomFile"
                                                               type="file"
                                                               name="license_image">
                                                        <label class="custom-file-label"
                                                               for="validatedCustomFile">اختر</label>
                                                        <div class="invalid-feedback">تحميل صوره</div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn btn-primary" type="submit">اضافه</button>
                                                    <button class="btn btn-secondary" type="button"
                                                            data-dismiss="modal">اغلاق
                                                    </button>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>عدد شركات الشحن {{ count($showShippingCompany) }}</h5>
                        </div>
                        @if( count($showShippingCompany) > 0)
                            @if (auth()->user()->hasPermissionTo('عرض_مندوب'))
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="display dataTable" id="basic-1">
                                            <thead>
                                            <tr>
                                                <th>صوره</th>
                                                <th>نوع شركه الشحن</th>
                                                <th>اسم الشركه</th>
                                                <th>رقم الهاتف</th>
                                                <th>رقم شركه الشحن</th>
                                                @if (auth()->user()->hasPermissionTo('تعديل_مندوب'))
                                                    <th>حاله الحساب</th>
                                                @endif
                                                <th>الطلبات المنفذه</th>
                                                <th>الاكشن</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($showShippingCompany as $account)
                                                <tr>
                                                    <td><img src="{{ $account->company_image }}" style="width: 50px">
                                                    </td>
                                                    @if($account->type == 1)
                                                        <td>شركه شحن محليه</td>
                                                    @else
                                                        <td>شركه شحن دوليه</td>
                                                    @endif
                                                    <td>{{ $account->company_ar }} </td>
                                                    <td>{{ $account->phone_number }}</td>
                                                    <td>{{ $account->company_number }}</td>
                                                    @if (auth()->user()->hasPermissionTo('تعديل_مندوب'))
                                                        <td>
                                                            <div class="media-body ">
                                                                <label class="switch">
                                                                    <input type="checkbox"
                                                                           {{ $account->status == 1 ? 'checked' : '' }}
                                                                           onchange="change_status_company({{ $account->id }},{{ $account->status }})"><span
                                                                        class="switch-state"></span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                    @endif
                                                    <td>{{ $account->shipping_orders($account->id) }}

                                                    </td>
                                                    <td>
                                                        {{--                                                        <a--}}
                                                        {{--                                                            href="{{ route('delegate_orders', $account->id) }}"--}}
                                                        {{--                                                            class="btn btn-info">--}}
                                                        {{--                                                            <i class="fa fa-info"></i>--}}
                                                        {{--                                                        </a>--}}
                                                        @if (auth()->user()->hasPermissionTo('تعديل_مندوب'))
                                                            <button class="btn btn-primary" type="button"
                                                                    data-toggle="modal"
                                                                    data-target="#{{ $account->id }}"
                                                                    data-whatever="@delegate"><i class="fa fa-edit"></i>
                                                            </button>
                                                        @endif
                                                        @if (auth()->user()->hasPermissionTo('حذف_مندوب'))
                                                            <form
                                                                action="{{ route('destroyCompany', $account->id) }}"
                                                                method="post" style="display: inline-block">
                                                                {{ csrf_field() }}
                                                                {{ method_field('delete') }}
                                                                <button type="submit"
                                                                        class="btn btn-danger delete btn-sm"><i
                                                                        class="fa fa-trash"></i>
                                                                </button>
                                                            </form>
                                                        @endif

                                                        <div class="modal fade" id="{{ $account->id }}" tabindex="-1"
                                                             role="dialog"
                                                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title"> تعديل حساب
                                                                            {{ $account->company_ar }}  </h5>

                                                                        <button class="close" type="button"
                                                                                data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">×</span></button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <form class="needs-validation" novalidate=""
                                                                              action="{{route('EditAccountCompany',['id'=>$account->id])}}"
                                                                              method="POST"
                                                                              enctype="multipart/form-data">
                                                                            {{ method_field('POST') }}
                                                                            {{ csrf_field() }}

                                                                            <div class="form-group">
                                                                                <label
                                                                                    class="col-form-label page-header-left">اسم
                                                                                    الشركه بالعربيه
                                                                                </label>
                                                                                <input class="form-control"
                                                                                       name="company_ar"
                                                                                       required
                                                                                       type="text"
                                                                                       value="{{ $account->company_ar }}">
                                                                                <div
                                                                                    class="invalid-feedback">{{ $errors->first('company_ar') }}</div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label
                                                                                    class="col-form-label page-header-left">اسم
                                                                                    الشركه بالانجليزيه
                                                                                </label>
                                                                                <input class="form-control"
                                                                                       name="company_en"
                                                                                       required
                                                                                       type="text"
                                                                                       value="{{ $account->company_en }}">
                                                                                <div
                                                                                    class="invalid-feedback">{{ $errors->first('company_en') }}</div>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label
                                                                                    class="col-form-label page-header-left">اسم
                                                                                    الشركه بالكرديه
                                                                                </label>
                                                                                <input class="form-control"
                                                                                       name="company_kur"
                                                                                       required
                                                                                       type="text"
                                                                                       value="{{ $account->company_kur }}">
                                                                                <div
                                                                                    class="invalid-feedback">{{ $errors->first('company_kur') }}</div>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label
                                                                                    class="col-form-label page-header-left">كلمه
                                                                                    المرور</label>
                                                                                <input class="form-control"
                                                                                       name="password"
                                                                                       type="password">
                                                                                <div
                                                                                    class="invalid-feedback">{{ $errors->first('password') }}</div>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label for="validationCustom05"
                                                                                       class="page-header-left">صوره
                                                                                    الحساب</label>
                                                                                <label for="file-input"
                                                                                       class="image-upload-label">
                                                                                    <img alt="upload-delegate-image"
                                                                                         name="company_image"
                                                                                         src="{{$account->company_image}}"
                                                                                         class="thumb"
                                                                                         style="width: 100px"/>
                                                                                </label>
                                                                                <div class="custom-file">
                                                                                    <input class="custom-file-input"
                                                                                           type="file"
                                                                                           name="company_image">
                                                                                    <label class="custom-file-label"
                                                                                           for="validatedCustomFile">اختر
                                                                                        صوره</label>
                                                                                    <div
                                                                                        class="invalid-feedback">{{ $errors->first('company_image') }}</div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="modal-footer">
                                                                                <button class="btn btn-primary"
                                                                                        type="submit">
                                                                                    تعديل
                                                                                </button>
                                                                                <button class="btn btn-secondary"
                                                                                        type="button"
                                                                                        data-dismiss="modal">اغلاق
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            @endif
                        @else
                            <h4 class="text-center"> لا يوجد شركات شحن لديك </h4>
                        @endif
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>طلبات شركات الشحن التى لم يتم قبولهم حتى الان</h5>
                        </div>
                        @if( count($shippingCompanyRequest) > 0)
                            @if (auth()->user()->hasPermissionTo('عرض_مندوب'))
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="display dataTable" id="basic-3">
                                            <thead>
                                            <tr>
                                                <th>صوره</th>
                                                <th>نوع شركه الشحن</th>
                                                <th>اسم الشركه</th>
                                                <th>رقم الهاتف</th>
                                                <th>رقم شركه الشحن</th>
                                                @if (auth()->user()->hasPermissionTo('تعديل_مندوب'))
                                                    <th>الاكشن</th>
                                                @endif

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($shippingCompanyRequest as $account)
                                                <tr>
                                                    <td><img src="{{ $account->company_image }}" style="width: 50px">
                                                    </td>
                                                    @if($account->type == 1)
                                                        <td>شركه شحن محليه</td>
                                                    @else
                                                        <td>شركه شحن دوليه</td>
                                                    @endif
                                                    <td>{{ $account->company_ar }} </td>
                                                    <td>{{ $account->phone_number }}</td>
                                                    <td>{{ $account->company_number }}</td>
                                                    @if (auth()->user()->hasPermissionTo('تعديل_مندوب'))
                                                        <td>
                                                            <div class="media-body ">
                                                                <label class="switch">
                                                                    <input type="checkbox"
                                                                           {{ $account->company_status == 1 ? 'checked' : '' }}
                                                                           onchange="confirm_company({{ $account->id }})"><span
                                                                        class="switch-state"></span>

                                                                </label>
                                                            </div>
                                                        </td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            @endif
                        @else
                            <h5 class="text-center"> لا يوجد طلبات مرسله من قبل شركات الشحن</h5>
                        @endif
                    </div>
                </div>
            </div>


        </div>
    </div>


    <script>


        function change_status_company(id, value) {
            if (value == 0) {
                value = 1;
            } else {
                value = 0;
            }
            axios.get('UpdateStatusCompany/' + id + '/' + value)
                .then(function (response) {
                    location.reload();
                })
                .catch(function (error) {
                    console.log(error);
                    alert(error);
                });
        };

        function confirm_company(id) {
            axios.get('confirmAccountCompany/' + id)
                .then(function (response) {
                    location.reload();
                })
                .catch(function (error) {
                    console.log(error);
                    alert(error);
                });
        };


    </script>

@endsection
