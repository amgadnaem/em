@extends('AdminPanel.backend.layouts.default')
@section('content')
    <!-- Right sidebar Ends-->
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">

                        <div class="page-header-left">

                            {{--                            <button class="btn btn-primary" type="button" data-toggle="modal"--}}
                            {{--                                    data-target="#category" data-whatever="@category">إرسال إشعار--}}
                            {{--                            </button>--}}
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    @if( count($shops) > 0)

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display dataTable" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>المسلسل</th>
                                        <th>إسم المتجر</th>
                                        <th>أخر رسالة من المتجر</th>
{{--                                        <th>حالة الرسالة</th>--}}
                                        <th>العمليات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($shops as $shop)
                                        <tr>
                                            <td>{{$shop->id}}</td>
                                            <td>{{$shop->name_app_ar}}</td>
                                            <td>{{$shop['messages']->first()['message'] ?: "-"}}</td>
{{--                                            <td>--}}
{{--                                                @if ($shop['messages']->first()['is_read'] == '0')--}}
{{--                                                    <span class="badge badge-success">جديدة</span>--}}
{{--                                                @elseif ($shop['messages']->first()['is_read'] == '1')--}}
{{--                                                    <span class="badge badge-dark">تم الرد</span>--}}
{{--                                                @endif--}}
{{--                                            </td>--}}
                                            <td>
                                                {{--                                                <form action="{{ route('destroyAdminNotification', $notification->id) }}"--}}
                                                {{--                                                      method="post" style="display: inline-block">--}}
                                                {{--                                                    {{ csrf_field() }}--}}
                                                {{--                                                    {{ method_field('delete') }}--}}
                                                {{--                                                    <button type="submit" class="btn btn-danger delete btn-sm">--}}
                                                {{--                                                        <i class="fa fa-trash"></i>--}}
                                                {{--                                                    </button>--}}
                                                {{--                                                </form>--}}
                                                <a href="{{route('SendshopsMessagesView',['id'=>$shop->id])}}" title="send" class="btn btn-success btn-sm"><i class="fa fa-send"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @else
                        <h4 class="text-center"> لا توجد رسائل </h4>
                    @endif

                </div>
            </div>
        </div>
    </div>

@endsection