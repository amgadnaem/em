@extends('AdminPanel.backend.layouts.default')
@section('content')

    <!-- Right sidebar Ends-->
    <div class="page-body">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h6>طلبات المستخدم </h6>
                </div>
                @if( count($all_orders) > 0)
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="basic-4" class="shop_orders display dataTable">
                                <thead>
                                <tr>
                                    <th>حاله الطلب</th>
                                    <th>رقم الطلب</th>
                                    <th>سعر الطلب</th>
                                    <th>تكلفه شحن الطلب</th>
                                    <th>تاريخ الطلب</th>
                                    <th>المستخدم</th>
                                    <th>المندوب</th>
                                    <th>عنوان الطلب</th>
                                    <th>تفاصيل الطلب</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($all_orders as $Order)
                                    <tr>
                                        <td>{{ $Order->status_order($Order->order_status)}}</td>
                                        <td>{{ $Order->order_number }}</td>
                                        <td>{{ $Order->subtotal_fees }}</td>
                                        <td>{{ $Order->shipping_fees }}</td>
                                        <td>{{ $Order->order_date }}</td>
                                        <td>{{ $Order->user->first_name }}</td>
                                        <td>{{ !empty($Order->delegate->first_name) ? $Order->delegate->first_name : ''  }}</td>
                                        <td>{{ !empty($Order->address->location) ? $Order->address->location : ''  }}</td>

                                        <td><a href="{{ route('order_details',['id'=> $Order->id]) }}">
                                                <i class="fa fa-list-alt"></i>
                                            </a>
                                            <a class="btnprn" href="{{ route('print', $Order->id) }}">طباعه</a>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>

                        </div>
                    </div>
                @else
                    <h6 class="text-center">لا توجد طلبات منفذه لهذا المستخدم</h6>
                @endif
            </div>
        </div>

    </div>
@endsection
