@extends('AdminPanel.backend.layouts.default')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">

                        @if( count($all_users) > 0)

                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="display dataTable" id="basic-1">
                                        <thead>
                                        <tr>
                                            <th>QR Code</th>
                                            <th>صوره</th>
                                            <th>الاسم</th>
                                            <th>رقم الهاتف</th>
                                            <th>عدد الطلبات</th>
                                            <th>حاله الحساب</th>
                                            <th>الاكشن</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($all_users as $account)
                                            <tr>
                                                <td><img
                                                        src="data:image/png;base64, {!! base64_encode(\SimpleSoftwareIO\QrCode\Facades\QrCode::format('png')->size(100)->generate($account->phone_number)) !!} ">
                                                </td>
                                                <td><img src="{{ $account->user_image }}" style="width: 50px" alt="">
                                                </td>
                                                <td>{{ $account->first_name }}</td>
                                                <td>{{ $account->phone_number }}</td>
                                                <td>{{ !empty($account->Orders_user($account->id)) ? $account->Orders_user($account->id) : 0 }}</td>
                                                <td>
                                                    <div class="media-body ">
                                                        <label class="switch">
                                                            <input type="checkbox"
                                                                   {{ $account->status == 1 ? 'checked' : '' }}
                                                                   onchange="change_status_users({{ $account->id }},{{ $account->status }})"><span
                                                                class="switch-state"></span>
                                                        </label>
                                                    </div>
                                                </td>

                                                <td>
                                                    <a href="{{ route('users_orders', $account->id) }}"
                                                       class="btn btn-info">
                                                        <i class="fa fa-info"></i>
                                                    </a>

                                                <form action="{{ route('deleteUser', $account->id) }}"
                                                      method="post" style="display: inline-block">
                                                    {{ csrf_field() }}
                                                    {{ method_field('delete') }}
                                                    <button type="submit"
                                                            class="btn btn-danger delete btn-sm">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </form>

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        @else
                            <h4 class="text-center"> لا يوجد مستخدمين </h4>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        function change_status_users(id, value) {
            if (value == 0) {
                value = 1;
            } else {
                value = 0;
            }
            axios.get('UpdateStatusUser/' + id + '/' + value)
                .then(function (response) {
                    location.reload();
                })
                .catch(function (error) {
                    console.log(error);
                    alert(error);
                });
        };


    </script>

@endsection
