@extends('AdminPanel.backend.layouts.default')
@section('content')

    <!-- Right sidebar Ends-->
    <div class="page-body">
        <div class="container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">

                        <div class="page-header-left">

                            <button class="btn btn-primary" type="button" data-toggle="modal"
                                    data-target="#category" data-whatever="@category">اضافه ارقام كروت تعبئه
                            </button>
                            <div class="modal fade" id="category" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">اضافه ارقام كرت تعبئه </h5>
                                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <form class="needs-validation" novalidate=""
                                                  action="{{route('addPackingNumber')}}"
                                                  method="POST" enctype="multipart/form-data">
                                                {{ method_field('POST') }}
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="page-header-left"> اسم الكرت</label>
                                                    <select name="card_id"
                                                            class="form-control"
                                                            required>
                                                        <option value="">اختر</option>
                                                        @foreach($packingCards as $card)
                                                            <option
                                                                value="{{ $card->id }}">{{ $card->card_name_ar }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('card_id') }}</div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">عدد الكروت
                                                    </label>
                                                    <input class="form-control" name="card_numbers"
                                                           type="text"
                                                           placeholder="ادخل عدد الكروت"
                                                           required="">
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('card_numbers') }}</div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn btn-primary" type="submit">اضافه</button>
                                                    <button class="btn btn-secondary" type="button"
                                                            data-dismiss="modal">اغلاق
                                                    </button>
                                                </div>
                                            </form>

                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        {{--        @php--}}
        {{--            $maps = ['اضافه', 'عرض', 'تعديل', 'حذف'];--}}
        {{--        @endphp--}}

        {{--        <ul class="nav nav-tabs nav-justified md-tabs indigo" id="myTabJust" role="tablist">--}}
        {{--            @foreach ($packingCards as $index=>$model)--}}
        {{--                <li class="nav-item">--}}
        {{--                    <a class="nav-link {{ $index == 0 ? 'active' : '' }} {{ $index == 0 ? 'show' : '' }}"--}}
        {{--                       href="#{{ $model }}"--}}
        {{--                       data-toggle="tab">{{ $model->card_name_ar }} </a>--}}
        {{--                </li>--}}
        {{--            @endforeach--}}
        {{--        </ul>--}}

        {{--        <div class="tab-content">--}}

        {{--            @foreach ($packingCards as $index=>$model)--}}

        {{--                <div class="tab-pane {{ $index == 0 ? 'active' : '' }}"--}}
        {{--                     id="{{ $model }}" role="tabpanel"--}}
        {{--                     aria-labelledby="contact-danger-tab">--}}
        {{--                    <br>--}}
        {{--                    --}}{{--                                    <div class="row">--}}
        {{--                    --}}{{--                                        <div class="col-sm-12">--}}
        {{--                    --}}{{--                                            <div class="card">--}}
        {{--                    --}}{{--                                                @if( count($model) > 0)--}}

        {{--                    --}}{{--                                                    <div class="card-body">--}}
        {{--                    --}}{{--                                                        <div class="table-responsive">--}}
        {{--                    --}}{{--                                                            <table class="display dataTable" id="basic-1">--}}
        {{--                    --}}{{--                                                                <thead>--}}
        {{--                    --}}{{--                                                                <tr>--}}
        {{--                    --}}{{--                                                                    <th>اسم الكرت بالانجليزيه</th>--}}
        {{--                    --}}{{--                                                                </tr>--}}
        {{--                    --}}{{--                                                                </thead>--}}
        {{--                    --}}{{--                                                                <tbody>--}}
        {{--                    --}}{{--                                                                    <tr>--}}
        {{--                    --}}{{--                                                                        <td>{{ $model->id }}</td>--}}
        {{--                    --}}{{--                                                                    </tr>--}}
        {{--                    --}}{{--                                                                </tbody>--}}
        {{--                    --}}{{--                                                            </table>--}}
        {{--                    --}}{{--                                                        </div>--}}
        {{--                    --}}{{--                                                    </div>--}}
        {{--                    --}}{{--                                                @else--}}
        {{--                    --}}{{--                                                    <h4 class="text-center"> لا توجد كروت مضافه </h4>--}}
        {{--                    --}}{{--                                                @endif--}}

        {{--                    --}}{{--                                            </div>--}}
        {{--                    --}}{{--                                        </div>--}}
        {{--                    --}}{{--                                    </div>--}}

        {{--                    <div class="text-center">--}}
        {{--                        @foreach ($maps as $map)--}}
        {{--                            <input type="checkbox"--}}
        {{--                                   name="permissions[]"--}}
        {{--                                   style="font-size: large"--}}
        {{--                                   value="{{ $map . '_' . $model }}"> {{ $map}}--}}

        {{--                        @endforeach--}}

        {{--                    </div>--}}
        {{--                </div>--}}

        {{--            @endforeach--}}
        {{--        </div>--}}

        {{--        <div class="row">--}}
        {{--            <div class="col-sm-12">--}}
        {{--                <div class="card">--}}
        {{--                    @if( count($packing_card) > 0)--}}

        {{--                        <div class="card-body">--}}
        {{--                            <div class="table-responsive">--}}
        {{--                                <table class="display dataTable" id="basic-1">--}}
        {{--                                    <thead>--}}
        {{--                                    <tr>--}}
        {{--                                        <th>اسم الكرت بالانجليزيه</th>--}}
        {{--                                        <th>اسم الكرت بالعربيه</th>--}}
        {{--                                        <th>الوصف بالانجليزيه</th>--}}
        {{--                                        <th>الوصف بالعربيه</th>--}}
        {{--                                        <th>عدد النقاط</th>--}}
        {{--                                        <th>حاله الكرت</th>--}}
        {{--                                        <th>الاكشن</th>--}}
        {{--                                    </tr>--}}
        {{--                                    </thead>--}}
        {{--                                    <tbody>--}}
        {{--                                    @foreach($packing_card as $card)--}}
        {{--                                        <tr>--}}
        {{--                                            <td>{{ $card->card_name_en }}</td>--}}
        {{--                                            <td>{{ $card->card_name_ar }}</td>--}}
        {{--                                            <td>{{ $card->desc_en }}</td>--}}
        {{--                                            <td>{{ $card->desc_ar }}</td>--}}
        {{--                                            <td>{{ $card->points }}</td>--}}
        {{--                                            <td>--}}
        {{--                                                <div class="media-body ">--}}
        {{--                                                    <label class="switch">--}}
        {{--                                                        <input type="checkbox"--}}
        {{--                                                               {{ $card->status == 1 ? 'checked' : '' }}--}}
        {{--                                                               onchange="change_status_packing({{ $card->id }},{{ $card->status }})"><span--}}
        {{--                                                            class="switch-state"></span>--}}
        {{--                                                    </label>--}}
        {{--                                                </div>--}}
        {{--                                            </td>--}}
        {{--                                            <td>--}}
        {{--                                                <form action="{{ route('packingDestroy', $card->id) }}"--}}
        {{--                                                      method="post" style="display: inline-block">--}}
        {{--                                                    {{ csrf_field() }}--}}
        {{--                                                    {{ method_field('delete') }}--}}
        {{--                                                    <button type="submit"--}}
        {{--                                                            class="btn btn-danger delete btn-sm"><i--}}
        {{--                                                            class="fa fa-trash"></i>--}}
        {{--                                                    </button>--}}
        {{--                                                </form>--}}
        {{--                                            </td>--}}
        {{--                                        </tr>--}}
        {{--                                    @endforeach--}}
        {{--                                    </tbody>--}}
        {{--                                </table>--}}
        {{--                            </div>--}}
        {{--                        </div>--}}
        {{--                    @else--}}
        {{--                        <h4 class="text-center"> لا توجد كروت مضافه </h4>--}}
        {{--                    @endif--}}

        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </div>--}}

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    @if( count($packingNumbers) > 0)

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display dataTable" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>QR Code</th>
                                        <th>رقم الكرت</th>
                                        <th>كرت تعبئه</th>
                                        <th>عدد النقاط</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($packingNumbers as $card)
                                        <tr>
                                            <td>
                                                <img
                                                    src="data:image/png;base64, {!! base64_encode(\SimpleSoftwareIO\QrCode\Facades\QrCode::format('png')->size(100)->generate($card->card_number)) !!} ">
                                            </td>
                                            <td>{{ $card->card_number }}</td>
                                            <td>{{ $card->getPacking($card->card_id) }}</td>
                                            <td>{{ $card->getPackingPoints($card->card_id) }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @else
                        <h4 class="text-center"> لا توجد كروت مضافه </h4>
                    @endif

                </div>
            </div>
        </div>


    </div>



    <script>

        function change_status_packing(id, value) {
            if (value == 0) {
                value = 1;
            } else {
                value = 0;
            }
            axios.get('updateStatusPacking/' + id + '/' + value)
                .then(function (response) {
                    // alert(response.data.category_status);
                    location.reload();
                })
                .catch(function (error) {
                    console.log(error);
                    alert(error);
                });
        };


    </script>

@endsection
