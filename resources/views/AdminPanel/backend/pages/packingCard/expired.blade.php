@extends('AdminPanel.backend.layouts.default')
@section('content')

    <!-- Right sidebar Ends-->
    <div class="page-body">

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    @if( count($expiredPackingNumbers) > 0)

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display dataTable" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>QR Code</th>
                                        <th>رقم الكرت</th>
                                        <th>كرت تعبئه</th>
                                        <th>عدد النقاط</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($expiredPackingNumbers as $card)
                                        <tr>
                                            <td>
                                                <img
                                                    src="data:image/png;base64, {!! base64_encode(\SimpleSoftwareIO\QrCode\Facades\QrCode::format('png')->size(100)->generate($card->card_number)) !!} ">
                                            </td>
                                            <td>{{ $card->card_number }}</td>
                                            <td>{{ $card->getPacking($card->card_id) }}</td>
                                            <td>{{ $card->getPackingPoints($card->card_id) }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @else
                        <h4 class="text-center"> لا توجد كروت مضافه </h4>
                    @endif

                </div>
            </div>
        </div>

    </div>



@endsection
