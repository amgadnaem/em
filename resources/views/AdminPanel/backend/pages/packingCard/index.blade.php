@extends('AdminPanel.backend.layouts.default')
@section('content')

    <!-- Right sidebar Ends-->
    <div class="page-body">
        <div class="container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">

                        <div class="page-header-left">

                            <button class="btn btn-primary" type="button" data-toggle="modal"
                                    data-target="#category" data-whatever="@category">اضافه كرت تعبئه
                            </button>
                            <div class="modal fade" id="category" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">اضافه كرت تعبئه </h5>
                                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <form class="needs-validation" novalidate=""
                                                  action="{{route('addPackingCard')}}"
                                                  method="POST" enctype="multipart/form-data">
                                                {{ method_field('POST') }}
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">اسم الكرت
                                                        بالانجليزيه</label>
                                                    <input class="form-control" name="card_name_en"
                                                           id="card_name_en"
                                                           type="text"
                                                           placeholder="ادخل اسم الكرت بالانجليزيه"
                                                           required="">
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('card_name_en') }}</div>
                                                </div>
                                                <span class="text-danger page-header-left"
                                                      style="color: red;">{{$errors->first('card_name_en')}}</span>
                                                <div class="form-group">

                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">اسم الكرت
                                                        بالعربى</label>
                                                    <input class="form-control" name="card_name_ar"
                                                           id="card_name_ar"
                                                           type="text"
                                                           placeholder="ادخل اسم الكرت بالعربيه"
                                                           required="">
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('card_name_ar') }}</div>
                                                </div>
                                                <span class="text-danger page-header-left"
                                                      style="color: red;">{{$errors->first('card_name_ar')}}</span>

                                                <div class="form-group">

                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">اسم الكرت
                                                        بالكرديه</label>
                                                    <input class="form-control" name="card_name_kur"
                                                           id="card_name_kur"
                                                           type="text"
                                                           placeholder="ادخل اسم الكرت بالكرديه"
                                                           required="">
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('card_name_kur') }}</div>
                                                </div>
                                                <span class="text-danger page-header-left"
                                                      style="color: red;">{{$errors->first('card_name_kur')}}</span>
                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">وصف الكرت
                                                        بالانجليزيه</label>
                                                    <input class="form-control" name="desc_en"
                                                           type="text"
                                                           placeholder="ادخل وصف الكرت بالانجليزيه"
                                                           required="">
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('desc_en') }}</div>
                                                </div>
                                                <span class="text-danger page-header-left"
                                                      style="color: red;">{{$errors->first('desc_en')}}</span>
                                                <div class="form-group">

                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">وصف الكرت
                                                        بالعربى</label>
                                                    <input class="form-control" name="desc_ar"
                                                           type="text"
                                                           placeholder="ادخل وصف الكرت بالعربيه"
                                                           required="">
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('desc_ar') }}</div>
                                                </div>
                                                <span class="text-danger page-header-left"
                                                      style="color: red;">{{$errors->first('desc_ar')}}</span>

                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">وصف الكرت
                                                        بالكرديه</label>
                                                    <input class="form-control" name="desc_kur"
                                                           type="text"
                                                           placeholder="ادخل وصف الكرت بالكرديه"
                                                           required="">
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('desc_kur') }}</div>
                                                </div>
                                                <span class="text-danger page-header-left"
                                                      style="color: red;">{{$errors->first('desc_kur')}}</span>
                                                <div class="form-group">

                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">عدد نقاط
                                                        الكرت</label>
                                                    <input class="form-control" name="points"
                                                           type="text"
                                                           placeholder="ادخل عدد نقاط الكرت"
                                                           required="">
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('points') }}</div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn btn-primary" type="submit">اضافه</button>
                                                    <button class="btn btn-secondary" type="button"
                                                            data-dismiss="modal">اغلاق
                                                    </button>
                                                </div>
                                            </form>

                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    @if( count($packing_card) > 0)

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display dataTable" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>اسم الكرت بالانجليزيه</th>
                                        <th>اسم الكرت بالعربيه</th>
                                        <th>الوصف بالانجليزيه</th>
                                        <th>الوصف بالعربيه</th>
                                        <th>عدد النقاط</th>
                                        <th>عدد الكروت المتاحه</th>
                                        <th>عدد الكروت تم استخدامها</th>
                                        <th>حاله الكرت</th>
                                        <th>الاكشن</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($packing_card as $card)
                                        <tr>
                                            <td>{{ $card->card_name_en }}</td>
                                            <td>{{ $card->card_name_ar }}</td>
                                            <td>{{ $card->desc_en }}</td>
                                            <td>{{ $card->desc_ar }}</td>
                                            <td>{{ $card->points }}</td>
                                            <td>{{ $card->getPacking($card->id) }}</td>
                                            <td>{{ $card->expiredPacking($card->id) }}</td>
                                            <td>
                                                <div class="media-body ">
                                                    <label class="switch">
                                                        <input type="checkbox"
                                                               {{ $card->status == 1 ? 'checked' : '' }}
                                                               onchange="change_status_packing({{ $card->id }},{{ $card->status }})"><span
                                                            class="switch-state"></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <form action="{{ route('packingDestroy', $card->id) }}"
                                                      method="post" style="display: inline-block">
                                                    {{ csrf_field() }}
                                                    {{ method_field('delete') }}
                                                    <button type="submit"
                                                            class="btn btn-danger delete btn-sm"><i
                                                            class="fa fa-trash"></i>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @else
                        <h4 class="text-center"> لا توجد كروت مضافه </h4>
                    @endif

                </div>
            </div>
        </div>
    </div>



    <script>

        function change_status_packing(id, value) {
            if (value == 0) {
                value = 1;
            } else {
                value = 0;
            }
            axios.get('updateStatusPacking/' + id + '/' + value)
                .then(function (response) {
                    // alert(response.data.category_status);
                    location.reload();
                })
                .catch(function (error) {
                    console.log(error);
                    alert(error);
                });
        };


    </script>

@endsection








