@extends('AdminPanel.backend.layouts.default')
@section('content')
    <style>

        #pac-input {
            background-color: #fff;
            font-family: Roboto;
            font-size: 25px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 400px;
        }

        #pac-input:focus {
            border-color: #4d90fe;
        }


    </style>
    <!-- Right sidebar Ends-->
    <div class="page-body">

        <!-- Default ordering (sorting) Starts-->
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h3>اضافه نقط بيع كروت </h3>
                </div>
            </div>
        </div>


        <!--our form-->

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <form class="needs-validation" novalidate="" action="{{route('addSellerPoints')}}"
                                  method="POST" enctype="multipart/form-data">
                                {{ method_field('POST') }}
                                {{ csrf_field() }}
                                <div class="modal-body">
                                    <div class="form-group">
                                        <div id="shoplocation" style="width:100%;height:350px"></div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom05">عنوان نقط بيع</label>
                                            <input type="text" class="form-control" name="location" id="autocomplete"
                                                   placeholder="البحث عن عنوان">
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom04">اسم نقطه بيع بالعربيه</label>
                                            <input class="form-control" type="text" name="seller_name_ar"
                                                   placeholder="ادخل اسم نقطه بيع بالعربيه"
                                                   required>
                                            <div class="invalid-feedback">{{ $errors->first('seller_name_ar') }}.</div>
                                        </div>
                                    </div>

                                    <div class="form-row">

                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom04">اسم نقطه بيع بالانجليزيه</label>
                                            <input class="form-control" type="text" name="seller_name_en"
                                                   placeholder="ادخل اسم نقطه بيع بالانجليزيه"
                                                   required>
                                            <div class="invalid-feedback">{{ $errors->first('seller_name_en') }}.</div>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom04">اسم نقطه بيع بالكرديه</label>
                                            <input class="form-control" type="text" name="seller_name_kur"
                                                   placeholder="ادخل اسم نقطه بيع بالكرديه"
                                                   required>
                                            <div class="invalid-feedback">{{ $errors->first('seller_name_kur') }}.</div>
                                        </div>

                                        <div class="col-md-6 mb-3" style="display: none">
                                            <label for="validationCustom04">Latitude</label>
                                            <input type="text" required class="form-control" name="lat" value=""
                                                   id="lat" placeholder="Enter lat ">
                                        </div>
                                        <div class="col-md-6 mb-3" style="display: none">
                                            <label for="validationCustom04">longitude</label>
                                            <input type="text" required class="form-control" name="lng" value=""
                                                   id="lng" placeholder="Enter long">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <button class="btn btn-primary" type="submit">اضافه نقطه بيع</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h6> نقط بيع </h6>
                        </div>
                        @if( count($sellerPoints) > 0)
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="display dataTable" id="basic-1">
                                        <thead>
                                        <tr>

                                            <th>اسم نقطه بيع</th>
                                            <th>عنوان نقطه بيع</th>
                                            <th>الاكشن</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($sellerPoints as $points)
                                            <tr>
                                                <td>{{ $points->seller_name_ar }}</td>
                                                <td>{{ $points->location }}</td>
                                                <td>
                                                    <form action="{{ route('destroySellerPoints', $points->id) }}"
                                                          method="post" style="display: inline-block">
                                                        {{ csrf_field() }}
                                                        {{ method_field('delete') }}
                                                        <button type="submit"
                                                                class="btn btn-danger delete btn-sm"><i
                                                                class="fa fa-trash"></i>
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>

                                    </table>
                                </div>
                            </div>

                        @else

                            <h6 class="text-center"> لا توجد نقط بيع مضافه</h6>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script>
        var marker = null;
        var placeSearch, autocomplete;

        function initMap() {
            autocomplete =
                new google.maps.places.Autocomplete((document.getElementById('autocomplete')),
                    {types: ['geocode']});
            var map = new google.maps.Map(document.getElementById('shoplocation'), {
                zoom: 10,
                center: {lat: 30.1258867, lng: 31.374936}
            });
            var MaekerPos = new google.maps.LatLng(0, 0);
            marker = new google.maps.Marker({
                position: MaekerPos,
                map: map
            });
            autocomplete.addListener('place_changed', function () {
                placeMarkerAndPanTo(autocomplete.getPlace().geometry.location, map);
                document.getElementById("lat").value = autocomplete.getPlace().geometry.location.lat();
                document.getElementById("lng").value = autocomplete.getPlace().geometry.location.lng();
            });
            map.addListener('click', function (e) {
                placeMarkerAndPanTo(e.latLng, map);
                document.getElementById("lat").value = e.latLng.lat();
                document.getElementById("lng").value = e.latLng.lng();
            });
        }

        function placeMarkerAndPanTo(latLng, map) {
            map.setZoom(9);
            marker.setPosition(latLng);
            map.panTo(latLng);
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDPN_XufKy-QTSCB68xFJlqtUjHQ8m6uUY&libraries=places&callback=initMap">
    </script>
@endsection
