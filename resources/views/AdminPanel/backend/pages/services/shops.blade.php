@extends('AdminPanel.backend.layouts.default')
@section('content')

    <div class="page-body">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>المتاجر التابعه لقسم {{ $showServices }} </h5>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        @if( count($serviceShops) > 0)
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="display dataTable" id="basic-1">
                                        <thead>
                                        <tr>
                                            <th>صوره المتجر</th>
                                            <th>اسم المتجر</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($serviceShops as $shops)
                                            <tr>
                                                <td><img style="width: 50px" src= {{ $shops->shop_image }} ></td>
                                                <td>{{ $shops->name_app_ar }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        @else

                            <h4 class="text-center"> لا توجد متاجر مضافه لهذا القسم </h4>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
