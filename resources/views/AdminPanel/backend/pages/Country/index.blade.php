@extends('AdminPanel.backend.layouts.default')
@section('content')

    <!-- Right sidebar Ends-->
    <div class="page-body">
        <div class="container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">

                        <div class="page-header-left">
                            @if (auth()->user()->hasPermissionTo('اضافه_الدول'))
                                <button class="btn btn-primary" type="button" data-toggle="modal"
                                        data-target="#country" data-whatever="@test">اضافه دوله
                                </button>
                            @endif

                            <div class="modal fade" id="country" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">اضافه الدول </h5>
                                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <form class="needs-validation" novalidate=""
                                                  action="{{route('AddCountry')}}"
                                                  method="POST" enctype="multipart/form-data">
                                                {{ method_field('POST') }}
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">اسم الدوله بااللغه
                                                        العربيه</label>
                                                    <input class="form-control" name="country_name_ar"
                                                           type="text" value="{{ old('country_name_ar') }}"
                                                           placeholder="ادخل اسم الدوله باللغه العربيه"
                                                           required="">
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('country_name_ar') }}</div>
                                                </div>
                                                <span class="text-danger page-header-left"
                                                      style="color: red;">{{$errors->first('country_name_ar')}}</span>
                                                <div class="form-group">

                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">اسم الدوله باللغه
                                                        الانجليزيه</label>
                                                    <input class="form-control" name="country_name_en"
                                                           type="text" value="{{ old('country_name_en') }}"
                                                           placeholder="ادخل اسم المدينه باللغه الانجليزيه" required>
                                                    <div
                                                        class=" invalid-feedback">{{ $errors->first('country_name_en') }}</div>
                                                </div>
                                                <span class="text-danger page-header-left"
                                                      style="color: red;">{{$errors->first('country_name_en')}}</span>

                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">اسم الدوله بااللغه
                                                        الكرديه</label>
                                                    <input class="form-control" name="country_name_kur"
                                                           type="text" value="{{ old('country_name_kur') }}"
                                                           placeholder="ادخل اسم الدوله باللغه الكرديه"
                                                           required="">
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('country_name_kur') }}</div>
                                                </div>
                                                <span class="text-danger page-header-left"
                                                      style="color: red;">{{$errors->first('country_name_kur')}}</span>

                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">كود الدوله</label>
                                                    <input class="form-control" name="code"
                                                           type="text" value="{{ old('code') }}"
                                                           placeholder="ادخل كود الدوله"
                                                           required="">
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('code') }}</div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">ضريبه الدوله(%)</label>
                                                    <input class="form-control" name="tax"
                                                           type="text" value="{{ old('tax') }}"
                                                           placeholder="ادخل ضريبه الدوله"
                                                           required="">
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('code') }}</div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="validationCustom05" class="page-header-left">صوره
                                                        الدوله</label>
                                                    <div class="custom-file">
                                                        <input class="custom-file-input" type="file"
                                                               name="country_image">
                                                        <label class="custom-file-label" for="validatedCustomFile">اختر
                                                            صوره</label>
                                                        <div
                                                            class="invalid-feedback">{{ $errors->first('country_image') }}</div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn btn-primary" type="submit">اضافه</button>
                                                    <button class="btn btn-secondary" type="button"
                                                            data-dismiss="modal">اغلاق
                                                    </button>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    @if( count($countries) > 0)
                        @if (auth()->user()->hasPermissionTo('عرض_الدول'))
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="display dataTable" id="basic-1">
                                        <thead>
                                        <tr>
                                            <th>صوره</th>
                                            <th>اسم الدوله بالغه العربيه</th>
                                            <th>اسم الدوله بالغه الانجليزيه</th>
                                            <th>اسم الدوله بالغه الكرديه</th>
                                            <th>كود الدوله</th>
                                            <th>ضريبه الدوله</th>
                                            <th>الاكشن</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($countries as $country)
                                            <tr>
                                                <td><img src="{{ $country->country_image }}" style="width: 50px"></td>
                                                <td>{{ $country->country_name_ar }}</td>
                                                <td>{{ $country->country_name_en }}</td>
                                                <td>{{ $country->country_name_kur }}</td>
                                                <td>{{ $country->code }}</td>
                                                <td>{{ $country->tax }}</td>

                                                <td>
                                                    @if (auth()->user()->hasPermissionTo('تعديل_الدول'))
                                                        <button class="btn btn-primary" type="button"
                                                                data-toggle="modal"
                                                                data-target="#{{ $country->id }}"
                                                                data-whatever="@test"><i class="fa fa-edit"></i>
                                                        </button>
                                                    @endif
                                                    @if (auth()->user()->hasPermissionTo('حذف_الدول'))
                                                        <form action="{{ route('destroyCountry', $country->id) }}"
                                                              method="post" style="display: inline-block">
                                                            {{ csrf_field() }}
                                                            {{ method_field('delete') }}
                                                            <button type="submit" class="btn btn-danger delete btn-sm">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </form>
                                                    @endif

                                                    <div class="modal fade" id="{{ $country->id }}" tabindex="-1"
                                                         role="dialog"
                                                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title"> تعديل بيانات
                                                                        {{ $country->country_name_ar }} </h5>
                                                                    <button class="close" type="button"
                                                                            data-dismiss="modal"
                                                                            aria-label="Close">
                                                                        <span aria-hidden="true">×</span></button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <form class="needs-validation" novalidate=""
                                                                          action="{{route('EditCountry',['id'=>$country->id])}}"
                                                                          method="POST" enctype="multipart/form-data">
                                                                        {{ method_field('POST') }}
                                                                        {{ csrf_field() }}

                                                                        <div class="form-group">

                                                                            <label for="validationCustom05"
                                                                                   class="col-form-label page-header-left">اسم
                                                                                الدوله باللغه
                                                                                العربيه</label>
                                                                            <input class="form-control"
                                                                                   name="country_name_ar"
                                                                                   type="text"
                                                                                   value="{{ $country->country_name_ar }}"
                                                                                   placeholder="ادخل اسم الدوله باللغه العربيه"
                                                                                   required="">
                                                                            <div
                                                                                class="invalid-feedback">{{ $errors->first('country_name_ar') }}</div>
                                                                        </div>
                                                                        <span class="text-danger page-header-left"
                                                                              style="color: red;">{{$errors->first('country_name_ar')}}</span>
                                                                        <div class="form-group">

                                                                            <label for="validationCustom05"
                                                                                   class="col-form-label page-header-left">اسم
                                                                                الدوله باللغه
                                                                                الانجليزيه</label>
                                                                            <input class="form-control"
                                                                                   name="country_name_en"
                                                                                   type="text"
                                                                                   value="{{ $country->country_name_en }}"
                                                                                   placeholder="ادخل اسم الدوله باللغه الانجليزيه"
                                                                                   required>
                                                                            <div
                                                                                class=" invalid-feedback">{{ $errors->first('country_name_en') }}</div>
                                                                        </div>
                                                                        <div class="form-group">

                                                                            <label for="validationCustom05"
                                                                                   class="col-form-label page-header-left">اسم
                                                                                الدوله باللغه
                                                                                الكرديه</label>
                                                                            <input class="form-control"
                                                                                   name="country_name_kur"
                                                                                   type="text"
                                                                                   value="{{ $country->country_name_kur }}"
                                                                                   placeholder="ادخل اسم الدوله باللغه الكرديه"
                                                                                   required>
                                                                            <div
                                                                                class=" invalid-feedback">{{ $errors->first('country_name_kur') }}</div>
                                                                        </div>


                                                                        <div class="form-group">
                                                                            <label for="validationCustom05"
                                                                                   class="col-form-label page-header-left">كود
                                                                                الدوله
                                                                            </label>
                                                                            <input class="form-control"
                                                                                   name="code"
                                                                                   type="text"
                                                                                   value="{{ $country->code }}"
                                                                                   placeholder="ادخل كود الدوله"
                                                                                   required>
                                                                            <div
                                                                                class=" invalid-feedback">{{ $errors->first('code') }}</div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label for="validationCustom05"
                                                                                   class="col-form-label page-header-left">ضريبه الدوله (%)
                                                                            </label>
                                                                            <input class="form-control"
                                                                                   name="tax"
                                                                                   type="text"
                                                                                   value="{{ $country->tax }}"
                                                                                   placeholder="ادخل ضريبه الدوله"
                                                                                   required>
                                                                            <div
                                                                                class=" invalid-feedback">{{ $errors->first('tax') }}</div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label for="validationCustom05"
                                                                                   class="page-header-left">صوره
                                                                                الدوله</label>
                                                                            <label for="file-input"
                                                                                   class="image-upload-label">
                                                                                <img alt="upload-service-image"
                                                                                     src="{{$country->country_image}}"
                                                                                     class="thumb"
                                                                                     style="width: 100px"/>
                                                                            </label>
                                                                            <div class="custom-file">
                                                                                <input class="custom-file-input"
                                                                                       type="file"
                                                                                       name="country_image">
                                                                                <label class="custom-file-label"
                                                                                       for="validatedCustomFile">اختر
                                                                                    صوره</label>
                                                                                <div
                                                                                    class="invalid-feedback">{{ $errors->first('country_image') }}</div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="modal-footer">
                                                                            <button class="btn btn-primary"
                                                                                    type="submit">
                                                                                تعديل
                                                                            </button>
                                                                            <button class="btn btn-secondary"
                                                                                    type="button"
                                                                                    data-dismiss="modal">اغلاق
                                                                            </button>
                                                                        </div>
                                                                    </form>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @endif
                    @else

                        <h4 class="text-center"> لا توجد دول مضافه </h4>
                    @endif

                </div>
            </div>
        </div>
    </div>

@endsection
