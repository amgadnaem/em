@extends('AdminPanel.backend.layouts.default')
@section('content')

    <!-- Right sidebar Ends-->
    <div class="page-body">
        <div class="container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">

                        <div class="page-header-left">
                            @if (auth()->user()->hasPermissionTo('اضافه_الدول'))
                                <button class="btn btn-primary" type="button" data-toggle="modal"
                                        data-target="#country" data-whatever="@test">اضافه مدينه
                                </button>
                            @endif

                            <div class="modal fade" id="country" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">اضافه مدينه </h5>
                                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <form class="needs-validation" novalidate=""
                                                  action="{{route('AddCity')}}"
                                                  method="POST" enctype="multipart/form-data">
                                                {{ method_field('POST') }}
                                                {{ csrf_field() }}

                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="page-header-left"> اسم الدوله</label>
                                                    <select name="country_id"
                                                            class="form-control"
                                                            required>
                                                        <option value="">اختر</option>
                                                        @foreach($countries as $country)
                                                            <option
                                                                value="{{ $country->id }}">{{ $country->country_name_ar }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('country_id') }}</div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">اسم المدينه بااللغه
                                                        العربيه</label>
                                                    <input class="form-control" name="city_name_ar"
                                                           type="text" value="{{ old('city_name_ar') }}"
                                                           placeholder="ادخل اسم المدينه باللغه العربيه"
                                                           required="">
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('city_name_ar') }}</div>
                                                </div>
                                                <span class="text-danger page-header-left"
                                                      style="color: red;">{{$errors->first('city_name_ar')}}</span>
                                                <div class="form-group">

                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">اسم المدينه باللغه
                                                        الانجليزيه</label>
                                                    <input class="form-control" name="city_name_en"
                                                           type="text" value="{{ old('city_name_en') }}"
                                                           placeholder="ادخل اسم المدينه باللغه الانجليزيه" required>
                                                    <div
                                                        class=" invalid-feedback">{{ $errors->first('city_name_en') }}</div>
                                                </div>
                                                <span class="text-danger page-header-left"
                                                      style="color: red;">{{$errors->first('city_name_en')}}</span>

                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">اسم المدينه بااللغه
                                                        الكرديه</label>
                                                    <input class="form-control" name="city_name_kur"
                                                           type="text" value="{{ old('city_name_kur') }}"
                                                           placeholder="ادخل اسم الدوله باللغه الكرديه"
                                                           required="">
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('city_name_kur') }}</div>
                                                </div>
                                                <span class="text-danger page-header-left"
                                                      style="color: red;">{{$errors->first('city_name_kur')}}</span>

                                                <div class="modal-footer">
                                                    <button class="btn btn-primary" type="submit">اضافه</button>
                                                    <button class="btn btn-secondary" type="button"
                                                            data-dismiss="modal">اغلاق
                                                    </button>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    @if( count($cities) > 0)
                        @if (auth()->user()->hasPermissionTo('عرض_الدول'))
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="display dataTable" id="basic-1">
                                        <thead>
                                        <tr>
                                            <th>اسم الدوله التابعه لها</th>
                                            <th>اسم المدينه بالغه العربيه</th>
                                            <th>اسم المدينه بالغه الانجليزيه</th>
                                            <th>اسم المدينه بالغه الكرديه</th>
                                            <th>الاكشن</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($cities as $city)
                                            <tr>
                                                <td>{{ $city->getCountry($city->country_id) }}</td>
                                                <td>{{ $city->city_name_ar }}</td>
                                                <td>{{ $city->city_name_en }}</td>
                                                <td>{{ $city->city_name_kur }}</td>

                                                <td>
                                                    @if (auth()->user()->hasPermissionTo('تعديل_الدول'))
                                                        <button class="btn btn-primary" type="button"
                                                                data-toggle="modal"
                                                                data-target="#{{ $city->id }}"
                                                                data-whatever="@test"><i class="fa fa-edit"></i>
                                                        </button>
                                                    @endif
                                                    @if (auth()->user()->hasPermissionTo('حذف_الدول'))
                                                        <form action="{{ route('destroyCity', $city->id) }}"
                                                              method="post" style="display: inline-block">
                                                            {{ csrf_field() }}
                                                            {{ method_field('delete') }}
                                                            <button type="submit" class="btn btn-danger delete btn-sm">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </form>
                                                    @endif

                                                    <div class="modal fade" id="{{ $city->id }}" tabindex="-1"
                                                         role="dialog"
                                                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title"> تعديل بيانات
                                                                        {{ $city->city_name_ar }} </h5>
                                                                    <button class="close" type="button"
                                                                            data-dismiss="modal"
                                                                            aria-label="Close">
                                                                        <span aria-hidden="true">×</span></button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <form class="needs-validation" novalidate=""
                                                                          action="{{route('EditCity',['id'=>$city->id])}}"
                                                                          method="POST" enctype="multipart/form-data">
                                                                        {{ method_field('POST') }}
                                                                        {{ csrf_field() }}

                                                                        <div class="form-group">

                                                                            <label for="validationCustom05"
                                                                                   class="col-form-label page-header-left">اسم
                                                                                المدينه باللغه
                                                                                العربيه</label>
                                                                            <input class="form-control"
                                                                                   name="city_name_ar"
                                                                                   type="text"
                                                                                   value="{{ $city->city_name_ar }}"
                                                                                   placeholder="ادخل اسم المدينه باللغه العربيه"
                                                                                   required="">
                                                                            <div
                                                                                class="invalid-feedback">{{ $errors->first('city_name_ar') }}</div>
                                                                        </div>
                                                                        <span class="text-danger page-header-left"
                                                                              style="color: red;">{{$errors->first('city_name_ar')}}</span>
                                                                        <div class="form-group">

                                                                            <label for="validationCustom05"
                                                                                   class="col-form-label page-header-left">اسم
                                                                                المدينه باللغه
                                                                                الانجليزيه</label>
                                                                            <input class="form-control"
                                                                                   name="city_name_en"
                                                                                   type="text"
                                                                                   value="{{ $city->city_name_en }}"
                                                                                   placeholder="ادخل اسم المدينه باللغه الانجليزيه"
                                                                                   required>
                                                                            <div
                                                                                class=" invalid-feedback">{{ $errors->first('city_name_en') }}</div>
                                                                        </div>
                                                                        <div class="form-group">

                                                                            <label for="validationCustom05"
                                                                                   class="col-form-label page-header-left">اسم
                                                                                المدينه باللغه
                                                                                الكرديه</label>
                                                                            <input class="form-control"
                                                                                   name="city_name_kur"
                                                                                   type="text"
                                                                                   value="{{ $city->city_name_kur }}"
                                                                                   placeholder="ادخل اسم المدينه باللغه الكرديه"
                                                                                   required>
                                                                            <div
                                                                                class=" invalid-feedback">{{ $errors->first('city_name_kur') }}</div>
                                                                        </div>


                                                                        <div class="modal-footer">
                                                                            <button class="btn btn-primary"
                                                                                    type="submit">
                                                                                تعديل
                                                                            </button>
                                                                            <button class="btn btn-secondary"
                                                                                    type="button"
                                                                                    data-dismiss="modal">اغلاق
                                                                            </button>
                                                                        </div>
                                                                    </form>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @endif
                    @else

                        <h4 class="text-center"> لا توجد مدن مضافه </h4>
                    @endif

                </div>
            </div>
        </div>
    </div>
    </div>



    <script>

        function change_status_category(id, value) {
            if (value == 0) {
                value = 1;
            } else {
                value = 0;
            }
            axios.get('updateCategory/' + id + '/' + value)
                .then(function (response) {
                    // alert(response.data.category_status);
                    location.reload();
                })
                .catch(function (error) {
                    console.log(error);
                    alert(error);
                });
        };


    </script>

@endsection
