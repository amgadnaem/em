@extends('AdminPanel.backend.layouts.default')
@section('content')

    <!-- Right sidebar Ends-->
    <div class="page-body">


        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>اضافه برمو كود</h3>
                            <div class="col-md-6">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <!-- Container-fluid starts-->
        <div class="container-fluid">

            <div class="row">
                <div class="col-sm-12">
                    <div class="card">


                        <div class="card-body">

                            <form class="needs-validation" novalidate="" action="{{route('addPromoCode')}}"
                                  method="POST" enctype="multipart/form-data">
                                {{ method_field('POST') }}
                                {{ csrf_field() }}

                                <div class="form-row">

                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom05">ادخل الكود</label>
                                        <input class="form-control" name="promo_code"
                                               type="text"
                                               placeholder="ادخل الكود"
                                               required="">

                                        <div class="invalid-feedback">{{ $errors->first('discounted_price') }}</div>
                                    </div>


                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom04">تاريخ بدايه العرض</label>
                                        <input class="datepicker-here form-control digits"
                                               type="text" required
                                               data-language="en"
                                               name="valid_from"
                                               data-multiple-dates-separator=", " data-position="bottom left"
                                               placeholder="تاريخ بدايه العرض" autocomplete="true">
                                        <div class="invalid-feedback">{{ $errors->first('from_date') }}.</div>
                                    </div>


                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom04">تاريخ نهايه العرض</label>
                                        <input class="datepicker-here form-control digits"
                                               type="text" required
                                               data-language="en"
                                               name="valid_to"
                                               data-multiple-dates-separator=", " data-position="bottom left"
                                               placeholder="تاريخ نهايه العرض" autocomplete="true">
                                        <div class="invalid-feedback">{{ $errors->first('valid_to') }}.</div>
                                    </div>

                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom05">نسبه العرض</label>
                                        <input class="form-control" name="discount_amount"
                                               type="text"
                                               placeholder="نسبه العرض"
                                               required="">

                                        <div class="invalid-feedback">{{ $errors->first('discount_amount') }}</div>
                                    </div>


                                </div>

                                <button class="btn btn-primary" type="submit">اضف</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>


            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        @if( count($all_promo) > 0)
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="display dataTable" id="basic-1">
                                        <thead>
                                        <tr>
                                            <th> رقم الكود</th>
                                            <th>تاريخ بدايه العرض</th>
                                            <th>تاريخ نهايه العرض</th>
                                            <th>نسبه العرض</th>
                                            <th>الاكشن</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($all_promo as $promo)
                                            <tr>
                                                <td>{{ $promo->code }}</td>
                                                <td>{{ $promo->valid_from }}</td>
                                                <td>{{ $promo->valid_to }}</td>
                                                <td>{{ $promo->discount_amount }}</td>
                                                <td>
                                                    <a href="{{ route('editPromo', $promo->id) }}"
                                                       class="btn btn-primary"><i class="fa fa-edit"></i>
                                                    </a>
                                                    <form action="{{ route('destroyPromo', $promo->id) }}"
                                                          method="post" style="display: inline-block">
                                                        {{ csrf_field() }}
                                                        {{ method_field('delete') }}
                                                        <button type="submit" class="btn btn-danger delete btn-sm"><i
                                                                    class="fa fa-trash"></i>
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @else
                            <h4 class="text-center"> لم يتم اضافه اى برمو كود</h4>
                        @endif
                    </div>
                </div>
            </div>

        </div>
        <!-- Container-fluid Ends-->
    </div>


@endsection