@extends('AdminPanel.backend.layouts.default')
@section('content')


    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>تعديل بيانات كود {{ $edit_promo->code }}</h3>
                            <div class="col-md-6">

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">


                        <div class="card-body">

                            <form class="needs-validation" novalidate=""
                                  action="{{route('updatePromo',['id'=> $edit_promo->id]) }}"
                                  method="POST" enctype="multipart/form-data">
                                {{ method_field('POST') }}
                                {{ csrf_field() }}

                                <div class="form-row">

                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom05">كود العرض</label>
                                        <input class="form-control" name="code"
                                               type="text" value="{{$edit_promo->code}}"
                                               required="">

                                        <div class="invalid-feedback">{{ $errors->first('discount_amount') }}</div>
                                    </div>

                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom04">تاريخ بدايه العرض</label>
                                        <input class="datepicker-here form-control digits"
                                               type="text" required
                                               data-language="en"
                                               name="valid_from" value="{{$edit_promo->valid_from}}"
                                               data-multiple-dates-separator=", " data-position="bottom left"
                                               placeholder="تاريخ بدايه العرض" autocomplete="true">
                                        <div class="invalid-feedback">{{ $errors->first('valid_from') }}.</div>
                                    </div>


                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom04">تاريخ نهايه العرض</label>
                                        <input class="datepicker-here form-control digits"
                                               type="text" required
                                               data-language="en"
                                               name="valid_to" value="{{$edit_promo->valid_to}}"
                                               data-multiple-dates-separator=", " data-position="bottom left"
                                               placeholder="تاريخ نهايه العرض" autocomplete="true">
                                        <div class="invalid-feedback">{{ $errors->first('valid_to') }}.</div>
                                    </div>

                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom05">نسبه العرض</label>
                                        <input class="form-control" name="discount_amount"
                                               type="text" value="{{$edit_promo->discount_amount}}"
                                               placeholder="نسبه العرض"
                                               required="">

                                        <div class="invalid-feedback">{{ $errors->first('discount_amount') }}</div>
                                    </div>


                                </div>
                                <input type="submit" class="btn btn-success" value="تعديل">
                            </form>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

    <!-- END PAGE CONTENT WRAPPER -->
@endsection
