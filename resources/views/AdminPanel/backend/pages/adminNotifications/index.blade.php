@extends('AdminPanel.backend.layouts.default')
@section('content')
    <!-- Right sidebar Ends-->
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">

                        <div class="page-header-left">

                            <button class="btn btn-primary" type="button" data-toggle="modal"
                                    data-target="#category" data-whatever="@category">إرسال إشعار
                            </button>
                            <div class="modal fade" id="category" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">إرسال إشعار </h5>
                                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <form class="needs-validation" novalidate=""
                                                  action="{{route('sendAdminNotification')}}"
                                                  method="POST" enctype="multipart/form-data">
                                                {{ method_field('POST') }}
                                                {{ csrf_field() }}

                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="page-header-left"> اسم الدوله</label>
                                                    <select name="country_id" id="country"
                                                            class="form-control"
                                                            required>
                                                        <option value="">اختر</option>
                                                        @foreach($data['countries'] as $country)
                                                            <option
                                                                value="{{ $country->id }}">{{ $country->country_name_ar }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('country_id') }}</div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="page-header-left"> المرسَل إليه</label>
                                                    <select name="person" id="persons"
                                                            class="form-control"
                                                            required>
                                                        <option disabled selected value="">اختر الدوله اولا</option>
                                                        {{--                                                        <option value="All_users">كل المستخدمين</option>--}}
                                                        {{--                                                        <option value="All_shops">كل المتاجر</option>--}}
                                                        {{--                                                        <optgroup label="المستخدمين">--}}
                                                        {{--                                                            @foreach($data['users'] as $user)--}}
                                                        {{--                                                                <option--}}
                                                        {{--                                                                    value="{{$user->id}}|user">{{$user->first_name}} {{$user->last_name}}</option>--}}
                                                        {{--                                                            @endforeach--}}
                                                        {{--                                                        </optgroup>--}}
                                                        {{--                                                        <hr/>--}}
                                                        {{--                                                        <optgroup label="المتاجر">--}}
                                                        {{--                                                            @foreach($data['shops'] as $shop)--}}
                                                        {{--                                                                <option--}}
                                                        {{--                                                                    value="{{$shop->id}}|shop">{{$shop->name_en}}</option>--}}
                                                        {{--                                                            @endforeach--}}
                                                        {{--                                                        </optgroup>--}}
                                                    </select>
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('person') }}</div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left"> عنوان الرساله</label>
                                                    <input class="form-control" name="title"

                                                           type="text"
                                                           placeholder="ادخل عنوان الرساله"
                                                           required="">
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('title_ar') }}</div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="page-header-left"> نص الرسالة  </label>
                                                    <textarea class="form-control" name="message" cols="20"
                                                              rows="4" required></textarea>
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn btn-primary" type="submit">إرسال</button>
                                                    <button class="btn btn-secondary" type="button"
                                                            data-dismiss="modal">اغلاق
                                                    </button>
                                                </div>
                                            </form>

                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    @if( count($data['notifications']) > 0)

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display dataTable" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>المسلسل</th>
                                        <th>موجهة لـ</th>
                                        <th>النوع</th>
                                        <th>عنوان الرساله</th>
                                        <th>نص الرساله</th>
                                        <th>العمليات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data['notifications'] as $notification)
                                        <tr>
                                            <td>
                                                {{$notification->id}}
                                            </td>
                                            <td>
                                                @if ($notification->type == '0')
                                                    <span class="badge badge-success">كل المستخدمين</span>
                                                @elseif ($notification->type == '1')
                                                    <span class="badge badge-success">كل المتاجر</span>
                                                @elseif ($notification->type == '2')
                                                    {{--                                                    <span class="badge badge-info">{{$notification->user->first_name}}</span>--}}
                                                    <span
                                                        class="badge badge-success">{{$notification->user->first_name}}</span>
                                                @elseif ($notification->type == '3')
                                                    {{--                                                    <span class="badge badge-info">{{$notification->shop->name}}</span>--}}
                                                    <span
                                                        class="badge badge-success">{{$notification->shop->name_en}}</span>
                                                @elseif ($notification->type == '5')
                                                    {{--                                                    <span class="badge badge-info">{{$notification->shop->name}}</span>--}}
                                                    <span
                                                        class="badge badge-success">{{$notification->user->first_name}}</span>
                                                @endif

                                            </td>
                                            <td>
                                                @if ($notification->type == '0')
                                                    <span class="badge badge-info">كل المستخدمين</span>
                                                @elseif ($notification->type == '1')
                                                    <span class="badge badge-info">كل المتاجر</span>
                                                @elseif ($notification->type == '2')
                                                    {{--                                                    <span class="badge badge-info">{{$notification->user->first_name}}</span>--}}
                                                    <span class="badge badge-info">مستخدم</span>
                                                @elseif ($notification->type == '3')
                                                    {{--                                                    <span class="badge badge-info">{{$notification->shop->name}}</span>--}}
                                                    <span class="badge badge-info">متجر</span>
                                                @elseif ($notification->type == '5')
                                                    {{--                                                    <span class="badge badge-info">{{$notification->shop->name}}</span>--}}
                                                    <span
                                                        class="badge badge-success">{{$notification->user->first_name}}</span>

                                                @endif
                                            </td>
                                            <td>{{isset($notification->title) ? str_limit($notification->title , 100) : ""}}</td>
                                            <td>{{isset($notification->message) ? str_limit($notification->message , 100) : ""}}</td>
                                            <td>
                                                <form
                                                    action="{{ route('destroyAdminNotification', $notification->id) }}"
                                                    method="post" style="display: inline-block">
                                                    {{ csrf_field() }}
                                                    {{ method_field('delete') }}
                                                    <button type="submit" class="btn btn-danger delete btn-sm">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @else
                        <h4 class="text-center"> لا توجد إشعارات </h4>
                    @endif

                </div>
            </div>
        </div>
    </div>

@endsection
