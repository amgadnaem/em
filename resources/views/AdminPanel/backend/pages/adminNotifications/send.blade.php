@extends('admin::admin.layouts.master_admin')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/select2.css') }}">
@endsection
@section('content')
    <!-- Container-fluid starts-->

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <form class="needs-validation"  novalidate="" method="post" action="{{route('admin.notifications.send')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">
                                <div class="mb-2">
                                    <div class="col-form-label">المرسل إليه</div>
                                    <select class="js-example-basic-single col-sm-12 custom-select" style="text-align: right;" name="person" required>
                                        <option disabled selected value="">اختر المرسَل إليه</option>
                                        <option value="All_users">كل المستخدمين (سيتم إرسال إشعار)</option>
                                        <option value="All_shops">كل المتاجر (سيتم إرسال بريد)</option>
                                        <optgroup label="المستخدمين (سيتم إرسال إشعار)">
                                            @foreach($data['users'] as $user)
                                                <option value="{{$user->id}}|user">{{$user->name}}</option>
                                            @endforeach
                                        </optgroup>
                                        <optgroup label="المتاجر (سيتم إرسال بريد)">
                                            @foreach($data['shops'] as $shop)
                                                <option value="{{$shop->id}}|shop">{{$shop->name}}</option>
                                            @endforeach
                                        </optgroup>
                                    </select>
                                    <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                    <div class="valid-feedback">بيانات صحيحة</div>
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label float-right" for="message">نص الرسالة</label>
                                    <textarea class="form-control" name="message" id="message" cols="20" rows="4" required></textarea>
                                    <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                    <div class="valid-feedback">بيانات صحيحة</div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-primary float-right" type="submit">حفظ</button>
                                <button class="btn btn-secondary" type="button" data-dismiss="modal">إغلاق</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection

@section('scripts')
    <script src="{{ url('/assets/js/select2/select2.full.min.js') }}"></script>
    <script src="{{ url('/assets/js/select2/select2-custom.js') }}"></script>
    <script src="{{ url('/assets/js/form-validation-custom.js') }}"></script>
@endsection
