@extends('AdminPanel.backend.layouts.default')
@section('content')
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <!-- Right sidebar Ends-->
    <div class="page-body">

        <!-- Default ordering (sorting) Starts-->
        @if (auth()->user()->hasPermissionTo('عرض_طلبات'))
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4>عرض الطلبات </h4>
                    </div>

                    @if( count($all_orders) > 0)


                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <form class="needs-validation" novalidate=""
                                                  action="{{route('ordersSearch')}}"
                                                  method="POST">
                                                {{ method_field('POST') }}
                                                {{ csrf_field() }}
                                                <div class="form-row">
                                                    <div class="col-md-4 mb-3">
                                                        <input class="datepicker-here form-control digits" name="from"
                                                               type="text" required
                                                               data-language="en"
                                                               data-multiple-dates-separator=", "
                                                               data-position="bottom left"
                                                               placeholder="بدايه من " autocomplete="true">
                                                        <div class="invalid-feedback">{{ $errors->first('from') }}.
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4 mb-3">

                                                        <input class="datepicker-here form-control digits" name="to"
                                                               type="text" required
                                                               data-language="en"
                                                               data-multiple-dates-separator=", "
                                                               data-position="bottom left"
                                                               placeholder="نهايه الى" autocomplete="true">
                                                        <div class="invalid-feedback">{{ $errors->first('to') }}.</div>
                                                    </div>
                                                    <div class="col-md-4 mb-3">

                                                        <button class="btn btn-primary" type="submit">بحث</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="all_orders" class="display dataTable">
                                    <thead>
                                    <tr id="filters">
                                        <th>حاله الطلب</th>
                                    </tr>
                                    <tr>
                                        <th>حاله الطلب</th>
                                        <th>رقم الطلب</th>
                                        <th>سعر الطلب</th>
                                        <th>تاريخ الطلب</th>
                                        <th>المستخدم</th>
                                        {{--                                    <th>عنوان صاحب الطلب</th>--}}
                                        <th>عنوان صاحب الطلب</th>
                                        <th>الاكشن</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($all_orders as $orders)
                                        <tr>
                                            <td>{{ $orders->getStatus($orders->order_status)}}</td>
                                            <td>{{ $orders->order_number }}</td>
                                            <td>{{ $orders->subtotal_fees }}</td>
                                            <td>{{ $orders->order_date }}</td>
                                            <td>{{ !empty($orders->user->name) ? $orders->user->name : null }}</td>

                                            {{--                                        <td>{{ $orders->address->address }}</td>--}}
                                            <td><p><a href="{{$orders->address['google_location']}}" target="_blank">العنوان
                                                        على الخريطه</a></p></td>
                                            <td>
                                                <a href="{{ route('order_details',['id'=> $orders->id]) }}">
                                                    <i class="btn btn-primary fa fa-eye"></i>
                                                </a>
                                                @if (auth()->user()->hasPermissionTo('تعديل_طلبات'))
                                                    <button class="btn btn-primary" type="button" data-toggle="modal"
                                                            data-target="#{{ $orders->id }}"
                                                            data-whatever="@test"><i class="fa fa-edit"></i>
                                                    </button>
                                                @endif
                                                <div class="modal fade" id="{{ $orders->id }}" tabindex="-1"
                                                     role="dialog"
                                                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title"> تعديل حاله
                                                                    {{ $orders->order_number .'#' }} </h5>

                                                                <button class="close" type="button"
                                                                        data-dismiss="modal"
                                                                        aria-label="Close">
                                                                    <span aria-hidden="true">×</span></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form class="needs-validation" novalidate=""
                                                                      action="{{route('EditStatusOrder',['id'=>$orders->id])}}"
                                                                      method="POST" enctype="multipart/form-data">
                                                                    {{ method_field('POST') }}
                                                                    {{ csrf_field() }}

                                                                    <div class="form-group">

                                                                        <label for="validationCustom05"
                                                                               class="page-header-left">حاله الطلب
                                                                        </label>
                                                                        <select name="order_status"
                                                                                class="form-control"
                                                                                required>

                                                                            @foreach($order_status as $status)
                                                                                @if($status->id == $orders->order_status)
                                                                                    <option
                                                                                            value="{{ $status->id }}"
                                                                                            selected>{{ $status->status_name_ar }}</option>
                                                                                @else
                                                                                    <option
                                                                                            value="{{ $status->id }}">{{ $status->status_name_ar }}</option>
                                                                                @endif
                                                                            @endforeach
                                                                        </select>
                                                                        <div
                                                                                class="invalid-feedback">{{ $errors->first('order_status') }}</div>
                                                                    </div>

                                                                    <div class="modal-footer">
                                                                        <button class="btn btn-primary"
                                                                                type="submit">
                                                                            تعديل
                                                                        </button>
                                                                        <button class="btn btn-secondary"
                                                                                type="button"
                                                                                data-dismiss="modal">اغلاق
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <a class="btnprn btn btn-primary "
                                                   href="{{ route('print', $orders->id) }}"><i
                                                            class="icofont icofont-printer"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    @else
                        <h4 class="text-center">لا توجد طلبات </h4>
                    @endif
                </div>
            </div>
        @endif
    </div>

    <script type="text/javascript">

        $(document).ready(function () {
            $('#all_orders').DataTable({
                responsive: true,
                ordering: true,
                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;

                        var select = $('<select><option value="">اختر</option></select>')
                            .appendTo($("#filters").find("th").eq(column.index()))
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val());

                                column.search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });
                        column.data().unique().sort().each(function (d, j) {
                            $(select).append('<option value="' + d + '">' + d + '</option>')
                        });
                    });
                }
            });
        });
    </script>


@endsection







