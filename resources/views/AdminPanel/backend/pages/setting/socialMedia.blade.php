@extends('AdminPanel.backend.layouts.default')
@section('content')

    <!-- Right sidebar Ends-->

    <div class="page-body">
        <div class="container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">

                        <div class="page-header-left">
                            <button class="btn btn-primary" type="button" data-toggle="modal"
                                    data-target="#city" data-whatever="@test">اضافه سوشيال ميديا
                            </button>
                            <div class="modal fade" id="city" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">اضافه سوشيال ميديا </h5>
                                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <form class="needs-validation" novalidate=""
                                                  action="{{route('AddSocialMedia')}}"
                                                  method="POST">
                                                {{ method_field('POST') }}
                                                {{ csrf_field() }}
                                                <div class="form-group">

                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">اسم سوشيال ميديا
                                                    </label>
                                                    <input class="form-control" name="key"
                                                           type="text" value="{{ old('key') }}"
                                                           placeholder="ادخل اسم سوشيال ميديا"
                                                           required="">
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('key') }}</div>
                                                </div>
                                                <span class="text-danger page-header-left"
                                                      style="color: red;">{{$errors->first('key')}}</span>
                                                <div class="form-group">

                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">العنوان
                                                    </label>
                                                    <input class="form-control" name="value"
                                                           type="text" value="{{ old('value') }}"
                                                           placeholder="ادخل بيانات السوشيال ميديا" required>
                                                    <div
                                                        class=" invalid-feedback">{{ $errors->first('value') }}</div>
                                                </div>
                                                <span class="text-danger page-header-left"
                                                      style="color: red;">{{$errors->first('value')}}</span>
                                                <div class="modal-footer">
                                                    <button class="btn btn-primary" type="submit">اضافه</button>
                                                    <button class="btn btn-secondary" type="button"
                                                            data-dismiss="modal">اغلاق
                                                    </button>
                                                </div>

                                            </form>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    @if( count($socialMedia) > 0)
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display dataTable" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>اسم السوشيال ميديا</th>
                                        <th>بيانات السوشيال ميديا</th>
                                        <th>حاله السوشيال ميديا</th>
                                        <th>الاكشن</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($socialMedia as $media)
                                        <tr>

                                            <td>{{ $media->key }}</td>
                                            <td>{{ $media->value }}</td>
                                            <td>
                                                <div class="media-body ">
{{--                                                    {{ $media->status == 2 ? 'مفعل' : 'غير مفعل' }}--}}
                                                    <label class="switch">
                                                        <input type="checkbox"
                                                               {{ $media->status == 2 ? 'checked' : '' }}
                                                               onchange="change_status_media({{ $media->id }},{{ $media->status }})"><span
                                                            class="switch-state"></span>

                                                    </label>
                                                </div>
                                            </td>

                                            <td>
                                                <button class="btn btn-primary" type="button" data-toggle="modal"
                                                        data-target="#{{ $media->id }}"
                                                        data-whatever="@socialMedia"><i class="fa fa-edit"></i>
                                                </button>
                                                <div class="modal fade" id="{{ $media->id }}" tabindex="-1"
                                                     role="dialog"
                                                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title"> تعديل بيانات
                                                                    {{ $media->key }} </h5>
                                                                <button class="close" type="button"
                                                                        data-dismiss="modal"
                                                                        aria-label="Close">
                                                                    <span aria-hidden="true">×</span></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form class="needs-validation" novalidate=""
                                                                      action="{{route('EditMedia',['id'=>$media->id])}}"
                                                                      method="POST" enctype="multipart/form-data">
                                                                    {{ method_field('POST') }}
                                                                    {{ csrf_field() }}
                                                                    <div class="form-group">
                                                                        <label for="validationCustom05"
                                                                               class="col-form-label page-header-left">اسم
                                                                            السوشيال ميديا
                                                                        </label>
                                                                        <input class="form-control" name="key"
                                                                               type="text"
                                                                               value="{{ $media->key }}"
                                                                               placeholder="ادخل اسم السوشيال ميديا"
                                                                               readonly>
                                                                        <div class="invalid-feedback">{{ $errors->first('key') }}</div>
                                                                    </div>
                                                                    <span class="text-danger page-header-left"
                                                                          style="color: red;">{{$errors->first('key')}}</span>
                                                                    <div class="form-group">

                                                                        <label for="validationCustom05"
                                                                               class="col-form-label page-header-left">اسم
                                                                            بيانات السوشيال ميديا</label>
                                                                        <input class="form-control" name="value"
                                                                               type="text"
                                                                               value="{{ $media->value }}"
                                                                               placeholder="ادخل بيانات السوشيال ميديا"
                                                                               required>
                                                                        <div
                                                                            class=" invalid-feedback">{{ $errors->first('value') }}</div>
                                                                    </div>

                                                                    <div class="modal-footer">
                                                                        <button class="btn btn-primary"
                                                                                type="submit">
                                                                            تعديل
                                                                        </button>
                                                                        <button class="btn btn-secondary"
                                                                                type="button"
                                                                                data-dismiss="modal">اغلاق
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>


                                            </td>


                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    @else

                        <h4 class="text-center"> لا توجد اقسام مضافه للمتاجر</h4>
                    @endif

                </div>
            </div>
        </div>
    </div>



    <script>

        function change_status_media(id, value) {
            if (value == 0) {
                value = 2;
            } else {
                value = 0;
            }
            axios.get('updateStatusMedia/' + id + '/' + value)
                .then(function (response) {
                    // alert(response.data.success);
                     location.reload();
                })
                .catch(function (error) {
                    console.log(error);
                    alert(error);
                });
        };


    </script>

@endsection
