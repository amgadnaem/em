@extends('AdminPanel.backend.layouts.default')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h4>الاعدادات</h4>

                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">

                        </div>
                        <form class="form theme-form" action="{{route('settings.update',1)}}" method="POST"
                              enctype="multipart/form-data">

                            {{ csrf_field() }}
                            <div class="card-body">

                                <div class="row">

                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="form-group mb-0">
                                            <label for="exampleFormControlTextarea4">عن التطبيق باللغه العربيه</label>
                                            <textarea class="form-control" name="about_us_ar" id="about_us_ar"
                                                      rows="3"> {{$setting->about_us_ar}}</textarea>
                                            <small class="text-danger">{{ $errors->first('about_us_ar') }}</small>
                                        </div>
                                    </div>

                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="form-group mb-0">
                                            <label for="exampleFormControlTextarea4">عن التطبيق باللغه
                                                الانجليزيه</label>
                                            <textarea class="form-control" name="about_us_en" id="about_us_en"
                                                      rows="3"> {{$setting->about_us_en}}</textarea>
                                            <small class="text-danger">{{ $errors->first('about_us_en') }}</small>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="form-group mb-0">
                                            <label for="exampleFormControlTextarea4">عن التطبيق باللغه
                                                الكرديه</label>
                                            <textarea class="form-control" name="about_us_en" id="about_us_en"
                                                      rows="3"> {{$setting->about_us_kur}}</textarea>
                                            <small class="text-danger">{{ $errors->first('about_us_kur') }}</small>
                                        </div>
                                    </div>

                                </div>
                                <br>

                                <div class="row">

                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="form-group mb-0">
                                            <label for="exampleFormControlTextarea4">الشروط والاحكام باللغه العربيه</label>
                                            <textarea class="form-control" name="terms_ar"
                                                      rows="3"> {{$setting->terms_ar}}</textarea>
                                            <small class="text-danger">{{ $errors->first('terms_ar') }}</small>
                                        </div>
                                    </div>

                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="form-group mb-0">
                                            <label for="exampleFormControlTextarea4">الشروط والاحكام باللغه
                                                الانجليزيه</label>
                                            <textarea class="form-control" name="terms_en"
                                                      rows="3"> {{$setting->terms_en}}</textarea>
                                            <small class="text-danger">{{ $errors->first('terms_en') }}</small>
                                        </div>
                                    </div>

                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="form-group mb-0">
                                            <label for="exampleFormControlTextarea4">الشروط والاحكام باللغه الكرديه</label>
                                            <textarea class="form-control" name="terms_ar"
                                                      rows="3"> {{$setting->terms_ar}}</textarea>
                                            <small class="text-danger">{{ $errors->first('terms_kur') }}</small>
                                        </div>
                                    </div>

                                </div>
                                <br>

                                <br>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-primary" type="submit">تعديل</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection






