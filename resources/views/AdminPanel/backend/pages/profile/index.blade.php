@extends('AdminPanel.backend.layouts.default')
@section('content')

    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>تعديل البروفايل </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="edit-profile">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title mb-0">البروفايل الشخصى</h4>
                                <div class="card-options"><a class="card-options-collapse" href="edit-profile.html#"
                                                             data-toggle="card-collapse"><i
                                            class="fe fe-chevron-up"></i></a><a class="card-options-remove"
                                                                                href="edit-profile.html#"
                                                                                data-toggle="card-remove"><i
                                            class="fe fe-x"></i></a></div>
                            </div>
                            <div class="card-body">
                                <form class="needs-validation" novalidate=""
                                      method="post" action="{{ route('editProfile',['id'=> Auth::user()->id]) }}"
                                      enctype="multipart/form-data">
                                    {{ method_field('POST') }}
                                    {{ csrf_field() }}
                                    <div class="row mb-2">
                                        <div class="col-auto"><img class="img-70 rounded-circle" alt=""
                                                                   src="{{ Auth::user()->admin_image }}">
                                        </div>
                                        <div class="col">
                                            <h3 class="mb-1">{{ Auth::user()->name }}</h3>
                                            <p class="mb-4">مدير النظام</p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label"> الاسم</label>
                                        <input class="form-control" name="admin_name"
                                               placeholder="your-email@domain.com"
                                               value="{{ Auth::user()->name }}">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">البريد الالكترونى</label>
                                        <input class="form-control" name="admin_email"
                                               placeholder="your-email@domain.com"
                                               value="{{ Auth::user()->email }}">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label"> رقم الهاتف</label>
                                        <input class="form-control" name="admin_phone"
                                               placeholder="your-email@domain.com"
                                               value="{{ Auth::user()->phone }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="validationCustom05" class="page-header-left">الصوره
                                            الشخصيه</label>
                                        <div class="custom-file">
                                            <input class="custom-file-input" type="file"
                                                   name="admin_image">
                                            <label class="custom-file-label" for="validatedCustomFile">اختر
                                                صوره</label>
                                            <div
                                                class="invalid-feedback">{{ $errors->first('admin_image') }}</div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <img src="{{ Auth::user()->admin_image }}"
                                             style="width: 100px"
                                             class="img-thumbnail image-preview" alt="">
                                    </div>
                                    <div class="form-footer">
                                        <button class="btn btn-primary btn-block">تعديل</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
