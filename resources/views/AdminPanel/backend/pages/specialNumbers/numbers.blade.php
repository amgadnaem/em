@extends('AdminPanel.backend.layouts.default')
@section('content')

    <!-- Right sidebar Ends-->
    <div class="page-body">
        <div class="container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">

                        <div class="page-header-left">

                            <button class="btn btn-primary" type="button" data-toggle="modal"
                                    data-target="#category" data-whatever="@category">اضافه ارقام مميزة
                            </button>
                            <div class="modal fade" id="category" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">اضافه ارقام مميزة </h5>
                                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <form class="needs-validation" novalidate=""
                                                  action="{{route('addSpecialNumber')}}"
                                                  method="POST" enctype="multipart/form-data">
                                                {{ method_field('POST') }}
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="page-header-left"> عدد الأرقام</label>
                                                    <select name="number_id"
                                                            class="form-control"
                                                            required>
                                                        <option value="">اختر</option>
                                                        @foreach($prices as $price)
                                                            <option
                                                                value="{{ $price->id }}">{{ $price->description }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('number_id') }}</div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn btn-primary" type="submit">اضافه</button>
                                                    <button class="btn btn-secondary" type="button"
                                                            data-dismiss="modal">اغلاق
                                                    </button>
                                                </div>
                                            </form>

                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    @if( count($special_numbers) > 0)

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display dataTable" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>رقم المسلسل</th>
                                        <th>الرقم المميز</th>
                                        <th>السعر</th>
                                        <th>الاكشن</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($special_numbers as $special_number)
                                        <tr>
                                            <td>{{ $special_number->id }}</td>
                                            <td>{{ $special_number->special_number }}</td>
                                            <td>{{ $special_number->cost->price }}</td>
                                            <td>
                                                {{--                                                    @if (auth()->user()->hasPermissionTo('حذف_الدول'))--}}
                                                <form action="{{ route('destroySpecialNumber', $special_number->id) }}"
                                                      method="post" style="display: inline-block">
                                                    {{ csrf_field() }}
                                                    {{ method_field('delete') }}
                                                    <button type="submit" class="btn btn-danger delete btn-sm">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </form>
                                                {{--                                                    @endif--}}

                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @else
                        <h4 class="text-center"> لا توجد أرقام مميزة </h4>
                    @endif

                </div>
            </div>
        </div>


    </div>



    <script>

        function change_status_packing(id, value) {
            if (value == 0) {
                value = 1;
            } else {
                value = 0;
            }
            axios.get('updateStatusPacking/' + id + '/' + value)
                .then(function (response) {
                    // alert(response.data.category_status);
                    location.reload();
                })
                .catch(function (error) {
                    console.log(error);
                    alert(error);
                });
        };


    </script>

@endsection
