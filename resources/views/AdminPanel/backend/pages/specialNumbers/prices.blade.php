@extends('AdminPanel.backend.layouts.default')
@section('content')

    <!-- Right sidebar Ends-->
    <div class="page-body">
        <div class="container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">

                        <div class="page-header-left">
{{--                            @if (auth()->user()->hasPermissionTo('اضافه_الدول'))--}}
                                <button class="btn btn-primary" type="button" data-toggle="modal"
                                        data-target="#price" data-whatever="@test">اضافه سعر
                                </button>
{{--                            @endif--}}

                            <div class="modal fade" id="price" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">اضافه سعر </h5>
                                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <form class="needs-validation" novalidate=""
                                                  action="{{route('AddPrice')}}"
                                                  method="POST" enctype="multipart/form-data">
                                                {{ method_field('POST') }}
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">الوصف
                                                    </label>
                                                    <input class="form-control" name="description"
                                                           type="text" value="{{ old('description') }}"
                                                           placeholder="1-9 , 11-99 , 111-999 ..etc"
                                                           required="">
                                                    <div class="invalid-feedback">{{ $errors->first('description') }}</div>
                                                </div>
                                                <span class="text-danger page-header-left"
                                                      style="color: red;">{{$errors->first('description')}}</span>
                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left"> عدد الأرقام
                                                        </label>
                                                    <input class="form-control" name="number_of_digits"
                                                           type="number" value="{{ old('number_of_digits') }}"
                                                           placeholder=" عدد الأرقام"
                                                           required="">
                                                    <div
                                                            class="invalid-feedback">{{ $errors->first('number_of_digits') }}</div>
                                                </div>
                                                <span class="text-danger page-header-left"
                                                      style="color: red;">{{$errors->first('number_of_digits')}}</span>
                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left"> السعر
                                                    </label>
                                                    <input class="form-control" name="price"
                                                           type="number" value="{{ old('price') }}"
                                                           placeholder="السعر"
                                                           required="">
                                                    <div
                                                            class="invalid-feedback">{{ $errors->first('price') }}</div>
                                                </div>
                                                <span class="text-danger page-header-left"
                                                      style="color: red;">{{$errors->first('price')}}</span>
                                                <div class="modal-footer">
                                                    <button class="btn btn-primary" type="submit">اضافه</button>
                                                    <button class="btn btn-secondary" type="button"
                                                            data-dismiss="modal">اغلاق
                                                    </button>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
{{--                    @if( count($cities) > 0)--}}
{{--                        @if (auth()->user()->hasPermissionTo('عرض_الدول'))--}}
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="display dataTable" id="basic-1">
                                        <thead>
                                        <tr>
                                            <th>المسلسل</th>
                                            <th>الوصف</th>
                                            <th>عدد الأرقام</th>
                                            <th>السعر</th>
                                            <th>الاكشن</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($prices as $price)
                                            <tr>
                                                <td>{{ $price->id }}</td>
                                                <td>{!!  substr(strip_tags($price->description ), 0, 200) !!}</td>
                                                <td>{{ $price->number_of_digits }}</td>
                                                <td>{{ $price->price }}</td>

                                                <td>
{{--                                                    @if (auth()->user()->hasPermissionTo('تعديل_الدول'))--}}
                                                        <button class="btn btn-primary" type="button"
                                                                data-toggle="modal"
                                                                data-target="#{{ $price->id }}"
                                                                data-whatever="@test"><i class="fa fa-edit"></i>
                                                        </button>
{{--                                                    @endif--}}
{{--                                                    @if (auth()->user()->hasPermissionTo('حذف_الدول'))--}}
                                                        <form action="{{ route('destroyPrice', $price->id) }}"
                                                              method="post" style="display: inline-block">
                                                            {{ csrf_field() }}
                                                            {{ method_field('delete') }}
                                                            <button type="submit" class="btn btn-danger delete btn-sm">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </form>
{{--                                                    @endif--}}

                                                    <div class="modal fade" id="{{ $price->id }}" tabindex="-1"
                                                         role="dialog"
                                                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title"> تعديل بيانات
                                                                        {{ $price->description }} </h5>
                                                                    <button class="close" type="button"
                                                                            data-dismiss="modal"
                                                                            aria-label="Close">
                                                                        <span aria-hidden="true">×</span></button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <form class="needs-validation" novalidate=""
                                                                          action="{{route('EditPrice',['id'=>$price->id])}}"
                                                                          method="POST" enctype="multipart/form-data">
                                                                        {{ method_field('POST') }}
                                                                        {{ csrf_field() }}

                                                                        <div class="form-group">

                                                                            <label for="validationCustom05"
                                                                                   class="col-form-label page-header-left">
                                                                                الوصف
                                                                                </label>
                                                                            <input class="form-control"
                                                                                   name="description"
                                                                                   type="text"
                                                                                   value="{{ $price->description }}"
                                                                                   placeholder="الوصف"
                                                                                   required="">
                                                                            <div
                                                                                    class="invalid-feedback">{{ $errors->first('description') }}</div>
                                                                        </div>
                                                                        <span class="text-danger page-header-left"
                                                                              style="color: red;">{{$errors->first('description')}}</span>
                                                                        <div class="form-group">
                                                                            <label for="validationCustom05"
                                                                                   class="col-form-label page-header-left">عدد الأرقام</label>
                                                                            <input class="form-control"
                                                                                   name="number_of_digits"
                                                                                   type="number"
                                                                                   value="{{ $price->number_of_digits }}"
                                                                                   placeholder="ادخل عدد الأرقام"
                                                                                   required>
                                                                            <div
                                                                                    class=" invalid-feedback">{{ $errors->first('number_of_digits') }}</div>
                                                                        </div>
                                                                        <span class="text-danger page-header-left"
                                                                              style="color: red;">{{$errors->first('number_of_digits')}}</span>

                                                                        <div class="form-group">

                                                                            <label for="validationCustom05"
                                                                                   class="col-form-label page-header-left">السعر</label>
                                                                            <input class="form-control"
                                                                                   name="price"
                                                                                   type="text"
                                                                                   value="{{ $price->price }}"
                                                                                   placeholder="ادخل السعر"
                                                                                   required>
                                                                            <div
                                                                                    class=" invalid-feedback">{{ $errors->first('price') }}</div>
                                                                        </div>
                                                                        <span class="text-danger page-header-left"
                                                                              style="color: red;">{{$errors->first('price')}}</span>
                                                                        <div class="modal-footer">
                                                                            <button class="btn btn-primary"
                                                                                    type="submit">
                                                                                تعديل
                                                                            </button>
                                                                            <button class="btn btn-secondary"
                                                                                    type="button"
                                                                                    data-dismiss="modal">اغلاق
                                                                            </button>
                                                                        </div>
                                                                    </form>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
{{--                        @endif--}}
{{--                    @else--}}

{{--                        <h4 class="text-center"> لا توجد مدن مضافه </h4>--}}
{{--                    @endif--}}

                </div>
            </div>
        </div>
    </div>
    </div>



    <script>

        function change_status_category(id, value) {
            if (value == 0) {
                value = 1;
            } else {
                value = 0;
            }
            axios.get('EditPrice/' + id + '/' + value)
                .then(function (response) {
                    // alert(response.data.category_status);
                    location.reload();
                })
                .catch(function (error) {
                    console.log(error);
                    alert(error);
                });
        };


    </script>

@endsection
