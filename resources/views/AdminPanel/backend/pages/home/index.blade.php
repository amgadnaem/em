@extends('AdminPanel.backend.layouts.default')
@section('content')

    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3> الصفحه الرئيسيه</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xl-12">
                    <div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel">
                        <ol class="carousel-indicators">
                            @foreach($allSliders as $key => $slider)
                                <li data-target="#carouselExampleIndicators" data-slide-to="{{$key}}"
                                    class="active"></li>
                            @endforeach
                        </ol>
                        <div class="carousel-inner">
                            @foreach($allSliders as $slider)
                                <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                                    <img class="d-block w-100" src="{{ $slider->image }}"
                                         style="height: 400px" alt="{{$slider->image}}">
                                </div>
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                           data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                           data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
            <br><br>
            <div class="row">
                <div class="col-sm-3 col-xl-3 col-lg-3">
                    <div class="card o-hidden">
                        <div class=" b-r-4 card-body" style="background-color: #1D2D44;color: #fff">
                            <div class="media static-top-widget">
                                <div class="align-self-center text-center"><i data-feather="users"></i></div>
                                <div class="media-body"><span class="m-0">عدد المستخدمين</span>
                                    <h4 class="mb-0 counter">{{$users}}</h4>
                                    <i class="icon-bg" data-feather="users"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-sm-3 col-xl-3 col-lg-3">
                    <div class="card o-hidden">
                        <div class="bg-primary b-r-4 card-body">
                            <div class="media static-top-widget">
                                <div class="align-self-center text-center"><i data-feather="users"></i></div>
                                <div class="media-body"><span class="m-0">عدد المناديب</span>
                                    <h4 class="mb-0 counter">{{ $delegate }}</h4>
                                    <i class="icon-bg" data-feather="message-square"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-sm-3 col-xl-3 col-lg-3">
                    <div class="card o-hidden">
                        <div class="bg-primary b-r-4 card-body">
                            <div class="media static-top-widget">
                                <div class="align-self-center text-center"><i data-feather="navigation"></i></div>
                                <div class="media-body"><span class="m-0">عدد المتاجر</span>
                                    <h4 class="mb-0 counter">{{ $shops }}</h4><i class="icon-bg"
                                                                                 data-feather="navigation"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 col-xl-3 col-lg-3">
                    <div class="card o-hidden">
                        <div class="b-r-4 card-body" style="background-color: #1D2D44;color: #fff">
                            <div class="media static-top-widget">
                                <div class="align-self-center text-center"><i data-feather="users"></i></div>
                                <div class="media-body"><span class="m-0">عدد الطلبات</span>
                                    <h4 class="mb-0 counter">{{ $orders }}</h4>
                                    <i class="icon-bg" data-feather="message-square"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 col-xl-3 col-lg-3">
                    <div class="card o-hidden">
                        <div class="b-r-4 card-body" style="background-color: #1D2D44;color: #fff">
                            <div class="media static-top-widget">
                                <div class="align-self-center text-center"><i data-feather="hard-drive"></i></div>
                                <div class="media-body"><span class="m-0">عدد شركات الشحن</span>
                                    <h4 class="mb-0 counter">{{ $shipping }}</h4>
                                    <i class="icon-bg" data-feather="message-square"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 col-xl-3 col-lg-3">
                    <div class="card o-hidden">
                        <div class="b-r-4 card-body" style="background-color: #1D2D44;color: #fff">
                            <div class="media static-top-widget">
                                <div class="align-self-center text-center"><i data-feather="hard-drive"></i></div>
                                <div class="media-body"><span class="m-0">عدد السائقين</span>
                                    <h4 class="mb-0 counter">{{ $drivers }}</h4>
                                    <i class="icon-bg" data-feather="message-square"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 col-sm-6 col-xl-6">
                    <div class="card">
                        <div class="card-header">
                            <h4>عدد المستخدمين اخر سبعه ايام <span class="digits"></span></h4>
                        </div>
                        @if($users_charts)
                            <div class="card-body chart-block">
                                <div class="chart-overflow" id="users"></div>
                            </div>
                        @else
                            <h6 style="text-align: center">لا يوجد مستخدمين مسجله</h6>
                        @endif


                    </div>
                </div>

                <div class="col-lg-6 col-sm-6 col-xl-6">
                    <div class="card">
                        <div class="card-header">
                            <h4>عدد المناديب اخر سبعه ايام <span class="digits"></span></h4>
                        </div>
                        @if($users_charts)
                            <div class="card-body chart-block">
                                <div class="chart-overflow" id="delegate"></div>
                            </div>
                        @else
                            <h6 style="text-align: center">لا يوجد مناديب مسجله</h6>
                        @endif


                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-sm-6 col-xl-6">
                <div class="card">
                    <div class="card-header">
                        <h4>عدد المتاجر المسجله اخر سبعه ايام <span class="digits"></span></h4>
                    </div>
                    @if($of_shops)
                        <div class="card-body chart-block">
                            <div class="chart-overflow" id="shops"></div>
                        </div>
                    @else
                        <h6 style="text-align: center">لا يوجد متاجر مسجله</h6>
                    @endif
                </div>
            </div>

            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4>عرض اخر 5 شكاوى او مقترحات</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="display" id="basic-1">
                                <thead>
                                <tr>
                                    <th>تطبيق صاحب الشكوى</th>
                                    <th> رقم الموبايل</th>
                                    <th>اسم صاحب الشكوى</th>
                                    <th>عنوان الشكوى</th>
                                    <th>الشكوى او الاقتراح</th>
                                    <th> حذف الشكوى</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($suggestions as $suggestion)
                                    <tr>
                                        @if($suggestion->type == 1)
                                            <td>مستخدم</td>
                                            <td>{{ !empty($suggestion->user->phone_number) ? $suggestion->user->phone_number : null }}</td>
                                            <td>{{ !empty($suggestion->user->first_name) ? $suggestion->user->first_name : 'بدون اسم'  }}</td>

                                        @elseif($suggestion->type == 3)
                                            <td>متجر</td>
                                            <td>{{ !empty($suggestion->shop->phone_number) ? $suggestion->shop->phone_number : null }}</td>
                                            <td>{{ !empty($suggestion->shop->name_app_ar) ? $suggestion->shop->name_app_ar : 'بدون اسم'  }}</td>
                                        @else
                                            <td>مندوب او سائق</td>
                                            <td>{{ !empty($suggestion->user->phone_number) ? $suggestion->user->phone_number : null }}</td>
                                            <td>{{ !empty($suggestion->user->first_name) ? $suggestion->user->first_name : 'بدون اسم'  }}</td>
                                        @endif
                                        <td>{{ $suggestion->title }}</td>
                                        <td>{{ $suggestion->comment }}</td>
                                        <td>
                                            <form action="{{ route('SuggestionDestroy', $suggestion->id) }}"
                                                  method="post" style="display: inline-block">
                                                {{ csrf_field() }}
                                                {{ method_field('delete') }}
                                                <button type="submit"
                                                        class="btn btn-danger delete btn-sm"><i
                                                        class="fa fa-trash"></i>
                                                </button>
                                            </form>
                                        </td>

                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- Container-fluid Ends-->

    </div>
@endsection





