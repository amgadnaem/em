@extends('AdminPanel.backend.layouts.default')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><i class="icon-plus"></i> إضافة خدمة جديدة </button>
                        </div>
                    </div>
                </div>
            </div>
        <!-- Container-fluid starts-->

            <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block row">
                        <div class="col-sm-12 col-lg-12 col-xl-12">
                            @if( count($services) > 0)
                                <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">رقم</th>
                                        <th scope="col">البداية</th>
                                        <th scope="col">الإنتظار</th>
                                         <th scope="col">الحد الأدنى للأجرة</th>
                                        <th scope="col">رسوم الخدمة</th>
                                        <th scope="col">إلغاء قبل الرسوم</th>
                                        <th scope="col">الغاء بعد الرسوم</th>
                                        <th>اجراءات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($services as $service)
                                    <tr>
                                        <td>{{$service->id}}</td>
                                        <td>{{$service->start}}</td>
                                        <td>{{$service->wait}}</td>
                                        <td>{{$service->minimum_fare}}</td>
                                        <td>{{$service->service_fees}}</td>
                                        <td>{{$service->cancel_before_fees}}</td>
                                        <td>{{$service->cancel_after_fees}}</td>
                                        <td>
                                            <a href="{{route('editserviceCar',['service_id' => $service->id])}}" title="تعديل الخدمة" class="buttons"><button class="btn btn-dark btn-condensed"><i class="fa fa-edit"></i></button></a>
                                            <a href="#" onclick='return deleteService({{$service->id}})' title="حذف الخدمة" class="buttons"><button class="btn btn-dark btn-condensed"><i class="fa fa-trash"></i></button></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @else
                            <h4 class="text-center"> لا يوجد خدمات</h4>
                        @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
        </div>
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">إضافة خدمة</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form class="form-horizontal" method="post" action="{{route('storeserviceCar')}}">
                        {{csrf_field()}}
                        <div class="modal-body">
                        <label class="col-lg-12 control-label text-lg-right" for="textinput">الدولة</label>

                            <select class="form-control field" data-type="select" id="type" name="country_id">
                                @foreach($countries as $country)
                                <option value="{{$country->id}}">{{$country->country_name_ar}}</option>
                                @endforeach
                            </select><br>


                            <div class="form-group row">
                                <label class="col-lg-12 control-label text-lg-right" for="textinput">البداية</label>
                                <div class="col-lg-12">
                                    <input name="start" type="number" placeholder="البداية" class="form-control btn-square" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-12 control-label text-lg-right" for="textinput">الإنتظار</label>
                                <div class="col-lg-12">
                                    <input  name="wait" type="number" placeholder="الإنتظار" class="form-control btn-square" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-12 control-label text-lg-right" for="textinput">الحد الأدنى للأجرة</label>
                                <div class="col-lg-12">
                                    <input name="minimum_fare" type="number" placeholder="الحد الأدنى للأجرة" class="form-control btn-square" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-12 control-label text-lg-right" for="passwordinput">رسوم الخدمة</label>
                                <div class="col-lg-12">
                                    <input name="service_fees" type="number" placeholder="رسوم الخدمة" class="form-control btn-square" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-12 control-label text-lg-right" for="passwordinput">إلغاء قبل الرسوم</label>
                                <div class="col-lg-12">
                                    <input name="cancel_before_fees" type="number" placeholder="إلغاء قبل الرسوم" class="form-control btn-square" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-12 control-label text-lg-right" for="passwordinput">الغاء بعد الرسوم</label>
                                <div class="col-lg-12">
                                    <input  name="cancel_after_fees" type="number" placeholder="الغاء بعد الرسوم" class="form-control btn-square" required>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-dark" data-dismiss="modal" style="margin-left:10px;">إغلاق</button>
                            <button class="btn btn-primary">حفظ</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </div>

    <script>
        function deleteService(service_id)
        {
            var id = service_id;
            swal({
                title: 'هل أنت متأكد!',
                text: 'عفواً لا يمكنك التراجع عن هذا الأمر',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'حذف',
                cancelButtonText: 'تراجع'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('deleteserviceCar')}}",
                        dataType: 'json',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: {service_id:id,"_token": "{{ csrf_token() }}"},

                        success: function(response){
                            window.location.reload();
                            toastr.success(response.success);
                        },
                        error: function() {
                            swal(
                                'يوجد خطأ ما',
                                'من فضلك حاول مرة أخري',
                                'خطأ'
                            )
                        }
                    });
                }
            });
            //
        }
    </script>

@endsection
