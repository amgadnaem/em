@extends('AdminPanel.backend.layouts.default')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>تعديل الخدمة</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->

        <div class="container-fluid">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block row">
                        <div class="col-sm-12 col-lg-12 col-xl-12">
                      
                <form class="form-horizontal" method="post" action="{{route('updateserviceCar',['service_id' => $service->id])}}">
                    {{csrf_field()}}
{{--                    @method('patch')--}}
                    <input type="hidden" name="service_id" value="{{$service->id}}"/>
                    <div class="modal-body">
                    <label class="col-lg-12 control-label text-lg-right" for="textinput">الدولة</label>
                        <select class="form-control field" data-type="select" id="type" name="country_id">
                            @foreach($countries as $country)
                            <option value="{{$country->id}}" @if($country->id==$service->country_id) selected @endif>{{$country->country_name_ar}}</option>
                            @endforeach
                        </select><br>
                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput">البداية</label>
                            <div class="col-lg-12">
                                <input name="start" type="text" placeholder="Start" value="{{$service->start}}" class="form-control btn-square" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput">الإنتظار</label>
                            <div class="col-lg-12">
                                <input  name="wait" value="{{$service->wait}}" type="text" placeholder="Wait" class="form-control btn-square" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput">الحد الأدنى للأجرة</label>
                            <div class="col-lg-12">
                                <input name="minimum_fare" value="{{$service->minimum_fare}}" type="text" placeholder="Minumum Fare" class="form-control btn-square" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="passwordinput">رسوم الخدمة</label>
                            <div class="col-lg-12">
                                <input name="service_fees" value="{{$service->service_fees}}" type="text" placeholder="Service Fees" class="form-control btn-square" required>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="passwordinput">إلغاء قبل الرسوم</label>
                            <div class="col-lg-12">
                                <input name="cancel_before_fees"  value="{{$service->cancel_before_fees}}" type="text" placeholder="Cancel Before Fees" class="form-control btn-square" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="passwordinput">الغاء بعد الرسوم</label>
                            <div class="col-lg-12">
                                <input  name="cancel_after_fees" value="{{$service->cancel_after_fees}}" type="text" placeholder="Cancel After Fees" class="form-control btn-square" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-dark" data-dismiss="modal">إغلاق</button>
                        <button class="btn btn-primary">حفظ</button>
                    </div>
                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>

               
@endsection
