@extends('AdminPanel.backend.layouts.default')
@section('content')

    <!-- Right sidebar Ends-->
    <div class="page-body">

        <!-- Default ordering (sorting) Starts-->
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h6>منتجات المتجر </h6>
                </div>
                @if( count($products) > 0)
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="display dataTable" id="basic-2">
                                <thead>
                                <tr>
                                    <th>QR Code</th>
                                    <th>صوره المنتج</th>
                                    <th>اسم المنتج</th>
                                    <th>وصف المنتج</th>
                                    <th>السعر العام</th>
                                    <th>السعر الخاص</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $product)
                                    <tr>
                                        <td>
                                            <img
                                                src="data:image/png;base64, {!! base64_encode(\SimpleSoftwareIO\QrCode\Facades\QrCode::format('png')->size(100)->generate($product->code)) !!} ">
                                        </td>
                                        <td><img src=" {{ $product->product_image }}" style="width: 50px"></td>
                                        <td>{{ $product->name_ar }}</td>
                                        <td>{{ $product->desc_ar }}</td>
                                        <td>{{ $product->general_price }}</td>
                                        <td>{{ $product->special_price }}</td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>

                @else

                    <h6 class="text-center">لا توجد منتجات مضافه لهذا المتجر</h6>
                @endif
            </div>
        </div>


    </div>
@endsection









