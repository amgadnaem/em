@extends('AdminPanel.backend.layouts.default')
@section('content')

    <!-- Right sidebar Ends-->
    <div class="page-body">

        <!-- Default ordering (sorting) Starts-->
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h6>اقسام المتجر </h6>
                </div>
                @if( count($CategoryShop) > 0)
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="display dataTable" id="basic-1">
                                <thead>
                                <tr>
                                    <th>صوره القسم</th>
                                    <th>صوره القسم</th>
                                    <th>اسم القسم</th>
                                    <th>عدد المنتجات التابعه للقسم</th>
                                    <th>المنتجات التابعه للقسم</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($CategoryShop as $category)
                                    <tr>
                                        <td><img style="width: 50px" src= {{ $category->category_image }} ></td>
                                        <td><img style="width: 50px" src= {{ $category->category_image }} ></td>
                                        <td>{{ $category->category_name_ar }}</td>
                                        <td>{{ $category->countProducts($category->id) }}</td>
                                        <td>
                                            <a href="{{ route('CategoryProducts', $category->id) }}"
                                               class="btn btn-info btn-sm"><i class="fa fa-info"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>

                @else

                    <h6 class="text-center"> لا توجد اقسام مضافه</h6>
                @endif
            </div>
        </div>


        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h6>طلبات المتجر </h6>
                </div>
                @if( count($shop_orders) > 0)
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="basic-4" class="shop_orders display dataTable">
                                <thead>
                                <tr>
                                    <th>حاله الطلب</th>
                                    <th>رقم الطلب</th>
                                    <th>سعر الطلب</th>
                                    <th>تكلفه شحن الطلب</th>
                                    <th>تاريخ الطلب</th>
                                    <th>المستخدم</th>
                                    <th>المندوب</th>
                                    <th>عنوان الطلب</th>
                                    <th>تفاصيل الطلب</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($shop_orders as $Order)
                                    <tr>
                                        <td>{{ $Order->OrderStatus->status_name_ar}}</td>
                                        <td>{{ $Order->order_number }}</td>
                                        <td>{{ $Order->subtotal_fees }}</td>
                                        <td>{{ $Order->shipping_fees }}</td>
                                        <td>{{ $Order->order_date }}</td>
                                        <td>{{ !empty($Order->user->first_name) ? $Order->user->first_name : null }}</td>
                                        <td>{{ !empty($Order->delegate->first_name) ? $Order->delegate->first_name : ''  }}</td>
                                        <td>{{ !empty($Order->address->location) ? $Order->address->location : ''  }}</td>

                                        <td><a href="{{ route('order_details',['id'=> $Order->id]) }}">
                                                <i class="fa fa-list-alt"></i>
                                            </a>
                                            <a class="btnprn" href="{{ route('print', $Order->id) }}">طباعه</a>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>

                        </div>
                    </div>
                @else
                    <h6 class="text-center">لا توجد طلبات منفذه لهذا المتجر</h6>
                @endif
            </div>
        </div>


        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h6>معلومات عن المتجر </h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="basic-5" class="shop_orders display dataTable">
                            <thead>
                            <tr>
                                <th> عدد الزبائن الدائمين</th>
                                <th> عدد المنتجات المستضافه</th>
                                <th> عدد المنتجات المشاركه</th>
                            </tr>
                            </thead>
                            <tbody>

                                <tr>
                                    <td>{{ $PermanentUser->count()}}</td>
                                    <td>{{ $product_host->count()}}</td>
                                    <td>{{ $product_shared->count()}}</td>

                                </tr>


                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection









