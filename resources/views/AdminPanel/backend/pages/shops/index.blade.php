@extends('AdminPanel.backend.layouts.default')
@section('content')

    <!-- Right sidebar Ends-->

    <div class="page-body">
        <div class="container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h6> المتاجر </h6>
                        </div>
                    </div>

                </div>
            </div>

            @if(count($all_shops) > 0)
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="display" id="basic-1">
                                        <thead>
                                        <tr>
                                            <th>رقم المتجر</th>
                                            <th>QR Code</th>
                                            <th>صوره المتجر</th>
                                            <th>اسم المتجر</th>
                                            <th>رقم هاتف المتجر</th>
                                            <th> دوله المتجر</th>
                                            <th> محافظه المتجر</th>
                                            <th>نوع الخدمه</th>
                                            <th>انتهاء اشتراك المتجر</th>
                                            <th>عدد المتابعين</th>
                                            <th>عدد الطلبات</th>
                                            <th>حاله المتجر</th>
                                            @if (auth()->user()->hasPermissionTo('تعديل_متجر'))
                                                <th>الاكشن</th>
                                            @endif
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($all_shops as $shops)
                                            <tr>
                                                <td>{{ $shops->shop_number }}</td>
                                                <td>
                                                    @if ($shops->phone_number)
                                                        <img src="data:image/png;base64, {!! base64_encode(\SimpleSoftwareIO\QrCode\Facades\QrCode::format('png')->size(100)->generate($shops->phone_number)) !!} ">

                                                    @endif
                                                </td>
                                                <td>
                                                    <img src="{{ $shops->shop_image }}" style="width: 50px">
                                                </td>
                                                <td>{{ $shops->name_app_ar }}</td>
                                                <td>{{ $shops->phone_number }}</td>
                                                <td>{{ $shops->country }}</td>
                                                <td>{{ $shops->city }}</td>
                                                <td>{{ $shops->get_service($shops->service_id) }}</td>
                                                <td>{{ $shops->Subscribe_end }}</td>
                                                <td>{{ $shops->follow($shops->id) }}</td>
                                                <td>{{ $shops->get_all_order($shops->id) }}</td>
                                                @if (auth()->user()->hasPermissionTo('تعديل_متجر'))
                                                    <td>
                                                        <div class="media-body">
                                                            <label class="switch">
                                                                <input type="checkbox"
                                                                       {{ $shops->status == 1 ? 'checked' : '' }}
                                                                       onchange="change_status_shops({{ $shops->id }},{{ $shops->status }})"><span
                                                                        class="switch-state"></span>

                                                            </label>
                                                        </div>

                                                    </td>
                                                @endif
                                                <td>
                                                    <a href="{{ route('editShop', $shops->id) }}"
                                                       class="btn btn-info btn-sm"><i class="fa fa-edit"></i>
                                                    </a>
                                                    <a href="{{ route('shopDetails', $shops->id) }}"
                                                       class="btn btn-info btn-sm"><i class="fa fa-info"></i>
                                                    </a>
                                                    <form action="{{ route('destroyShop', $shops->id) }}"
                                                          method="post" style="display: inline-block">
                                                        {{ csrf_field() }}
                                                        {{ method_field('delete') }}
                                                        <button type="submit"
                                                                class="btn btn-danger delete btn-sm"><i
                                                                    class="fa fa-trash"></i>
                                                        </button>
                                                    </form>

                                                </td>

                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            @else

                                <h4 class="text-center"> لا توجد متجر مضافه</h4>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <h5>طلبات المتاجر التى لم يتم قبولهم حتى الان</h5>
                            </div>
                            @if( count($shopRequest) > 0)
                                @if (auth()->user()->hasPermissionTo('عرض_متجر'))
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="display dataTable" id="basic-3">
                                                <thead>
                                                <tr>
                                                    <th>QR Code</th>
                                                    <th>صوره المتجر</th>
                                                    <th>اسم المتجر</th>
                                                    <th>رقم هاتف المتجر</th>
                                                    <th>نوع الخدمه</th>
                                                    @if (auth()->user()->hasPermissionTo('تعديل_متجر'))
                                                        <th>حاله المتجر</th>
                                                    @endif
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($shopRequest as $shops)
                                                    <tr>
                                                        <td>
                                                            <img src="data:image/png;base64, {!! base64_encode(\SimpleSoftwareIO\QrCode\Facades\QrCode::format('png')->size(100)->generate($shops->phone_number)) !!} ">
                                                        </td>
                                                        <td><img src="{{ $shops->shop_image }}" style="width: 50px">
                                                        </td>
                                                        <td>{{ $shops->name_app_ar }}</td>
                                                        <td>{{ $shops->phone_number }}</td>
                                                        <td>{{ $shops->get_service($shops->service_id) }}</td>
                                                        @if (auth()->user()->hasPermissionTo('تعديل_متجر'))
                                                            <td>
                                                                <div class="media-body">
                                                                    <label class="switch">
                                                                        <input type="checkbox"
                                                                               {{ $shops->shop_status == 1 ? 'checked' : '' }}
                                                                               onchange="accept_shop({{ $shops->id }},{{ $shops->shop_status }})"><span
                                                                                class="switch-state"></span>
                                                                    </label>
                                                                </div>
                                                            </td>
                                                        @endif

                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                @endif
                            @else
                                <h5 class="text-center"> لا يوجد طلبات مرسله من قبل المتاجر</h5>
                            @endif
                        </div>
                    </div>
                </div>
        </div>
    </div>


    <script>

        function change_status_shops(id, value) {
            if (value == 0) {
                value = 1;
            } else {
                value = 0;
            }
            axios.get('updateStatusShop/' + id + '/' + value)
                .then(function (response) {
                    location.reload();
                })
                .catch(function (error) {
                    console.log(error);
                });
        };

        function accept_shop(id, value) {
            if (value == 0) {
                value = 1;
            } else {
                value = 0;
            }
            axios.get('acceptShop/' + id + '/' + value)
                .then(function (response) {
                    location.reload();
                })
                .catch(function (error) {
                    console.log(error);
                });
        };
    </script>

@endsection
