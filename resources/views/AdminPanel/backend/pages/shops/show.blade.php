@extends('AdminPanel.backend.layouts.default')
@section('content')

    <!-- Right sidebar Ends-->

    <div class="page-body">
        <div class="container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h6> المتاجر </h6>
                        </div>
                    </div>

                </div>
            </div>

            @if(count($AllShops) > 0)
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="display" id="basic-1">
                                        <thead>
                                        <tr>
                                            <th>صوره المتجر</th>
                                            <th>اسم المتجر</th>
                                            <th>حاله المتجر</th>
                                            <th>انتهاء اشتراك المتجر</th>
                                            <th>عدد الطلبات</th>
                                            @if (auth()->user()->hasPermissionTo('تعديل_متجر'))
                                                <th>الاكشن</th>
                                            @endif
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($AllShops as $shops)
                                            <tr>
                                                <td><img src="{{ $shops->shop_image }}" style="width: 50px"></td>
                                                <td>{{ $shops->shop_name }}</td>
                                                @if (auth()->user()->hasPermissionTo('تعديل_متجر'))
                                                    <td>
                                                        <div class="media-body">
                                                            <label class="switch">
                                                                <input type="checkbox"
                                                                       {{ $shops->status == 1 ? 'checked' : '' }}
                                                                       onchange="change_status_shops({{ $shops->id }},{{ $shops->status }})"><span
                                                                    class="switch-state"></span>

                                                            </label>
                                                        </div>

                                                    </td>
                                                @endif

                                                <td>{{ $shops->Subscribe_end }}</td>
                                                <td>{{ $shops->get_all_order($shops->id) }}</td>

                                                <td>
                                                    <a href="{{ route('ShopDetails', $shops->id) }}"
                                                       class="btn btn-info btn-sm"><i class="fa fa-info"></i>
                                                    </a>


                                                </td>

                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            @else

                                <h4 class="text-center"> لا توجد متجر مضافه</h4>
                            @endif
                        </div>
                    </div>
                </div>


                <!--     Shop stop         --->


                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h6>المتاجر التى وقفت عنها الخدمه </h6>
                        </div>

                        @if(count($stop_shops) > 0)
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="display" id="basic-2">
                                        <thead>
                                        <tr>
                                            <th>صوره المتجر</th>
                                            <th>اسم المتجر</th>
                                            <th>انتهاء اشتراك المتجر</th>
                                            @if (auth()->user()->hasPermissionTo('تعديل_متجر'))
                                            <th>الاكشن</th>
                                                @endif
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($stop_shops as $shops)
                                            <tr>
                                                <td><img src="{{ $shops->shop_image }}" style="width: 50px"></td>
                                                <td>{{ $shops->shop_name }}</td>
                                                <td>{{ $shops->Subscribe_end }}</td>
                                                @if (auth()->user()->hasPermissionTo('تعديل_متجر'))
                                                <td>
                                                    <form action="{{ route('editSubscribe', $shops->id) }}"
                                                          method="post" style="display: inline-block">
                                                        {{ csrf_field() }}
                                                        {{ method_field('post') }}
                                                        <input class="datepicker-here  digits" type="text"
                                                               required
                                                               data-language="en"
                                                               name="subscribe_end"
                                                               data-multiple-dates-separator=", "
                                                               data-position="top left"
                                                               value="{{ $shops->Subscribe_end }}"
                                                               placeholder="تاريخ اشتراك المتجر" autocomplete="true">
                                                        <button type="submit" class="btn btn-primary edit btn-sm"><i
                                                                class="fa fa-edit"></i>
                                                        </button>
                                                    </form>

                                                </td>
                                                    @endif

                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @else

                            <h4 class="text-center"> لا توجد متاجر </h4>
                        @endif
                    </div>
                </div>



                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h6>المتاجر التى سوف تقف عنها الخدمه خلال 15 يوم </h6>
                        </div>

                        @if(count($will_stop_shops) > 0)
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="display" id="basic-3">
                                        <thead>
                                        <tr>
                                            <th>صوره المتجر</th>
                                            <th>اسم المتجر</th>
                                            <th>انتهاء اشتراك المتجر</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($will_stop_shops as $shops)

                                            <tr>
                                                <td><img src="{{ $shops->shop_image }}" style="width: 50px"></td>
                                                <td>{{ $shops->shop_name }}</td>
                                                <td>{{ $shops->Subscribe_end }}</td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @else

                            <h4 class="text-center"> لا توجد متاجر </h4>
                        @endif
                    </div>
                </div>
        </div>
    </div>



    <script>

        function change_status_shops(id, value) {

            if (value == 0) {
                value = 1;
            } else {
                value = 0;
            }
            axios.get('updateAdminShop/' + id + '/' + value)
                .then(function (response) {
                    location.reload();
                })
                .catch(function (error) {
                    console.log(error);
                });
        };
    </script>

@endsection

















