@extends('AdminPanel.backend.layouts.default')
@section('content')
    <!-- Right sidebar Ends-->
    <div class="page-body">

        <!-- Default ordering (sorting) Starts-->
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h3>تعديل بيانات متجر{{ $editShop->name_app_ar }} </h3>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <form class="tocify-content" action="{{route('updateShop',['id'=> $editShop->id]) }}"
                                  method="POST" enctype="multipart/form-data">
                                {{ method_field('POST') }}
                                {{ csrf_field() }}
                                <div class="modal-body">

                                    <div class="form-row">

      
                                        
                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom04"> اسم المتجر فى التطبيق باللغه
                                                العربيه</label>
                                            <input class="form-control" type="text" name="name_app_ar"
                                                   value="{{$editShop->name_app_ar}}"
                                                   required>
                                            <div class="invalid-feedback">{{ $errors->first('name_app_ar') }}.</div>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom04">اسم المتجر فى التطبيق باللغه
                                                الانجليزيه</label>
                                            <input class="form-control" type="text" name="name_app_en"
                                                   value="{{$editShop->name_app_en}}"
                                                   required>
                                            <div class="invalid-feedback">{{ $errors->first('name_app_en') }}.</div>
                                        </div>

                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom04">اسم المتجر فى التطبيق باللغه الكرديه</label>
                                            <input class="form-control" type="text" name="name_app_kur"
                                                   value="{{$editShop->name_app_kur}}"
                                                   required>
                                            <div class="invalid-feedback">{{ $errors->first('name_app_kur') }}.</div>
                                        </div>

                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom04">كلمه المرور</label>
                                            <input class="form-control" type="password" id="password" name="password"
                                                   placeholder="ادخل كلمه المرور"
                                            >
                                            <div class="invalid-feedback">{{ $errors->first('password') }}.</div>
                                        </div>


                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom05">صوره المتجر</label>
                                            <div class="custom-file">
                                                <input class="custom-file-input" id="shop_image" type="file"
                                                       name="shop_image">
                                                <label class="custom-file-label" for="validatedCustomFile">اختر
                                                    صوره</label>
                                                <div class="invalid-feedback">{{ $errors->first('shop_image') }}</div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <img src="{{$editShop->shop_image }}"
                                                 style="width: 100px"
                                                 class="img-thumbnail image-preview" alt="">
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom04">تاريخ انتهاء الاشتراك</label>
                                            <input class="datepicker-here form-control digits" name="subscribe_end"
                                                   type="text" required
                                                   data-language="en"
                                                   data-multiple-dates-separator=", " data-position="top left"
                                                   value="{{ $editShop->Subscribe_end }}" autocomplete="true">
                                            <div class="invalid-feedback">{{ $errors->first('scribe_end') }}.</div>
                                        </div>

                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom09">هل المتجر وكيل رسمى
                                            </label>
                                            <div
                                                class="form-group page-header-left m-checkbox-inline mb-0 custom-radio-ml">

                                                <div class="radio radio-primary">

                                                    <input id="vip1" type="radio" name="vip" value="1"
                                                        {{ $editShop->vip == '1' ? 'checked' : '' }} >
                                                    <label class="mb-0" for="vip1">نعم</label>
                                                </div>

                                                <div class="radio radio-primary">
                                                    <input id="vip2" type="radio" name="vip"
                                                           {{ $editShop->vip == '0' ? 'checked' : '' }}
                                                           value="0">
                                                    <label class="mb-0" for="vip2">لا</label>
                                                </div>

                                            </div>
                                        </div>


                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom04"> وصف عن المتجر بالغه العربيه</label>
                                            <textarea class="form-control" name="desc_ar"
                                                      required>{{ $editShop->desc_ar }}</textarea>
                                            <div class="invalid-feedback">{{ $errors->first('desc_ar') }}.</div>
                                        </div>

                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom04"> وصف عن المتجر باللغه الانجليزيه</label>
                                            <textarea class="form-control" name="desc_en"
                                                      required>{{ $editShop->desc_en }}</textarea>
                                            <div class="invalid-feedback">{{ $errors->first('desc_en') }}.</div>
                                        </div>

                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom04"> وصف عن المتجر بالغه الكرديه</label>
                                            <textarea class="form-control" name="desc_kur"
                                                      required>{{ $editShop->desc_kur }}</textarea>
                                            <div class="invalid-feedback">{{ $errors->first('desc_kur') }}.</div>
                                        </div>
                                        
                                             <div class="col-md-6 mb-3">
                                            <label for="validationCustom05">الاقسام</label>
                                            <select name="service_id" class="form-control" required>
                                                @foreach($services as $service)
                                                    @if($service->id == $editShop->service_id )
                                                        <option
                                                            value="{{ $service->id }}" selected>{{ $service->service_name_ar }}</option>
                                                    @else
                                                        <option
                                                            value="{{ $service->id }}">{{ $service->service_name_ar }}</option>
                                                    @endif
                                                @endforeach

                                            </select>
                                            <div class="invalid-feedback">{{ $errors->first('service_name') }}</div>
                                        </div>


                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <button class="btn btn-primary" type="submit">تعديل بيانات متجر</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
