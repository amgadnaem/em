@extends('AdminPanel.backend.layouts.default')
@section('content')
    <style>

        #pac-input {
            background-color: #fff;
            font-family: Roboto;
            font-size: 25px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 400px;
        }

        #pac-input:focus {
            border-color: #4d90fe;
        }


    </style>
    <!-- Right sidebar Ends-->
    <div class="page-body">

        <!-- Default ordering (sorting) Starts-->
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h3>اضافه متجر </h3>
                </div>
            </div>
        </div>

    {{--        <!--map div-->--}}
    {{--        <div class="form-group">--}}
    {{--            <input id="pac-input" class="controls" type="text" placeholder="Search Box" style="width:100%;height:350px">--}}
    {{--            <div id="map"></div>--}}
    {{--        </div>--}}

    <!--our form-->

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <form class="needs-validation" novalidate="" action="{{route('InsertShop')}}"
                                  method="POST" enctype="multipart/form-data">
                                {{ method_field('POST') }}
                                {{ csrf_field() }}
                                <div class="modal-body">

                                    <div class="form-group">
                                        <div id="shoplocation" style="width:100%;height:350px"></div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom05">عنوان المتجر</label>
                                            <input type="text" class="form-control" name="location" id="autocomplete"
                                                   placeholder="البحث عن عنوان">
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom05">الاقسام</label>
                                            <select name="service_id" class="form-control" required>
                                                <option value="">اختر</option>
                                                @foreach($services as $service)
                                                    <option
                                                        value="{{ $service->id }}">{{ $service->service_name_ar }}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">{{ $errors->first('service_name') }}</div>
                                        </div>
                                    </div>

                                    <div class="form-row">


                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom05"
                                                   class="page-header-left"> اسم الدوله</label>
                                            <select name="country_id" id="country_id"
                                                    class="form-control"
                                                    required>
                                                <option value="">اختر</option>
                                                @foreach($countries as $country)
                                                    <option
                                                        value="{{ $country->id }}">{{ $country->country_name_ar }}</option>
                                                @endforeach
                                            </select>
                                            <div
                                                class="invalid-feedback">{{ $errors->first('country_id') }}</div>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom05"
                                                   class="page-header-left"> اسم المدينه</label>
                                            <select name="city_id" id="city_id"
                                                    class="form-control"
                                                    required>
                                                <option value="">اختر اسم الدوله اولا</option>
                                            </select>
                                            <div
                                                class="invalid-feedback">{{ $errors->first('city_id') }}</div>
                                        </div>

                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom04">اسم المتجر الحقيقي</label>
                                            <input class="form-control" type="text" name="real_name"
                                                   placeholder="ادخل اسم  المتجر الحقيقي"
                                                   required>
                                            <div class="invalid-feedback">{{ $errors->first('shop_name') }}.</div>
                                        </div>

                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom04">اسم المتجر فى التطبيق</label>
                                            <input class="form-control" type="text"  name="name_app"
                                                   placeholder="ادخل اسم  المتجر فى التطبيق"
                                                   required>
                                            <div class="invalid-feedback">{{ $errors->first('name_app') }}.</div>
                                        </div>


                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom04">كلمه المرور</label>
                                            <input class="form-control" type="password" id="password" name="password"
                                                   placeholder="ادخل كلمه المرور"
                                                   required>
                                            <div class="invalid-feedback">{{ $errors->first('password') }}.</div>
                                        </div>

                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom04">رقم الهاتف</label>
                                            <input class="form-control" type="text" name="phone_number"
                                                   placeholder="ادخل رقم الهاتف"
                                                   required>
                                            <div class="invalid-feedback">{{ $errors->first('phone_number') }}.</div>
                                        </div>

                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom04">تاريخ انتهاء الاشتراك</label>
                                            <input class="datepicker-here form-control digits" name="subscribe_end"
                                                   type="text" required
                                                   data-language="en"
                                                   data-multiple-dates-separator=", " data-position="top left"
                                                   placeholder="تاريخ انتهاء الاشتراك" autocomplete="true">
                                            <div class="invalid-feedback">{{ $errors->first('scribe_end') }}.</div>
                                        </div>


                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom04">الرقم القومى</label>
                                            <input class="form-control" type="text" name="ID_number"
                                                   placeholder="ادخل  الرقم القومى" required>
                                            <div class="invalid-feedback">{{ $errors->first('ID_number') }}.</div>
                                        </div>

                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom05">صوره المتجر</label>
                                            <div class="custom-file">
                                                <input class="custom-file-input" id="shop_image" type="file"
                                                       name="shop_image">
                                                <label class="custom-file-label" for="validatedCustomFile">اختر
                                                    صوره</label>
                                                <div class="invalid-feedback">{{ $errors->first('shop_image') }}</div>
                                            </div>
                                        </div>


                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom05">صوره الرقم القومى</label>
                                            <div class="custom-file">
                                                <input class="custom-file-input" type="file"
                                                       name="ID_image">
                                                <label class="custom-file-label" for="validatedCustomFile">اختر
                                                    صوره</label>
                                                <div class="invalid-feedback">{{ $errors->first('ID_image') }}</div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom05">صوره الرخصه</label>
                                            <div class="custom-file">
                                                <input class="custom-file-input" type="file"
                                                       name="shop_license">
                                                <label class="custom-file-label" for="validatedCustomFile">اختر
                                                    صوره</label>
                                                <div class="invalid-feedback">{{ $errors->first('shop_license') }}</div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom05"
                                                   class="page-header-left">  اختر رقم مميز</label>
                                            <select name="special_number"
                                                    class="form-control"
                                                    required>
                                                <option value="">اختر</option>
                                                @foreach($special_numbers as $numbers)
                                                    <option
                                                        value="{{ $numbers->id }}">{{ $numbers->special_number }}</option>
                                                @endforeach
                                            </select>
                                            <div
                                                class="invalid-feedback">{{ $errors->first('special_number') }}</div>
                                        </div>

                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom04">تكلفه شحن الطلب</label>
                                            <input class="form-control" type="text" name="shipping"
                                                   value="{{$shipping}}"
                                                   required readonly>
                                            <div class="invalid-feedback">{{ $errors->first('shipping') }}.</div>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom09">هل المتجر وكيل رسمى
                                            </label>

                                            <div
                                                class="form-group page-header-left m-checkbox-inline mb-0 custom-radio-ml">
                                                <div class="radio radio-primary">
                                                    <input id="radioinline1" type="radio" name="vip"
                                                           value="1">
                                                    <label class="mb-0" for="radioinline1">نعم</label>
                                                </div>
                                                <div class="radio radio-primary">
                                                    <input id="radioinline2" type="radio" name="vip"
                                                           value="0" checked>
                                                    <label class="mb-0" for="radioinline2">لا</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom04"> وصف عن المتجر</label>
                                            <textarea class="form-control"  name="description"
                                                      required></textarea>
                                            <div class="invalid-feedback">{{ $errors->first('description') }}.</div>
                                        </div>

                                        <div class="col-md-6 mb-3" style="display: none">
                                            <label for="validationCustom04">Latitude</label>
                                            <input type="text" required class="form-control" name="lat" value=""
                                                   id="lat" placeholder="Enter lat ">
                                        </div>
                                        <div class="col-md-6 mb-3" style="display: none">
                                            <label for="validationCustom04">longitude</label>
                                            <input type="text" required class="form-control" name="lng" value=""
                                                   id="lng" placeholder="Enter long">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <button class="btn btn-primary" type="submit">اضافه متجر</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script>
        var marker = null;
        var placeSearch, autocomplete;

        function initMap() {
            autocomplete =
                new google.maps.places.Autocomplete((document.getElementById('autocomplete')),
                    {types: ['geocode']});
            var map = new google.maps.Map(document.getElementById('shoplocation'), {
                zoom: 10,
                center: {lat: 30.1258867, lng: 31.374936}
            });
            var MaekerPos = new google.maps.LatLng(0, 0);
            marker = new google.maps.Marker({
                position: MaekerPos,
                map: map
            });
            autocomplete.addListener('place_changed', function () {
                placeMarkerAndPanTo(autocomplete.getPlace().geometry.location, map);
                document.getElementById("lat").value = autocomplete.getPlace().geometry.location.lat();
                document.getElementById("lng").value = autocomplete.getPlace().geometry.location.lng();
            });
            map.addListener('click', function (e) {
                placeMarkerAndPanTo(e.latLng, map);
                document.getElementById("lat").value = e.latLng.lat();
                document.getElementById("lng").value = e.latLng.lng();
            });
        }

        function placeMarkerAndPanTo(latLng, map) {
            map.setZoom(9);
            marker.setPosition(latLng);
            map.panTo(latLng);
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDPN_XufKy-QTSCB68xFJlqtUjHQ8m6uUY&libraries=places&callback=initMap">
    </script>
@endsection







