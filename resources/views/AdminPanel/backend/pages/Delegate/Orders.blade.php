@extends('AdminPanel.backend.layouts.default')
@section('content')
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <!-- Right sidebar Ends-->
    <div class="page-body">

        <!-- Default ordering (sorting) Starts-->
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4>عرض الطلبات </h4>
                </div>
                @if( count($OrdersDelegate) > 0)
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="orders" class="display dataTable">
                                <thead>
                                <tr id="filters">
                                    <th>حاله الطلب</th>
                                </tr>
                                <tr>
                                    <th>حاله الطلب</th>
                                    <th>رقم الطلب</th>
                                    <th>سعر الطلب</th>
                                    <th>تكلفه شحن الطلب</th>
                                    <th>تاريخ الطلب</th>
                                    <th>المستخدم</th>
                                    <th>المندوب</th>
                                    <th>عنوان الطلب</th>
                                    <th>تفاصيل الطلب</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($OrdersDelegate as $Order)
                                    <tr>
                                        <td>{{ $Order->OrderStatus->status_name_ar}}</td>
                                        <td>{{ $Order->order_number }}</td>
                                        <td>{{ $Order->subtotal_fees }}</td>
                                        <td>{{ $Order->shipping_fees }}</td>
                                        <td>{{ $Order->created_at }}</td>
                                        <td>{{ $Order->user->first_name }}</td>
                                        <td>{{ !empty($Order->delegate->first_name) ? $Order->delegate->first_name : ''  }}</td>
                                        <td><p><a href="{{$Order->address['google_location']}}" target="_blank">موقع
                                                    صاحب الطلب</a></p></td>
                                        <td>
                                            <a href="{{ route('order_details',['id'=> $Order->id]) }}">
                                                <i class="btn btn-primary fa fa-eye"></i>
                                            </a>
                                            <a class="btnprn btn btn-primary "
                                               href="{{ route('print', $Order->id) }}"><i
                                                    class="icofont icofont-printer"></i></a>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>

                        </div>
                    </div>

                @else

                    <h4 class="text-center">لا توجد طلبات منفذه لهذا المندوب</h4>
                @endif
            </div>
        </div>
    </div>

    <script type="text/javascript">

        $(document).ready(function () {
            $('#orders').DataTable({
                responsive: true,
                ordering: true,
                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;

                        var select = $('<select><option value="">اختر</option></select>')
                            .appendTo($("#filters").find("th").eq(column.index()))
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val());

                                column.search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });
                        column.data().unique().sort().each(function (d, j) {
                            $(select).append('<option value="' + d + '">' + d + '</option>')
                        });
                    });
                }
            });
        });
    </script>


@endsection







