@extends('AdminPanel.backend.layouts.default')
@section('content')


    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">

                        <div class="page-header-left">
                            @if (auth()->user()->hasPermissionTo('اضافه_مندوب'))
                                <button class="btn btn-primary" type="button" data-toggle="modal"
                                        data-target="#AddDelegate" data-whatever="@delegate">اضافه مندوب
                                </button>
                            @endif
                            <div class="modal fade" id="AddDelegate" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">اضافه مندوب</h5>
                                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <form class="needs-validation" novalidate=""
                                                  action="{{route('addDelegate')}}"
                                                  method="POST" enctype="multipart/form-data">
                                                {{ method_field('POST') }}
                                                {{ csrf_field() }}

                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">الاسم الاول</label>
                                                    <input class="form-control" value="{{ old('first_name') }}"
                                                           name="first_name" id="validationCustomUsername"
                                                           type="text"
                                                           placeholder="ادخل الاسم الاول"
                                                           aria-describedby="inputGroupPrepend"
                                                           required="">
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('first_name') }}</div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">الاسم الاخير</label>
                                                    <input class="form-control" value="{{ old('last_name') }}"
                                                           name="last_name"
                                                           id="validationCustomUsername"
                                                           type="text"
                                                           placeholder="ادخل الاسم الاخير"
                                                           aria-describedby="inputGroupPrepend"
                                                           required="">
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('last_name') }}</div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="validationCustom04"
                                                           class="col-form-label page-header-left">ادخل رقم
                                                        الهاتف</label>
                                                    <input class="form-control"
                                                           {{ old('phone_number') }} name="phone_number"
                                                           id="validationCustom04" type="text"
                                                           placeholder="ادخل رقم الموبايل"
                                                           required="">
                                                    <div class="invalid-feedback">{{ $errors->first('phone_number') }}.
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">كلمه المرور</label>
                                                    <input class="form-control" name="password" id="validationCustom05"
                                                           type="password"
                                                           placeholder="ادخل كلمه المرور"
                                                           required="">
                                                    <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">تاريخ الميلاد</label>
                                                    <input class="form-control" name="dob" value="{{ old('dob') }}"
                                                           id="validationCustomUsername"
                                                           type="date"
                                                           placeholder="ادخل تاريخ الميلاد"
                                                           aria-describedby="inputGroupPrepend"
                                                           required="">
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('dob') }}</div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="page-header-left"> اسم الدوله</label>
                                                    <select name="country_id" id="country_id"
                                                            class="form-control"
                                                            required>
                                                        <option value="">اختر</option>
                                                        @foreach($countries as $country)
                                                            <option
                                                                value="{{ $country->id }}">{{ $country->country_name_ar }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('country_id') }}</div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="page-header-left"> اسم المدينه</label>
                                                    <select name="city_id" id="city_id"
                                                            class="form-control"
                                                            required>
                                                        <option value="">اختر اسم الدوله اولا</option>
                                                    </select>
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('city_id') }}</div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">صوره المندوب</label>

                                                    <div class="custom-file">
                                                        <input class="custom-file-input" id="validatedCustomFile"
                                                               type="file"
                                                               name="delegate_image">
                                                        <label class="custom-file-label"
                                                               for="validatedCustomFile">اختر</label>
                                                        <div class="invalid-feedback">تحميل صوره</div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn btn-primary" type="submit">اضافه</button>
                                                    <button class="btn btn-secondary" type="button"
                                                            data-dismiss="modal">اغلاق
                                                    </button>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>عدد المناديب {{ count($ShowDelegate) }}</h5>
                        </div>
                        @if( count($ShowDelegate) > 0)
                            @if (auth()->user()->hasPermissionTo('عرض_مندوب'))
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="display dataTable" id="basic-1">
                                            <thead>
                                            <tr>
                                                <th>QR Code</th>
                                                <th>صوره</th>
                                                <th>الاسم</th>
                                                <th>رقم الهاتف</th>
                                                <th>رقم ال ID</th>
                                                @if (auth()->user()->hasPermissionTo('تعديل_مندوب'))
                                                    <th>حاله الحساب</th>
                                                @endif
                                                <th>الطلبات المنفذه</th>
                                                <th>الاكشن</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($ShowDelegate as $account)
                                                <tr>
                                                    <td><img
                                                            src="data:image/png;base64, {!! base64_encode(\SimpleSoftwareIO\QrCode\Facades\QrCode::format('png')->size(100)->generate($account->phone_number)) !!} ">
                                                    </td>
                                                    <td><img src="{{ $account->user_image }}" style="width: 50px"></td>
                                                    <td>{{ $account->first_name }}  {{ $account->last_name }}</td>
                                                    <td>{{ $account->phone_number }}</td>
                                                    <td>{{ $account->user_number }}</td>
                                                    @if (auth()->user()->hasPermissionTo('تعديل_مندوب'))
                                                        <td>
                                                            <div class="media-body ">
                                                                <label class="switch">
                                                                    <input type="checkbox"
                                                                           {{ $account->status == 1 ? 'checked' : '' }}
                                                                           onchange="change_status_delegate({{ $account->id }},{{ $account->status }})"><span
                                                                        class="switch-state"></span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                    @endif
                                                    <td>{{ $account->delegate_orders($account->id) }}
                                                    </td>
                                                    <td>
                                                        <a
                                                            href="{{ route('delegate-orders', $account->id) }}"
                                                            class="btn btn-info">
                                                            <i class="fa fa-info"></i>
                                                        </a>
                                                        @if (auth()->user()->hasPermissionTo('تعديل_مندوب'))
                                                            <button class="btn btn-primary" type="button"
                                                                    data-toggle="modal"
                                                                    data-target="#{{ $account->id }}"
                                                                    data-whatever="@delegate"><i class="fa fa-edit"></i>
                                                            </button>
                                                        @endif
                                                        @if (auth()->user()->hasPermissionTo('حذف_مندوب'))
                                                            <form
                                                                action="{{ route('deleteAccountDelegate', $account->id) }}"
                                                                method="post" style="display: inline-block">
                                                                {{ csrf_field() }}
                                                                {{ method_field('delete') }}
                                                                <button type="submit"
                                                                        class="btn btn-danger delete btn-sm"><i
                                                                        class="fa fa-trash"></i>
                                                                </button>
                                                            </form>
                                                        @endif

                                                        <div class="modal fade" id="{{ $account->id }}" tabindex="-1"
                                                             role="dialog"
                                                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title"> تعديل حساب
                                                                            {{ $account->first_name }}  {{ $account->last_name }} </h5>

                                                                        <button class="close" type="button"
                                                                                data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">×</span></button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <form class="needs-validation" novalidate=""
                                                                              action="{{route('EditAccountDelegate',['id'=>$account->id])}}"
                                                                              method="POST"
                                                                              enctype="multipart/form-data">
                                                                            {{ method_field('POST') }}
                                                                            {{ csrf_field() }}

                                                                            <div class="form-group">
                                                                                <label
                                                                                    class="col-form-label page-header-left">الاسم
                                                                                </label>
                                                                                <input class="form-control"
                                                                                       name="first_name"
                                                                                       required
                                                                                       type="text"
                                                                                       value="{{ $account->first_name }}">
                                                                                <div
                                                                                    class="invalid-feedback">{{ $errors->first('first_name') }}</div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label
                                                                                    class="col-form-label page-header-left">الاسم
                                                                                </label>
                                                                                <input class="form-control"
                                                                                       name="last_name"
                                                                                       required
                                                                                       type="text"
                                                                                       value="{{ $account->last_name }}">
                                                                                <div
                                                                                    class="invalid-feedback">{{ $errors->first('last_name') }}</div>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label
                                                                                    class="col-form-label page-header-left">كلمه
                                                                                    المرور</label>
                                                                                <input class="form-control"
                                                                                       name="password"
                                                                                       type="password">
                                                                                <div
                                                                                    class="invalid-feedback">{{ $errors->first('password') }}</div>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label
                                                                                    class="col-form-label page-header-left">رقم
                                                                                    الهاتف
                                                                                </label>
                                                                                <input class="form-control"
                                                                                       name="phone_number"
                                                                                       type="text"
                                                                                       value="{{ $account->phone_number }}"
                                                                                       required>
                                                                                <div
                                                                                    class="invalid-feedback">{{ $errors->first('phone') }}</div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="validationCustom05"
                                                                                       class="col-form-label page-header-left">تاريخ
                                                                                    الميلاد</label>
                                                                                <input class="form-control" name="dob"
                                                                                       value="{{ $account->dob }}"
                                                                                       id="validationCustomUsername"
                                                                                       type="date"
                                                                                       placeholder="ادخل تاريخ الميلاد"
                                                                                       aria-describedby="inputGroupPrepend"
                                                                                       required="">
                                                                                <div
                                                                                    class="invalid-feedback">{{ $errors->first('dob') }}</div>
                                                                            </div>


                                                                            <div class="form-group">
                                                                                <label for="validationCustom05"
                                                                                       class="page-header-left">صوره
                                                                                    الحساب</label>
                                                                                <label for="file-input"
                                                                                       class="image-upload-label">
                                                                                    <img alt="upload-delegate-image"
                                                                                         name="delegate_image"
                                                                                         src="{{$account->user_image}}"
                                                                                         class="thumb"
                                                                                         style="width: 100px"/>
                                                                                </label>
                                                                                <div class="custom-file">
                                                                                    <input class="custom-file-input"
                                                                                           type="file"
                                                                                           name="delegate_image">
                                                                                    <label class="custom-file-label"
                                                                                           for="validatedCustomFile">اختر
                                                                                        صوره</label>
                                                                                    <div
                                                                                        class="invalid-feedback">{{ $errors->first('user_image') }}</div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="modal-footer">
                                                                                <button class="btn btn-primary"
                                                                                        type="submit">
                                                                                    تعديل
                                                                                </button>
                                                                                <button class="btn btn-secondary"
                                                                                        type="button"
                                                                                        data-dismiss="modal">اغلاق
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            @endif
                        @else
                            <h4 class="text-center"> لا يوجد مناديب لديك</h4>
                        @endif
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>طلبات المناديب التى لم يتم قبولهم حتى الان</h5>
                        </div>
                        @if( count($delegateRequest) > 0)
                            @if (auth()->user()->hasPermissionTo('عرض_مندوب'))
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="display dataTable" id="basic-3">
                                            <thead>
                                            <tr>
                                                <th>صوره</th>
                                                <th>الاسم</th>
                                                <th>رقم الهاتف</th>
                                                <th>رقم ال ID</th>
                                            @if (auth()->user()->hasPermissionTo('تعديل_مندوب'))
                                                    <th>قبول الطلب</th>
                                                @endif
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($delegateRequest as $account)
                                                <tr>
                                                    <td><img src="{{ $account->user_image }}" style="width: 50px"></td>
                                                    <td>{{ $account->first_name }} {{ $account->last_name }}</td>
                                                    <td>{{ $account->phone_number }}</td>
                                                    <td>{{ $account->user_number }}</td>
                                                    @if (auth()->user()->hasPermissionTo('تعديل_مندوب'))
                                                        <td>
                                                            <div class="media-body ">
                                                                <label class="switch">
                                                                    <input type="checkbox"
                                                                           {{ $account->confirm_account == 1 ? 'checked' : '' }}
                                                                           onchange="confirm_delegate({{ $account->id }})"><span
                                                                        class="switch-state"></span>

                                                                </label>
                                                            </div>
                                                        </td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            @endif
                        @else
                            <h5 class="text-center"> لا يوجد طلبات مرسله من قبل المناديب</h5>
                        @endif
                    </div>
                </div>
            </div>


        </div>
    </div>


    <script>


        function change_status_delegate(id, value) {
            if (value == 0) {
                value = 1;
            } else {
                value = 0;
            }
            axios.get('UpdateStatusDelegate/' + id + '/' + value)
                .then(function (response) {
                    location.reload();
                })
                .catch(function (error) {
                    console.log(error);
                    alert(error);
                });
        };

        function confirm_delegate(id) {
            axios.get('confirmAccountDelegate/' + id)
                .then(function (response) {
                    location.reload();
                })
                .catch(function (error) {
                    console.log(error);
                    alert(error);
                });
        };


    </script>

@endsection
