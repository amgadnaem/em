<!DOCTYPE html>
<html lang="{{app()->getLocale()}}" class="body-full-height">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="csrf-token" content="{{csrf_token()}}">

    <link rel="icon" href="{{ asset('images/grand.png')}}" type="image/x-icon">
    <link rel="shortcut icon" href="{{ asset('images/grand.png')}}" type="image/x-icon">
    <title>EM</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <link
        href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/fontawesome.css')}}">

    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/icofont.css')}}">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/themify.css')}}">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/flag-icon.css')}}">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/feather-icon.css')}}">
    <!-- Plugins css start-->
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" href="{{ asset('js/noty/noty.css') }}">
    <script src="{{ asset('js/noty/noty.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css')}}">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/style.css')}}">
    <link id="color" rel="stylesheet" href="{{ asset('/css/light-1.css')}}" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/responsive.css')}}">
    <style>
        .authentication-main {
            background-image: url("{{ asset('images/logo/lo.jpeg') }}");
            background-repeat: no-repeat;
            background-size: cover;

        }
    </style>
</head>
<body main-theme-layout="rtl">
<!-- Loader starts-->
<div class="loader-wrapper">
    <div class="loader bg-white">
        <div class="whirly-loader"></div>
    </div>
</div>
<!-- Loader ends-->
<!-- page-wrapper Start-->
<div class="page-wrapper">
    <div class="container-fluid p-0">
        <!-- login page start-->
        <div class="authentication-main">
            <div class="row">
                <div class="col-md-12">
                    <div class="auth-innerright">

                        <div class="authentication-box">
                            @if ($message = \Illuminate\Support\Facades\Session::get('message'))
                                <div class="alert alert-danger" role="alert">
                                    <strong>{{   $message }}</strong>
                                </div>
                            @endif
                            <div class="text-center">
                                <img style="width: 40%;"
                                     src="{{ asset('images/logo/em.png')}}" alt="">
                            </div>
                            <div class="card mt-4">
                                <div class="card-body">

                                    <form role="form" method="POST" action='{{url()->current()}}' class="theme-form">
                                        {{ csrf_field() }}
                                        <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                                            <label class="col-form-label page-header-left pt-0">رقم الهاتف
                                            </label>
                                            <input  type="text" class="form-control" name="phone_number"
                                                   value="{{ old('phone_number') }}" required
                                                   autofocus>

                                            @if ($errors->has('phone_number'))
                                                <span class="help-block">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                                            @endif
                                        </div>

                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <label for="validationCustom05" class="col-form-label page-header-left">كلمه
                                                المرور</label>
                                            <label for="password"></label><input id="password" type="password"
                                                                                 class="form-control "
                                                                                 name="password" required>
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                  <strong>{{ $errors->first('password') }}</strong></span>
                                            @endif
                                        </div>

                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <label for="validationCustom05" class="col-form-label page-header-left">نوع
                                                الحساب
                                            </label>
                                            <label for="type"></label>
                                            <select name="type" dir="rtl"
                                                    class="form-control"
                                                    required>
                                                <option value="">اختر</option>
                                                <option value="6">ادمن</option>
                                                <option value="3">متجر</option>
                                                <option value="2">شركه شحن</option>
                                            </select>
                                            <div
                                                class="invalid-feedback">{{ $errors->first('type') }}</div>
                                        </div>


                                        <div class="form-group form-row mt-3 mb-0">
                                            <button class="btn btn-primary btn-block" type="submit">تسجيل دخول</button>
                                        </div>

                                        <div class="form-group form-row mt-3 mb-0">
                                            <a class="btn btn-primary btn-block" href="">
                                                {{ __('نسيت كلمه المرور') }}
                                            </a>
                                        </div>


                                    </form>
                                </div>

                            </div>
                       
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- login page end-->
    </div>
</div>
<!-- latest jquery-->
<script src="{{ asset('js/jquery-3.2.1.min.js')}}"></script>
<!-- Bootstrap js-->
<script src="{{ asset('js/bootstrap/popper.min.js')}}"></script>
<script src="{{ asset('js/bootstrap/bootstrap.js')}}"></script>
<!-- feather icon js-->
<script src="{{asset('js/notify/bootstrap-notify.min.js')}}"></script>

<script src="{{ asset('js/icons/feather-icon/feather.min.js')}}"></script>
<script src="{{ asset('js/icons/feather-icon/feather-icon.js')}}"></script>
<!-- Sidebar jquery-->
<script src="{{ asset('js/sidebar-menu.js')}}"></script>
<script src="{{ asset('js/config.js')}}"></script>
<!-- Plugins JS start-->
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="{{ asset('js/script.js')}}"></script>
<!-- Plugin used-->


@if (session('failed'))

    <script>
        $(document).ready(function () {
            new Noty({
                type: 'error',
                layout: 'bottomRight',
                text: "{{ session('failed') }}",
                timeout: 2000,
                killer: true
            }).show();
        });
    </script>

@endif
</body>
</html>
