@extends('AdminPanel.backend.layouts.company')
@section('content')
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <!-- Right sidebar Ends-->
    <div class="page-body">

        <div class="container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">

                        <div class="page-header-left">
                            {{--                            @if (auth()->user()->hasPermissionTo('اضافه_اقسام'))--}}

                            <button class="btn btn-primary" type="button" data-toggle="modal"
                                    data-target="#category" data-whatever="@category">اضافه تكلفة توصيل
                            </button>
                            {{--                            <a href="{{route('AddShippingFees')}}" class="btn btn-primary">اضافه تكلفة توصيل</a>--}}
                            {{--                            @endif--}}
                            <div class="modal fade" id="category" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">اضافه تكلفة شحن</h5>
                                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <form class="needs-validation" novalidate=""
                                                  action="{{route('AddShippingFees')}}"
                                                  method="POST" enctype="multipart/form-data">
                                                {{ method_field('POST') }}
                                                {{ csrf_field() }}

                                                <input class="form-control"
                                                       name="type" value="{{$type}}"
                                                       type="hidden">

                                                <div class="form-group">

                                                    <label for="validationCustom05"
                                                           class="page-header-left">توصيل من {{$name}}
                                                    </label>
                                                    <select name="city_from"
                                                            class="form-control"
                                                            required>
                                                        <option value="" disabled selected>اختر {{$name}}</option>
                                                        @foreach($cities as $city)
                                                            @if($type == 1)
                                                                <option
                                                                    value="{{ $city->id }}">{{ $city->city_name_ar }}</option>
                                                            @else
                                                                <option
                                                                    value="{{ $city->id }}">{{ $city->country_name_ar }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('city_from') }}</div>
                                                </div>

                                                <div class="form-group">

                                                    <label for="validationCustom05"
                                                           class="page-header-left">توصيل الى {{$name}}
                                                    </label>
                                                    <select name="city_to"
                                                            class="form-control"
                                                            required>
                                                        <option value="" disabled selected>اختر {{$name}}</option>
                                                        @foreach($cities as $city)
                                                            @if($type == 1)
                                                                <option
                                                                    value="{{ $city->id }}">{{ $city->city_name_ar }}</option>
                                                            @else
                                                                <option
                                                                    value="{{ $city->id }}">{{ $city->country_name_ar }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('city_to') }}</div>
                                                </div>

                                                @if($type == 1)
                                                    <div class="form-group">

                                                        <label for="validationCustom05"
                                                               class="col-form-label page-header-left">تكلفة
                                                            التوصيل</label>
                                                        <input class="form-control"
                                                               name="shipping_fees"
                                                               id="shipping_fees"
                                                               type="text"
                                                               value=""
                                                               placeholder="تكلفة التوصيل"
                                                               required="">
                                                        <div
                                                            class="invalid-feedback">{{ $errors->first('shipping_fees') }}</div>
                                                    </div>

                                                    <span class="text-danger page-header-left"
                                                          style="color: red;">{{$errors->first('shipping_fees')}}</span>
                                                @endif
                                                <div class="modal-footer">
                                                    <button class="btn btn-primary" type="submit">اضافه</button>
                                                    <button class="btn btn-secondary" type="button"
                                                            data-dismiss="modal">اغلاق
                                                    </button>
                                                </div>
                                            </form>

                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4>تكلفة التوصيل</h4>
                    </div>

                    @if( count($shipping_fees) > 0)
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display dataTable" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>المسلسل</th>
                                        <th>التوصيل من {{$name}}</th>
                                        <th>التوصيل الى {{$name}}</th>
                                        @if($type == 1)
                                            <th>تكلفة التوصيل</th>
                                        @endif
                                        <th>الاكشن</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($shipping_fees as $shipping_fee)
                                        <tr>
                                            <td>{{ $shipping_fee->id }}</td>
                                            @if($type == 1)
                                                <td>{{ $shipping_fee->city_from->city_name_ar }}</td>
                                                <td>{{ $shipping_fee->city_to->city_name_ar }}</td>
                                            @else
                                                <td>{{ $shipping_fee->city_from->country_name_ar }}</td>
                                                <td>{{ $shipping_fee->city_to->country_name_ar }}</td>
                                            @endif
                                            @if($type == 1)
                                                <td>{{ !empty($shipping_fee->shipping_fees) ? $shipping_fee->shipping_fees : 0 }}</td>
                                            @endif
                                            <td>
                                                {{--                                                @if (auth()->user()->hasPermissionTo('تعديل_طلبات'))--}}
                                                <button class="btn btn-primary" type="button" data-toggle="modal"
                                                        data-target="#{{ $shipping_fee->id }}"
                                                        data-whatever="@test"><i class="fa fa-edit"></i>
                                                </button>
                                                {{--                                                @endif--}}
                                                {{--                                                @if (auth()->user()->hasPermissionTo('حذف_اقسام'))--}}

                                                <form action="{{ route('destroyShippingFees', $shipping_fee->id) }}"
                                                      method="post" style="display: inline-block">
                                                    {{ csrf_field() }}
                                                    {{ method_field('delete') }}
                                                    <button type="submit"
                                                            class="btn btn-danger delete btn-sm"><i
                                                            class="fa fa-trash"></i>
                                                    </button>
                                                </form>
                                                {{--                                                @endif--}}
                                                <div class="modal fade" id="{{ $shipping_fee->id }}" tabindex="-1"
                                                     role="dialog"
                                                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                @if($type == 1)
                                                                    <h5 class="modal-title"> تعديل تكلفة الشحن {{$name}}
                                                                        {{ $shipping_fee->city_from->city_name_ar }} </h5>
                                                                @else
                                                                    <h5 class="modal-title"> تعديل الشحن {{$name}}
                                                                        {{ $shipping_fee->city_from->country_name_ar }} </h5>
                                                                @endif

                                                                <button class="close" type="button"
                                                                        data-dismiss="modal"
                                                                        aria-label="Close">
                                                                    <span aria-hidden="true">×</span></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form class="needs-validation" novalidate=""
                                                                      action="{{route('EditShippingFees',['id'=>$shipping_fee->id])}}"
                                                                      method="POST" enctype="multipart/form-data">
                                                                    {{ method_field('POST') }}
                                                                    {{ csrf_field() }}

                                                                    @if($type == 1)

                                                                        <div class="form-group">

                                                                            <label for="validationCustom05"
                                                                                   class="page-header-left">التوصيل من
                                                                                مدينه
                                                                            </label>
                                                                            <select name="city_from"
                                                                                    class="form-control"
                                                                                    required>

                                                                                @foreach($cities as $city)
                                                                                    @if($city->id == $shipping_fee->city_from->id)
                                                                                        <option
                                                                                            value="{{ $city->id }}"
                                                                                            selected>{{ $city->city_name_ar }}</option>
                                                                                    @else
                                                                                        <option
                                                                                            value="{{ $city->id }}">{{ $city->city_name_ar }}</option>
                                                                                    @endif
                                                                                @endforeach
                                                                            </select>
                                                                            <div
                                                                                class="invalid-feedback">{{ $errors->first('city_from') }}</div>
                                                                        </div>

                                                                        <div class="form-group">

                                                                            <label for="validationCustom05"
                                                                                   class="page-header-left">التوصيل الى
                                                                                مدينه
                                                                            </label>
                                                                            <select name="city_to"
                                                                                    class="form-control"
                                                                                    required>

                                                                                @foreach($cities as $city)
                                                                                    @if($city->id == $shipping_fee->city_to->id)
                                                                                        <option
                                                                                            value="{{ $city->id }}"
                                                                                            selected>{{ $city->city_name_ar }}</option>
                                                                                    @else
                                                                                        <option
                                                                                            value="{{ $city->id }}">{{ $city->city_name_ar }}</option>
                                                                                    @endif
                                                                                @endforeach
                                                                            </select>
                                                                            <div
                                                                                class="invalid-feedback">{{ $errors->first('city_to') }}</div>
                                                                        </div>
                                                                        <div class="form-group">

                                                                            <label for="validationCustom05"
                                                                                   class="col-form-label page-header-left">تكلفة
                                                                                التوصيل</label>
                                                                            <input class="form-control"
                                                                                   name="shipping_fees"
                                                                                   id="shipping_fees"
                                                                                   type="text"
                                                                                   value="{{$shipping_fee->shipping_fees}}"
                                                                                   placeholder="تكلفة التوصيل"
                                                                                   required="">
                                                                            <div
                                                                                class="invalid-feedback">{{ $errors->first('shipping_fees') }}</div>
                                                                        </div>
                                                                        <span class="text-danger page-header-left"
                                                                              style="color: red;">{{$errors->first('shipping_fees')}}</span>
                                                                    @else
                                                                        <input class="form-control"
                                                                               name="type" value="{{$type}}"
                                                                               type="hidden">

                                                                        <div class="form-group">

                                                                            <label for="validationCustom05"
                                                                                   class="page-header-left">التوصيل من
                                                                                دوله
                                                                            </label>
                                                                            <select name="city_from"
                                                                                    class="form-control"
                                                                                    required>

                                                                                @foreach($cities as $city)
                                                                                    @if($city->id == $shipping_fee->country_from)
                                                                                        <option
                                                                                            value="{{ $city->id }}"
                                                                                            selected>{{ $city->country_name_ar }}</option>
                                                                                    @else
                                                                                        <option
                                                                                            value="{{ $city->id }}">{{ $city->country_name_ar }}</option>
                                                                                    @endif
                                                                                @endforeach
                                                                            </select>
                                                                            <div
                                                                                class="invalid-feedback">{{ $errors->first('city_from') }}</div>
                                                                        </div>

                                                                        <div class="form-group">

                                                                            <label for="validationCustom05"
                                                                                   class="page-header-left">التوصيل الى
                                                                                دوله
                                                                            </label>
                                                                            <select name="city_to"
                                                                                    class="form-control"
                                                                                    required>

                                                                                @foreach($cities as $city)
                                                                                    @if($city->id == $shipping_fee->country_to)
                                                                                        <option
                                                                                            value="{{ $city->id }}"
                                                                                            selected>{{ $city->country_name_ar }}</option>
                                                                                    @else
                                                                                        <option
                                                                                            value="{{ $city->id }}">{{ $city->country_name_ar }}</option>
                                                                                    @endif
                                                                                @endforeach
                                                                            </select>
                                                                            <div
                                                                                class="invalid-feedback">{{ $errors->first('city_to') }}</div>
                                                                        </div>


                                                                    @endif
                                                                    <div class="modal-footer">
                                                                        <button class="btn btn-primary"
                                                                                type="submit">
                                                                            تعديل
                                                                        </button>
                                                                        <button class="btn btn-secondary"
                                                                                type="button"
                                                                                data-dismiss="modal">اغلاق
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    @else
                        <h4 class="text-center">لا توجد تكاليف توصيل </h4>
                    @endif
                </div>
            </div>
        </div>
    </div>



@endsection







