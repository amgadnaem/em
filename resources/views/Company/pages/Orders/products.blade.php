@extends('AdminPanel.backend.layouts.company')

@section('content')
    <!-- Right sidebar Ends-->
    <div class="page-body">
        <!-- Container-fluid starts-->
        @if( count($products) > 0)
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="invoice">
                                    <div>
                                        <div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="media">
                                                        <div class="media-left"><img class="media-object img-60"
                                                                                     src="{{  $products->shop->shop_image  }}"
                                                                                     alt=""></div>
                                                        <div class="media-body m-l-20">
                                                            <h4 class="media-heading">{{ $products->shop->shop_name_en }}</h4>
                                                            <p>{{ $products->shop->email }}<br><span
                                                                    class="digits">{{ $products->shop->phone }}</span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <!-- End Info-->
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="text-md-right">
                                                        <h3>Order Number #<span
                                                                class="digits counter">{{$products->order_number}}</span>
                                                        </h3>
                                                        <br> Order Date: <span
                                                            class="digits">{{ $products->order_date }}</span></p>
                                                    </div>
                                                    <!-- End Title-->
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <!-- End InvoiceTop-->
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="media">
                                                    <div class="media-left"><img
                                                            class="media-object rounded-circle img-60"
                                                            src="{{  $products->user->user_image  }}"
                                                            alt="">
                                                    </div>
                                                    <div class="media-body m-l-20">
                                                        <h4 class="media-heading">{{  $products->address->name  }}</h4>
                                                        <p>{{  $products->user->email  }}<br><span
                                                                class="digits">{{  $products->address->phone  }}</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="text-md-right" id="project">
                                                    <h6>User Data</h6>
                                                    <p>{{  $products->address->location  }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Invoice Mid-->
                                        <div>
                                            <div class="table-responsive invoice-table" id="table">
                                                <table class="table table-bordered table-striped">
                                                    <tbody>
                                                    <tr>
                                                        <td class="item">
                                                            <h6 class="p-2 mb-0">Product Name</h6>
                                                        </td>
                                                        <td class="Hours">
                                                            <h6 class="p-2 mb-0">Order Qty</h6>
                                                        </td>
                                                        <td class="Rate">
                                                            <h6 class="p-2 mb-0">Price</h6>
                                                        </td>
                                                        <td class="subtotal">
                                                            <h6 class="p-2 mb-0">Sub-total</h6>
                                                        </td>
                                                        <td class="subtotal">
                                                            <h6 class="p-2 mb-0">Product Color</h6>
                                                        </td>
                                                        <td class="subtotal">
                                                            <h6 class="p-2 mb-0">Product Size</h6>
                                                        </td>
                                                    </tr>
                                                    @foreach($Orders_details as $details)
                                                        <tr>
                                                            <td>

                                                                <p class="m-0">{{ $details->product_name }}</p>
                                                            </td>
                                                            @if(!empty($Order->size))
                                                                <td>{{ $Order->price }}</td>
                                                                <td>
                                                                    <p class="itemtext digits">{{ $details->price }}</p>
                                                                </td>
                                                                <td>
                                                                    <p class="itemtext digits">{{ $details->order_qty }}</p>
                                                                </td>
                                                                <td>
                                                                    <p class="itemtext digits">{{ $details->price * $details->order_qty }}</p>
                                                                </td>

                                                            @else

                                                                <td>
                                                                    <p class="itemtext digits">{{ $details->product_price }}</p>
                                                                </td>
                                                                <td>
                                                                    <p class="itemtext digits">{{ $details->order_qty }}</p>
                                                                </td>
                                                                <td>
                                                                    <p class="itemtext digits">{{ $details->product_price * $details->order_qty }}</p>
                                                                </td>

                                                            @endif

                                                            <td>
                                                                <p class="itemtext digits">{{ $details->product_color }}</p>
                                                            </td>
                                                            @if($details->size == 1)
                                                                <td>
                                                                    <p class="itemtext digits">S</p>
                                                                </td>
                                                            @elseif($details->size == 2)
                                                                <td>
                                                                    <p class="itemtext digits">M</p>
                                                                </td>
                                                            @elseif($details->size == 3)
                                                                <td>
                                                                    <p class="itemtext digits">L</p>
                                                                </td>
                                                            @elseif($details->size == 4)
                                                                <td>
                                                                    <p class="itemtext digits">XL</p>
                                                                </td>
                                                            @elseif($details->size == 5)
                                                                <td>
                                                                    <p class="itemtext digits">XXL</p>
                                                                </td>
                                                            @else

                                                                <td>
                                                                    <p class="itemtext digits">{{ $details->product_color }}</p>
                                                                </td>
                                                            @endif

                                                        </tr>
                                                    @endforeach
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td class="Rate">
                                                            <h6 class="mb-0 p-2">Total</h6>
                                                        </td>

                                                        <td class="payment digits">
                                                            <h6 class="mb-0 p-2">{{ $products->subtotal_fees }}</h6>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- End Table-->
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div>
                                                        <p class="legal"><strong>Thank you for your business!</strong> 
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <form class="text-right">
                                                        <input type="image" style="width: 40%"
                                                               src="{{ asset('images/profile/logo-login.png') }}"
                                                               name="submit">
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End InvoiceBot-->
                                    </div>
                                    <div class="col-sm-12 text-center mt-3">
                                        <button class="btn btn btn-primary mr-2" type="button" onclick="myFunction()">
                                            Print
                                        </button>
                                        <button class="btn btn-secondary" type="button">Cancel</button>
                                    </div>
                                    <!-- End Invoice-->
                                    <!-- End Invoice Holder-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @else

            <h4 class="text-center"> No Users Orders Added</h4>
    @endif
    <!-- Container-fluid Ends-->


    </div>

@endsection











