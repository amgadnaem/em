@extends('AdminPanel.backend.layouts.company')
@section('content')
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <!-- Right sidebar Ends-->
    <div class="page-body">

        <!-- Default ordering (sorting) Starts-->
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4>عرض الطلبات </h4>
                </div>
                @if( count($orderSearch) > 0)

                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body">

                                        <form class="needs-validation" novalidate="" action="{{route('ordersSearch')}}"
                                              method="POST">
                                            {{ method_field('POST') }}
                                            {{ csrf_field() }}
                                            <div class="form-row">

                                                <div class="col-md-4 mb-3">
                                                    <input class="datepicker-here form-control digits" name="from"
                                                           type="text" required
                                                           data-language="en"
                                                           data-multiple-dates-separator=", "
                                                           data-position="bottom left"
                                                           placeholder="بدايه من " autocomplete="true">
                                                    <div class="invalid-feedback">{{ $errors->first('from') }}.</div>
                                                </div>

                                                <div class="col-md-4 mb-3">

                                                    <input class="datepicker-here form-control digits" name="to"
                                                           type="text" required
                                                           data-language="en"
                                                           data-multiple-dates-separator=", "
                                                           data-position="bottom left"
                                                           placeholder="نهايه الى" autocomplete="true">
                                                    <div class="invalid-feedback">{{ $errors->first('to') }}.</div>
                                                </div>
                                                <div class="col-md-4 mb-3">

                                                    <button class="btn btn-primary" type="submit">بحث</button>
                                                </div>


                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="all_orders" class="display dataTable">
                                <thead>
                                <tr id="filters">
                                    <th>حاله الطلب</th>
                                </tr>
                                <tr>
                                    <th>حاله الطلب</th>
                                    <th>رقم الطلب</th>
                                    <th>سعر الطلب</th>
                                    <th>تاريخ الطلب</th>
                                    <th>المستخدم</th>
                                    <th>عنوان صاحب الطلب</th>
                                    <th>عنوان صاحب الطلب</th>
                                    <th>الاكشن</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orderSearch as $orders)
                                    <tr>
                                        <td>{{ $orders->status_order($orders->order_status)}}</td>
                                        <td>{{ $orders->order_number }}</td>
                                        <td>{{ $orders->subtotal_fees }}</td>
                                        <td>{{ $orders->order_date }}</td>
                                          <td>{{ !empty($orders->user->first_name) ? $orders->user->first_name : null }}</td>
                                           <td>{{ !empty($orders->address->address) ? $orders->address->address : null }}</td>

                                        <td><p><a href="{{$orders->address->google_location}}" target="_blank">العنوان
                                                    على الخريطه</a></p></td>
                                        <td>
                                            <a class="btnprn btn btn-primary " href="{{ route('print', $orders->id) }}"><i
                                                    class="icofont icofont-printer"></i></a>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>

                        </div>
                    </div>

                @else

                    <h4 class="text-center">لا توجد طلبات فى هذه الفتره </h4>
                @endif
            </div>
        </div>
    </div>

    <script type="text/javascript">

        $(document).ready(function () {
            $('#all_orders').DataTable({
                responsive: true,
                ordering: true,
                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;

                        var select = $('<select><option value="">اختر</option></select>')
                            .appendTo($("#filters").find("th").eq(column.index()))
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val());

                                column.search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });
                        column.data().unique().sort().each(function (d, j) {
                            $(select).append('<option value="' + d + '">' + d + '</option>')
                        });
                    });
                }
            });
        });
    </script>


@endsection







