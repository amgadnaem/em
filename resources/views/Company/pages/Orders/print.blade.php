<!doctype html>
<head>
    <meta charset="utf-8">
    <title>Order Details</title>

    <style>
        .invoice-box {
            max-width: 800px;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, .15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table tr td:nth-child(2) {
            text-align: right;
        }

        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }

        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
        }

        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.item td {
            border-bottom: 1px solid #eee;
        }

        .invoice-box table tr.item.last td {
            border-bottom: none;
        }

        .invoice-box table tr.total td:nth-child(2) {
            border-top: 2px solid #eee;
            font-weight: bold;
        }

        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }

        /** RTL **/
        .rtl {
            direction: rtl;
            font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        }

        .rtl table {
            text-align: right;
        }

        .rtl table tr td:nth-child(2) {
            text-align: left;
        }
    </style>
</head>

<body>
<div class="invoice-box">
    <table>
        <tr class="top">
            <td colspan="2">
                <table>
                    <tr>
                        <td class="title">
                            <img src="{{ $order->shop->shop_image }}" style="width:100%; max-width:100px;">
                        </td>
                        <td>
                            رقم الطلب: {{$order->order_number}}<br>
                            تاريخ الطلب: {{ $order->order_date }}<br>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>


        <div class="table-responsive">
            <table class="display dataTable">
                <tbody>
                <td colspan="5">
                    <table>
                        <tr>
                            <td>
                                {{ $order->shop->name_app_ar }}<br>
                                {{ $order->shop->phone_number }}<br>
                            </td>

                            <td>
                                {{ $order->user->first_name }}<br>
                                {{ $order->user->phone_number }}<br>
                            </td>
                        </tr>
                    </table>
                </td>
                <tr class="details">
                    <td>
                        {{ $order->subtotal_fees }}
                    </td>

                    <td>
                        تكلفه شحن الطلب
                    </td>
                </tr>


                <tr class="heading">
                    <td>
                        المنتج
                    </td>

                    <td>
                        سعر المنتج
                    </td>
                    <td>
                        الكميه
                    </td>
                    <td>
                        السعر الكلى
                    </td>

                    <td>
                        المقاسات
                    </td>
                    {{--                    <td>--}}
                    {{--                        سعر الاضافات--}}
                    {{--                    </td>--}}
                </tr>
                @foreach($order_products as $details)
                    <tr class="item">
                        <td>
                            {{ $details->getProductName($details->product_id) }}
                        </td>
                        @if(!empty($details->size))
                            <td>
                                {{ $details->price }}
                            </td>
                            <td>    {{ $details->order_qty }}</td>
                            <td>
                                {{ $details->price * $details->order_qty   }}
                            </td>
                        @else
                            <td>
                                {{ $details->getProductPrice($details->product_id) }}
                            </td>
                            <td>    {{ $details->order_qty }}</td>
                            <td>
                                {{ $details->getProductPrice($details->product_id) * $details->order_qty   }}
                            </td>
                        @endif

                        @if(!empty($details->size))
                            <td>
                                {{ $details->getSizeName($details->size) }}
                            </td>
                        @endif


                    </tr>
                @endforeach
                <tr class="total">
                    <td></td>

                    <td>
                        {{ $order->subtotal_fees }} السعر الاجمالى
                    </td>
                </tr>
                </tbody>

            </table>
        </div>


    </table>
</div>
</body>
</html>
