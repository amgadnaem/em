@extends('AdminPanel.backend.layouts.shop')
@section('content')


    <div class="page-body">
        <div class="container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">

                        <div class="page-header-left">
                            {{--                            @if ( auth()->user()->can('اضافه_مشرف'))--}}
                            @if ( (!empty($roles) ) || ( auth()->user()->hasPermissionTo('اضافه_مشرف') ) )
                                <button class="btn btn-primary" type="button" data-toggle="modal"
                                        data-target="#AddSuper" data-whatever="@test">اضافه مشرف
                                </button>
                            @endif
                            <div class="modal fade" id="AddSuper" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">اضافه مشرف</h5>
                                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <form class="needs-validation" novalidate=""
                                                  action="{{route('CreateSuperVisor')}}"
                                                  method="POST" enctype="multipart/form-data">
                                                {{ method_field('POST') }}
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">الاسم الاول</label>
                                                    <input class="form-control" value="{{ old('first_name') }}"
                                                           name="first_name" id="validationCustomUsername"
                                                           type="text"
                                                           placeholder="ادخل الاسم الاول"
                                                           aria-describedby="inputGroupPrepend"
                                                           required="">
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('first_name') }}</div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">الاسم الاخير</label>
                                                    <input class="form-control" value="{{ old('last_name') }}"
                                                           name="last_name"
                                                           id="validationCustomUsername"
                                                           type="text"
                                                           placeholder="ادخل الاسم الاخير"
                                                           aria-describedby="inputGroupPrepend"
                                                           required="">
                                                    <div
                                                        class="invalid-feedback">{{ $errors->first('last_name') }}</div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="validationCustom04"
                                                           class="col-form-label page-header-left">ادخل رقم
                                                        الهاتف</label>
                                                    <input class="form-control" {{ old('phone') }} name="phone"
                                                           id="validationCustom04" type="text"
                                                           placeholder="ادخل رقم الموبايل"
                                                           required="">
                                                    <div class="invalid-feedback">{{ $errors->first('phone') }}.
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">كلمه المرور</label>
                                                    <input class="form-control" name="password" id="validationCustom05"
                                                           type="password"
                                                           placeholder="ادخل كلمه المرور"
                                                           required="">
                                                    <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">صور</label>

                                                    <div class="custom-file">
                                                        <input class="custom-file-input" id="validatedCustomFile"
                                                               type="file"
                                                               name="user_image">
                                                        <label class="custom-file-label"
                                                               for="validatedCustomFile">اختر</label>
                                                        <div class="invalid-feedback">تحميل صوره</div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="validationCustom05"
                                                           class="col-form-label page-header-left">الصلاحيات</label>
                                                    <div class="nav-tabs-custom">

                                                        @php
                                                            $models = ['منتجات','مشرف','متجر','قسم','طلبات'];
                                                            $maps = ['اضافه','عرض','تعديل','حذف'];
                                                        @endphp

                                                        <ul class="nav nav-tabs">
                                                            @foreach ($models as $index=>$model)
                                                                <li class="nav-item">
                                                                    <a class="nav-link {{ $index == 0 ? 'active' : '' }} {{ $index == 0 ? 'show' : '' }}"
                                                                       href="#{{ $model }}"
                                                                       data-toggle="tab">{{ $model }} </a>
                                                                </li>
                                                            @endforeach
                                                        </ul>

                                                        <div class="tab-content">

                                                            @foreach ($models as $index=>$model)

                                                                <div class="tab-pane {{ $index == 0 ? 'active' : '' }}"
                                                                     id="{{ $model }}" role="tabpanel"
                                                                     aria-labelledby="contact-danger-tab">
                                                                    <br>
                                                                    <div class="text-center">
                                                                        @foreach ($maps as $map)

                                                                            <label>
                                                                                <input type="checkbox"
                                                                                       name="permissions[]"
                                                                                       style="font-size: large"
                                                                                       value="{{ $map . '_' . $model }}"> {{ $map}}
                                                                            </label>
                                                                        @endforeach

                                                                    </div>


                                                                </div>

                                                            @endforeach
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="modal-footer">
                                                    <button class="btn btn-primary" type="submit">اضافه</button>
                                                    <button class="btn btn-secondary" type="button"
                                                            data-dismiss="modal">اغلاق
                                                    </button>
                                                </div>

                                            </form>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        @if( count($select_accounts) > 0)
                            @if ( (!empty($roles) ) || ( auth()->user()->hasPermissionTo('عرض_مشرف') ) )

                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="display dataTable" id="basic-1">
                                            <thead>
                                            <tr>
                                                <th>صوره</th>
                                                <th>الاسم</th>
                                                <th>رقم الهاتف</th>
                                                @if ( (!empty($roles) ) || ( auth()->user()->hasPermissionTo('تعديل_مشرف') ) )
                                                    <th>حاله الحساب</th>
                                                @endif
                                                <th>الاكشن</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($select_accounts as $account)
                                                <tr>
                                                    <td><img src="{{ $account->user_image }}" style="width: 50px"></td>
                                                    <td>{{ $account->first_name }} {{ $account->last_name }}</td>
                                                    <td>{{ $account->phone_number }}</td>
                                                    @if ( (!empty($roles) ) || ( auth()->user()->hasPermissionTo('تعديل_مشرف') ) )
                                                        <td>
                                                            <div class="media-body ">
                                                                <label class="switch">
                                                                    <input type="checkbox"
                                                                           {{ $account->status == 1 ? 'checked' : '' }}
                                                                           onchange="change_status_account({{ $account->id }},{{ $account->status }})"><span
                                                                        class="switch-state"></span>

                                                                </label>
                                                            </div>
                                                        </td>
                                                    @endif

                                                    <td>
                                                        @if ( (!empty($roles) ) || ( auth()->user()->hasPermissionTo('تعديل_مشرف') ) )
                                                            <button class="btn btn-primary" type="button"
                                                                    data-toggle="modal"
                                                                    data-target="#{{ $account->id }}"
                                                                    data-whatever="@test"><i
                                                                    class="fa fa-edit"></i>
                                                            </button>
                                                        @endif
                                                        @if ( (!empty($roles) ) || ( auth()->user()->hasPermissionTo('حذف_مشرف') ) )
                                                            <form action="{{ route('deleteAccount', $account->id) }}"
                                                                  method="post" style="display: inline-block">
                                                                {{ csrf_field() }}
                                                                {{ method_field('delete') }}
                                                                <button type="submit"
                                                                        class="btn btn-danger delete btn-sm"><i
                                                                        class="fa fa-trash"></i>
                                                                </button>
                                                            </form>
                                                        @endif

                                                        <div class="modal fade" id="{{ $account->id }}" tabindex="-1"
                                                             role="dialog"
                                                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title"> تعديل حساب
                                                                            {{ $account->first_name }} </h5>

                                                                        <button class="close" type="button"
                                                                                data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">×</span></button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <form class="needs-validation" novalidate=""
                                                                              action="{{route('EditAccount',['id'=>$account->id])}}"
                                                                              method="POST"
                                                                              enctype="multipart/form-data">
                                                                            {{ method_field('POST') }}
                                                                            {{ csrf_field() }}

                                                                            <div class="form-group">
                                                                                <label
                                                                                    class="col-form-label page-header-left">الاسم الاول
                                                                                </label>
                                                                                <input class="form-control"
                                                                                       name="first_name"
                                                                                       required
                                                                                       type="text"
                                                                                       value="{{ $account->first_name }}">
                                                                                <div
                                                                                    class="invalid-feedback">{{ $errors->first('first_name') }}</div>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label
                                                                                    class="col-form-label page-header-left">  الاسم الاخير
                                                                                </label>
                                                                                <input class="form-control"
                                                                                       name="last_name"
                                                                                       required
                                                                                       type="text"
                                                                                       value="{{ $account->last_name }}">
                                                                                <div
                                                                                    class="invalid-feedback">{{ $errors->first('last_name') }}</div>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label
                                                                                    class="col-form-label page-header-left">كلمه
                                                                                    المرور</label>
                                                                                <input class="form-control"
                                                                                       name="password"
                                                                                       type="password"
                                                                                       >
                                                                                <div
                                                                                    class="invalid-feedback">{{ $errors->first('password') }}</div>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label
                                                                                    class="col-form-label page-header-left">رقم
                                                                                    الهاتف
                                                                                </label>
                                                                                <input class="form-control" name="phone"
                                                                                       type="text"
                                                                                       value="{{ $account->phone_number }}"
                                                                                       required>
                                                                                <div
                                                                                    class="invalid-feedback">{{ $errors->first('phone') }}</div>
                                                                            </div>


                                                                            <div class="form-group">
                                                                                <label for="validationCustom05"
                                                                                       class="page-header-left">صوره
                                                                                    الحساب</label>
                                                                                <label for="file-input"
                                                                                       class="image-upload-label">
                                                                                    <img alt="upload-service-image"
                                                                                         name="user_image"
                                                                                         src="{{$account->user_image}}"
                                                                                         class="thumb"
                                                                                         style="width: 100px"/>
                                                                                </label>
                                                                                <div class="custom-file">
                                                                                    <input class="custom-file-input"
                                                                                           id="user_image" type="file"
                                                                                           name="product_image">
                                                                                    <label class="custom-file-label"
                                                                                           for="validatedCustomFile">اختر
                                                                                        صوره</label>
                                                                                    <div
                                                                                        class="invalid-feedback">{{ $errors->first('user_image') }}</div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="modal-footer">
                                                                                <button class="btn btn-primary"
                                                                                        type="submit">
                                                                                    تعديل
                                                                                </button>
                                                                                <button class="btn btn-secondary"
                                                                                        type="button"
                                                                                        data-dismiss="modal">اغلاق
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            @endif
                        @else
                            <h4 class="text-center"> لا يوجد مشرفيين لديك</h4>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>

        function change_status_account(id, value) {
            if (value == 0) {
                value = 1;
            } else {
                value = 0;
            }
            axios.get('UpdateStatus/' + id + '/' + value)
                .then(function (response) {
                    location.reload();
                })
                .catch(function (error) {
                    console.log(error);
                    alert(error);
                });
        };


    </script>

@endsection








