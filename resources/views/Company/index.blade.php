@extends('AdminPanel.backend.layouts.company')
@section('content')

    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>الرئيسيه</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6 col-xl-6 col-lg-6">
                    <div class="card o-hidden">
                        <div class="bg-primary b-r-4 card-body">
                            <div class="media static-top-widget">
                                <div class="align-self-center text-center"><i class="fa fa-first-order"></i></div>
                                <div class="media-body"><span class="m-0">عدد الطلبات</span>
                                    <h4 class="mb-0 counter">{{ $orders }}</h4>
                                    <i class="icon-bg" data-feather="users"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="col-sm-6 col-xl-6 col-lg-6">
                    <div class="card o-hidden">
                        <div class="bg-primary b-r-4 card-body">
                            <div class="media static-top-widget">
                                <div class="align-self-center text-center"><i data-feather="navigation"></i></div>
                                <div class="media-body"><span class="m-0">الحد الادنى للطلب</span>
                                    <h4 class="mb-0 counter">{{ $shipping }}</h4><i class="icon-bg"
                                                                                    data-feather="navigation"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        <!-- Container-fluid Ends-->

    </div>

@endsection





