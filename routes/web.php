<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});
Route::get('/qr-code', function () {
    return \App\Helper\AppHelper::generateCustomQRCode('01222502043');
});

Route::get('qr-code1', function () {

//Connects to an open, hidden WiFi network.
return QrCode::wiFi([
    'ssid' => 'we2',
    'hidden' => 'true'
]);
//     $image = \QrCode::format('png')
//         ->merge('images/logo/logo.png', 0.5, true)
//         ->size(500)->errorCorrection('H')
//         ->generate('welcome');
//     return response($image)->header('Content-type','image/png');
});

Route::get('/clear', function () {
    $exitCode = Artisan::call('cache:clear');
    return 'test';
});

Route::group(['namespace' => 'AdminPanel', 'prefix' => 'admin'], function () {

    Route::get('login', 'LoginController@showLogin')->name('admin.login');
    Route::post('login', 'LoginController@login');

});

Route::group(["middleware" => "FrontLogin_middleware", 'prefix' => 'admin', 'namespace' => 'AdminPanel'], function () {

    Route::get('dashboard', 'DashboardController@Dashboard')->name('admin.dashboard');

    Route::get('logout', 'LoginController@logout')->name('admin.logout');


    //crud country
    Route::get('countries', 'CountryController@index')->name('countries');

    Route::post('AddCountry', 'CountryController@AddCountry')->name('AddCountry');

    Route::post('EditCountry/{id}', 'CountryController@EditCountry')->name('EditCountry');

    Route::delete('destroyCountry/{id}', 'CountryController@destroy')->name('destroyCountry');


    //crud country
    Route::get('cities', 'CityController@index')->name('cities');

    Route::post('AddCity', 'CityController@AddCity')->name('AddCity');

    Route::post('EditCity/{id}', 'CityController@EditCity')->name('EditCity');

    Route::delete('destroyCity/{id}', 'CityController@destroy')->name('destroyCity');

    //Add Supervisor and make permission each supervisor
    Route::get('supervisor', 'SuperVisorController@index')->name('supervisor');

    Route::get('AddSuper', 'SuperVisorController@AddSuper')->name('admin.AddSuper');

    Route::post('AddSupervisor', 'SuperVisorController@AddSupervisor')->name('AddSupervisor');

    Route::get('UpdateStatusSuper/{id}/{status}', 'SuperVisorController@UpdateStatusSuper')->name('UpdateStatusSuper');

    Route::post('EditAccountSupervisor/{id}', 'SuperVisorController@EditAccountSupervisor')->name('EditAccountSupervisor');

    Route::delete('destroyAccount/{id}', 'SuperVisorController@destroyAccount')->name('destroyAccount');

    // create delegate
    Route::get('delegate', 'DelegateController@AllDelegate')->name('delegate');

    Route::get('getCities', 'DelegateController@getCities')->name('getCities');

    Route::post('addDelegate', 'DelegateController@add_delegate')->name('addDelegate');
    Route::get('confirmAccountDelegate/{id}', 'DelegateController@confirmAccountDelegate')->name('confirmAccountDelegate');

    Route::get('UpdateStatusDelegate/{id}/{status}', 'DelegateController@UpdateStatus')->name('UpdateStatusDelegate');

    Route::post('EditAccountDelegate/{id}', 'DelegateController@EditAccount')->name('EditAccountDelegate');

    Route::delete('deleteAccountDelegate/{id}', 'DelegateController@destroy')->name('deleteAccountDelegate');

    Route::get('delegate-orders/{id}', 'DelegateController@delegate_order')->name('delegate-orders');

    //////////////////// drivers
    Route::get('drivers', 'DriverController@index')->name('drivers');
    Route::get('getCities', 'DriverController@getCities')->name('getCities');
    Route::get('addDriver', 'DriverController@create')->name('createDriver');
    Route::post('addDriver', 'DriverController@store')->name('storeDriver');
    Route::get('editDriver', 'DriverController@edit')->name('editDriver');
    Route::post('editDriver', 'DriverController@update')->name('updateDriver');
    Route::post('deleteDriver', 'DriverController@delete')->name('deleteDriver');
    Route::get('confirmAccountDriver/{id}', 'DriverController@confirmAccountDelegate')->name('confirmAccountDriver');
//    Route::get('UpdateStatusDriver/{id}/{status}', 'DriverController@UpdateStatus')->name('UpdateStatusDriver');
    Route::post('/change-status', 'DriverController@changeStatus')->name('changeDriverStatus');
    Route::post('EditAccountDriver/{id}', 'DriverController@EditAccount')->name('EditAccountDriver');
    Route::delete('deleteAccountDriver/{id}', 'DriverController@destroy')->name('deleteAccountDriver');
    Route::get('driver_orders/{id}', 'DriverController@delegate_orders')->name('delegate_orders');


//    Service Cars
    //////////////////// drivers
    Route::get('serviceCars', 'ServiceCarsController@index')->name('serviceCars');
    Route::post('storeserviceCar', 'ServiceCarsController@store')->name('storeserviceCar');
    Route::get('editserviceCar', 'ServiceCarsController@edit')->name('editserviceCar');
    Route::post('updateserviceCar', 'ServiceCarsController@update')->name('updateserviceCar');
    Route::post('deleteserviceCar', 'ServiceCarsController@delete')->name('deleteserviceCar');




    // shipping company

    Route::get('shippingCompany', 'ShippingCompany@allShipping')->name('shippingCompany');
    Route::get('getCities', 'ShippingCompany@getCities')->name('getCities');
    Route::post('addShipping', 'ShippingCompany@add_shipping')->name('addShipping');
    Route::get('confirmAccountCompany/{id}', 'ShippingCompany@confirmAccountCompany')->name('confirmAccountCompany');
    Route::get('UpdateStatusCompany/{id}/{status}', 'ShippingCompany@UpdateStatusCompany')->name('UpdateStatusCompany');
    Route::post('EditAccountCompany/{id}', 'ShippingCompany@EditAccountCompany')->name('EditAccountCompany');
    Route::delete('destroyCompany/{id}', 'ShippingCompany@destroyCompany')->name('destroyCompany');
    Route::get('delegate_orders/{id}', 'ShippingCompany@delegate_orders')->name('delegate_orders');

    //User in admin panel
    Route::get('users', 'UserController@index')->name('users');

    Route::get('UpdateStatusUser/{id}/{status}', 'UserController@UpdateStatus')->name('UpdateStatusUser');
    Route::delete('deleteUser/{id}', 'UserController@destroy')->name('deleteUser');

    Route::get('users_orders/{id}', 'UserController@users_orders')->name('users_orders');


    //Slider Images

    Route::get('sliders', 'SlidersController@index')->name('sliders');

    Route::post('createSlider', 'SlidersController@createSlider')->name('createSlider');

    Route::post('EditSlider/{id}', 'SlidersController@updateSlider')->name('EditSlider');

    Route::delete('deleteSlider/{id}', 'SlidersController@deleteSlider')->name('deleteSlider');

    //Services
    Route::get('services', 'ServicesController@index')->name('services');
    Route::post('AddService', 'ServicesController@AddService')->name('AddService');
    Route::post('updateService/{id}', 'ServicesController@updateService')->name('updateService');

    Route::get('updateStatusService/{id}/{status}', 'ServicesController@updateStatus')->name('updateStatusService');

    Route::get('shops_service/{id}', 'ServicesController@shops_service')->name('shops_service');

    Route::delete('ServiceDestroy/{id}', 'ServicesController@destroy')->name('ServiceDestroy');

    // Suggestions and complain

    Route::get('suggestions', 'SuggestionsController@index')->name('suggestions');
    Route::delete('SuggestionDestroy/{id}', 'SuggestionsController@destroy')->name('SuggestionDestroy');


    //Shops in main admin

    Route::get('shops', 'ShopsController@AllShops')->name('shops');

    Route::get('editShop/{id}', 'ShopsController@editShop')->name('editShop');

    Route::post('updateShop/{id}', 'ShopsController@updateShop')->name('updateShop');

    Route::get('updateStatusShop/{id}/{status}', 'ShopsController@updateStatusShop')->name('updateStatusShop');
    Route::get('acceptShop/{id}/{status}', 'ShopsController@acceptShop')->name('acceptShop');

    Route::get('shopDetails/{id}', 'ShopsController@shop_details')->name('shopDetails');
    Route::get('CategoryProducts/{id}', 'ShopsController@CategoryProducts')->name('CategoryProducts');


    Route::get('AddShops', 'ShopsController@index')->name('AddShops');
    Route::delete('destroyShop/{id}', 'ShopsController@destroyShop')->name('destroyShop');


    Route::get('all_orders/{id}', 'ShopsController@all_orders')->name('all_orders');

    Route::post('InsertShop', 'ShopsController@AddShop')->name('InsertShop');

    Route::get('ShopDetails/{id}', 'ShopsController@shop_details')->name('ShopDetails');

    Route::get('shop/ShopProducts/{id}', 'ShopsController@ShopProducts')->name('shop.ShopProducts');

    Route::post('editSubscribe/{id}', 'ShopsController@editSubscribe')->name('editSubscribe');

    // packing card
    Route::get('packingCard', 'PackingCardController@index')->name('packingCard');

    Route::post('addPackingCard', 'PackingCardController@addPackingCard')->name('addPackingCard');

    Route::get('updateStatusPacking/{id}/{status}', 'PackingCardController@updateStatus')->name('updateStatusPacking');

    Route::delete('packingDestroy/{id}', 'PackingCardController@destroy')->name('packingDestroy');

    Route::get('packingNumber', 'PackingCardController@packingNumber')->name('packingNumber');

    Route::post('addPackingNumber', 'PackingCardController@addPackingNumber')->name('addPackingNumber');

    Route::get('expiredCard', 'PackingCardController@expiredCard')->name('expiredCard');

    Route::get('updateStatusPacking/{id}/{status}', 'PackingCardController@updateStatus')->name('updateStatusPacking');

    Route::delete('packingDestroy/{id}', 'PackingCardController@destroy')->name('packingDestroy');
// seller points
    Route::get('sellerPoints', 'PackingCardController@sellerPoints')->name('sellerPoints');
    Route::post('addSellerPoints', 'PackingCardController@addSellerPoints')->name('addSellerPoints');
    Route::delete('destroySellerPoints/{id}', 'PackingCardController@destroySellerPoints')->name('destroySellerPoints');





    //Settings

    Route::get('settings', 'SettingController@index')->name('settings');

    Route::post('settings/update/{id}', 'SettingController@UpdateSetting')->name('settings.update');

    Route::get('socialMedia', 'SettingController@socialMedia')->name('socialMedia');

    Route::post('AddSocialMedia', 'SettingController@AddSocialMedia')->name('AddSocialMedia');

    Route::post('EditMedia/{id}', 'SettingController@EditMedia')->name('EditMedia');

    Route::get('updateStatusMedia/{id}/{status}', 'SettingController@updateStatusMedia')->name('updateStatusMedia');


    Route::get('promoCode', 'PromoCodeController@index')->name('promoCode');
    Route::get('editPromo/{id}', 'PromoCodeController@editPromo')->name('editPromo');
    Route::post('updatePromo/{id}', 'PromoCodeController@updatePromo')->name('updatePromo');
    Route::post('addPromoCode', 'PromoCodeController@addPromoCode')->name('addPromoCode');
    Route::delete('destroyPromo/{id}', 'PromoCodeController@destroyPromo')->name('destroyPromo');


    // orders

    Route::get('order_details/{id}', 'OrdersController@order_details')->name('order_details');
    Route::get('print/{order_id}', 'OrdersController@print')->name('print');


    //crud prices
    Route::get('prices', 'SpecialNumbersPricesController@index')->name('prices');

    Route::post('AddPrice', 'SpecialNumbersPricesController@AddPrice')->name('AddPrice');

    Route::post('EditPrice/{id}', 'SpecialNumbersPricesController@EditPrice')->name('EditPrice');

    Route::delete('destroyPrice/{id}', 'SpecialNumbersPricesController@destroy')->name('destroyPrice');

    //crud special-numbers
    Route::get('specialNumbers', 'SpecialNumbersController@index')->name('specialNumbers');

    Route::post('addSpecialNumber', 'SpecialNumbersController@Add')->name('addSpecialNumber');

    Route::delete('destroySpecialNumber/{id}', 'SpecialNumbersController@destroy')->name('destroySpecialNumber');

    //crud notifications
    Route::get('accounts-from-countries', 'NotificationsController@accounts_from_countries')->name('accounts-from-countries');

    Route::get('adminNotifications', 'NotificationsController@index')->name('adminNotifications');

    Route::post('sendAdminNotification', 'NotificationsController@Send')->name('sendAdminNotification');

    Route::delete('destroyAdminNotification/{id}', 'NotificationsController@destroy')->name('destroyAdminNotification');

    //Companies Messages from Admin
    Route::get('CompanyMessages', 'CompaniesMessagingController@index')->name('companiesMessages');

    Route::get('SendCompanyMessage/{id}', 'CompaniesMessagingController@sendView')->name('SendcompaniesMessagesView');

    Route::post('SendCompanyMessage', 'CompaniesMessagingController@Send')->name('SendcompaniesMessages');

//Shops Messages from Admin
    Route::get('ShopMessages', 'ShopsMessagingController@index')->name('shopsMessages');

    Route::get('SendShopMessage/{id}', 'ShopsMessagingController@sendView')->name('SendshopsMessagesView');

    Route::post('SendShopMessage', 'ShopsMessagingController@Send')->name('SendshopsMessages');


});






Route::group(["middleware" => "FrontLogin_middleware", 'prefix' => 'shop', 'namespace' => 'AdminShop'], function () {

    Route::get('dashboard', 'DashboardController@dashboard')->name('shop.dashboard');

    Route::get('index', 'ShopController@index')->name('shop.index');

    Route::post('update/{id}', 'ShopController@update')->name('shop.update');

    Route::get('ShopCategories', 'ShopController@categories')->name('ShopCategories');

    Route::post('AddCategoryShop', 'ShopController@AddCategoryShop')->name('AddCategoryShop');

    Route::post('updateCategory/{id}', 'ShopController@updateCategory')->name('updateCategory');

    Route::get('updateCategoryShop/{id}/{status}', 'ShopController@updateCategoryShop')->name('updateCategoryShop');
    Route::delete('destroyCategory/{id}', 'ShopController@destroyCategory')->name('destroyCategory');

    // branches

    Route::get('branches', 'BranchesController@index')->name('shop.branches');
    Route::post('addBranch/{id}', 'BranchesController@AddBranch')->name('shop.addBranch');


    //Products
//    Route::get('products', 'ShopController@products')->name('products');
    Route::get('products', 'ShopController@products')->name('products');
    Route::get('add-products', 'ShopController@addShopProducts')->name('addProducts');
    Route::post('addProduct', 'ShopController@addProduct')->name('addProduct');
//    Route::get('editShopProducts/{id}', 'ShopController@editShopProducts')->name('editShopProducts');
//    Route::post('editProduct', 'ShopController@editProduct')->name('editProduct');
    Route::get('addBulkProducts', 'ShopController@importProductsView')->name('addBulkView');
    Route::post('addBulkProducts', 'ShopController@importProductsFile')->name('addBulk');
    Route::get('addBulkImagesView', 'ShopController@addBulkImagesView')->name('addBulkImagesView');
    Route::post('addBulkProductsImages', 'ShopController@addBulkProductsImages')->name('addBulkProductsImages');
    Route::get('exportProductsSample', 'ShopController@exportProductsSample')->name('exportProductsSample');

    Route::get('getProductFromCategory/{id?}', 'ShopController@getProductFromCategory')->name('getProductFromCategory');
    Route::get('addProductColor/{id}', 'ShopController@addProductColor')->name('addProductColor');

    Route::post('addAdditions/{id}', 'ShopController@addAdditions')->name('addAdditions');

    Route::get('/updateProduct/{id}/{status}', 'ShopController@updateProduct')->name('updateProduct');

    //Edit Product

    Route::post('editProducts/{id}', 'ShopController@editProducts')->name('editProducts');

    Route::delete('deleteProduct/{id}', 'ShopController@destroy')->name('deleteProduct');

    Route::post('addSizes/{id}', 'ShopController@addSizes')->name('addSizes');

    Route::post('updateSizes/{id}', 'ShopController@updateSizes')->name('updateSizes');

    Route::delete('destroySize/{id}', 'ShopController@destroySize')->name('destroySize');


    //Orders
    Route::get('Orders', 'OrdersController@index')->name('Orders');

    Route::post('ordersSearch', 'OrdersController@ordersSearch')->name('ordersSearch');

    Route::get('order_details/{id}', 'OrdersController@order_details')->name('order_details');

    Route::get('print/{order_id}', 'OrdersController@print')->name('print');

    //Add Supervisor and make permission each supervisor

    Route::get('SupervisorShop', 'SuperVisorController@ShowAccountsAdmin')->name('SupervisorShop');

    Route::get('AddSuper', 'SuperVisorController@AddSuper')->name('admin.AddSuper');

    Route::post('CreateSuperVisor', 'SuperVisorController@CreateSuperVisor')->name('CreateSuperVisor');

    Route::get('UpdateStatus/{id}/{status}', 'SuperVisorController@UpdateStatus')->name('UpdateStatus');

    Route::post('EditAccount/{id}', 'SuperVisorController@EditAccount')->name('EditAccount');

    Route::delete('deleteAccount/{id}', 'SuperVisorController@destroy')->name('deleteAccount');

    //messages
    Route::get('shopMessages', 'MessagesController@index')->name('shopMessages');

    Route::get('shopMessages/reply/{id}', 'MessagesController@reply')->name('shopMessages.reply');

    Route::POST('createShopMessage', 'MessagesController@createAdminMessage')->name('createShopMessage');

    //profile
    Route::get('profileShop', 'ProfileController@index')->name('profileShop');
    Route::post('editProfileShop/{id}/{type}', 'ProfileController@editProfile')->name('editProfileShop');

    Route::get('AdminShopMessages', 'AdminMessagesController@sendView')->name('AdminShopMessagesView');

    Route::post('AdminShopMessages', 'AdminMessagesController@Send')->name('AdminShopMessages');

});

Route::group(['prefix' => 'company', 'namespace' => 'Company'], function () {

    Route::get('dashboard', 'DashboardController@dashboard')->name('company.dashboard');

    //Orders
    Route::get('companyOrders', 'OrdersController@index')->name('companyOrders');

    Route::post('companyOrdersSearch', 'OrdersController@ordersSearch')->name('companyOrdersSearch');

    Route::get('companyOrder_details/{id}', 'OrdersController@order_details')->name('companyOrder_details');

    Route::get('companyPrint/{order_id}', 'OrdersController@print')->name('companyPrint');

    Route::post('EditStatusOrder/{id}', 'OrdersController@EditStatusOrder')->name('EditStatusOrder');

    //crud shipping fees
    Route::get('shippingFees', 'ShippingFeesController@index')->name('shippingFees');

    Route::post('addShippingFees', 'ShippingFeesController@Add')->name('AddShippingFees');

    Route::post('EditShippingFees/{id}', 'ShippingFeesController@Edit')->name('EditShippingFees');

    Route::delete('destroyShippingFees/{id}', 'ShippingFeesController@destroy')->name('destroyShippingFees');

    Route::get('AdminCompanyMessages', 'MessagesController@sendView')->name('AdminCompanyMessagesView');

    Route::post('AdminCompanyMessages', 'MessagesController@Send')->name('AdminCompanyMessages');

    Route::get('profileShop', 'ProfileController@index')->name('profileShop');
    Route::post('editProfileShop/{id}/{type}', 'ProfileController@editProfile')->name('editProfileShop');


});

Route::get('/clear-cache', function() {
    return Artisan::call('cache:clear');
});
