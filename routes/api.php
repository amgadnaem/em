<?php

use Illuminate\Support\Facades\Route;
use SimpleSoftwareIO\QrCode\Facades\QrCode;


Route::group(['namespace' => 'API'], function () {
    Route::get('getCountries', 'indexController@getCountries');
    Route::get('services', 'ServicesController@index');
    Route::post('register', 'UserController@register');
    Route::post('updateLocation', 'UserController@update_location');
    Route::post('login', 'UserController@login');
    Route::post('ActivateAccount', 'UserController@activateAccount');
    Route::post('checkPhone', 'UserController@checkPhone');
    Route::post('checkPhoneRegister', 'UserController@checkPhoneRegister');
    Route::post('forgetPassword', 'UserController@forgetPassword');
    //Packing Card
    Route::post('location', 'DriverController@location');
    Route::get('card', 'UserPointController@index');
    Route::post('SellerPoints', 'UserPointController@SellerPoints');
    Route::get('storeCard', 'UserPointController@storeCard');
    Route::post('addUserCard', 'UserPointController@addUserCard');
    Route::post('sendMoney', 'UserPointController@sendMoney');
    Route::post('receiveMoney', 'UserPointController@receiveMoney');
    Route::post('acceptBorrowPoints', 'UserPointController@acceptBorrowPoints');

    // settings
    Route::get('settings', 'SettingsController@index');

    // notification setting
    Route::group(['prefix' => 'notifications'], function () {

        Route::get('/', 'NotificationController@index');
        Route::get('setting', 'NotificationController@settings');
        Route::post('statusNotification', 'NotificationController@statusNotification');
    });

    //Suggestion CRUD Routes
    Route::group(['prefix' => 'suggestion'], function () {

        Route::post('/add', 'SuggestionController@store');

    });


//Shop Api CRUD Routes
    Route::group(['namespace' => 'Shops', 'prefix' => 'shops'], function () {
        // add shop account
        Route::post('/addShop', 'indexController@addShop');
        Route::post('/completeAccountShop', 'indexController@completeAccountShop');
        // add special number
        Route::get('/addSpecialNumber', 'SpecialNumberController@addSpecialNumber');
        Route::get('/allSpecialNumber', 'SpecialNumberController@getAllSpecialNumber');
        Route::post('/buySpecialNumber', 'SpecialNumberController@buySpecialNumber');
        // accounts shops
        Route::get('/accounts', 'SpecialNumberController@accounts');
        //add category in shops
        Route::post('/addCategory', 'indexController@addCategory');
        // show all products in shops
        Route::get('/products', 'indexController@products');
        Route::get('/all_category', 'indexController@all_category');
        Route::get('/productsDetails', 'indexController@productsDetails');
        Route::post('/addProduct', 'ProductsController@addProduct');
        // add product host
        Route::post('/add_product_host', 'HostProductController@add_product_host');
        Route::post('/confirm_host_product', 'HostProductController@confirm_host_product');
        Route::get('/shared_hosted_product', 'HostProductController@shared_hosted_product');
        Route::get('/deleteSharedProduct', 'HostProductController@delete_shared_product');


        Route::post('/editProduct', 'ProductsController@editProduct');
        Route::post('/deleteProduct', 'ProductsController@deleteProduct');
        Route::post('/searchProducts', 'ProductsController@searchProducts');

        // search about users
        Route::post('/searchPersons', 'indexController@searchPersons');
        // get all permanent users
        Route::get('/getAllPermanentUsers', 'PermanentsController@getAllPermanentUsers');
        Route::post('/addDeletePermanents', 'PermanentsController@addDeletePermanents');

        // get all orders shops
        Route::get('/orders', 'OrderController@shop_order');
        Route::post('/secret_password', 'ProfileController@secret_password');

        // edit profile
        Route::post('/profile/edit', 'ProfileController@edit');
        Route::post('/profile/add_phone', 'ProfileController@add_phone');
        Route::get('/profile/all_phones', 'ProfileController@all_phones');


        // shop chat
        Route::get('/allChat', 'indexController@all_chats');

    });

    //Shop Api CRUD Routes
    Route::group(['namespace' => 'Delegate', 'prefix' => 'delegate'], function () {

        Route::post('/ConfirmOrderFromDelegate', 'indexController@ConfirmOrderFromDelegate');
        Route::get('/orderDetails', 'indexController@OrderDetails');


    });
});

Route::group(['namespace' => 'API', 'prefix' => 'user'], function () {

    Route::post('shops', 'ShopsController@index');
    Route::post('/checkPasswordShop', 'ShopsController@checkPasswordShop');

    Route::post('searchShops', 'ShopsController@searchShops');

    Route::post('getCategory', 'ShopsController@getCategory');

    //FollowShop
    Route::post('createFollowShop', 'ShopFollowController@createFollowShop');
    //Favorite Shop
    Route::post('createFavoriteShop', 'ShopFavoriteController@createFavoriteShop');

    //Favorite Product
    Route::get('/favorite', 'ProductFavoriteController@index');
    Route::post('createFavoriteProduct', 'ProductFavoriteController@createFavoriteProduct');

    //chat Between users and shops
    Route::get('/chat', 'ChatController@index');
    Route::post('/chat/createShopChat', 'ChatController@createShopChat');

    //getProductFromCategory
    Route::post('productsFromCategory', 'ShopsController@getProductFromCategory');
    Route::post('getDetailsProduct', 'ProductController@getDetailsProduct');
    Route::post('getColorsFromSize', 'ProductController@getColorsFromSize');

    // add rate to product
    Route::post('addRateProduct', 'UserReviewController@store');

    //Cart CRUD Routes
    Route::post('cart/add_or_update', 'UserCartController@store');

    Route::post('cart/delete', 'UserCartController@delete');
    Route::get('cart', 'UserCartController@index');
    Route::get('getShippingCompany', 'UserCartController@getShippingCompany');

    // user trip
    Route::post('createTrip', 'DriverController@createTrip');
    Route::get('sendTripToDriver', 'DriverController@sendTripToDriver');

    // profile
    Route::post('/profile/edit', 'ProfileController@edit');
    Route::post('/profile/change_password', 'ProfileController@changePassword');
    // search about product
    Route::get('general-search', 'SearchController@search_product');

});


//Order CRUD Routes
Route::group(['namespace' => 'API', 'prefix' => 'order'], function () {

    //orders users to driver
    Route::post('/confirmOrderFromDriver', 'DriverController@confirmOrderFromDriver');
    Route::post('/user/complete', 'OrdersController@completeOrder');


});


//Order CRUD Routes
Route::group(['namespace' => 'API', 'prefix' => 'order'], function () {

    Route::post('/user/confirm', 'OrdersController@confirmOrder');

    Route::get('/user/list', 'OrdersController@listOrder');
    Route::get('/user/orderDetails', 'OrdersController@OrderDetails');
    // chat between user and delegate
    Route::post('/user/orderChat', 'OrderChatController@orderChat');
    Route::get('/user/chat', 'OrderChatController@index');

    Route::post('/user/updateAddress', 'AddressController@updateAddress');

    // packing card order
    Route::post('/packingCard', 'CardOrderController@packing_card_order');


});


Route::get('qrcode', function () {
    return QrCode::size(250)
        ->generate('12345600');
});

